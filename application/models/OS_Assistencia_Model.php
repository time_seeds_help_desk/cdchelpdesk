<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class OS_Assistencia_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function cadastrar($ordem_de_servico) {
        $dados['id_ordem_de_servico'] = 0;
        $dados['status'] = false;
        $ordem_de_servico['fl_suporte'] = false;

        if ($this->db->insert("ordem_de_servico", $ordem_de_servico)) {
            $dados['id_ordem_de_servico'] = $this->db->insert_id();
            $dados['status'] = true;
        }
        return $dados;
    }

    public function alterar($id_ordem_de_servico, $ordem_de_servico) {
        $this->db->where('id_ordem_de_servico', $id_ordem_de_servico);
        return $this->db->update('ordem_de_servico', $ordem_de_servico);
    }

    public function buscar_todos($id_inicio = 0) {
        $this->db->select("ordem_de_servico.*");
        $this->db->select("status_os.*");
        $this->db->where('ordem_de_servico.fl_suporte', false);
        $this->db->select("res.nome_nm_fantasia as nome_responsavel");
        $this->db->select("tec.nome_nm_fantasia as nome_tecnico");
        $this->db->select("tec.id_pessoa as tecnico_id");
        $this->db->select("cli.nome_nm_fantasia as nome_cliente");
        $this->db->select("cli.id_pessoa as cliente_id");
        $this->db->from('ordem_de_servico');
        $this->db->join('status_os', 'status_os.id_status_os = ordem_de_servico.id_status');
        $this->db->join('pessoa tec', 'tec.id_pessoa = ordem_de_servico.id_tecnico');
        $this->db->join('pessoa cli', 'cli.id_pessoa = ordem_de_servico.id_cliente');
        $this->db->join('pessoa res', 'res.id_pessoa = ordem_de_servico.id_responsavel_abertura');
        $this->db->order_by("id_ordem_de_servico", "desc");
        $this->db->limit(10, $id_inicio);
        return $this->db->get()->result();
    }

    public function buscar_por_id_atividade($id_atividade) {
        $this->db->select("*");
        $this->db->where("id_atividade",$id_atividade);
        $this->db->limit(1, 0);
        return $this->db->get('atividade_os')->result();
    }
    
    
    public function buscar_por_id_equipamento($id_equimameonto) {
        $this->db->select("*");
        $this->db->where("id_equipamento",$id_equimameonto);
        $this->db->limit(1, 0);
        return $this->db->get('equipamento_os')->result();
    }
    
     public function buscar_por_id_cliente($id_cliente) {
        $this->db->select("*");
        $this->db->where("id_cliente",$id_cliente);
        return $this->db->get('ordem_de_servico')->result();
    }
    
    public function buscar_por_id_tecnico($id_tecnico) {
        $this->db->select("*");
        $this->db->where("id_tecnico",$id_tecnico);
        return $this->db->get('ordem_de_servico')->result();
    }
    
    public function buscar_por_id_responsavel($id_responsavel) {
        $this->db->select("*");
        $this->db->where("id_responsavel_abertura",$id_responsavel);
        return $this->db->get('ordem_de_servico')->result();
    }

    public function buscar($id_inicio = 0, $id_tecnico = 0, $id_usuario = 0, $id_status = 0, $protocolo = "") {
        $this->db->select("ordem_de_servico.*");
        $this->db->select("status_os.*");
        $this->db->select("res.nome_nm_fantasia as nome_responsavel");
        if ($protocolo != "") {
            $this->db->where("ordem_de_servico.protocolo", $protocolo);
        }

        if ($id_tecnico != 0) {
            $this->db->where("ordem_de_servico.id_tecnico", $id_tecnico);
        }

        if ($id_usuario != 0) {
            $this->db->where("ordem_de_servico.id_responsavel_abertura", $id_usuario);
        }

        if ($id_status != 0) {
            $this->db->where("ordem_de_servico.id_status", $id_status);
        }
        $this->db->select("tec.nome_nm_fantasia as nome_tecnico");
        $this->db->select("tec.id_pessoa as tecnico_id");
        $this->db->select("cli.nome_nm_fantasia as nome_cliente");
        $this->db->select("cli.id_pessoa as cliente_id");
        $this->db->from('ordem_de_servico');
        $this->db->where('ordem_de_servico.fl_suporte', false);
        $this->db->join('status_os', 'status_os.id_status_os = ordem_de_servico.id_status');
        $this->db->join('pessoa tec', 'tec.id_pessoa = ordem_de_servico.id_tecnico');
        $this->db->join('pessoa cli', 'cli.id_pessoa = ordem_de_servico.id_cliente');
        $this->db->join('pessoa res', 'res.id_pessoa = ordem_de_servico.id_responsavel_abertura');
        $this->db->order_by("id_ordem_de_servico", "desc");
        $this->db->limit(10, $id_inicio);
        return $this->db->get()->result();
    }

    public function buscar_por_id($id) {
        $this->db->select("ordem_de_servico.*");
        $this->db->select("status_os.*");
        $this->db->select("res.nome_nm_fantasia as nome_responsavel");
        $this->db->select("tec.nome_nm_fantasia as nome_tecnico");
        $this->db->select("tec.id_pessoa as tecnico_id");
        $this->db->select("cli.nome_nm_fantasia as nome_cliente");
        $this->db->select("cli.id_pessoa as cliente_id");

        $this->db->select("cli.cpf_cnpj as cpf_cnpj_cliente");
        $this->db->select("cli.telefone as telefone_cliente");
        $this->db->select("cli.email as email_cliente");

        $this->db->where('ordem_de_servico.fl_suporte', false);
        $this->db->from('ordem_de_servico');
        $this->db->where('id_ordem_de_servico', $id);
        $this->db->join('status_os', 'status_os.id_status_os = ordem_de_servico.id_status');
        $this->db->join('pessoa tec', 'tec.id_pessoa = ordem_de_servico.id_tecnico');
        $this->db->join('pessoa cli', 'cli.id_pessoa = ordem_de_servico.id_cliente');
        $this->db->join('pessoa res', 'res.id_pessoa = ordem_de_servico.id_responsavel_abertura');
        $this->db->order_by("id_ordem_de_servico", "desc");
        return $this->db->get()->result();
    }

    public function adicionar_equipamento($equipamento) {
        return $this->db->insert_batch('equipamento_os', $equipamento);
    }

    public function adicionar_atividade($atividade) {
        return $this->db->insert_batch('atividade_os', $atividade);
    }

    public function excluir_atividade($id_atividade_os) {
        $this->db->where('id_atividade_os', $id_atividade_os);
        return $this->db->delete('atividade_os');
    }

    public function excluir_equipamento($id_equipamento_os) {
        $this->db->where('id_equipamento_os', $id_equipamento_os);
        return $this->db->delete('equipamento_os');
    }

    public function buscar_atividades($id_ordem_de_servico) {
        $this->db->select("*");
        $this->db->from('atividade_os');
        $this->db->where('id_ordem_de_servico', $id_ordem_de_servico);
        $this->db->join('atividade', 'atividade.id_atividade = atividade_os.id_atividade');
        $this->db->order_by("atividade.nome_atividade");
        return $this->db->get()->result();
    }

    public function buscar_equipamentos($id_ordem_de_servico) {
        $this->db->select("*");
        $this->db->from('equipamento_os');
        $this->db->where('id_ordem_de_servico', $id_ordem_de_servico);
        $this->db->join('equipamento', 'equipamento.id_equipamento = equipamento_os.id_equipamento');
        $this->db->order_by("equipamento.nome_equipamento");
        return $this->db->get()->result();
    }

    public function contar_todas() {
        $this->db->where('fl_suporte', 0);
        return $this->db->from("ordem_de_servico")->count_all_results();
    }

    public function contar_todas_busca($id_tecnico = 0, $id_usuario = 0, $id_status = 0, $protocolo = "") {
        if ($protocolo != "") {
            $this->db->where("ordem_de_servico.protocolo", $protocolo);
        }

        if ($id_tecnico != 0) {
            $this->db->where("ordem_de_servico.id_tecnico", $id_tecnico);
        }

        if ($id_usuario != 0) {
            $this->db->where("ordem_de_servico.id_responsavel_abertura", $id_usuario);
        }

        if ($id_status != 0) {
            $this->db->where("ordem_de_servico.id_status", $id_status);
        }
        $this->db->where('fl_suporte', 0);
        return $this->db->from("ordem_de_servico")->count_all_results();
    }

    public function contar_para_protocolo() {
        $this->db->where('data_abertura', date('Y-m-d'));
        return $this->db->from("ordem_de_servico")->count_all_results();
    }

}
