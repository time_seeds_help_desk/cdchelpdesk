<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Autorizada_Model extends CI_Model {

    public function cadastrar($autorizada) {
        return $this->db->insert('autorizada', $autorizada);
    }

    public function alterar($autorizada, $id_autorizada) {
        $this->db->where('id_autorizada', $id_autorizada);
        return $this->db->update('autorizada', $autorizada);
    }

    public function buscar_todas($id_inicio = 0, $paginacao = true) {
        $this->db->select('*');
        $this->db->order_by('autorizada.nome_autorizada');
        $this->db->from('autorizada');
        $this->db->join('cidade', 'autorizada.id_cidade = cidade.id_cidade');
        $this->db->join('estado', 'cidade.sigla = estado.sigla');
        if ($paginacao == true) {
            $this->db->limit(10, $id_inicio);
        }
        return $this->db->get()->result();
    }

    public function buscar_por_email($email) {
        $this->db->select('*');
        $this->db->from('autorizada');
        $this->db->where('email', $email);
        $this->db->join('cidade', 'autorizada.id_cidade = cidade.id_cidade');
        $this->db->join('estado', 'cidade.sigla = estado.sigla');
        return $this->db->get()->result();
    }

    public function buscar_por_email_editar($email, $id_autorizada) {
        $this->db->select('*');
        $this->db->where('email', $email);
        $this->db->where('id_autorizada !=', $id_autorizada);
        return $this->db->get('autorizada')->result();
    }

    public function buscar_por_cnpj_editar($cnpj, $id_autorizada) {
        $query = "SELECT * FROM autorizada WHERE id_autorizada != $id_autorizada and cnpj = '$cnpj'";
        $this->db->select('*');
        $this->db->where('cnpj', $cnpj);
        $this->db->where('id_autorizada !=', $id_autorizada);
        return $this->db->get('autorizada')->result();
    }

    public function buscar_por_cnpj($cnpj) {
        $this->db->select('*');
        $this->db->from('autorizada');
        $this->db->where('cnpj', $cnpj);
        $this->db->join('cidade', 'autorizada.id_cidade = cidade.id_cidade');
        $this->db->join('estado', 'cidade.sigla = estado.sigla');
        return $this->db->get()->result();
    }
    
    public function buscar_por_nome($nome_autorizada){
        $this->db->select('*');
        $this->db->order_by('autorizada.nome_autorizada');
        $this->db->from('autorizada');
        $this->db->join('cidade', 'autorizada.id_cidade = cidade.id_cidade');
        $this->db->join('estado', 'cidade.sigla = estado.sigla');
        $this->db->like('autorizada.nome_autorizada', $nome_autorizada, 'after');
        return $this->db->get()->result();
    }

    public function excluir($id_autorizada) {
        $this->db->where('id_autorizada', $id_autorizada);
        return $this->db->delete('autorizada');
    }

    public function buscar_por_id($id_autorizada) {
        $this->db->select('*');
        $this->db->from('autorizada');
        $this->db->where('id_autorizada', $id_autorizada);
        $this->db->join('cidade', 'autorizada.id_cidade = cidade.id_cidade');
        $this->db->join('estado', 'cidade.sigla = estado.sigla');
        return $this->db->get()->result();
    }

    public function buscar_autorizada_os($id_autorizada) {
        $this->db->select('*');
        $this->db->where('id_autorizada', $id_autorizada);
        return $this->db->get('ordem_de_servico')->result();
    }

    public function contar_todas() {
        return $this->db->from("autorizada")->count_all_results();
    }

}
