<?php

defined('BASEPATH') OR exit('No direct script access allowed');

abstract class Pessoa_Model extends CI_Model {

    abstract function cadastrar($pessoa);

    abstract function atualizar($id_pessoa, $pessoa);

    abstract function buscar_todos();

    abstract function buscar_por_id($id);

    abstract function buscar_por_email($email);

    abstract function buscar_por_cpf_cnpj($cpf_cnpj);

    abstract function excluir($id);

    public function buscar_por_email_id($email, $id_pessoa) {
        $this->db->select('*');
        $this->db->where('email', $email);
        $this->db->where("id_pessoa != ", $id_pessoa);
        return $this->db->get('pessoa')->result();
    }

}
