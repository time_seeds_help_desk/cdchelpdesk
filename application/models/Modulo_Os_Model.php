<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Modulo_Os_Model extends CI_Model{
    public function cadastrar($modulo_os){
        return $this->db->insert('modulo_sistema_os', $modulo_os);
    }
    
    public function buscar_por_id_os($id_os){
        $this->db->select('*');
        $this->db->from('modulo_sistema_os');
        $this->db->join('modulo_sistema', 'modulo_sistema.id_modulo_sistema =modulo_sistema_os.modulo_sistema');
        $this->db->join('sistema', 'modulo_sistema.id_sistema = sistema.id_sistema');
        return $this->db->get()->result();
    }
        
}
