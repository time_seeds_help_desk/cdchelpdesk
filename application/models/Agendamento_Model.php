<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Agendamento_Model
 *
 * @author Helder dos Santos
 */
class Agendamento_Model extends CI_Model {

    public function cadastrar($agendamento) {
        return $this->db->insert('agendamento', $agendamento);
    }

    public function buscar_por_id_os_tecnico($id_ordem_de_servico, $id_pessoa_tecnico) {
        $this->db->select('*');
        $this->db->from('agendamento');
        $this->db->select('pessoa.*');
        $this->db->select('cidade.*');
        $this->db->select('estado.*');
        $this->db->where('id_ordem_de_servico', $id_ordem_de_servico);
        $this->db->where('id_pessoa_tecnico', $id_pessoa_tecnico);
        $this->db->order_by('data');
        $this->db->order_by('horario');
        $this->db->join('pessoa', 'pessoa.id_pessoa = agendamento.id_pessoa_cliente');
        $this->db->join('cidade', 'pessoa.id_cidade = cidade.id_cidade');
        $this->db->join('estado', 'estado.sigla = cidade.sigla');
        return $this->db->get()->result();
    }

    public function alterar($agendamento, $id_agendamento) {
        $this->db->where('id_agendamento', $id_agendamento);
        return $this->db->update('agendamento', $agendamento);
    }

    public function buscar_todos_tecnico($id_tecnico) {
        $this->db->select('*');
        $this->db->from('agendamento');
        $this->db->select('pessoa.*');
        $this->db->select('cidade.*');
        $this->db->select('estado.*');
        $this->db->order_by('data');
        $this->db->order_by('horario');
        $this->db->where('data >= ', date('Y-m-d'));
        $this->db->where('fl_concluida', false);
        $this->db->where('id_pessoa_tecnico', $id_tecnico);
        $this->db->join('pessoa', 'pessoa.id_pessoa = agendamento.id_pessoa_cliente');
        $this->db->join('cidade', 'pessoa.id_cidade = cidade.id_cidade');
        $this->db->join('estado', 'estado.sigla = cidade.sigla');
        return $this->db->get()->result();
    }

}
