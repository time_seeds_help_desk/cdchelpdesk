<?php

class Historico_Os_Model extends CI_Model{
    public function cadastrar($historico_os){
        return $this->db->insert('historico_os', $historico_os);
    }
    
    public function buscar_por_id_os($id_ordem_de_servico){
        $this->db->select("*");
        $this->db->where("id_ordem_de_servico", $id_ordem_de_servico);
        return $this->db->get('historico_os')->result();
    }
}
