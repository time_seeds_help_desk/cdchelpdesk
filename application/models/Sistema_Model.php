<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Sistema_Model
 *
 * @author Helder dos Santos
 */
class Sistema_Model extends CI_Model {
    
    public function alterar($sistema, $id_sistema){
        $this->db->where('id_sistema', $id_sistema);
        return $this->db->update('sistema', $sistema);
    }

    public function cadastrar($sistema) {
        return $this->db->insert('sistema', $sistema);
    }

    public function buscar_todos() {
        $this->db->select("*");
        $this->db->order_by("nome_sistema");
        return $this->db->get('sistema')->result();
    }
    
    public function buscar_por_id($id_sistema) {
        $this->db->select("*");
        $this->db->where("id_sistema", $id_sistema);
        return $this->db->get('sistema')->result();
    }
    
    public function excluir($id_sistema){
        $this->db->where('id_sistema', $id_sistema);
        return $this->db->delete('sistema');
    }

}
