<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'application/models/Pessoa_Model.php';

class Cliente_Fisico_Model extends Pessoa_Model {

    public function atualizar($id_pessoa, $pessoa) {
        unset($pessoa['fl_cliente_fisico']);
        $this->db->where('id_pessoa', $id_pessoa);
        return $this->db->update('pessoa', $pessoa);
    }

    public function buscar_por_nome($nome) {
        $this->db->select('*');
        $this->db->from('pessoa');
        $this->db->like('pessoa.nome_nm_fantasia', $nome, 'after');
        $this->db->where('cliente.fl_cliente_fisico', true);
        $this->db->join('cliente', 'cliente.id_pessoa = pessoa.id_pessoa');
        $this->db->join('cidade', 'pessoa.id_cidade = cidade.id_cidade');
        $this->db->join('estado', 'cidade.sigla = estado.sigla');

        return $this->db->get()->result();
    }

    public function buscar_por_cpf_cnpj($cpf_cnpj) {
        $this->db->select('*');
        $this->db->from('pessoa');
        $this->db->where('pessoa.cpf_cnpj', $cpf_cnpj);
        $this->db->where('cliente.fl_cliente_fisico', true);
        $this->db->join('cliente', 'cliente.id_pessoa = pessoa.id_pessoa');
        $this->db->join('cidade', 'pessoa.id_cidade = cidade.id_cidade');
        $this->db->join('estado', 'cidade.sigla = estado.sigla');

        return $this->db->get()->result();
    }

    public function buscar_por_email($email) {
        
    }

    public function buscar_por_id($id) {
        $this->db->select('*');
        $this->db->from('pessoa');
        $this->db->where('pessoa.id_pessoa', $id);
        $this->db->where('cliente.fl_cliente_fisico', true);
        $this->db->join('cliente', 'cliente.id_pessoa = pessoa.id_pessoa');
        $this->db->join('cidade', 'pessoa.id_cidade = cidade.id_cidade');
        $this->db->join('estado', 'cidade.sigla = estado.sigla');

        return $this->db->get()->result();
    }

    public function buscar_todos($id_inicio = 0) {
        $this->db->select('*');
        $this->db->from('pessoa');
        $this->db->limit(10, $id_inicio);
        $this->db->order_by('pessoa.nome_nm_fantasia');
        $this->db->where('pessoa.fl_funcionario', 0);
        $this->db->where('cliente.fl_cliente_fisico', true);
        $this->db->join('cidade', 'pessoa.id_cidade = cidade.id_cidade');
        $this->db->join('cliente', 'cliente.id_pessoa = pessoa.id_pessoa');
        $this->db->join('estado', 'cidade.sigla = estado.sigla');

        return $this->db->get()->result();
    }

    public function cadastrar($pessoa) {
        $dados_cliente['fl_cliente_fisico'] = $pessoa['fl_cliente_fisico'];
        unset($pessoa['fl_cliente_fisico']);

        if ($this->db->insert('pessoa', $pessoa)) {
            $id_pessoa = $this->db->insert_id();

            $dados_cliente['id_pessoa'] = $id_pessoa;

            if ($this->db->insert('cliente', $dados_cliente)) {
                return true;
            }
        }

        return false;
    }

    public function buscarParaOS($nome) {
        $this->db->select('*');
        $this->db->from('pessoa');
        $this->db->where('fl_funcionario', 0);
        $this->db->order_by('nome_nm_fantasia');
        $this->db->like('nome_nm_fantasia', $nome, 'after');

        return $this->db->get()->result();
    }

    public function excluir($id) {
        $this->db->where('id_pessoa', $id);
        $resultado = $this->db->delete('pessoa');

        if ($resultado) {
            $this->db->where('id_pessoa', $id);
            $this->db->where('fl_cliente_fisico', 1);
            $resultado = $this->db->delete('cliente');
        }

        return $resultado;
    }

    public function contar_todos() {
        return $this->db->where('fl_cliente_fisico', true)->from("cliente")->count_all_results();
    }

}
