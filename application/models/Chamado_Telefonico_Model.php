<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Chamado_Telefonico_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function cadastrar($chamado_telefonico) {
        $status = $this->db->insert('chamado_telefonico', $chamado_telefonico);

        if ($status) {
            return array('status' => true, 'id_chamado_telefonico' => $this->db->insert_id());
        } else {
            return array('status' => false, 'id_chamado_telefonico' => 0);
        }
    }

    public function alterar($chamado, $id_chamado) {
        $this->db->where('id_chamado_telefonico', $id_chamado);
        return $this->db->update('chamado_telefonico', $chamado);
    }

    public function buscar_todos($id_inicio = 0) {
        $this->db->select('*');
        $this->db->select('pessoa.*');
        $this->db->select('cidade.*');
        $this->db->select('estado.*');
        $this->db->select('cliente.*');
        $this->db->from('chamado_telefonico');
        $this->db->join('pessoa', 'chamado_telefonico.id_pessoa_cliente = pessoa.id_pessoa');
        $this->db->join('cidade', 'cidade.id_cidade = pessoa.id_cidade');
        $this->db->join('estado', 'estado.sigla = cidade.sigla');
        $this->db->join('cliente', 'cliente.id_pessoa = pessoa.id_pessoa');
        $this->db->order_by("id_chamado_telefonico", "desc");
        $this->db->limit(10, $id_inicio);
        return $this->db->get()->result();
    }

    public function buscar_por_id($id_chamado_telefonico) {
        $this->db->select('*');
        $this->db->select('pessoa.*');
        $this->db->select('cidade.*');
        $this->db->select('estado.*');
        $this->db->select('cliente.*');
        $this->db->where('id_chamado_telefonico', $id_chamado_telefonico);
        $this->db->from('chamado_telefonico');
        $this->db->join('pessoa', 'chamado_telefonico.id_pessoa_cliente = pessoa.id_pessoa');
        $this->db->join('cidade', 'cidade.id_cidade = pessoa.id_cidade');
        $this->db->join('estado', 'estado.sigla = cidade.sigla');
        $this->db->join('cliente', 'cliente.id_pessoa = pessoa.id_pessoa');
        $this->db->order_by("id_chamado_telefonico", "desc");
        return $this->db->get()->result();
    }

    public function contar_todos() {
        return $this->db->from("chamado_telefonico")->count_all_results();
    }

    public function buscar($id_inicio = 0, $nome_nm_fantasia = "", $fl_resolvido = "") {
        $this->db->select('*');
        $this->db->select('pessoa.*');
        $this->db->select('cidade.*');
        $this->db->select('estado.*');
        $this->db->select('cliente.*');
        if($nome_nm_fantasia != ""){
            $this->db->like('pessoa.nome_nm_fantasia', $nome_nm_fantasia,'after');
        }
        
        if($fl_resolvido != "-1"){
            $this->db->where('chamado_telefonico.fl_resolvido', $fl_resolvido);
        }
        $this->db->from('chamado_telefonico');
        $this->db->join('pessoa', 'chamado_telefonico.id_pessoa_cliente = pessoa.id_pessoa');
        $this->db->join('cidade', 'cidade.id_cidade = pessoa.id_cidade');
        $this->db->join('estado', 'estado.sigla = cidade.sigla');
        $this->db->join('cliente', 'cliente.id_pessoa = pessoa.id_pessoa');
        $this->db->order_by("id_chamado_telefonico", "desc");
        $this->db->limit(10, $id_inicio);
        return $this->db->get()->result();
    }
    
    public function contar_para_busca($nome_nm_fantasia = "", $fl_resolvido = ""){
        $this->db->select('*');
        $this->db->select('pessoa.*');
        if($nome_nm_fantasia != ""){
            $this->db->like('pessoa.nome_nm_fantasia', $nome_nm_fantasia,'after');
        }
        
        if($fl_resolvido != ""){
            $this->db->where('chamado_telefonico.fl_resolvido', $fl_resolvido);
        }
        $this->db->from('chamado_telefonico');
        $this->db->join('pessoa', 'chamado_telefonico.id_pessoa_cliente = pessoa.id_pessoa');
        return count($this->db->get()->result());
    }

}
