<?php

/**
 * Description of Usuario_Model
 *
 * @author Helder dos Santos
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario_Model extends CI_Model {

    public function buscar($usuario) {
        $this->db->select("*");
        $this->db->from("pessoa");
        $this->db->where("pessoa.email", $usuario);
        $this->db->join("usuario", "usuario.id_pessoa = pessoa.id_pessoa");
        return $this->db->get()->result();
    }

    public function adicionar_cod_recupera_senha($recupera_senha) {
        return $this->db->insert('recupera_senha', $recupera_senha);
    }

    public function buscar_chave_recuperar_senha($chave) {
        $this->db->select('*');
        $this->db->where('chave', $chave);
        return $this->db->get('recupera_senha')->result();
    }

    public function bloquear_recupera_senha($id_recupera_senha) {
        $this->db->where('id_recupera_senha', $id_recupera_senha);
        return $this->db->update('recupera_senha', array('bloqueado' => true));
    }

    public function alterar_senha($id_pessoa, $dados) {
        $this->db->where('id_pessoa', $id_pessoa);
        return $this->db->update('usuario', $dados);
    }

}
