<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Estado_Model extends CI_Model {

    public function buscar_todos() {
        $this->db->select('*');
        return $this->db->get('estado')->result();
    }

}
