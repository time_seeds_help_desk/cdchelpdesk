<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'application/models/Pessoa_Model.php';

/**
 * Classe model para CRUD de 
 * Cliente Jurídico
 *
 * @author Helder dos Santos
 */
class Cliente_Juridico_Model extends Pessoa_Model {

    public function atualizar($id_pessoa, $pessoa) {
        
        $cliente['razao_social'] = $pessoa['razao_social'];
        $cliente['responsavel'] = $pessoa['responsavel'];
        $cliente['fl_cliente_fisico'] = false;
        $this->db->where('id_pessoa', $id_pessoa);
        $resultado = $this->db->update('cliente', $cliente);
        
        unset($pessoa['razao_social']);
        unset($pessoa['responsavel']);
        unset($pessoa['fl_cliente_fisico']);

        if ($resultado === true) {
            $this->db->where('id_pessoa', $id_pessoa);
            return $this->db->update('pessoa', $pessoa);
        }else{
            return false;
        }
    }

    public function buscar_por_cpf_cnpj($cpf_cnpj) {
        $this->db->select('*');
        $this->db->from('pessoa');
        $this->db->where('pessoa.cpf_cnpj', $cpf_cnpj);
        $this->db->where('cliente.fl_cliente_fisico', false);
        $this->db->join('cliente', 'cliente.id_pessoa = pessoa.id_pessoa');
        $this->db->join('cidade', 'pessoa.id_cidade = cidade.id_cidade');
        $this->db->join('estado', 'cidade.sigla = estado.sigla');

        return $this->db->get()->result();
    }
    
    public function buscar_por_nome_fantasia($nome) {
        $this->db->select('*');
        $this->db->from('pessoa');
        $this->db->like('pessoa.nome_nm_fantasia', $nome,'after');
        $this->db->where('cliente.fl_cliente_fisico', false);
        $this->db->join('cliente', 'cliente.id_pessoa = pessoa.id_pessoa');
        $this->db->join('cidade', 'pessoa.id_cidade = cidade.id_cidade');
        $this->db->join('estado', 'cidade.sigla = estado.sigla');

        return $this->db->get()->result();
    }

    public function buscar_por_email($email) {
        
    }

    public function buscar_por_id($id) {
        $this->db->select('*');
        $this->db->from('pessoa');
        $this->db->where('pessoa.id_pessoa', $id);
        $this->db->where('cliente.fl_cliente_fisico', false);
        $this->db->join('cliente', 'cliente.id_pessoa = pessoa.id_pessoa');
        $this->db->join('cidade', 'pessoa.id_cidade = cidade.id_cidade');
        $this->db->join('estado', 'cidade.sigla = estado.sigla');

        return $this->db->get()->result();
    }

    public function buscar_todos($id_inicio = 0) {
        $this->db->select('*');
        $this->db->from('pessoa');
        $this->db->limit(10, $id_inicio);
        $this->db->order_by('pessoa.nome_nm_fantasia');
        $this->db->where('pessoa.fl_funcionario', false);
        $this->db->where('cliente.fl_cliente_fisico', false);
        $this->db->join('cliente', 'pessoa.id_pessoa = cliente.id_pessoa');
        $this->db->join('usuario', 'pessoa.id_pessoa = usuario.id_pessoa');
        $this->db->join('cidade', 'pessoa.id_cidade = cidade.id_cidade');
        $this->db->join('estado', 'cidade.sigla = estado.sigla');

        return $this->db->get()->result();
    }
    
    public function buscarParaOS($nome) {
        $this->db->select('pessoa.*');
        $this->db->from('pessoa');
        $this->db->join('cliente', 'cliente.id_pessoa = pessoa.id_pessoa');
        $this->db->where('pessoa.fl_funcionario', 0);
        $this->db->where('cliente.fl_cliente_fisico', 0);
        $this->db->order_by('pessoa.nome_nm_fantasia');
        $this->db->like('pessoa.nome_nm_fantasia', $nome, 'after');

        return $this->db->get()->result();
    }

    public function cadastrar($pessoa) {
        $cliente_juridico['razao_social'] = $pessoa['razao_social'];
        $cliente_juridico['responsavel'] = $pessoa['responsavel'];
        $usuario = array('senha' => $pessoa['senha']);

        unset($pessoa['razao_social']);
        unset($pessoa['responsavel']);
        unset($pessoa['senha']);

        if ($this->db->insert('pessoa', $pessoa)) {
            $id_pessoa = $this->db->insert_id();

            $cliente_juridico['id_pessoa'] = $id_pessoa;
            $cliente_juridico['fl_cliente_fisico'] = false;
            $usuario['id_pessoa'] = $id_pessoa;

            if ($this->db->insert('cliente', $cliente_juridico) && $this->db->insert('usuario', $usuario)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function excluir($id) {
        $this->db->where('id_pessoa', $id);
        $this->db->where('fl_funcionario', false);
        $resultado = $this->db->delete('pessoa');

        if ($resultado) {
            $this->db->where('id_pessoa', $id);
            $resultado = $this->db->delete('cliente');
            if ($resultado) {
                $this->db->where('id_pessoa', $id);
                $resultado = $this->db->delete('usuario');
            }
        }

        return $resultado;
    }
    
    public function contar_todos(){
        return $this->db->where('fl_cliente_fisico',false)->from("cliente")->count_all_results();
    }

}
