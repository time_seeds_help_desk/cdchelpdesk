<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tipo_Funcionario_Model extends CI_Model {

    public function buscar_todos() {
        $this->db->select('*');
        $this->db->order_by('nome_tipo_funcionario');
        return $this->db->get('tipo_funcionario')->result();
    }

}
