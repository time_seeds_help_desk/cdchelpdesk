<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Atividade_Model
 *
 * @author Helder dos Santos
 */
class Atividade_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function cadastrar($atividade) {
        return $this->db->insert("atividade", $atividade);
    }

    public function alterar($atividade) {
        $id_atividade = $atividade['id_atividade'];

        unset($atividade['id_atividade']);

        $this->db->where('id_atividade', $id_atividade);
        return $this->db->update('atividade', $atividade);
    }

    public function buscar_todos($id_inicio = 0, $tem_paginacao = true) {
        $this->db->select('*');
        $this->db->order_by('nome_atividade');
        if ($tem_paginacao) {
            $this->db->limit(10, $id_inicio);
        }
        return $this->db->get('atividade')->result();
    }

    public function buscar_por_id($id_atividade) {
        $this->db->select('*');
        $this->db->where('id_atividade', $id_atividade);
        $atividade = $this->db->get('atividade')->result();

        if (count($atividade) > 0) {
            return $atividade[0];
        } else {
            return null;
        }
    }

    public function buscar_por_nome($nome) {
        $this->db->select('*');
        $this->db->like('nome_atividade', $nome);
        return $this->db->get('atividade')->result();
    }

    public function excluir($id) {
        $this->db->where('id_atividade', $id);
        return $this->db->delete('atividade');
    }

    public function contar_todos() {
        return $this->db->from("atividade")->count_all_results();
    }

}
