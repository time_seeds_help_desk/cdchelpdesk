<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class OS_Suporte_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function cadastrar($ordem_de_servico) {
        $dados['id_ordem_de_servico'] = 0;
        $dados['status'] = false;
        $ordem_de_servico['fl_suporte'] = true;

        if ($this->db->insert("ordem_de_servico", $ordem_de_servico)) {
            $dados['id_ordem_de_servico'] = $this->db->insert_id();
            $dados['status'] = true;
        }
        return $dados;
    }

    public function alterar($id_ordem_de_servico, $ordem_de_servico) {
        $this->db->where('id_ordem_de_servico', $id_ordem_de_servico);
        return $this->db->update('ordem_de_servico', $ordem_de_servico);
    }

    public function buscar_todos($id_inicio = 0) {
        $this->db->select("ordem_de_servico.*");
        $this->db->select("status_os.*");
        $this->db->select("sistema.*");
        $this->db->where('ordem_de_servico.fl_suporte', true);
        $this->db->select("res.nome_nm_fantasia as nome_responsavel");
        $this->db->select("tec.nome_nm_fantasia as nome_tecnico");
        $this->db->select("tec.id_pessoa as tecnico_id");
        $this->db->select("cli.nome_nm_fantasia as nome_cliente");
        $this->db->select("cli.id_pessoa as cliente_id");
        $this->db->from('ordem_de_servico');
        $this->db->join('modulo_sistema_os', 'modulo_sistema_os.ordem_de_servico = ordem_de_servico.id_ordem_de_servico');
        $this->db->join('modulo_sistema', 'modulo_sistema.id_modulo_sistema = modulo_sistema_os.modulo_sistema');
        $this->db->join('sistema', 'modulo_sistema.id_sistema = sistema.id_sistema');
        $this->db->join('status_os', 'status_os.id_status_os = ordem_de_servico.id_status');
        $this->db->join('pessoa tec', 'tec.id_pessoa = ordem_de_servico.id_tecnico');
        $this->db->join('pessoa cli', 'cli.id_pessoa = ordem_de_servico.id_cliente');
        $this->db->join('pessoa res', 'res.id_pessoa = ordem_de_servico.id_responsavel_abertura');
        $this->db->order_by("id_ordem_de_servico", "desc");
        $this->db->limit(10, $id_inicio);
        return $this->db->get()->result();
        /* $this->db->select("ordem_de_servico.*");
          $this->db->select("status_os.*");
          $this->db->select("modulo_sistema_os.*");
          $this->db->select("modulo_sistema.*");
          $this->db->select("sistema.*");
          $this->db->select("res.nome_nm_fantasia as nome_responsavel");
          $this->db->select("tec.nome_nm_fantasia as nome_tecnico");
          $this->db->select("tec.id_pessoa as tecnico_id");
          $this->db->select("cli.nome_nm_fantasia as nome_cliente");
          $this->db->select("cli.id_pessoa as cliente_id");

          $this->db->select("cli.cpf_cnpj as cpf_cnpj_cliente");
          $this->db->select("cli.telefone as telefone_cliente");
          $this->db->select("cli.email as email_cliente");
          $this->db->select("cli.rua as rua_cliente");
          $this->db->select("cli.numero as numero_cliente");
          $this->db->select("cli.bairro as bairro_cliente");
          $this->db->select("cidade.*");
          $this->db->select("estado.*");
          $this->db->select("modulo_sistema_os.*");
          $this->db->select("modulo_sistema.*");
          $this->db->select("sistema.*");

          $this->db->where('ordem_de_servico.fl_suporte', true);
          $this->db->from('ordem_de_servico');
          $this->db->join('modulo_sistema_os', 'modulo_sistema_os.ordem_de_servico = ordem_de_servico.id_ordem_de_servico');
          $this->db->join('modulo_sistema', 'modulo_sistema.id_modulo_sistema = modulo_sistema_os.modulo_sistema');
          $this->db->join('sistema', 'modulo_sistema.id_sistema = sistema.id_sistema');
          $this->db->join('status_os', 'status_os.id_status_os = ordem_de_servico.id_status');
          $this->db->join('pessoa tec', 'tec.id_pessoa = ordem_de_servico.id_tecnico');
          $this->db->join('pessoa cli', 'cli.id_pessoa = ordem_de_servico.id_cliente');
          $this->db->join('pessoa res', 'res.id_pessoa = ordem_de_servico.id_responsavel_abertura');

          $this->db->join('cidade', 'cidade.id_cidade = cli.id_cidade');
          $this->db->join('estado', 'cidade.sigla = estado.sigla');

          $this->db->order_by("id_ordem_de_servico", "desc");
          $this->db->limit(10, $id_inicio);
          return $this->db->get()->result(); */
    }

    public function buscar($id_inicio = 0, $id_tecnico = 0, $id_usuario = 0, $id_status = 0, $protocolo = "") {
        $this->db->select("ordem_de_servico.*");
        $this->db->select("status_os.*");
        $this->db->select("res.nome_nm_fantasia as nome_responsavel");
        $this->db->where('ordem_de_servico.fl_suporte', true);
        $this->db->select("modulo_sistema_os.*");
        $this->db->select("modulo_sistema.*");
        $this->db->select("sistema.*");
        if ($protocolo != "") {
            $this->db->where("ordem_de_servico.protocolo", $protocolo);
        }

        if ($id_tecnico != 0) {
            $this->db->where("ordem_de_servico.id_tecnico", $id_tecnico);
        }

        if ($id_usuario != 0) {
            $this->db->where("ordem_de_servico.id_responsavel_abertura", $id_usuario);
        }

        if ($id_status != 0) {
            $this->db->where("ordem_de_servico.id_status", $id_status);
        }
        $this->db->select("tec.nome_nm_fantasia as nome_tecnico");
        $this->db->select("tec.id_pessoa as tecnico_id");
        $this->db->select("cli.nome_nm_fantasia as nome_cliente");
        $this->db->select("cli.id_pessoa as cliente_id");

        $this->db->select("cli.cpf_cnpj as cpf_cnpj_cliente");
        $this->db->select("cli.telefone as telefone_cliente");
        $this->db->select("cli.email as email_cliente");
        $this->db->select("cli.rua as rua_cliente");
        $this->db->select("cli.numero as numero_cliente");
        $this->db->select("cli.bairro as bairro_cliente");
        $this->db->select("cidade.*");
        $this->db->select("estado.*");
        $this->db->select("modulo_sistema_os.*");
        $this->db->select("modulo_sistema.*");
        $this->db->select("sistema.*");


        $this->db->from('ordem_de_servico');
        $this->db->join('modulo_sistema_os', 'modulo_sistema_os.ordem_de_servico = ordem_de_servico.id_ordem_de_servico');
        $this->db->join('modulo_sistema', 'modulo_sistema.id_modulo_sistema = modulo_sistema_os.modulo_sistema');
        $this->db->join('sistema', 'modulo_sistema.id_sistema = sistema.id_sistema');
        $this->db->join('status_os', 'status_os.id_status_os = ordem_de_servico.id_status');
        $this->db->join('pessoa tec', 'tec.id_pessoa = ordem_de_servico.id_tecnico');
        $this->db->join('pessoa cli', 'cli.id_pessoa = ordem_de_servico.id_cliente');
        $this->db->join('pessoa res', 'res.id_pessoa = ordem_de_servico.id_responsavel_abertura');

        $this->db->join('cidade', 'cidade.id_cidade = cli.id_cidade');
        $this->db->join('estado', 'cidade.sigla = estado.sigla');

        $this->db->order_by("id_ordem_de_servico", "desc");
        $this->db->limit(10, $id_inicio);
        return $this->db->get()->result();
    }

    public function buscar_por_id($id) {
        $this->db->select("ordem_de_servico.*");
        $this->db->select("status_os.*");
        $this->db->select("res.nome_nm_fantasia as nome_responsavel");
        $this->db->select("tec.nome_nm_fantasia as nome_tecnico");
        $this->db->select("tec.id_pessoa as tecnico_id");
        $this->db->select("cli.nome_nm_fantasia as nome_cliente");
        $this->db->select("cli.id_pessoa as cliente_id");

        $this->db->select("cli.cpf_cnpj as cpf_cnpj_cliente");
        $this->db->select("cli.telefone as telefone_cliente");
        $this->db->select("cli.email as email_cliente");
        $this->db->select("cli.rua as rua_cliente");
        $this->db->select("cli.numero as numero_cliente");
        $this->db->select("cli.bairro as bairro_cliente");
        $this->db->select("cidade.*");
        $this->db->select("estado.*");
        $this->db->select("modulo_sistema_os.*");
        $this->db->select("modulo_sistema.*");
        $this->db->select("sistema.*");

        $this->db->select("modulo_sistema_os.*");
        $this->db->select("modulo_sistema.*");
        $this->db->select("sistema.*");
        $this->db->from('ordem_de_servico');
        $this->db->where('id_ordem_de_servico', $id);
        $this->db->where('ordem_de_servico.fl_suporte', true);
        $this->db->join('modulo_sistema_os', 'modulo_sistema_os.ordem_de_servico = ordem_de_servico.id_ordem_de_servico');
        $this->db->join('modulo_sistema', 'modulo_sistema.id_modulo_sistema = modulo_sistema_os.modulo_sistema');
        $this->db->join('sistema', 'modulo_sistema.id_sistema = sistema.id_sistema');
        $this->db->join('status_os', 'status_os.id_status_os = ordem_de_servico.id_status');
        $this->db->join('pessoa tec', 'tec.id_pessoa = ordem_de_servico.id_tecnico');
        $this->db->join('pessoa cli', 'cli.id_pessoa = ordem_de_servico.id_cliente');

        $this->db->join('cidade', 'cidade.id_cidade = cli.id_cidade');
        $this->db->join('estado', 'cidade.sigla = estado.sigla');


        $this->db->join('pessoa res', 'res.id_pessoa = ordem_de_servico.id_responsavel_abertura');
        $this->db->order_by("id_ordem_de_servico", "desc");
        return $this->db->get()->result();
    }

    public function contar_todas() {
        $this->db->where('fl_suporte', 1);
        return $this->db->from("ordem_de_servico")->count_all_results();
    }

    public function contar_todas_busca($id_tecnico = 0, $id_usuario = 0, $id_status = 0, $protocolo = "") {
        if ($protocolo != "") {
            $this->db->where("ordem_de_servico.protocolo", $protocolo);
        }

        if ($id_tecnico != 0) {
            $this->db->where("ordem_de_servico.id_tecnico", $id_tecnico);
        }

        if ($id_usuario != 0) {
            $this->db->where("ordem_de_servico.id_responsavel_abertura", $id_usuario);
        }

        if ($id_status != 0) {
            $this->db->where("ordem_de_servico.id_status", $id_status);
        }
        $this->db->where('fl_suporte', true);
        return $this->db->from("ordem_de_servico")->count_all_results();
    }

    public function contar_para_protocolo() {
        $this->db->where('data_abertura', date('Y-m-d'));
        return $this->db->from("ordem_de_servico")->count_all_results();
    }

}
