<?php

/**
 * Description of Status_OS
 *
 * @author Helder dos Santos
 */
class Status_Os_Model extends CI_Model {

    public function buscar_todos() {
        $this->db->select('*');
        $this->db->order_by('nome_status_os');
        return $this->db->get('status_os')->result();
    }
    
    public function alterar($status_os){
        $id_status_os = $status_os['id_status_os'];
        unset($status_os['id_status_os']);
        
        $this->db->where('id_status_os', $id_status_os);
        return $this->db->update('status_os', $status_os);
    }


    public function cadastrar($status_os){
        return $this->db->insert('status_os', $status_os);
    }
    
    public function excluir($id_status_os){
        $this->db->where('id_status_os', $id_status_os);
        return $this->db->delete('status_os');
    }
    
    public function buscar_por_id($id_status_os) {
        $this->db->select('*');
        $this->db->where('id_status_os', $id_status_os);
        $status_os = $this->db->get('status_os')->result();

        if (count($status_os) > 0) {
            return $status_os[0];
        } else {
            return null;
        }
    }

}
