<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cidade_Model extends CI_Model {

    public function buscar_por_sigla($sigla) {
        $this->db->select('*');
        $this->db->where('sigla', $sigla);
        $this->db->order_by('nome_cidade');
        return $this->db->get('cidade')->result();
    }

}
