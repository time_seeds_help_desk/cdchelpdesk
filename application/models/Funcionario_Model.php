<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'application/models/Pessoa_Model.php';

class Funcionario_Model extends Pessoa_Model {

    public function atualizar($id_pessoa, $pessoa) {
        $funcionario = array('id_tipo_funcionario' => $pessoa['id_tipo_funcionario']);
        unset($pessoa['senha']);
        unset($pessoa['id_tipo_funcionario']);

        $this->db->where('id_pessoa', $id_pessoa);

        if ($this->db->update('funcionario', $funcionario)) {
            $this->db->where('id_pessoa', $id_pessoa);
            return $this->db->update('pessoa', $pessoa);
        } else {
            return false;
        }
    }

    public function buscar_por_cpf_cnpj($cpf_cnpj) {
        $this->db->select('*');
        $this->db->where('cpf_cnpj', $cpf_cnpj);
        return $this->db->get('pessoa')->result();
    }

    public function buscar_por_email($email) {
        $this->db->select('*');
        $this->db->where('email', $email);
        return $this->db->get('pessoa')->result();
    }

    public function buscar_por_nome($nome) {
        $this->db->select('*');
        $this->db->from('pessoa');
        $this->db->order_by('pessoa.nome_nm_fantasia');
        $this->db->like('pessoa.nome_nm_fantasia', $nome, 'after');
        $this->db->where('pessoa.fl_funcionario', true);
        $this->db->join('funcionario', 'pessoa.id_pessoa = funcionario.id_pessoa');
        $this->db->join('tipo_funcionario', 'funcionario.id_tipo_funcionario = tipo_funcionario.id_tipo_funcionario');
        $this->db->join('usuario', 'pessoa.id_pessoa = usuario.id_pessoa');

        return $this->db->get()->result();
    }

    public function buscar_por_id($id) {
        $this->db->select('*');
        $this->db->from('pessoa');
        $this->db->where('pessoa.id_pessoa', $id);
        $this->db->where('pessoa.fl_funcionario', true);
        $this->db->join('funcionario', 'pessoa.id_pessoa = funcionario.id_pessoa');
        $this->db->join('tipo_funcionario', 'funcionario.id_tipo_funcionario = tipo_funcionario.id_tipo_funcionario');
        $this->db->join('usuario', 'pessoa.id_pessoa = usuario.id_pessoa');

        return $this->db->get()->result();
    }

    public function buscar_todos($id_inicio = 0) {
        $this->db->select('*');
        $this->db->limit(10, $id_inicio);
        $this->db->where('fl_funcionario', 1);
        $this->db->from('pessoa');
        $this->db->order_by('nome_nm_fantasia');
        $this->db->join('funcionario', 'pessoa.id_pessoa = funcionario.id_pessoa');
        $this->db->join('tipo_funcionario', 'funcionario.id_tipo_funcionario = tipo_funcionario.id_tipo_funcionario');
        $this->db->join('usuario', 'pessoa.id_pessoa = usuario.id_pessoa');

        return $this->db->get()->result();
    }
    
    public function listar_todos() {
        $this->db->select('*');
        $this->db->where('fl_funcionario', 1);
        $this->db->from('pessoa');
        $this->db->order_by('nome_nm_fantasia');
        $this->db->join('funcionario', 'pessoa.id_pessoa = funcionario.id_pessoa');
        $this->db->join('tipo_funcionario', 'funcionario.id_tipo_funcionario = tipo_funcionario.id_tipo_funcionario');
        $this->db->join('usuario', 'pessoa.id_pessoa = usuario.id_pessoa');

        return $this->db->get()->result();
    }
    
    public function buscar_todos_assistencia() {
        $this->db->select('*');
        $this->db->where('fl_funcionario', 1);
        $this->db->where('nome_tipo_funcionario', 'Assistência Técnica');
        $this->db->from('pessoa');
        $this->db->order_by('nome_nm_fantasia');
        $this->db->join('funcionario', 'pessoa.id_pessoa = funcionario.id_pessoa');
        $this->db->join('tipo_funcionario', 'funcionario.id_tipo_funcionario = tipo_funcionario.id_tipo_funcionario');
        $this->db->join('usuario', 'pessoa.id_pessoa = usuario.id_pessoa');

        return $this->db->get()->result();
    }
    
    public function buscar_todos_suporte() {
        $this->db->select('*');
        $this->db->where('fl_funcionario', 1);
        $this->db->where('tipo_funcionario.nome_tipo_funcionario', 'Suporte Técnico');
        $this->db->from('pessoa');
        $this->db->order_by('nome_nm_fantasia');
        $this->db->join('funcionario', 'pessoa.id_pessoa = funcionario.id_pessoa');
        $this->db->join('tipo_funcionario', 'funcionario.id_tipo_funcionario = tipo_funcionario.id_tipo_funcionario');
        $this->db->join('usuario', 'pessoa.id_pessoa = usuario.id_pessoa');

        return $this->db->get()->result();
    }

    public function cadastrar($pessoa) {
        $funcionario = array('id_tipo_funcionario' => $pessoa['id_tipo_funcionario']);
        $usuario = array('senha' => $pessoa['senha']);

        unset($pessoa['id_tipo_funcionario']);
        unset($pessoa['senha']);

        if ($this->db->insert('pessoa', $pessoa)) {
            $id_pessoa = $this->db->insert_id();

            $funcionario['id_pessoa'] = $id_pessoa;
            $usuario['id_pessoa'] = $id_pessoa;

            if ($this->db->insert('funcionario', $funcionario) && $this->db->insert('usuario', $usuario)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function excluir($id) {
        $this->db->where('id_pessoa', $id);
        $this->db->where('fl_funcionario', true);
        $resultado = $this->db->delete('pessoa');

        if ($resultado) {
            $this->db->where('id_pessoa', $id);
            $resultado = $this->db->delete('funcionario');
            if ($resultado) {
                $this->db->where('id_pessoa', $id);
                $resultado = $this->db->delete('usuario');
            }
        }

        return $resultado;
    }

    public function contar_todos() {
        //$this->db->where('fl_funcionario', 1);
        // return $this->db->count_all("pessoa");
        /*$this->db->where('fl_funcionario', true)
                ->get('pessoa');
        return $this->db->count_all_results();*/
        
        return $this->db->where('fl_funcionario',true)->from("pessoa")->count_all_results();
    }

}
