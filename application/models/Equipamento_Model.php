<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Equipamento_Model
 *
 * @author Helder dos Santos
 */
class Equipamento_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function cadastrar($equipamento) {
        return $this->db->insert("equipamento", $equipamento);
    }

    public function alterar($equipamento) {
        $id_equipamento = $equipamento['id_equipamento'];

        unset($equipamento['id_equipamento']);

        $this->db->where('id_equipamento', $id_equipamento);
        return $this->db->update('equipamento', $equipamento);
    }

    public function buscar_todos($id_inicio = 0, $tem_paginacao = false) {
        $this->db->select('*');
        $this->db->order_by('nome_equipamento');
        if ($tem_paginacao) {
            $this->db->limit(10, $id_inicio);
        }
        return $this->db->get('equipamento')->result();
    }

    public function buscar_por_nome($nome) {
        $this->db->select('*');
        $this->db->like('nome_equipamento', $nome);
        return $this->db->get('equipamento')->result();
    }

    public function buscar_por_id($id_equipamento) {
        $this->db->select('*');
        $this->db->where('id_equipamento', $id_equipamento);
        $equipamento = $this->db->get('equipamento')->result();

        if (count($equipamento) > 0) {
            return $equipamento[0];
        } else {
            return null;
        }
    }

    public function excluir($id) {
        $this->db->where('id_equipamento', $id);
        return $this->db->delete('equipamento');
    }

    public function contar_todos() {
        return $this->db->from("equipamento")->count_all_results();
    }

}
