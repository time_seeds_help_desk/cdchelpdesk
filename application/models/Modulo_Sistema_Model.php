<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Modulo_Sistema_Model
 *
 * @author Helder dos Santos
 */
class Modulo_Sistema_Model extends CI_Model{
    
    public function cadastrar($modulo_sistema){
        return $this->db->insert('modulo_sistema', $modulo_sistema);
    }
    
    public function alterar($modulo_sistema, $id_modulo_sistema){
        $this->db->where('id_modulo_sistema', $id_modulo_sistema);
        return $this->db->update('modulo_sistema', $modulo_sistema);
    }

    public function buscar_todos($id_sistema){
        $this->db->select('*');
        $this->db->where('id_sistema', $id_sistema);
        $this->db->order_by('nome_modulo_sistema');
        return $this->db->get('modulo_sistema')->result();
    }
    
    public function buscar_por_id($id_modulo_sistema){
        $this->db->select('*');
        $this->db->where('id_modulo_sistema', $id_modulo_sistema);
        return $this->db->get('modulo_sistema')->result();
    } 
    
    public function excluir($id_modulo_sistema) {
        $this->db->where('id_modulo_sistema', $id_modulo_sistema);
        return $this->db->delete('modulo_sistema');
    }
}
