<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Código para envio de SMS
 */
class Send_Sms{

    public function enviar_sms($telefone, $menSms) {
        $ch = curl_init();

        $data = array('key' => 'ODJ0ZYT2H3V60ZLK8989VOJ5...',
            'type' => '9',
            'number' => $telefone,
            'msg' => $menSms,
            'refer' => 'CDC_HelpDesk');

        curl_setopt($ch, CURLOPT_URL, 'http://api.smsempresa.com.br/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $res = curl_exec($ch);
        $err = curl_errno($ch);
        $errmsg = curl_error($ch);
        $header = curl_getinfo($ch);

        curl_close($ch);
        
        $xml = simplexml_load_string($res); 
        
        if($xml->retorno['situacao'] != 'ERRO'){
            return true;
        }else{
            return false;
        }
    }

}
