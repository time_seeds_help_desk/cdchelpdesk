<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'dompdf/autoload.inc.php';

use Dompdf\Dompdf;

class Pdf {
    public $ci;
    public function __construct() {
        $this->ci =&get_instance();
    }

    public function gerar($view, $dados = array()) {
        $html = $this->ci->load->view($view, $dados, true);
        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $dompdf->stream("Ordem de Serviço da assistência.pdf", array("Attachment" => false));
    }

}
