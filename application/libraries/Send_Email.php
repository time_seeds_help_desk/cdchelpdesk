<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Email
 *
 * @author Helder dos Santos
 */
class Send_Email {

    public $ci;

    public function __construct() {
        $this->ci = &get_instance();
    }

    /**
     * Enviar e-mail com EMTP
     * @param type $assunto
     * @param type $mensagem
     * @param type $destinatario
     * @return type
     */
    public function enviar_email($assunto, $mensagem, $destinatario, $titulo) {
        $this->ci->load->library('email');

        $config['protocol'] = 'smtp';
        $config['charset'] = 'UTF-8';
        $config['mailtype'] = 'html';
        $config['smtp_host'] = 'mail.seedstech.com.br';
        $config['smtp_port'] = '25';
        $config['smtp_user'] = 'cdctecnologia@seedstech.com.br';
        $config['smtp_pass'] = '16E6Zq2018';
        $email = $this->montar_mensagem($titulo, $mensagem);

        $this->ci->email->initialize($config);

        $this->ci->email->from('cdctecnologia@seedstech.com.br', 'CDC Tecnologia');
        $this->ci->email->to($destinatario);

        $this->ci->email->subject($assunto);
        $this->ci->email->message($email);


        return $this->ci->email->send();
    }

    private function montar_mensagem($titulo, $conteudo) {
        $html = '<div style="width: calc(100% - 40px); background-color: #8610c6; padding: 20px; color:#fff">
                        <div style="background-color: #fff; padding: 10px; text-align: center">
                                <img src="' . base_url('assets/img/logo_cdc.png') . '" style="width: 150px; height: auto">
                        </div>
                        <h1 style="padding: 5px; text-align: center; color:#fff"">' . $titulo . '</h1>

                        <p style="text-align: justify; font-size: 14pt">' . $conteudo . '</p>
                 </div>';

        return $html;
    }

}
