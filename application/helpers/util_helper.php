<?php

define("ADMINISTRADOR", 'Administrador');
define("ATENDENTE", 'Atendente');
define("SUPORTE", 'Suporte Técnico');
define("ASSISTENCIA", "Assistência Técnica");
define("FINANCEIRO", 'Financeiro');
define('CLIENTE', 'Cliente');

function carregar_menu($dados_sessao) {
    $caminho = 'menus/';
    if (isset($dados_sessao['tipo_usuario'])) {
        if ($dados_sessao['tipo_usuario'] == ADMINISTRADOR) {
            $caminho .= "administrador";
        } else if ($dados_sessao['tipo_usuario'] == ATENDENTE) {
            $caminho .= "atendente";
        } else if ($dados_sessao['tipo_usuario'] == SUPORTE) {
            $caminho .= "suporte";
        } else if ($dados_sessao['tipo_usuario'] == ASSISTENCIA) {
            $caminho .= "assistencia";
        } else if ($dados_sessao['tipo_usuario'] == FINANCEIRO) {
            $caminho .= "financeiro";
        } else if ($dados_sessao['tipo_usuario'] == CLIENTE) {
            $caminho .= "cliente_juridico";
        }
    }
    return $caminho;
}

/**
 * Convete o valor monetário com pontos e virgulas 
 * para o formato compatível com o banco de dados
 * e retorna esse valor convertido.
 * ----------------------------------------------
 * Ex.: 2.531.154,00 para 2531154.00
 * ----------------------------------------------
 * @param type $valor
 * @return type
 */
function converte_valor_db($valor) {
    $valor = str_replace('.', '', $valor);
    return str_replace(',', '.', $valor);
}

/**
 * Converte o valor para o formato utilizado no BRASIL
 * ex.: 1222.50 para 1.222,00
 * @param type $valor
 * @return type
 */
function converte_valor_usuario($valor) {
    $quantidade = explode('.', $valor);

    if (count($quantidade) > 1) {
        return number_format($valor, 2, ',', '.');
    } else {
        return number_format($valor . '.00', 2, ',', '.');
    }
}

/**
 * gera um array com as informações necessárias para a view
 * @param type $titulo
 * @param type $titulo2
 * @param type $menu_selecionado
 * @return type
 */
function get_dados_view($titulo, $titulo2, $menu_selecionado) {
    return array(
        'titulo' => $titulo,
        'titulo2' => $titulo2,
        'menu_selecionado' => $menu_selecionado
    );
}

function config_paginacao($url, $total_registros) {
    $config_paginacao = array(
        'base_url' => base_url($url),
        'num_links' => 8,
        'uri_segment' => 3,
        'per_page' => 10,
        'total_rows' => $total_registros,
        'full_tag_open' => "<ul class='pagination pull-right'>",
        'full_tag_close' => '</ul>',
        'first_link' => false,
        'last_link' => false,
        'first_tag_open' => '<li>',
        'first_tag_close' => '</li>',
        'prev_link' => '<span aria-hidden="true"><i class="material-icons">arrow_back</i></span>',
        'prev_tag_open' => "<li class='prev'>",
        'prev_tag_close' => "</li>",
        'next_link' => '<span aria-hidden="true"><i class="material-icons">arrow_forward</i></span>',
        'next_tag_open' => "<li class='next'>",
        'next_tag_close' => "</li>",
        'last_tag_open' => "<li>",
        'last_tag_close' => "</li>",
        'cur_tag_open' => "<li class='active'><a href='javascript:void(0)'>",
        'cur_tag_close' => "</a></li>",
        'num_tag_open' => "<li>",
        'num_tag_close' => "</li>",
    );

    return $config_paginacao;
}

function validar_cpf($cpf) {
    // Verifica se um número foi informado
    if (empty($cpf)) {
        return false;
    }

    // Elimina possivel mascara
    $cpf = preg_replace("/[^0-9]/", "", $cpf);
    $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);

    // Verifica se o numero de digitos informados é igual a 11 
    if (strlen($cpf) != 11) {
        return false;
    }
    // Verifica se nenhuma das sequências invalidas abaixo 
    // foi digitada. Caso afirmativo, retorna falso
    else if ($cpf == '00000000000' ||
            $cpf == '11111111111' ||
            $cpf == '22222222222' ||
            $cpf == '33333333333' ||
            $cpf == '44444444444' ||
            $cpf == '55555555555' ||
            $cpf == '66666666666' ||
            $cpf == '77777777777' ||
            $cpf == '88888888888' ||
            $cpf == '99999999999') {
        return false;
        // Calcula os digitos verificadores para verificar se o
        // CPF é válido
    } else {

        for ($t = 9; $t < 11; $t++) {

            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf{$c} * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf{$c} != $d) {
                return false;
            }
        }

        return true;
    }
}

/**
 * recebe os dados da sessão criada e um array de níveis de 
 * acesso permitidos e verifica se o nível da sessão tem permissão para acessar 
 * o conteúdo da página
 * @param type $sessao
 * @param type $niveis_permitidos
 */
function verificar_permissao($sessao, $niveis_permitidos) {
    $acesso_permitido = false;
    if (isset($sessao['logado'])) {
        foreach ($niveis_permitidos as $valor) {
            if ($sessao['tipo_usuario'] == $valor) {
                $acesso_permitido = true;
            }
        }
    }

    if ($acesso_permitido == false) {
        redirect(base_url());
    }
}

function formatar_data($data) {
    return date('d/m/Y', strtotime($data));
}

/**
 * Código para verificar se o cnpj é válido
 * @param type $cnpj
 * @return boolean
 */
function validar_cnpj($cnpj) {
    $cnpj = preg_replace('/[^0-9]/', '', (string) $cnpj);
    // Valida tamanho
    if (strlen($cnpj) != 14)
        return false;
    // Valida primeiro dígito verificador
    for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++) {
        $soma += $cnpj{$i} * $j;
        $j = ($j == 2) ? 9 : $j - 1;
    }
    $resto = $soma % 11;
    if ($cnpj{12} != ($resto < 2 ? 0 : 11 - $resto))
        return false;
    // Valida segundo dígito verificador
    for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++) {
        $soma += $cnpj{$i} * $j;
        $j = ($j == 2) ? 9 : $j - 1;
    }
    $resto = $soma % 11;
    return $cnpj{13} == ($resto < 2 ? 0 : 11 - $resto);
}

function formatar_cpf($cpf) {
    $cpf = substr_replace($cpf, '.', 3, 0);
    $cpf = substr_replace($cpf, '.', 7, 0);
    $cpf = substr_replace($cpf, '-', 11, 0);
    return$cpf;
}

function validar_email($email) {
    if (filter_var($email, FILTER_VALIDATE_EMAIL)){
        return true;
    }
    
    return false;
}
