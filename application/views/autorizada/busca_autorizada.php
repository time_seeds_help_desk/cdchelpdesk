<div class="body">
    <div class="row">
        <div class="col-lg-6">
            <a href="<?= base_url('Autorizada/pagina_cadastro') ?>" class="btn btn-primary"><i class="material-icons">add</i> Nova Terceirizada</a>
        </div>
        <div class="col-lg-6">
            <form action="<?= base_url("Autorizada/buscar") ?>" method="post">
                <div class="col-md-9">
                    <input name="nome_autorizada" type="text" class="form-control" onkeypress="mascara(this, soLetrasNum)" placeholder="Buscar por nome" value="<?= $busca ?>">
                </div>
                <button class="btn btn-primary" style="height: 33px">BUSCAR</button>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nome</th>
                        <th>CNPJ</th>
                        <th>Responsável</th>
                        <th>Telefone</th>
                        <th>E-mail</th>
                        <th>Opções</th>
                    </tr>
                </thead>

                <tbody>

                    <?php
                    $count = 1;
                    foreach ($autorizadas as $a):
                        ?>
                        <tr>
                            <td><?= $count ?></td>
                            <td><?= $a->nome_autorizada ?></td>
                            <td><?= $a->cnpj ?></td>
                            <td><?= $a->responsavel ?></td>
                            <td><?= $a->telefone ?></td>
                            <td><?= $a->email ?></td>
                            <td>
                                <a href="<?= base_url('Autorizada/pagina_editar/' . $a->id_autorizada) ?>" class="btn btn-primary">Editar</a>
                                <a href="javascript:void(0)" onclick="abrirConfirmaExcluir(<?= $a->id_autorizada?>, '<?= $a->nome_autorizada ?>')" class="btn btn-danger">Excluir</a>
                            </td>
                        </tr> 
                        <?php
                        $count++;
                    endforeach;
                    ?>

                </tbody>
            </table>

            <br>
            <br>

            <?php if (count($autorizadas) < 1): ?>
                <center>
                    <h1 style="font-size: 30px;" class="text-primary">
                        Nenhum Terceirizada com nome <?=$busca?> Cadastrada
                    </h1>
                </center>
            <?php endif; ?>

        </div>
    </div>

</div>


<div id="excluir-autorizada" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/atencao.png') ?>"></h4>
            </div>
            <div class="modal-body">
                <p class="msg-modal text-center"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="deletarAutorizada()">Excluir</button>
            </div>
        </div>

    </div>
</div>


<div id="msm-sucesso" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/sucesso.png') ?>"></h4>
            </div>
            <div class="modal-body">

                <p class="msg-modal text-success text-center"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="location.reload()">Ok</button>
            </div>
        </div>

    </div>
</div>


<div id="msg-erro" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/erro.png') ?>"></h4>
            </div>
            <div class="modal-body">
                <p class="msg-modal text-danger text-center">

                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="location.reload()">Ok</button>
            </div>
        </div>

    </div>
</div>
<script src="<?= base_url('assets/js/requisicoes/autorizada.js') ?>" type="text/javascript"></script>

