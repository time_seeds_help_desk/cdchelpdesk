<form method="POST" action="<?= base_url('Autorizada/editar') ?>" id="form-editar-autorizada">
    <input type="hidden" name="id_autorizada" value="<?= $autorizada[0]->id_autorizada ?>">
    <div class="body">
        <div class="row">
            <div class="col-md-12">
                <a href="<?= base_url('Autorizada') ?>" class="btn btn-primary" title="voltar">
                    <i class="material-icons">arrow_back</i> Voltar
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" style="color: red">
                Campos com asteríscos (*) são obrigatórios.
            </div>
            <input type="hidden" name="tipo_pessoa" id="tipo_pessoa" value="<?= strlen($autorizada[0]->cnpj) === 18?'juridica':'fisica' ?>">
            <div class="col-md-6">
                <label>Nome<span> *</span></label>
                <input type="text" onkeypress="mascara(this, soLetras)" class="form-control" autofocus="" required="" name="nome_autorizada" value="<?= $autorizada[0]->nome_autorizada ?>">
            </div>

            <div class="col-md-6">
                <label>Responsável<span> *</span></label>
                <input type="text" onkeypress="mascara(this, soLetras)" class="form-control" autofocus="" required="" name="responsavel" value="<?= $autorizada[0]->responsavel ?>">
            </div>           

            <div class="col-md-6">
                <label>Ramo<span> *</span></label>
                <input type="text" onkeypress="mascara(this, soLetras)" class="form-control" autofocus="" required="" name="ramo" value="<?= $autorizada[0]->ramo ?>">
            </div>


            <div class="col-md-3">
                <label>CNPJ<span> *</span></label>
                <input type="text" class="form-control" name="cnpj" id="cpf-cnpj" value="<?= $autorizada[0]->cnpj ?>">
            </div>

            <div class="col-md-3">
                <label>Telefone<span> *</span></label>
                <input type="text" class="form-control mascara-telefone" name="telefone" value="<?= $autorizada[0]->telefone ?>">
            </div>



            <div class="col-md-12">
                <label>E-mail<span> *</span></label>
                <input type="email" class="form-control"  name="email" value="<?= $autorizada[0]->email ?>">
            </div>

            <div class="col-md-5">
                <label>Rua<span> *</span></label>
                <input type="text" class="form-control" onkeypress="mascara(this, soLetrasNum)" required="" name="rua" value="<?= $autorizada[0]->rua ?>">
            </div>

            <div class="col-md-2">
                <label>Número<span> *</span></label>
                <input type="text" class="form-control"onkeypress="mascara(this, soLetrasNum)"  name="numero" required="" value="<?= $autorizada[0]->numero ?>">
            </div>

            <div class="col-md-5">
                <label>Bairro<span> *</span></label>
                <input type="text" class="form-control" onkeypress="mascara(this, soLetrasNum)" required="" name="bairro" value="<?= $autorizada[0]->bairro ?>">
            </div>

            <div class="col-md-6">
                <label>CEP<span> *</span></label>
                <input type="text" class="form-control mascara-cep" name="cep" value="<?= $autorizada[0]->cep ?>">
            </div>

            <div class="col-md-6">
                <label>Complemento</label>
                <input type="text" class="form-control" onkeypress="mascara(this, soLetrasNum)" name="complemento" value="<?= $autorizada[0]->complemento ?>"">
            </div>

            <div class="col-sm-6">
                <label>Estado<span> *</span></label>
                <select class="form-control" id="sigla_estado" required="">
                    <option value="" selected="">Selecione o Estado</option>
                    <?php
                    $opcao_selecionada = "";
                    foreach ($estados as $estado):
                        if ($estado->sigla == $estado_selecionado) {
                            $opcao_selecionada = "selected=''";
                        }
                        ?>
                        <option value="<?= $estado->sigla ?>" <?= $opcao_selecionada ?> ><?= $estado->nome_estado ?></option>
                        <?php
                        $opcao_selecionada = "";
                    endforeach;
                    ?>
                </select>
            </div>

            <div class="col-sm-6">
                <label>Cidade<span> *</span></label>
                <select class="form-control" name="id_cidade" id="id_cidade" required="">
                    <?php
                    $opcao_selecionada = "";
                    if (isset($cidades)):
                        echo "<option value=''>Selecione a Cidade</option>";
                        foreach ($cidades as $cidade):
                            if ($cidade->id_cidade == $cidade_selecionada) {
                                $opcao_selecionada = "selected=''";
                            }
                            ?>
                            <option value="<?= $cidade->id_cidade ?>" <?= $opcao_selecionada ?>><?= $cidade->nome_cidade ?></option>
                            <?php
                            $opcao_selecionada = "";
                        endforeach;
                    endif;
                    ?>
                </select>
            </div>



        </div>

        <div class="row">
            <div class="col-md-12" style="text-align: right">
                <a href="<?= base_url("Autorizada") ?>" class="btn btn-warning btn-lg">Cancelar</a>
                <button type="submit" class="btn btn-primary btn-lg">Alterar</button>
            </div>
        </div>

    </div>
</form>


<div id="msm-sucesso" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/sucesso.png') ?>"></h4>
            </div>
            <div class="modal-body">

                <p class="msg-modal text-success text-center"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="voltarListaAutorizada()">Ok</button>
            </div>
        </div>

    </div>
</div>



<div id="msg-erro" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/erro.png') ?>"></h4>
            </div>
            <div class="modal-body">
                <p class="msg-modal text-danger text-center">

                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
            </div>
        </div>

    </div>
</div>


<script src="<?= base_url('assets/js/requisicoes/autorizada.js') ?>" type="text/javascript"></script>






