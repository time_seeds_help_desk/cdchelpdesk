<script src="<?= base_url('assets/js/requisicoes/modulo_sistema.js') ?>" type="text/javascript"></script>
<script src="<?= base_url('assets/js/requisicoes/sistema.js') ?>" type="text/javascript"></script>
<div class="body">
    <div class="row">
        <div class="col-lg-6">
            <a href="<?= base_url('Sistema/pagina_cadastro') ?>" class="btn btn-primary"><i class="material-icons">add</i> Novo Sistema</a>
        </div>
        <div class="col-lg-6">
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nome</th>
                        <th>Fornecedor</th>
                        <th>Versão</th>
                        <th>Valor Mensalidade</th>
                        <th>Opções</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    $count = 1;
                    foreach ($sistemas as $s):
                        ?>
                        <tr>
                            <td><?= $count ?></td>
                            <td><?= $s->nome_sistema ?></td>
                            <td><?= $s->fornecedor ?></td>
                            <td><?= $s->versao ?></td>
                            <td><?= "R$ " . converte_valor_usuario($s->valor_mensalidade) ?></td>
                            <td>
                                <a href="javascript:void(0)" class="btn btn-default carregar-modulos" idsistema="<?= $s->id_sistema ?>" nomesistema="<?= $s->nome_sistema ?>">Módulos</a>
                                <a href="<?= base_url('Sistema/pagina_editar/' . $s->id_sistema) ?>" class="btn btn-primary">Editar</a>
                                <a href="javascript:void(0)" onclick="abrirConfirmaExcluir(<?= $s->id_sistema ?>, '<?= $s->nome_sistema ?>')" class="btn btn-danger">Excluir</a>
                            </td>
                        </tr> 
                        <?php
                        $count++;
                    endforeach;
                    ?>
                </tbody>
            </table>
            <br><br>
            <?php if (count($sistemas) < 1): ?>
                <center>
                    <h1 style="font-size: 30px;" class="text-primary">
                        Nenhum Sistema Cadastrado
                    </h1>
                </center>
            <?php endif; ?>

        </div>
    </div>

</div>


<div id="excluir-sistema" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/atencao.png') ?>"></h4>
            </div>
            <div class="modal-body">
                <p class="msg-modal text-center"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="deletarSistema()">Excluir</button>
            </div>
        </div>

    </div>
</div>


<div id="msm-sucesso" class="modal fade" role="dialog" style="z-index: 20000">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/sucesso.png') ?>"></h4>
            </div>
            <div class="modal-body">

                <p class="msg-modal text-success text-center"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="location.reload()">Ok</button>
            </div>
        </div>

    </div>
</div>

<div id="msm-erro" class="modal fade" role="dialog" style="z-index: 20000">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/erro.png') ?>"></h4>
            </div>
            <div class="modal-body">

                <p class="msg-modal text-danger text-center"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>

<!--============================MODAL EXCLUIR DE MÓDULO=======================-->
<div id="confirma-excluir-modulo" class="modal fade" role="dialog" style="z-index: 1500">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center img-atencao"><img src="<?= base_url('assets/img/atencao.png') ?>"></h4>
                <h4 class="modal-title text-center img-erro"><img src="<?= base_url('assets/img/erro.png') ?>"></h4>
                <h4 class="modal-title text-center img-sucesso"><img src="<?= base_url('assets/img/sucesso.png') ?>"></h4>
            </div>
            <div class="modal-body">
                <p class="msg-modal text-center">

                </p>
            </div>
            <div class="modal-footer btn-confirma">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-danger" onclick="excluirModulo()">Confirmar</button>
            </div>

            <div class="modal-footer btn-msg">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="atualizaListaModolos()">Ok</button>
            </div>
        </div>

    </div>
</div>
<!--============================MODAL EXCLUIR DE MÓDULO=======================-->


<!--============================MODAL CADASTRO DE MÓDULO=======================-->
<div id="cadastrar-modulo" class="modal fade" role="dialog" style="z-index: 1500">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center img-atencao"><img src="<?= base_url('assets/img/atencao.png') ?>"></h4>
                <h4 class="modal-title text-center img-erro"><img src="<?= base_url('assets/img/erro.png') ?>"></h4>
                <h4 class="modal-title text-center img-sucesso"><img src="<?= base_url('assets/img/sucesso.png') ?>"></h4>
            </div>
            <div class="modal-body">
                <p class="msg-modal text-center">

                </p>
            </div>
            <div class="modal-footer" id="btn-confirma">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-danger" onclick="excluirModulo()">Confirmar</button>
            </div>

            <div class="modal-footer" id="btn-msg">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="atualizaListaModolos()">Ok</button>
            </div>
        </div>

    </div>
</div>
<!--============================MODAL CADASTRO DE MÓDULO=======================-->


<!-- Modal Módulos -->
<div id="modal-modulos" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="titulo-modal-modulo"></h4>
            </div>
            <div class="modal-body">


                <div id="lista-modulos-sistema">
                    <div class="row">
                        <div class="col-lg-12">
                            <a href="javascript:void(0)" class="btn btn-primary btn-opcao-modulo"  pagina='cadastrar-modulo'><i class="material-icons">add</i> Novo Módulo</a>
                        </div>

                    </div>
                    <br>
                    <div class="row">
                        <div class="col-lg-12 table-responsive">
                            <table id="tabela-modulo-sistema" class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Nome</th>
                                        <th>Opções</th>
                                    </tr>
                                </thead>

                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>   

                <div id="form-cad-modulo">
                    <div class="row">
                        <div class="col-lg-12">
                            <a href="javascript:void(0)" class="btn btn-primary  btn-opcao-modulo" pagina='listar-modulo'><i class="material-icons">arrow_back</i> Voltar</a>
                        </div>
                    </div>
                    <br>
                    <form action="<?= base_url('ModuloSistema/cadastrar') ?>">
                        <input type="hidden" name="id_sistema" id="input_id_sistema">
                        <div class="row">
                            <div class="col-md-12">
                                <label>Nome do Módulo</label>
                                <input type="text" class="form-control" name="nome_modulo_sistema" required="">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12" style="text-align: right">
                                <button type="reset" class="btn btn-warning btn-opcao-modulo" pagina='listar-modulo'><i class="material-icons">cancel</i> Cancelar</button>
                                <button type="submit" class="btn btn-success"><i class="material-icons">sd_storage</i> Cadastrar</button>
                            </div>
                        </div>

                    </form>
                </div>

                <div id="form-editar-modulo">
                    <div class="row">
                        <div class="col-lg-12">
                            <a href="javascript:void(0)" class="btn btn-primary  btn-opcao-modulo"  pagina='listar-modulo'><i class="material-icons">arrow_back</i> Voltar</a>
                        </div>
                    </div>
                    <br>
                    <form action="<?= base_url('ModuloSistema/editar') ?>" id="form-editar-modulo">
                        <input type="hidden" name="id_modulo_sistema" id="id_modulo">
                        <div class="row">
                            <div class="col-md-12">
                                <label>Nome do Módulo</label>
                                <input type="text" class="form-control" name="nome_modulo_sistema" id="editar_nome_modulo_sistema" required="">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12" style="text-align: right">
                                <button type="reset" class="btn btn-warning btn-opcao-modulo" pagina='listar-modulo'><i class="material-icons">cancel</i> Cancelar</button>
                                <button type="submit" class="btn btn-primary"><i class="material-icons">sd_storage</i> Alterar</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>


        </div>

    </div>
</div>
<!-- Modal Módulos -->






