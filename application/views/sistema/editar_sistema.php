
<form method="POST" action="<?= base_url('Sistema/editar') ?>" id="form-cad-sistema">
    <input type="hidden" name="id_sistema" value="<?= $sistema[0]->id_sistema ?>">
    <div class="body">
        <div class="row">
            <div class="col-md-12">
                <a href="<?= base_url('Sistema') ?>" class="btn btn-primary" title="voltar">
                    <i class="material-icons">arrow_back</i> Voltar
                </a>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12" style="color: red">
                Campos com asterísco (*) são obrigatórios
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <label>Nome do Sistema<span> *</span></label>
                <input type="text" class="form-control" autofocus="" required="" name="nome_sistema" value="<?= $sistema[0]->nome_sistema ?>">
            </div>

            <div class="col-md-6">
                <label>Fornecedor<span> *</span></label>
                <input type="text" class="form-control" required="" name="fornecedor" value="<?= $sistema[0]->fornecedor ?>">
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <label>Versão<span> *</span></label>
                <input type="text" onkeypress="mascara(this, soLetrasNum)" class="form-control" autofocus="" required="" name="versao" value="<?= $sistema[0]->versao ?>">
            </div>

            <div class="col-md-6">
                <label>Valor da Mensalizada<span> *</span></label>
                <input type="text" class="form-control mascara-dinheiro" name="valor_mensalidade" required="" value="<?= converte_valor_usuario($sistema[0]->valor_mensalidade) ?>">
            </div>
        </div>

        <div class="row">
            <div class="col-md-12" style="text-align: right">
                <a href="<?= base_url('Sistema') ?>" class="btn btn-warning"><i class="material-icons">cancel</i> Cancelar</a>
                &nbsp;&nbsp;<button type="submit" class="btn btn-primary"><i class="material-icons">sd_storage</i> Alterar</button>
            </div>
        </div>

    </div>
</form>




<div id="msm-sucesso" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" onclick="voltar('Sistema')">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/sucesso.png') ?>"></h4>
            </div>
            <div class="modal-body">

                <p class="msg-modal text-success text-center"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="voltar('Sistema')">Ok</button>
            </div>
        </div>

    </div>
</div>



<div id="msg-erro" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/erro.png') ?>"></h4>
            </div>
            <div class="modal-body">
                <p class="msg-modal text-danger text-center">

                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="voltarListaEquip()">Ok</button>
            </div>
        </div>

    </div>
</div>


<script src="<?= base_url('assets/js/requisicoes/sistema.js') ?>" type="text/javascript"></script>





