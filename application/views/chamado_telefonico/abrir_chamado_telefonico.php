<script src="<?= base_url('assets/js/requisicoes/chamado_telefonico.js') ?>" type="text/javascript"></script>

<div class="body">
    <div class="row" id="btn-os-suporte">
        <div class="col-md-12">
            <a class="btn btn-primary" href="<?= base_url('ChamadoTelefonico') ?>">
                <i class="material-icons">arrow_back</i>
                Voltar
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 table-responsive">
            <table class="table table-bordered table-striped">
                <tr>
                    <td style="width: 150px;"><strong>Protocolo</strong></td>
                    <td><?= $chamado_telefonico[0]->protocolo ?></td>
                </tr>
                <tr>
                    <td><strong>Cliente</strong></td>
                    <td>
                        <?= "<strong>Nome: </strong>" . $chamado_telefonico[0]->nome_nm_fantasia . "&nbsp;&nbsp;&nbsp; <strong>CNPJ: </strong>" . $chamado_telefonico[0]->cpf_cnpj . "&nbsp;&nbsp;&nbsp; <strong>TELEFONE: </strong>" . $chamado_telefonico[0]->telefone ?>
                        <br>
                        <strong>Endereço: </strong>
                        <?=
                        $chamado_telefonico[0]->rua . ', ' .
                        $chamado_telefonico[0]->numero . ', ';
                        ?>
                        <?=
                        $chamado_telefonico[0]->complemento == "" ? '' : $chamado_telefonico[0]->complemento . ', ';
                        ?>
                        <?=
                        $chamado_telefonico[0]->bairro . ', ' .
                        $chamado_telefonico[0]->nome_cidade . ' - ' .
                        $chamado_telefonico[0]->sigla;
                        ?>
                    </td>
                </tr>
                <tr>
                    <td><strong>Data</strong></td>
                    <td><?= formatar_data($chamado_telefonico[0]->data) ?></td>
                </tr>
                <tr>
                    <td><strong>Hora Início</strong></td>
                    <td><?= $chamado_telefonico[0]->hora_inicio ?></td>
                </tr>
                <tr>
                    <td><strong>Hora Término</strong></td>
                    <td><?= $chamado_telefonico[0]->hora_termino ?></td>
                </tr>
                <tr>
                    <td><strong>Status</strong></td>
                    <td><?= $chamado_telefonico[0]->fl_resolvido == true ? "Resolvida" : "Inicializa uma OS" ?></td>
                </tr>
                <tr>
                    <td><strong>Descrição do Problema</strong></td>
                    <td><?= $chamado_telefonico[0]->descricao_problema ?></td>
                </tr>
            </table>
        </div>        
    </div>
</div>





