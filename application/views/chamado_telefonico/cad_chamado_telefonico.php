<link href="<?= base_url('assets/css/estilo_autocomplete.css') ?>" type="text/css" rel="stylesheet">

<form method="POST" action="<?= base_url('ChamadoTelefonico/cadastrar') ?>" id="cad-chamado-telefonico">
    <input type="hidden" name="id_pessoa_cliente" id="id_cliente">
    <input type="hidden" name="protocolo" id="protocolo" value="<?= $protocolo ?>">
    <input type="hidden" name="hora_inicio" id="hora_inicio">
    <input type="hidden" name="hora_termino" id="hora_termino">
    <div class="body">
        <div class="row">
            <div class="col-md-12">
                <a href="<?= base_url('ChamadoTelefonico') ?>" class="btn btn-primary" title="voltar">
                    <i class="material-icons">arrow_back</i> Voltar
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" style="color: red">
                Campos com asterísco (*) são obrigatórios
            </div>
            <div class="col-md-3">
                <label>Protocolo</label>
                <input type="text" class="form-control" disabled="" value="<?= $protocolo ?>">
            </div>

            <div class="col-md-3">
                <label>Data</label>
                <input type="text" class="form-control" disabled="" value="<?= date('d/m/Y') ?>">
            </div> 

            <div class="col-md-3">
                <label>Hora Início</label>
                <input type="text" class="form-control" disabled="" id="input_hora_inicio">
            </div>

            <div class="col-md-3">
                <label>Hora Término</label>
                <input type="text" class="form-control" disabled="" id="input_hora_terminio">
            </div>

            <div class="col-md-12">
                <label class="col-12">Cliente<span> *</span></label>
                <div class="row">
                    <div class="col-md-12">                        
                        <input class="form-control" id="cliente" autocomplete="off" autofocus="" placeholder="Nome Fantasia do Cliente" required="">

                        <!--Preloader-->
                        <div class="preloader-input" id="load-cli">
                            <div class="preloader pl-size-xs">
                                <div class="spinner-layer pl-indigo">
                                    <div class="cicle-clipper left">
                                        <div class="circle"></div>
                                    </div>
                                    <div class="circle-clipper right">
                                        <div class="circle"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Preloader-->
                        <div class="autocomplete" id="cliente-autocomplete">
                            <table>
                                <thead>
                                    <tr>
                                        <th>Nome</th>
                                        <th>CNPJ</th>
                                    </tr>
                                </thead>
                                <tbody id="opcoes-cliente">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <label>Descrição do Problema<span> *</span></label>
                <textarea name="descricao_problema" id="descricao_problema" class="form-control" rows="4" required="" placeholder="Informe o problema descrito pelo cliente"></textarea>
            </div>

        </div>

        <div class="row">
            <div class="col-md-12" style="text-align: right">
                <a href="<?= base_url('ChamadoTelefonico') ?>" class="btn btn-primary btn-lg" id="btn-vincular-os">Vincular a OS</a>&nbsp;&nbsp;&nbsp;
                <button type="submit" class="btn btn-primary btn-lg">Finalizar</button>
            </div>
        </div>

    </div>
</form>


<div id="msm-sucesso" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/sucesso.png') ?>"></h4>
            </div>
            <div class="modal-body">

                <p class="msg-modal text-success text-center"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="voltarListaChamadosTelefonicos()">Ok</button>
            </div>
        </div>

    </div>
</div>



<div id="msg-erro" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/erro.png') ?>"></h4>
            </div>
            <div class="modal-body">
                <p class="msg-modal text-danger text-center">

                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>

<div id="msg-sucesso" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" onclick="voltar('Sistema')">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/sucesso.png') ?>"></h4>
            </div>
            <div class="modal-body">
                <p class="msg-modal text-success text-center"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="voltarListaChamadosTelefonicos()">Ok</button>
            </div>
        </div>

    </div>
</div>



<script src="<?= base_url('assets/js/requisicoes/chamado_telefonico.js') ?>" type="text/javascript"></script>




