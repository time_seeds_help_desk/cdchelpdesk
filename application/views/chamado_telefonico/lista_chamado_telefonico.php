<script src="<?= base_url('assets/js/requisicoes/chamado_telefonico.js') ?>" type="text/javascript"></script>

<div class="body">
    <div class="row" id="btn-os-chamado">
        <div class="col-md-3">
            <a class="btn btn-primary btn-block" href="<?= base_url('ChamadoTelefonico/pagina_cadastro') ?>">
                <i class="material-icons">add_circle_outline</i>
                Novo Chamado
            </a>
        </div>
        <div class="col-md-3">

            <button class="btn btn-primary btn-block" onclick="ativaFormBuscaOS()">
                <i class="material-icons">search</i>
                Pesquisar
            </button>
        </div>
    </div>

    <div style="display: none" id="form-os-chamado-buscar">
        <?php echo form_open_multipart(base_url('ChamadoTelefonico/buscar')); ?>
        <div class="row">
            <div class="col-md-6">
                <label>Nome do Cliente</label>
                <input type="text" class="form-control" name="nome_nm_fantasia" id="buscar_nome_nm_fantasia">
            </div>

            <div class="col-md-6">
                <label>Status</label>
                <select class="form-control" name="fl_resolvido" id="buscar_fl_resolvido">
                    <option value="-1">Selecione</option>
                     <option value="1">Resolvido</option>
                      <option value="0">Vinculado a O.S.</option>
                </select>
            </div>

        </div>

        <div class="row">
            <div class="col-md-6" style="text-align: right">

            </div>

            <div class="col-md-6" style="text-align: right">
                <a href="<?= base_url('ChamadoTelefonico') ?>" class="btn btn-warning">
                    <i class="material-icons">cancel</i>
                    Cancelar
                </a>
                <button type="submit" class="btn btn-primary">
                    <i class="material-icons">search</i>
                    Buscar
                </button>
            </div>
        </div>
        </form>
    </div>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <div>
                CHAMADOS TELEFÔNICOS
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12 table-responsive">
                    <table class="table table-hover" id="tbl-lista-chamados">
                        <thead  style="text-align: center">
                            <tr>
                                <th>PROTOCOLO</th>
                                <th>CLIENTE</th>
                                <th>DATA</th>
                                <th>HORA INÍCIO</th>
                                <th>HORA TÉRMINO</th>
                                <th>STATUS</th>
                                <th>ABRIR</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php
                            foreach ($chamados_telefonicos as $c):
                                $status = $c->fl_resolvido == true ? "Resolvido" : "<a class='btn btn-success' href='" . base_url('OSSuporte/abrir/' . $c->id_ordem_de_servico) . "'>Vinculado a O.S.</a>";
                                ?>
                                <tr>
                                    <td><?= $c->protocolo ?></td>
                                    <td><?= $c->nome_nm_fantasia ?></td>
                                    <td><?= formatar_data($c->data) ?></td>
                                    <td><?= $c->hora_inicio ?></td>
                                    <td><?= $c->hora_termino ?></td>
                                    <td><?= $status ?></td>
                                    <td><a href="<?= base_url('ChamadoTelefonico/abrir/' . $c->id_chamado_telefonico) ?>" class="btn btn-primary">Abrir</a></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="paginacao">
        <div class="col-md-12">
            <?= $tem_pagina == true ? $paginacao : "" ?>
        </div>
    </div>
</div>



