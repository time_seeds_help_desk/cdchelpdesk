<!DOCTYPE html>
<html lang="pt-br">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title>CDC Tecnologia - HelpDesk</title>
        <!-- Favicon-->
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <link rel="stylesheet" href="<?= base_url('assets/css/estilo.css') ?>">

        
        <!-- Jquery Core Js -->
        <script src="<?= base_url('assets/template/plugins/jquery/jquery.min.js') ?>"></script>

        <script src="<?= base_url('assets/js/requisicoes_ajax/comum.js') ?>"></script>
        <script src="<?= base_url('assets/js/mascaras/jquery.mask.min.js') ?>"></script>

        <script src="<?= base_url('assets/js/sweetalert2.js') ?>"></script>

        <!-- Bootstrap Core Js -->
        <script src="<?= base_url('assets/template/plugins/bootstrap/js/bootstrap.js') ?>"></script>


        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

        <!-- Bootstrap Core Css -->
        <link href="<?= base_url('assets/template/plugins/bootstrap/css/bootstrap.css') ?>" rel="stylesheet">

        <!-- Waves Effect Css -->
        <link href="<?= base_url('assets/template/plugins/node-waves/waves.css') ?>" rel="stylesheet" />

        <!-- Animation Css -->
        <link href="<?= base_url('assets/template/plugins/animate-css/animate.css') ?>" rel="stylesheet" />

        <!-- Morris Chart Css-->
        <link href="<?= base_url('assets/template/plugins/morrisjs/morris.css') ?>" rel="stylesheet" />

        <!-- Custom Css -->
        <link href="<?= base_url('assets/template/css/style.css') ?>" rel="stylesheet">

        <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
        <link href="<?= base_url('assets/template/css/themes/all-themes.css') ?>" rel="stylesheet" />
        <script>
            var base_url = '<?= base_url() ?>';
            $(document).ready(function (){
                $("#pelicula").hide();
            });
        </script>
    </head>

    <body class="theme-indigo">

        <div class="carregando pelicula-inativa" id="pelicula">
            <div class="loader" id="img-carregando">
                <div class="preloader">
                    <div class="spinner-layer pl-red">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div>
                        <div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>    
                </div>
            </div>
        </div>
        <!-- Page Loader -->
        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="preloader">
                    <div class="spinner-layer pl-red">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div>
                        <div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>
                <p>Por favor, espere...</p>
            </div>
        </div>
        <!-- #END# Page Loader -->
        <!-- Overlay For Sidebars -->
        <div class="overlay"></div>
        <!-- #END# Overlay For Sidebars -->
        <!-- Search Bar -->
        <div class="search-bar">
            <div class="search-icon">
                <i class="material-icons">search</i>
            </div>
            <input type="text" placeholder="START TYPING...">
            <div class="close-search">
                <i class="material-icons">close</i>
            </div>
        </div>
        <!-- #END# Search Bar -->
        <!-- Top Bar -->
        <nav class="navbar">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a href="javascript:void(0);" class="bars"></a>
                    <a class="navbar-brand" href="<?= base_url() ?>">CDC Tecnologia - HelpDesk</a>
                    <!--<a href="<? base_url('login/sair') ?>" id="btn-sair" class="btn btn-warning" title="Sair">Sair</a>-->
                    <div class="btn-group user-helper-dropdown" id="btn-sair">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="color: white">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            
                            <li><a href="<?= base_url('Login/sair')?>"><i class="material-icons">input</i>Sair</a></li>
                        </ul>
                    </div>
                </div>                
            </div>
        </nav>
        <!-- #Top Bar -->
        <section>
            <!-- Left Sidebar -->
            <aside id="leftsidebar" class="sidebar">
                <!-- User Info -->
                <div class="user-info">
                    <div class="image">
                        <img src="<?= base_url('assets/template/images/user.png') ?>" width="48" height="48" alt="User" />
                    </div>
                    <div class="info-container">
                        <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?=$this->session->userdata('nome')?></div>
                        <div class="email"><?=$this->session->userdata('email')?></div>
                        <div class="btn-group user-helper-dropdown">
                            <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="<?= base_url('Login/sair')?>"><i class="material-icons">input</i>Sair</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- #User Info -->
                <!-- Menu -->
                <div class="menu">
                    <ul class="list">
                        <li class="header">MENU PRINCIPAL</li>



