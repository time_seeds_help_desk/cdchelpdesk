<script type="text/javascript" src="<?= base_url('assets/js/requisicoes/cliente.js') ?>"></script>
<div id="cad-cliente" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="modal-title text-center" id="titulo-cad-cliente">Cadastro de Cliente</h2>
            </div>


            <div class="modal-body" id="botoes-escolha-form">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <div class="col-md-6">
                            <button formopen="cliente-fisico" class="btn btn-success btn-block btn-escolhido-cad-cli"><i class="material-icons">add</i> Novo Cliente Físico</button>
                        </div>
                        <div class="col-md-6">
                            <button formopen="cliente-jurico" class="btn btn-success btn-block  btn-escolhido-cad-cli"><i class="material-icons">add</i> Novo Cliente Jurídico</button>
                        </div>
                    </div>
                </div>
                <br><br><br>
            </div>

            <!--==================CLIENTE FÍSICO==================-->
            <form id="cad-cliente-fisico">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12" style="color: red">
                            Campos com asterísco (*) são obrigatórios
                            <br>
                            <br>
                        </div>
                        <div class="col-md-12">
                            <label>Nome<span> *</span></label>
                            <input autocomplete="off" type="text" onkeypress="mascara(this, soLetras)" class="form-control" autofocus="" required="" name="nome_nm_fantasia">
                            <br>
                        </div>

                        <div class="col-md-3">
                            <label>CPF</label>
                            <input autocomplete="off" type="text" class="form-control mascara-cpf" name="cpf_cnpj" >
                            <br>
                        </div>

                        <div class="col-md-3">
                            <label>Telefone<span> *</span></label>
                            <input autocomplete="off" type="text" class="form-control mascara-telefone" name="telefone" required="">
                            <br>
                        </div>



                        <div class="col-md-6">
                            <label>E-mail</label>
                            <input autocomplete="off" type="email" class="form-control"  name="email" >
                            <br>
                        </div>

                        <div class="col-md-8">
                            <label>Rua<span> *</span></label>
                            <input autocomplete="off" type="text" class="form-control" onkeypress="mascara(this, soLetrasNum)" required="" name="rua">
                            <br>
                        </div>

                        <div class="col-md-4">
                            <label>Número<span> *</span></label>
                            <input autocomplete="off" required="" type="text" class="form-control"onkeypress="mascara(this, soLetrasNum)"  name="numero">
                            <br>
                        </div>

                        <div class="col-md-5">
                            <label>Bairro<span> *</span></label>
                            <input autocomplete="off" type="text" class="form-control" onkeypress="mascara(this, soLetrasNum)" required="" name="bairro" >
                            <br>
                        </div>

                        <div class="col-md-2">
                            <label>CEP<span> *</span></label>
                            <input autocomplete="off" type="text" class="form-control mascara-cep" name="cep" required="">
                            <br>
                        </div>

                        <div class="col-md-5">
                            <label>Complemento</label>
                            <input autocomplete="off" type="text" class="form-control" onkeypress="mascara(this, soLetrasNum)" name="complemento">
                            <br>
                        </div>

                        <div class="col-sm-6">
                            <label>Estado<span> *</span></label>
                            <select class="form-control sigla_estado" name="sigla_estado" id="cf_sigla_estado" required="">
                                <option value="" selected="">Selecione o Estado</option>
                            </select>
                            <br>
                        </div>

                        <div class="col-sm-6">
                            <label>Cidade<span> *</span></label>
                            <select class="form-control id_cidade" name="id_cidade" required="">

                            </select>
                            <br>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reser" class="btn btn-warning" data-dismiss="modal"><i class="material-icons">cancel</i> Cancelar</button>
                    <button type="submit" class="btn btn-success"><i class="material-icons">sd_storage</i> Cadastrar</button>
                </div>
            </form>
            <!--================CLIENTE FÍCICO====================-->


            <!--=================CLIENTE JURÍDICO=================-->
            <form id="cad-cliente-juridico">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12" style="color: red">
                            Campos com asterísco (*) são obrigatórios
                            <br>
                            <br>
                        </div>
                        <div class="col-md-6">
                            <label>Nome Fantasia<span> *</span></label>
                            <input autocomplete="off" type="text" class="form-control" required=""  name="nome_nm_fantasia">
                            <br>
                        </div>

                        <div class="col-md-6">
                            <label>Razão Social<span> *</span></label>
                            <input autocomplete="off" type="text" class="form-control" required=""  name="razao_social">
                            <br>
                        </div>

                        <div class="col-md-6">
                            <label>CNPJ<span> *</span></label>
                            <input autocomplete="off" type="text" class="form-control mascara-cnpj" required=""  name="cpf_cnpj">
                            <br>
                        </div>

                        <div class="col-md-6">
                            <label>Responsável<span> *</span></label>
                            <input autocomplete="off" type="text" onkeypress="mascara(this, soLetras)" class="form-control" autofocus=""  required="" name="responsavel">
                            <br>
                        </div>


                        <div class="col-md-6">
                            <label>Telefone<span> *</span></label>
                            <input autocomplete="off" type="text" class="form-control mascara-telefone" name="telefone">
                            <br>
                        </div>

                        <div class="col-md-6">
                            <label>Rua<span> *</span></label>
                            <input autocomplete="off" type="text" onkeypress="mascara(this, soLetrasNum)" class="form-control" required=""  name="rua" value="<?= set_value('rua') ?>">
                            <br>
                        </div>

                        <div class="col-md-2">
                            <label>Número<span> *</span></label>
                            <input autocomplete="off" type="text" class="form-control" required=""  name="numero">
                            <br>
                        </div>

                        <div class="col-md-5">
                            <label>Bairro<span> *</span></label>
                            <input autocomplete="off" type="text" onkeypress="mascara(this, soLetrasNum)" class="form-control" required=""  name="bairro">
                            <br>
                        </div>

                        <div class="col-md-2">
                            <label>CEP<span> *</span></label>
                            <input autocomplete="off" type="text" class="form-control mascara-cep" required=""  name="cep">
                            <br>
                        </div>

                        <div class="col-md-3">
                            <label>Complemento</label>
                            <input autocomplete="off" type="text" class="form-control" onkeypress="mascara(this, soLetrasNum)" name="complemento">
                            <br>
                        </div>

                        <div class="col-sm-6">
                            <label>Estado<span> *</span></label>
                            <select class="form-control sigla_estado" name="sigla_estado" id="cj_sigla_estado" required="" >
                                <option value="" selected="">Selecione</option>
                                <option value="1">teste</option>
                            </select>
                            <br>
                        </div>

                        <div class="col-sm-6">
                            <label>Cidade<span> *</span></label>
                            <select class="form-control id_cidade" name="id_cidade" required="" >
                                <option value="" selected="">Selecione</option>
                                <option value="1">teste</option>
                            </select>
                            <br>
                        </div>

                        <div class="col-md-12">
                            <label>E-mail<span> *</span></label>
                            <input autocomplete="off" type="email" class="form-control"  name="email" required="" >
                            <br>
                        </div>

                        <div class="col-md-3" style="display: none">
                            <label>Senha<span> *</span></label>
                            <input autocomplete="off" type="password" class="form-control" name="senha">
                            <br>
                        </div>

                        <div class="col-md-3" style="display: none">
                            <label>Confirmar Senha<span> *</span></label>
                            <input autocomplete="off" type="password" class="form-control" name="confirma_senha">
                            <br>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reser" class="btn btn-warning" data-dismiss="modal"><i class="material-icons">cancel</i> Cancelar</button>
                    <button type="submit" class="btn btn-success"><i class="material-icons">sd_storage</i> Cadastrar</button>
                </div>
            </form>

            <!--=================CLIENTE JURÍDICO==================-->
        </div>
    </div>
</div>


<div id="msm-sucesso-cad-cli" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" onclick="voltarListaOSAssis()">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/sucesso.png') ?>"></h4>
            </div>
            <div class="modal-body">

                <p class="msg-modal text-success text-center"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
            </div>
        </div>

    </div>
</div>



<div id="msg-erro-cad-cli" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/erro.png') ?>"></h4>
            </div>
            <div class="modal-body">
                <p class="msg-modal text-danger text-center">

                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
            </div>
        </div>

    </div>
</div>

