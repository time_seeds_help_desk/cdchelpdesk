<link href="<?= base_url('assets/css/estilo_autocomplete.css') ?>" type="text/css" rel="stylesheet">
<script src="<?= base_url('assets/js/requisicoes/os_assistencia.js') ?>" type="text/javascript"></script>


<div class="body">

    <div class="row">
        <div class="col-md-12">
            <a href="<?= base_url('OSAssistencia') ?>" class="btn btn-primary" title="voltar">
                <i class="material-icons">arrow_back</i> Voltar
            </a>
        </div>
    </div>

    <form id="form-cadastro" enctype="multipart/form-data" method="post" accept-charset="utf-8" action="<?= base_url('OSAssistencia/cadastrar') ?>">
        <input type="hidden" name="id_cliente" id="id_cliente">
        <input type="hidden" name="protocolo" id="protocolo" value="<?= $protocolo ?>">
        <div class="row">
            <div class="col-md-12" style="color: red">
                Campos com asterísco (*) são obrigatórios
            </div>
            <div class="col-md-4">
                <label>Protocolo</label>
                <input type="text" class="form-control" disabled="" value="<?= $protocolo ?>">
            </div>

            <div class="col-md-4">
                <label>Usuário</label>
                <input type="text" class="form-control" disabled="" value="<?= $this->session->userdata('nome') ?>">
            </div>

            <div class="col-md-4">
                <label>Técnico<span> *</span></label>
                <select name="id_tecnico" id="id_tecnico" class="form-control" required="" >
                    <option value="">Selecione</option>
                    <?php foreach ($tecnicos as $t): ?>
                        <option value="<?= $t->id_pessoa ?>"><?= $t->nome_nm_fantasia ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <div class="col-md-4">
                <label>Data de Abertura</label>
                <input type="text" class="form-control" value="<?= date('d/m/Y') ?>" disabled="">
            </div>

            <div class="col-md-4">
                <label>Status<span> *</span></label>
                <select class="form-control" name="id_status" id="id_status" required="">
                    <option value="">Selecione</option>
                    <?php foreach ($status_os as $s): ?>
                        <option value="<?= $s->id_status_os ?>"><?= $s->nome_status_os ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <div class="col-md-4">
                <label>Setor</label>
                <input type="text" class="form-control" disabled="" value="Assistência Técnica">
            </div>

            <div class="col-md-6">
                <label class="col-12">Cliente<span> *</span></label>
                <div class="row">
                    <div class="col-md-10">                        
                        <input class="form-control" id="cliente" autocomplete="off" placeholder="Nome do Cliente" required="">

                        <!--Preloader-->
                        <div class="preloader-input" id="load-cli">
                            <div class="preloader pl-size-xs">
                                <div class="spinner-layer pl-indigo">
                                    <div class="cicle-clipper left">
                                        <div class="circle"></div>
                                    </div>
                                    <div class="circle-clipper right">
                                        <div class="circle"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Preloader-->
                        <div class="autocomplete" id="cliente-autocomplete">
                            <table>
                                <thead>
                                    <tr>
                                        <th>Nome</th>
                                        <th>CPF/CNPJ</th>
                                    </tr>
                                </thead>
                                <tbody id="opcoes-cliente">
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <a id="abrir-modal-cad-cliente" href="#cad-cliente" class="btn btn-primary" title="Novo Cliente" style="padding: 4px 8px;" data-toggle="modal">
                        <i class="material-icons">add</i>
                    </a>
                </div>
            </div>

            <div class="col-md-6">
                <label>Anexo (Tamanho Máximo: 8 MB)</label>
                <input type="file" class="form-control" id="anexo" name="anexo">
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <input type="checkbox" name="fl_enviar_sms" id="fl_enviar_sms">
                <label for="fl_enviar_sms" style="font-weight: bold">Enviar SMS</label>
            </div>
            <div class="col-md-3">
                <input type="checkbox" name="fl_enviar_email" id="fl_enviar_email">
                <label for="fl_enviar_email" style="font-weight: bold">Enviar E-mail</label>
            </div>
        </div>
        <div class="row">
            <fieldset class="col-md-12">
                <legend class="text-primary">Informações do Equipamento</legend>
                <div class="row">
                    <div class="col-md-6">
                        <label>Equipamento<span> *</span></label>
                        <input type="text" class="form-control" name="nome_equipamento_recebido" id="nome_equipamento_recebido" required="" autocomplete="off">
                    </div>     

                    <div class="col-md-6">
                        <label>Modelo</label>
                        <input type="text" class="form-control" name="modelo_equipamento" id="modelo_equipamento" autocomplete="off">
                    </div> 

                    <div class="col-md-6">
                        <label>Marca</label>
                        <input type="text" class="form-control" name="marca_equipamento" id="marca_equipamento" autocomplete="off">
                    </div> 

                    <div class="col-md-6">
                        <label>Cor</label>
                        <input type="text" class="form-control" name="cor_equipamento" id="cor_equipamento" autocomplete="off">
                    </div> 

                    <div class="col-md-6">
                        <label>Número de Série</label>
                        <input type="text" class="form-control" name="num_serie_equipamento" id="num_serie_equipamento" autocomplete="off">
                    </div> 

                    <div class="col-md-6">
                        <label>Patrimônio</label>
                        <input type="text" class="form-control" name="patrimonio" id="patrimonio" autocomplete="off">
                    </div> 
                </div>
            </fieldset>
        </div>


        <div class="row">
            <fieldset class="col-md-12">
                <legend class="text-primary">Problema</legend>
                <div class="row">
                    <div class="col-md-6">
                        <label >Descrição do Problema<span> *</span></label>
                        <textarea required="" class="form-control" rows="5" name="descricao_problema" id="descricao_problema" autocomplete="off"></textarea>
                    </div>     

                    <div class="col-md-6">
                        <label>Observação</label>
                        <textarea class="form-control" rows="5" name="observacao" id="observacao" autocomplete="off"></textarea>
                    </div> 
                </div>
            </fieldset>
        </div>

        <div class="row">
            <div class="col-md-6" style="text-align: right">

            </div>

            <div class="col-md-6" style="text-align: right">
                <a href="<?= base_url('OSAssistencia') ?>" class="btn btn-warning btn-lg">
                    Cancelar
                </a>
                <button type="submit" class="btn btn-success btn-lg">
                    Cadastrar
                </button>
            </div>
        </div>
    </form>
</div>

<?php $this->load->view('os_assistencia/modal_cad_cliente') ?>

<div id="msm-sucesso" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" onclick="voltarListaOSAssis()">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/sucesso.png') ?>"></h4>
            </div>
            <div class="modal-body">

                <p class="msg-modal text-success text-center"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="voltarListaOSAssis()">Ok</button>
            </div>
        </div>

    </div>
</div>



<div id="msg-erro" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/erro.png') ?>"></h4>
            </div>
            <div class="modal-body">
                <p class="msg-modal text-danger text-center">

                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
            </div>
        </div>

    </div>
</div>

<?php if (isset($msg)): ?>
    <script>
        abrirMsmCadastro("<?= $msg ?>", "<?= $cadastrou == true ? "sim" : "nao" ?>");
    </script>
<?php endif; ?>