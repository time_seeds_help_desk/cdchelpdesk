<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Ordem de Serviço da assistência.pdf</title>

        <style>
            *{
                margin: 0;
                padding: 0;
            }

            body{
                margin: 30px 60px;
            }

            #cabecalho{
                text-align: center;
            }
            #cabecalho img{
                width: 200px;
                height: auto;
            }

            hr{
                margin: 15px 0 0 0;
                background-color:  #003162;
            }

            h1{
                font-size: 20pt;
                font-weight: bold;
                color: #003162;
            }

            h2{
                font-size: 15pt;
                font-weight: 300;
            }

            table{
                margin-top: 20px;
                width: 100%;
                border-collapse:collapse
            }

            table th{
                text-align: center;
                background-color: #98bcc8;
            }

            table td, table th{
                padding: 2px 4px;
            }

            table td, table tr, table th{
                border: 1px solid #000;
                border-collapse: collapse;
            }

            .assinaturas{
                margin: 20px 0;
            }

            .assinaturas div{
                border-top: 1px solid #000;
                width: 60%;
                margin: 40px auto;
                text-align: center;                
            }

        </style>
    </head>

    <body>
        <div id="cabecalho">
            <img src="assets/img/logo_cdc.png" alt=""/>
            <!--<h1>CDC Tecnologia</h1>-->
            <h2>Ordem de Serviço - Assistência Técnica</h2>
        </div>
        <hr>

        <br><br>
        <strong>Cliente:</strong> <?= $ordem_de_servico[0]->nome_cliente ?>  &nbsp; &nbsp;
        <strong>CPF/CNPJ:</strong> <?= $ordem_de_servico[0]->cpf_cnpj_cliente ?> &nbsp; &nbsp;
        <strong>Telefone:</strong>  <?= $ordem_de_servico[0]->telefone_cliente ?><br>
        <strong>E-mail:</strong>  <?= $ordem_de_servico[0]->email_cliente ?> <br>

        <!--<table>
            <thead>
                <tr>
                    <th colspan="4">Dados do Cliente</th>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td><strong>Nome:</strong> <? $ordem_de_servico[0]->nome_cliente ?> </td>
                    <td><strong>CPF/CNPJ:</strong> <? $ordem_de_servico[0]->cpf_cnpj_cliente ?> </td>
                    <td><strong>Telefone:</strong>  <? $ordem_de_servico[0]->telefone_cliente ?> </td>
                    <td><strong>E-mail:</strong>  <? $ordem_de_servico[0]->email_cliente ?> </td>
                </tr>
            </tbody>
        </table>-->



        <table>
            <thead>
                <tr>
                    <th colspan="3">Informação da Ordem de Serviço</th>
                </tr>
            </thead>

            <tbody>

                <tr>
                    <td colspan="2"><strong>Responsável pela abertura:</strong> <?= $ordem_de_servico[0]->nome_responsavel ?></td>
                    <td colspan="1"><strong>Técnico Responsável:</strong> <?= $ordem_de_servico[0]->nome_tecnico ?></td>                    
                </tr>

                <tr>
                    <td colspan="3"><strong>Data de Abertura:</strong> <?= date("d/m/Y", strtotime($ordem_de_servico[0]->data_abertura)) ?></td>               
                </tr>

                <tr>
                    <td><strong>Equipamento:</strong> <?= $ordem_de_servico[0]->nome_equipamento_recebido ?> </td>
                    <td><strong>Marca:</strong> <?= $ordem_de_servico[0]->marca_equipamento ?></td>
                    <td><strong>Modelo:</strong> <?= $ordem_de_servico[0]->modelo_equipamento ?> </td>

                </tr>

                <tr>
                    <td><strong>Cor:</strong> <?= $ordem_de_servico[0]->cor_equipamento ?> </td> 
                    <td><strong>Patrimônio:</strong> <?= $ordem_de_servico[0]->patrimonio ?> </td>
                    <td><strong>N/S:</strong> <?= $ordem_de_servico[0]->num_serie_equipamento ?></td>
                </tr>

                <tr>
                    <td colspan="3"><strong>Descrição do Problema:</strong> <?= $ordem_de_servico[0]->descricao_problema ?> </td>
                </tr>

                <tr>
                    <td colspan="3"><strong>Observação:</strong> <?= $ordem_de_servico[0]->observacao ?> </td>
                </tr>
            </tbody>
        </table>


        <table>
            <thead>
                <tr>
                    <th colspan="2">Laudo Técnico</th>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td><strong>Autorizada:</strong> <?= isset($autorizada[0]->nome_autorizada) ? $autorizada[0]->nome_autorizada : "" ?></td>
                    <td><strong>Descrição do Problema: </strong><?= $ordem_de_servico[0]->descricao_problema_autorizada ?></td>
                </tr>


                <tr>
                    <td colspan="2"><strong>Laudo: </strong> <?= $ordem_de_servico[0]->laudo_tecnico ?></td>
                </tr>
            </tbody>
        </table>



        <table>
            <thead>
                <tr>
                    <th colspan="2">Equipamentos</th>
                </tr>
            </thead>

            <tbody>
                <tr style="background-color: #e2f0f6; font-weight: bold">
                    <td>Nome</td>
                    <td>Valor</td>
                </tr>


                <?php
                $valor_total = 0;
                foreach ($equipamentos_os as $e):
                    $valor_total += $e->valor_equipamento;
                    ?>
                    <tr>
                        <td><?= $e->nome_equipamento ?></td>
                        <td>R$ <?= converte_valor_usuario($e->valor_equipamento) ?></td>
                    </tr>
                <?php endforeach; ?>

            </tbody>
        </table>


        <table>
            <thead>
                <tr>
                    <th colspan="2">Atividades</th>
                </tr>
            </thead>

            <tbody>
                <tr style="background-color: #e2f0f6; font-weight: bold">
                    <td>Nome</td>
                    <td>Valor</td>
                </tr>

                <?php
                foreach ($atividades_os as $a):
                    $valor_total += $a->valor_atividade;
                    ?>
                    <tr>
                        <td><?= $a->nome_atividade ?></td>
                        <td>R$ <?= converte_valor_usuario($a->valor_atividade) ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

        <table>
            <thead>
                <tr>
                    <th>Valor Total</th>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td style="text-align: center">R$ <?= converte_valor_usuario($valor_total) ?></td>
                </tr>
            </tbody>
        </table>

        <div class="assinaturas">
            <div class="divisor">Assinatura do Cliente</div>
            <div class="divisor">Assinatura do Técnico</div>            
        </div>
    </body>
</html>
