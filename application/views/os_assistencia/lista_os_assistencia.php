<script src="<?= base_url('assets/js/requisicoes/os_assistencia.js') ?>" type="text/javascript"></script>

<div class="body">
    <div class="row" id="btn-os-assistencia">
        <div class="col-md-3">
            <a class="btn btn-primary btn-block" href="<?= base_url('OSAssistencia/pagina_cadastro') ?>">
                <i class="material-icons">add_circle_outline</i>
                Nova Ordem de Serviço
            </a>
        </div>
        <div class="col-md-3">

            <button class="btn btn-primary btn-block" onclick="ativaFormBuscaOS()">
                <i class="material-icons">search</i>
                Pesquisar
            </button>
        </div>
    </div>

    <div style="display: none" id="form-os-assistencia">
        <?php echo form_open_multipart(base_url('OSAssistencia/buscar')); ?>

        <div class="row">
            <div class="col-md-3">
                <label>Protocolo</label>
                <input type="text" class="form-control" name="protocolo" id="protocolo">
            </div>

            <div class="col-md-3">
                <label>Técnico</label>
                <select class="form-control" name="tecnico" id="tecnico">
                    <option value="0">Selecione</option>
                    <?php foreach ($tecnicos as $t): ?>
                        <option value="<?= $t->id_pessoa ?>"><?= $t->nome_nm_fantasia ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <div class="col-md-3">
                <label>Usuário</label>
                <select class="form-control" name="usuario" id="usuario">
                    <option value="0">Selecione</option>
                    <?php foreach ($funcionarios as $f): ?>
                        <option value="<?= $f->id_pessoa ?>"><?= $f->nome_nm_fantasia ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <div class="col-md-3">
                <label>Status</label>
                <select class="form-control" name="status" id="status">
                    <option value="0">Selecione</option>
                    <?php foreach ($status_os as $s): ?>
                        <option value="<?= $s->id_status_os ?>"><?= $s->nome_status_os ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6" style="text-align: right">

            </div>

            <div class="col-md-6" style="text-align: right">
                <a href="<?= base_url('OSAssistencia') ?>" class="btn btn-warning">
                    <i class="material-icons">cancel</i>
                    Cancelar
                </a>
                <button type="submit" class="btn btn-primary">
                    <i class="material-icons">search</i>
                    Buscar
                </button>
            </div>
        </div>
        </form>
    </div>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <div>
                ORDENS DE SERVIÇO DA ASSISTÊNCIA TÉCNICA
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12 table-responsive">
                    <table class="table table-hover" id="tbl-lista-os">
                        <thead  style="text-align: center">
                            <tr>
                                <th>PROTOCOLO</th>
                                <th>DATA DE ABERTURA</th>
                                <th>DATA DE CONCLUSÃO</th>
                                <th>STATUS</th>
                                <th>TÉCNICO RESPONSÁVEL</th>
                                <th>CLIENTE</th>
                                <th>EQUIPAMENTO</th>
                                <th style="width: 100px;">OPÇÕES</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php foreach ($os as $o): ?>
                                <tr>
                                    <td><?= $o->protocolo ?></td>
                                    <td><?= formatar_data($o->data_abertura) ?></td>
                                    <td><?= $o->data_conclusao != "" ? formatar_data($o->data_conclusao) : "" ?></td>
                                    <td><?= $o->nome_status_os ?></td>
                                    <td><?= $o->nome_tecnico ?></td>
                                    <td><?= $o->nome_cliente ?></td>
                                    <td><?= $o->nome_equipamento_recebido ?></td>
                                    <td style="width: 120px;">
                                        <div class="row">
                                            <a target="_blank" title="PDF" href="<?= base_url("OSAssistencia/gerar_pdf/" . $o->id_ordem_de_servico) ?>" class="btn btn-danger">
                                                <i class="material-icons">picture_as_pdf</i>
                                            </a>
                                            <a title="ABRIR" href="<?= base_url("OSAssistencia/abrir/" . $o->id_ordem_de_servico) ?>" class="btn btn-primary">
                                                <i class="material-icons">folder_open</i>
                                            </a>
                                        </div>                                        
                                    </td>
                                </tr>
                            <?php endforeach; ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="paginacao">
        <div class="col-md-12">
            <?= $tem_pagina == true ? $paginacao : "" ?>
        </div>
    </div>
</div>