<script>
    //Variável para ser utilizada no arquivo os_assistencia.js para conseguir fazer requisições
    //buscando dados da OS da Assistência técnica
    var idOsAssistenciaSelecionada = "<?= $ordem_de_servico[0]->id_ordem_de_servico ?>";
    var idAutorizadaOsSelecionada = "<?= $ordem_de_servico[0]->id_autorizada ?>";
</script>
<script src="<?= base_url('assets/js/requisicoes/os_assistencia.js') ?>" type="text/javascript"></script>


<div class="body">
    <div class="row">
        <div class="col-md-2">
            <a href="<?= base_url('OSAssistencia') ?>" class="btn btn-primary btn-block">
                <i class="material-icons">arrow_back</i> Voltar
            </a>
        </div>
        <div class="col-md-8">
            <ul class="abas">
                <li class="selecionada" pagina="informacoes">
                    <a>Informações da O.S.</a>
                </li>

                <li pagina="laudo">
                    <a>Laudo Técnico</a>      
                </li>

                <li pagina="anexo">
                    <a>Anexo</a>
                </li>
            </ul>
        </div>

        <div class="col-md-2">

        </div>
    </div>

    <div class="conteudo-abas">
        <div class="opcao ativo" id="informacoes">
            <form method="post" action="" id="form-editar-os">
                <input type="hidden" name="id_ordem_de_servico" id="id_ordem_de_servico" value="<?= $ordem_de_servico[0]->id_ordem_de_servico ?>">
                <input type="hidden" name="fl_enviar_sms" id="fl_enviar_sms" value="<?= $ordem_de_servico[0]->fl_enviar_sms != 0? "true":"false" ?>">
                <input type="hidden" name="fl_enviar_email" id="fl_enviar_email" value="<?= $ordem_de_servico[0]->fl_enviar_email != 0? "true":"false" ?>">
                <input type="hidden" name="id_cliente" id="id_cliente" value="<?= $ordem_de_servico[0]->id_cliente ?>">
                <input type="hidden" name="protocolo" id="protocolo" value="<?= $ordem_de_servico[0]->protocolo ?>">
                <div class="row">
                    <div class="col-md-4">
                        <label>Protocolo</label>
                        <input type="text" class="form-control" disabled="" style="background-color: #efefef" value="<?= $ordem_de_servico[0]->protocolo ?>">
                    </div>

                    <div class="col-md-4">
                        <label>Usuário</label>
                        <input type="text" class="form-control" disabled="" value="<?= $ordem_de_servico[0]->nome_responsavel ?>" style="background-color: #efefef">
                    </div>

                    <div class="col-md-4">
                        <label>Técnico</label>
                        <input type="text" class="form-control" disabled="" value="<?= $ordem_de_servico[0]->nome_tecnico ?>" style="background-color: #efefef">
                    </div>

                    <div class="col-md-4">
                        <label>Data de Abertura</label>
                        <input type="text" class="form-control" value="<?= formatar_data($ordem_de_servico[0]->data_abertura) ?>" disabled="" style="background-color: #efefef">
                    </div>

                    <div class="col-md-4">
                        <label>Status (*)</label>
                        <select class="form-control" name="id_status" id="id_status" required="">
                            <option value="">Selecione</option>
                            <?php foreach ($status_os as $s): ?>
                                <option value="<?= $s->id_status_os ?>" <?= $ordem_de_servico[0]->id_status == $s->id_status_os ? 'selected' : '' ?>><?= $s->nome_status_os ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="col-md-4">
                        <label>Setor</label>
                        <input type="text" class="form-control" disabled="" value="Assistência Técnica" style="background-color: #efefef">
                    </div>

                    <div class="col-md-6">
                        <label class="col-12">Cliente</label>
                        <input type="text" class="form-control" disabled="" value="<?= $ordem_de_servico[0]->nome_cliente ?>" style="background-color: #efefef">
                    </div>
                </div>

                <div class="row">
                    <fieldset class="col-md-12">
                        <legend class="text-primary">Informações do Equipamento</legend>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Equipamento</label>
                                <input type="text" class="form-control" value="<?= $ordem_de_servico[0]->nome_equipamento_recebido ?>" disabled="" style="background-color: #efefef" name="nome_equipamento_recebido" id="nome_equipamento_recebido" required="" autocomplete="off">
                            </div>     

                            <div class="col-md-6">
                                <label>Modelo</label>
                                <input type="text" class="form-control" disabled="" value="<?= $ordem_de_servico[0]->modelo_equipamento ?>" style="background-color: #efefef" name="modelo_equipamento" id="modelo_equipamento" autocomplete="off">
                            </div> 

                            <div class="col-md-6">
                                <label>Marca</label>
                                <input type="text" class="form-control" disabled="" value="<?= $ordem_de_servico[0]->marca_equipamento ?>"style="background-color: #efefef" name="marca_equipamento" id="marca_equipamento" autocomplete="off">
                            </div> 

                            <div class="col-md-6">
                                <label>Cor</label>
                                <input type="text" class="form-control" disabled="" value="<?= $ordem_de_servico[0]->cor_equipamento ?>" style="background-color: #efefef" name="cor_equipamento" id="cor_equipamento" autocomplete="off">
                            </div> 

                            <div class="col-md-6">
                                <label>Número de Série</label>
                                <input type="text" class="form-control"  disabled="" value="<?= $ordem_de_servico[0]->num_serie_equipamento ?>" style="background-color: #efefef" name="num_serie_equipamento" id="num_serie_equipamento" autocomplete="off">
                            </div> 

                            <div class="col-md-6">
                                <label>Patrimônio</label>
                                <input type="text" class="form-control" disabled="" value="<?= $ordem_de_servico[0]->patrimonio ?>" style="background-color: #efefef" name="patrimonio" id="patrimonio" autocomplete="off">
                            </div> 
                        </div>
                    </fieldset>
                </div>


                <div class="row">
                    <fieldset class="col-md-12">
                        <legend class="text-primary">Problema</legend>
                        <div class="row">
                            <div class="col-md-6">
                                <label >Descrição do Problema</label>
                                <textarea required="" class="form-control" rows="5" disabled="" style="background-color: #efefef"><?= $ordem_de_servico[0]->descricao_problema ?></textarea>
                            </div>     

                            <div class="col-md-6">
                                <label>Observação</label>
                                <textarea class="form-control" rows="5" style="background-color: #efefef" disabled=""><?= $ordem_de_servico[0]->observacao ?></textarea>
                            </div> 
                        </div>
                    </fieldset>
                </div>

                <div class="row">
                    <div class="col-md-6" style="text-align: right">

                    </div>

                    <div class="col-md-6" style="text-align: right">
                        <button type="reset" class="btn btn-warning btn-lg">
                            Cancelar
                        </button>
                        <button type="submit" class="btn btn-primary btn-lg">
                            Alterar
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div class="opcao" id="laudo">
            <?php
            $this->load->view('os_assistencia/abrir_laudo_tecnico', array('ordem_de_servico' => $ordem_de_servico));
            ?>
        </div>
        <div class="opcao" id="anexo"> 
            <?php
            $this->load->view('os_assistencia/abrir_anexo', array('ordem_de_servico' => $ordem_de_servico));
            ?>
        </div>
    </div>
</div>

<div id="msm-sucesso" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/sucesso.png') ?>"></h4>
            </div>
            <div class="modal-body">
                <p class="msg-modal text-success text-center"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
            </div>
        </div>

    </div>
</div>



<div id="msg-erro" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/erro.png') ?>"></h4>
            </div>
            <div class="modal-body">
                <p class="msg-modal text-danger text-center">

                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
            </div>
        </div>

    </div>
</div>

<?php if (isset($msg)): ?>
    <script>
    abrirMsmCadastro("<?= $msg ?>", "<?= $cadastrou == true ? "sim" : "nao" ?>");
    </script>
<?php endif; ?>

