<?php if ($this->session->userdata('tipo_usuario') === ATENDENTE): ?>
    <style>
        #custos-os a{
            display:none;
        }

    </style>
<?php endif; ?>

<div class="row">
    <div class="col-md-3">
        <strong>Protocolo:</strong> <?= $ordem_de_servico[0]->protocolo ?>
    </div>
    <div class="col-md-3">
        <strong>Equipamento:</strong> <?= $ordem_de_servico[0]->nome_equipamento_recebido ?>
    </div>
    <div class="col-md-3">
        <strong>Modelo:</strong> <?= $ordem_de_servico[0]->modelo_equipamento ?>
    </div>
    <div class="col-md-3">
        <strong>Marca:</strong> <?= $ordem_de_servico[0]->marca_equipamento ?>
    </div>
</div>

<div class="row">
    <div class="col-md-3">
        <strong>Cor:</strong> <?= $ordem_de_servico[0]->cor_equipamento ?>
    </div>
    <div class="col-md-3">
        <strong>Número de Série:</strong> <?= $ordem_de_servico[0]->num_serie_equipamento ?>
    </div>
    <div class="col-md-3">
        <strong>Patrimonio:</strong> <?= $ordem_de_servico[0]->patrimonio ?>
    </div>
    <div class="col-md-3">
        <strong>Status:</strong> <?= $ordem_de_servico[0]->nome_status_os ?>
    </div>
</div>

<div class="row">
    <?php
    $tem_autorizada = false;
    if ($ordem_de_servico[0]->id_autorizada != "" && $ordem_de_servico[0]->id_autorizada != 0) :
        $tem_autorizada = true;
        ?>
        <script>

            $(document).ready(function () {
                $.ajax({
                    method: "POST",
                    url: base_url + "Autorizada/buscar_por_id/<?= $ordem_de_servico[0]->id_autorizada ?>",
                    data: {},
                    dataType: "json"
                }).done(function (resposta) {
                    autorizadas = resposta.autorizadas;
                    html = "<strong>Terceirizada: </strong>";
                    for (var i = 0; i < autorizadas.length; i++) {
                        html += autorizadas[i].nome_autorizada + " / Ramo: " + autorizadas[i].ramo + " / Cidade: " + autorizadas[i].nome_cidade + " / Telefone: " + autorizadas[i].telefone + " / E-mail: " + autorizadas[i].email;
                    }
                    $("#info-autorizada").html(html);
                });
            });
        </script>
        <?php
    endif;
    ?>
    <div class="col-md-12" id="info-autorizada">
        <strong>Terceirizada:</strong> <?= $tem_autorizada == false ? "Equipamento não foi enviado à terceirizada." : "" ?>
    </div>
</div>

<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-3">
        <button id="btn-enviar-autorizada" class="btn btn-success btn-block btn-lg"><i class="material-icons">location_city</i> <?= $this->session->userdata('tipo_usuario') != ATENDENTE ? "Enviar para Terceirizada":"Terceirizada"?></button>
    </div>
    <div class="col-md-3">
        <button id="btn-solucionar" class="btn btn-success btn-block btn-lg"><i class="material-icons">build</i> <?= $this->session->userdata('tipo_usuario') != ATENDENTE ? "Solucionar":"Solução Técnica"?></button>
    </div>
    <div class="col-md-3"></div>
</div>

<div class="row form-laudo">
    <div class="col-md-12 opcao-laudo" id="form-enviar-autorizada">
        <fieldset>
            <legend class="text-primary">Enviar para Terceirizada</legend>
            <form method="post" id="form-add-autorizada">
                <input type="hidden" id="form-add-id-os" value="<?= $ordem_de_servico[0]->id_ordem_de_servico ?>">
                <div class="col-md-12">
                    <label>Responsável<span> *</span></label>
                    <select name="id_autorizada" id="add-id-autorizada" class="form-control" required="" <?= $this->session->userdata('tipo_usuario') != ATENDENTE ? '' : "disabled=''" ?> >
                        <option value="" >Selecione a Terceirizada</option>
                    </select>
                </div>

                <div class="col-md-12">
                    <label>Descrição do Problema<span> *</span></label>
                    <textarea rows="6" name="descricao_problema_autorizada" id="descricao_problema_autorizada" class="form-control" placeholder="Informe o problema que está levando o produto a ser enviado à autorizada." <?= $this->session->userdata('tipo_usuario') != ATENDENTE ? '' : "disabled=''" ?> ><?= $ordem_de_servico[0]->descricao_problema_autorizada ?></textarea>
                </div>
                <?php if ($this->session->userdata('tipo_usuario') != ATENDENTE): ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6"></div>

                            <div class="col-md-3">
                                <button id='btn-cancel-solucionar' type="reset" class="btn btn-warning btn-block"><i class="material-icons">cancel</i> Cancelar</button>
                            </div>
                            <div class="col-md-3">
                                <button type="submit" class="btn btn-success btn-block"><i class="material-icons">sd_storage</i> Confirmar</button>
                            </div>
                        </div>
                    </div>
                <?php else: ?>
                    <div class="row">
                        <div class="col-md-9"></div>
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-12">
                                    <a id='btn-cancel-autorizada-atendente' class="btn btn-warning btn-block btn-atendente"><i class="material-icons">cancel</i> Fechar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

            </form>
        </fieldset>
    </div>

    <div class="col-md-12 opcao-laudo" id="form-solucionar">
        <fieldset>
            <legend class="text-primary"><?= $this->session->userdata('tipo_usuario') != ATENDENTE ? "Solucionar Problema":"Solução Técnica"?></legend>
            <form id="formulario-laudo">
                <div class="row">
                    <div class="col-md-12">
                        <label>Laudo Técnico<span> *</span></label>
                        <textarea <?= $this->session->userdata('tipo_usuario') != ATENDENTE ? '' : "disabled=''" ?> id="texto-laudo" class="form-control" rows="4" placeholder="Digite aqui os problemas que você encontrou no produto"><?= $ordem_de_servico[0]->laudo_tecnico ?></textarea>
                    </div>
                    <?php if ($this->session->userdata('tipo_usuario') != ATENDENTE): ?>
                        <div class="row">
                            <!--CAMPOS EQUIPAMENTOS E ATIVIDADES-->
                            <div class="col-md-6">
                                <label style="width: calc(100%); margin-left: 15px;">Produto</label>
                                <div class="col-md-9">
                                    <select name="sol-equipamento" id="sol-equipamento" class="form-control">
                                        <option value="">Selecione o Produto</option>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <a class="btn btn-primary btn-block" id="btn-adicionar-equipamento">
                                        <i class="material-icons">add_circle</i>
                                    </a>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <label style="width: calc(100%); margin-left: 15px;">Serviço</label>
                                <div class="col-md-9">
                                    <select name="sol-atividade" id="sol-atividade" class="form-control">
                                        <option value="">Selecione o Serviço</option>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <a class="btn btn-primary btn-block" id="btn-adicionar-atividade">
                                        <i class="material-icons">add_circle</i>
                                    </a>
                                </div>
                            </div>

                            <!--CAMPOS EQUIPAMENTOS E ATIVIDADES-->
                        </div>
                    <?php endif; ?>



                    <!--TABELA-->
                    <div class="col-md-12 ">
                        <div style="border: 1px solid #222; height: 250px; max-height: 250px; overflow: auto">
                            <table id="custos-os" class="table table-responsive">
                                <thead>
                                    <tr style="background-color: #222; color: #fff">
                                        <th>Custos</th>
                                        <th>Valor</th>
                                        <th>Excluir</th>
                                    </tr>
                                </thead>

                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        <div id="rodape-tbl-custos">
                            Total: R$ 0,00
                        </div>
                    </div>

                    <!--TABELA-->

                </div>

                <?php if ($this->session->userdata('tipo_usuario') != ATENDENTE): ?>
                    <div class="row">
                        <div class="col-md-6"></div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-6">
                                    <button id='btn-cancel-solucionar' type="reset" class="btn btn-warning btn-block"><i class="material-icons">cancel</i> Cancelar</button>
                                </div>
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-success btn-block"><i class="material-icons">sd_storage</i> Salvar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php else: ?>
                    <div class="row">
                        <div class="col-md-9"></div>
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-12">
                                    <a id='btn-cancel-solucionar-atendente' class="btn btn-warning btn-block btn-atendente"><i class="material-icons">cancel</i> Fechar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

            </form>
        </fieldset>
    </div>
</div>

