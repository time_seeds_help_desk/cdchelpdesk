<li class="<?= $menu_selecionado == 'inicio' ? 'active' : '' ?>">
    <a href="<?= base_url() ?>">
        <i class="material-icons">home</i>
        <span>Início</span>
    </a>
</li>

<li class="<?= $menu_selecionado == 'clientes fisicos' ? 'active' : '' ?>">
    <a href="<?= base_url('ClienteFisico') ?>">
        <i class="material-icons">person</i>
        <span>Clientes Físicos</span>
    </a>
</li>

<li class="<?= $menu_selecionado == 'OS-Assistencia' ? 'active' : '' ?>">
    <a href="<?= base_url("OSAssistencia") ?>">
        <i class="material-icons">widgets</i>
        <span>O.S. Assistência</span>
    </a>
</li>



</ul>
</div>
<!-- #Menu -->
</aside>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2 style="text-transform: uppercase "><?= $titulo ?></h2>
        </div>

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header" style="text-transform: uppercase">
                        <?= isset($titulo2) ? $titulo2 : '' ?>
                    </div> 




