<li class="<?= $menu_selecionado == 'inicio' ? 'active' : '' ?>">
    <a href="<?= base_url() ?>">
        <i class="material-icons">home</i>
        <span>Início</span>
    </a>
</li>

<li class="<?= $menu_selecionado == 'clientes juridicos' ? 'active' : '' ?>">
    <a href="<?= base_url('ClienteJuridico') ?>">
        <i class="material-icons">person</i>
        <span>Solicitações de Atendimento</span>
    </a>
</li>

</ul>
</div>
<!-- #Menu -->
</aside>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2 style="text-transform: uppercase "><?= $titulo ?></h2>
        </div>

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header" style="text-transform: uppercase">
                        <?= isset($titulo2) ? $titulo2 : '' ?>
                    </div> 







