<li class="<?= $menu_selecionado == 'inicio' ? 'active' : '' ?>">
    <a href="<?= base_url() ?>">
        <i class="material-icons">home</i>
        <span>Início</span>
    </a>
</li>

<li class="<?= $menu_selecionado == 'agendamento' ? 'active' : '' ?>">
    <a href="<?= base_url("Agendamento") ?>">
        <i class="material-icons">date_range</i>
        <span>Agenda</span>
    </a>
</li>

<li class="<?= $menu_selecionado == 'clientes fisicos' ? 'active' : '' ?>">
    <a href="<?= base_url('ClienteFisico') ?>">
        <i class="material-icons">person</i>
        <span>Clientes Físicos</span>
    </a>
</li>

<li class="<?= $menu_selecionado == 'clientes juridicos' ? 'active' : '' ?>">
    <a href="<?= base_url('ClienteJuridico') ?>">
        <i class="material-icons">person</i>
        <span>Clientes Jurídicos</span>
    </a>
</li>

<li class="<?= $menu_selecionado == 'autorizada' ? 'active' : '' ?>">
    <a href="<?= base_url('Autorizada') ?>">
        <i class="material-icons">person</i>
        <span>Terceirizada</span>
    </a>
</li>

<li class="<?= $menu_selecionado == 'funcionarios' ? 'active' : '' ?>">
    <a href="<?= base_url('Funcionario') ?>">
        <i class="material-icons">person</i>
        <span>Funcionários</span>
    </a>
</li>

<li class="<?= $menu_selecionado == 'chamados' ? 'active' : '' ?>">
    <a href="<?= base_url('ChamadoTelefonico') ?>">
        <i class="material-icons">person</i>
        <span>Chamados</span>
    </a>
</li>

<li>
    <a href="javascript:void(0);" class="menu-toggle <?= $menu_selecionado == 'status os' ? 'toggled' : '' ?>">
        <i class="material-icons">widgets</i>
        <span>Ordem de Serviço</span>
    </a>
    <ul class="ml-menu">

        <li>
            <a href="<?= base_url("OSAssistencia") ?>">O.S. Assistência</a>
        </li>
        <li>
            <a href="<?= base_url("OSSuporte") ?>">O.S. Suporte</a>
        </li>

    </ul>
</li>

<li>
    <a href="javascript:void(0);" class="menu-toggle <?= $menu_selecionado == 'status os' ? 'toggled' : '' ?>">
        <i class="material-icons">widgets</i>
        <span>Outros</span>
    </a>
    <ul class="ml-menu">
        <li>
            <a href="<?= base_url("Atividade") ?>">Serviços</a>
        </li>

        <li>
            <a href="<?= base_url("Equipamento") ?>">Produtos</a>
        </li>

        <li>
            <a href="<?= base_url("StatusOS") ?>">Status OS</a>
        </li>
        
        <li>
            <a href="<?= base_url("Sistema") ?>">Sistemas</a>
        </li>
    </ul>
    
    <br><br>
</li>

</ul>
</div>
<!-- #Menu -->
</aside>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2 style="text-transform: uppercase "><?= $titulo ?></h2>
        </div>

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header" style="text-transform: uppercase">
                        <?= isset($titulo2) ? $titulo2 : '' ?>
                    </div> 



