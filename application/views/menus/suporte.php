<li class="<?= $menu_selecionado == 'inicio' ? 'active' : '' ?>">
    <a href="<?= base_url() ?>">
        <i class="material-icons">home</i>
        <span>Início</span>
    </a>
</li>
<li class="<?= $menu_selecionado == 'agendamento' ? 'active' : '' ?>">
    <a href="<?= base_url("Agendamento") ?>">
        <i class="material-icons">date_range</i>
        <span>Agenda</span>
    </a>
</li>
<li class="<?= $menu_selecionado == 'clientes juridicos' ? 'active' : '' ?>">
    <a href="<?= base_url('ClienteJuridico') ?>">
        <i class="material-icons">person</i>
        <span>Clientes Jurídicos</span>
    </a>
</li>
<li class="<?= $menu_selecionado == 'chamados' ? 'active' : '' ?>">
    <a href="<?= base_url('ChamadoTelefonico') ?>">
        <i class="material-icons">person</i>
        <span>Chamados</span>
    </a>
</li>

<li class="<?= $menu_selecionado == 'OS-Suporte' ? 'active' : '' ?>">
    <a href="<?= base_url("OSSuporte") ?>">
        <i class="material-icons">featured_play_list</i>
        <span>O.S. Suporte</span>
    </a>
</li>




</ul>
</div>
<!-- #Menu -->
</aside>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2 style="text-transform: uppercase "><?= $titulo ?></h2>
        </div>

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header" style="text-transform: uppercase">
                        <?= isset($titulo2) ? $titulo2 : '' ?>
                    </div> 


                    
                    



