
<form method="POST" action="<?= base_url('Atividade/cadastrar') ?>">
    <div class="body">
        <div class="row">
            <div class="col-md-12">
                <a href="<?= base_url('Atividade') ?>" class="btn btn-primary" title="voltar">
                    <i class="material-icons">arrow_back</i> Voltar
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label>Nome</label>
                <input type="text" onkeypress="mascara(this, soLetras)" class="form-control" autofocus="" required="" name="nome_atividade" value="<?= set_value('nome_atividade') ?>">
                <span class="help-block" style="color: red"><?= form_error('nome_atividade') ?></span>
            </div>

            <div class="col-md-12">
                <label>Valor(R$)</label>
                <input type="text" class="form-control mascara-dinheiro" name="valor_atividade" value="<?= set_value('valor_atividade') ?>">
                <span class="help-block" style="color: red"><?= form_error('valor_atividade') ?></span>
            </div>

        </div>

        <div class="row">
            <div class="col-md-12" style="text-align: right">
                 <a href="<?= base_url('Atividade') ?>" class="btn btn-warning btn-lg">Cancelar</a>
                <button type="submit" class="btn btn-success btn-lg">Cadastrar</button>
            </div>
        </div>

    </div>
</form>


<div id="msm-sucesso" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/sucesso.png') ?>"></h4>
            </div>
            <div class="modal-body">

                <p class="msg-modal text-success text-center"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="voltarListaAtiv()">Ok</button>
            </div>
        </div>

    </div>
</div>



<div id="msg-erro" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/erro.png') ?>"></h4>
            </div>
            <div class="modal-body">
                <p class="msg-modal text-danger text-center">

                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="voltarListaAtiv()">Ok</button>
            </div>
        </div>

    </div>
</div>


<script src="<?= base_url('assets/js/requisicoes/atividade.js') ?>" type="text/javascript"></script>

<?php if (isset($msg)): ?>
    <script>
             abrirMsmCadastro("<?= $msg ?>", "<?= $cadastrou == true ? "sim" : "nao" ?>");
    </script>
<?php endif; ?>

