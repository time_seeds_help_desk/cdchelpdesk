
<form method="POST" action="<?= base_url('Atividade/editar') ?>">
    <input type="hidden" name="id_atividade" value="<?= $atividade->id_atividade ?>">
    <div class="body">
        <div class="row">
            <div class="col-md-12">
                <a href="<?= base_url('Atividade') ?>" class="btn btn-primary" title="voltar">
                    <i class="material-icons">arrow_back</i> Voltar
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label>Nome</label>
                <input type="text" onkeypress="mascara(this, soLetras)" class="form-control" autofocus="" required="" name="nome_atividade" value="<?= $atividade->nome_atividade ?>">
            </div>

            <div class="col-md-12">
                <label>Valor(R$)</label>
                <input type="text" class="form-control mascara-dinheiro" name="valor_atividade" value="<?= converte_valor_usuario($atividade->valor_atividade)?>" required="">
            </div>

        </div>

        <div class="row">
            <div class="col-md-12" style="text-align: right">
                <a href="<?= base_url('Atividade') ?>" class="btn btn-warning btn-lg">Cancelar</a>
                <button type="submit" class="btn btn-success btn-lg">Alterar</button>
            </div>
        </div>

    </div>
</form>




