<div class="body">
    <div class="row">
        <div class="col-lg-6">
            <a href="<?= base_url('Atividade/pagina_cadastro') ?>" class="btn btn-primary"><i class="material-icons">add</i> Nova Atividade</a>
        </div>

        <div class="col-lg-6">
            <form action="<?= base_url('Atividade/buscar') ?>" method="post">
                <div class="col-md-9">
                    <input name="nome_atividade" onkeypress="mascara(this, soLetras)" type="text" class="form-control" placeholder="Buscar por nome" value="<?= $busca ?>">
                </div>
                <button class="btn btn-primary" style="height: 33px">BUSCAR</button>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nome</th>
                        <th>valor</th>
                        <th>Opçoes</th>
                    </tr>
                </thead>

                <tbody>

                    <?php
                    $count = 1;
                    foreach ($atividades as $atividade):
                        ?>
                        <tr>
                            <td><?= $count ?></td>
                            <td><?= $atividade->nome_atividade ?></td>
                            <td>R$ <?= converte_valor_usuario($atividade->valor_atividade) ?></td>
                            <td>
                                <a href="<?= base_url('Atividade/pagina_editar/' . $atividade->id_atividade) ?>" class="btn btn-primary">Editar</a>

                                <a href="javascript:void(0)" onclick="abrirConfirmaExcluir(<?= $atividade->id_atividade ?>, '<?= $atividade->nome_atividade ?>')" class="btn btn-danger">Excluir</a>
                            </td>
                        </tr> 
                        <?php
                        $count++;
                    endforeach;
                    ?>

                </tbody>
            </table>

            <br>
            <br>

            <div class="row">
                <div class="col-md-12">
                    <?= $tem_pagina == true ? $paginacao : "" ?>
                </div>
            </div>

            <?php if (count($atividades) < 1): ?>
                <center>
                    <h1 style="font-size: 30px;" class="text-primary">
                        Nenhuma atividade <span style="color:#000"><?=$busca?></span> encontrada!
                    </h1>
                </center>
            <?php endif; ?>

        </div>
    </div>

</div>


<div id="excluir-atividade" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/atencao.png') ?>"></h4>
            </div>
            <div class="modal-body">
                <p class="msg-modal text-center"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="deletarAtividade()">Excluir</button>
            </div>
        </div>

    </div>
</div>


<div id="msm-sucesso" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/sucesso.png') ?>"></h4>
            </div>
            <div class="modal-body">

                <p class="msg-modal text-success text-center"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="location.reload()">Ok</button>
            </div>
        </div>

    </div>
</div>


<div id="msg-erro" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/erro.png') ?>"></h4>
            </div>
            <div class="modal-body">
                <p class="msg-modal text-danger text-center">

                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="location.reload()">Ok</button>
            </div>
        </div>

    </div>
</div>
<script src="<?= base_url('assets/js/requisicoes/atividade.js') ?>" type="text/javascript"></script>



