<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>CDC Tecnologia - Erro 404</title>

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?= base_url('assets/template/plugins/bootstrap/css/bootstrap.css')?>" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?= base_url('assets/template/plugins/node-waves/waves.css')?>" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?= base_url('assets/template/css/style.css')?>" rel="stylesheet">
</head>

<body class="four-zero-four" style="background-color: #00bcd4">
    <div class="four-zero-four-container">
        <div class="error-code" style="color: #fff">404</div>
        <div class="error-message text-lighten-1" style="color: #ffff66">Essa página não existe!</div>
        <div class="button-place">
            <a href="<?= base_url()?>" class="btn btn-lg bg-pink waves-effect">Voltar Para o Início</a>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="<?= base_url('assets/template/plugins/jquery/jquery.min.js')?>"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?= base_url('assets/template/plugins/bootstrap/js/bootstrap.js')?>"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?= base_url('assets/template/plugins/node-waves/waves.js')?>"></script>
</body>

</html>
