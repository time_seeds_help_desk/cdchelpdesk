<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title>CDC Tecnologia - HelpDesk</title>
        <!-- Favicon-->
        <link rel="icon" href="../../favicon.ico" type="image/x-icon">
        <script>
            var base_url = "<?= base_url() ?>";
        </script>

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

        <!-- Bootstrap Core Css -->
        <link href="<?= base_url('assets/template/plugins/bootstrap/css/bootstrap.css') ?>" rel="stylesheet">

        <!-- Waves Effect Css -->
        <link href="<?= base_url('assets/template/plugins/node-waves/waves.css') ?>" rel="stylesheet" />

        <!-- Animation Css -->
        <link href="<?= base_url('assets/template/plugins/animate-css/animate.css') ?>" rel="stylesheet" />

        <!-- Custom Css -->
        <link href="<?= base_url('assets/template/css/style.css" rel="stylesheet') ?>">

        <link rel="stylesheet" href="<?= base_url('assets/css/estilo.css') ?>">
    </head>

    <body class="login-page">
        <div class="login-box">
            <div class="logo">
                <a href="javascript:void(0);"><b>LOGIN</b></a>
                <small>CDC Tecnologia</small>
            </div>
            <div class="card">
                <div class="body">
                    <form id="form-login" action="<?= base_url('login/entrar') ?>" method="POST">
                        <div class="msg">Faça login para iniciar sua sessão</div>

                        <div class="row">
                            <div class="col-md-12 text-lighten-1">
                                <div id="texto-erro" class="bg-red no-block" style="padding: 10px 0; text-align: center">
                                    Texto erro
                                </div>
                            </div>
                        </div>

                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">person</i>
                            </span>
                            <div class="form-line">
                                <input autocomplete="off" type="email" id="email" class="form-control" name="email" placeholder="Usuario" autofocus autocomplete="off">
                            </div>
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">lock</i>
                            </span>
                            <div class="form-line">
                                <input type="password" id="senha" class="form-control" name="senha" placeholder="senha">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 img-responsive">
                                <img id="img-captch" src="<?= base_url('login/gerar_captcha') ?>" style="width: 100%; height: auto">
                               
                                <a id="bnt-captch" href="<?= base_url('login')?>" class="btn bg-red waves-effect btn-circle" title="Recarregar">
                                    <i class="material-icons">loop</i>
                                </a>
                                
                                <div class="input-group">
                                    <div class="form-line">
                                        <input autocomplete="off" type="text" id="codigo" class="form-control" name="codigo" placeholder="Digite o Código de Segurança">
                                    </div>
                                </div>

                            </div>

                        </div>


                        <div class="row">
                            <div class="col-xs-12">
                                <button class="btn btn-block bg-pink waves-effect" type="submit">ENTRAR</button>
                            </div>
                        </div>
                        <div class="row m-t-15 m-b--20">
                            <div class="col-xs-6">
                            </div>
                            <div class="col-xs-6 align-right">
                                <a href="<?= base_url('Login/pagina_recuperar_senha')?>">Recuperar Senha</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>



        <!-- Jquery Core Js -->
        <script src="<?= base_url('assets/template/plugins/jquery/jquery.min.js') ?>"></script>

        <script src="<?= base_url('assets/js/requisicoes/login.js') ?>"></script>

        <!-- Bootstrap Core Js -->
        <script src="<?= base_url('assets/template/plugins/bootstrap/js/bootstrap.js') ?>"></script>

        <!-- Waves Effect Plugin Js -->
        <script src="<?= base_url('assets/template/plugins/node-waves/waves.js') ?>"></script>

        <!-- Validation Plugin Js -->
        <script src="<?= base_url('assets/template/plugins/jquery-validation/jquery.validate.js') ?>"></script>

        <!-- Custom Js -->
        <script src="<?= base_url('assets/template/js/admin.js') ?>"></script>
        <script src="<?= base_url('assets/template/js/pages/examples/sign-in.js') ?>"></script>
    </body>

</html>

