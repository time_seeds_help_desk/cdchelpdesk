<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Ordem de Serviço da assistência.pdf</title>

        <style>
            *{
                margin: 0;
                padding: 0;
            }

            body{
                margin: 30px 60px;
            }

            #cabecalho{
                text-align: center;
            }
            #cabecalho img{
                width: 200px;
                height: auto;
            }

            hr{
                margin: 15px 0 0 0;
                background-color:  #003162;
            }

            h1{
                font-size: 20pt;
                font-weight: bold;
                color: #003162;
            }

            h2{
                font-size: 15pt;
                font-weight: 300;
            }

            table{
                margin-top: 20px;
                width: 100%;
                border-collapse:collapse
            }

            table th{
                text-align: center;
                background-color: #98bcc8;
            }

            table td, table th{
                padding: 2px 4px;
            }

            table td, table tr, table th{
                border: 1px solid #000;
                border-collapse: collapse;
            }

            .assinaturas{
                margin: 20px 0;
            }

            .assinaturas div{
                border-top: 1px solid #000;
                width: 60%;
                margin: 40px auto;
                text-align: center;                
            }

        </style>
    </head>

    <body>
        <div id="cabecalho">
            <img src="assets/img/logo_cdc.png" alt=""/>
            <!--<h1>CDC Tecnologia</h1>-->
            <h2>Ordem de Serviço - Suporte Técnico</h2>
        </div>
        <hr>

        <br><br>
        <strong>Cliente:</strong> <?= $ordem_de_servico[0]->nome_cliente ?><br>
        <strong>CPF/CNPJ:</strong> <?= $ordem_de_servico[0]->cpf_cnpj_cliente ?><br>
        <strong>Telefone:</strong>  <?= $ordem_de_servico[0]->telefone_cliente ?><br>
        <strong>E-mail:</strong>  <?= $ordem_de_servico[0]->email_cliente ?> <br>
        <strong>Endereço:</strong>  
        <?=
        $ordem_de_servico[0]->rua_cliente . ", "
        . $ordem_de_servico[0]->numero_cliente . ', '
        . $ordem_de_servico[0]->bairro_cliente . ', '
        . $ordem_de_servico[0]->nome_cidade . ' - '
        . $ordem_de_servico[0]->nome_estado
        ?> <br>

        <!--<table>
            <thead>
                <tr>
                    <th colspan="4">Dados do Cliente</th>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td><strong>Nome:</strong> <? $ordem_de_servico[0]->nome_cliente ?> </td>
                    <td><strong>CPF/CNPJ:</strong> <? $ordem_de_servico[0]->cpf_cnpj_cliente ?> </td>
                    <td><strong>Telefone:</strong>  <? $ordem_de_servico[0]->telefone_cliente ?> </td>
                    <td><strong>E-mail:</strong>  <? $ordem_de_servico[0]->email_cliente ?> </td>
                </tr>
            </tbody>
        </table>-->



        <table>
            <thead>
                <tr>
                    <th>Informação da Ordem de Serviço</th>
                </tr>
            </thead>

            <tbody>

                <tr>
                    <td><strong>Responsável pela abertura:</strong> <?= $ordem_de_servico[0]->nome_responsavel ?></td>
                                    
                </tr>
                <tr>
                    <td ><strong>Técnico Responsável:</strong> <?= $ordem_de_servico[0]->nome_tecnico ?></td>    
                </tr>

                <tr>
                    <td><strong>Data de Abertura:</strong> <?= date("d/m/Y", strtotime($ordem_de_servico[0]->data_abertura)) ?></td>               
                </tr>

                <tr>
                    <td><strong>Descrição do Problema:</strong> <?= $ordem_de_servico[0]->descricao_problema ?> </td>
                </tr>
            </tbody>
        </table>
        
        <table>
            <thead>
                <tr>
                    <th colspan="3">Informação do Sistema</th>
                </tr>
            </thead>

            <tbody>

                <tr>
                    <td colspan="2"><strong>Sistema:</strong> <?= $ordem_de_servico[0]->nome_sistema?></td>
                    <td colspan="1"><strong>Módulo:</strong> <?= $ordem_de_servico[0]->nome_modulo_sistema ?></td>                    
                </tr>

            </tbody>
        </table>

        <div class="assinaturas">
            <div class="divisor">Assinatura do Cliente</div>
            <div class="divisor">Assinatura do Técnico</div>            
        </div>
    </body>
</html>
