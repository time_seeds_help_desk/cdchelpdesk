<?php
$endereco_arquivo = $ordem_de_servico[0]->anexo;
$extensao_arquivo;
$imagem;
$nome_arquivo = '';

if ($endereco_arquivo != "") {
    $extensao_arquivo = explode('.', $endereco_arquivo)[1];

    $imagem = 'image';

    if ($extensao_arquivo === 'pdf') {
        $imagem = 'picture_as_pdf';
    }

    $endereco_arquivo = explode('/', $endereco_arquivo);

    $nome_arquivo = $endereco_arquivo[3];
}
?>


<div id="arquivo-anexo" class="text-center">
    <?php if ($endereco_arquivo != "") { ?>
        <a  href="<?= base_url($ordem_de_servico[0]->anexo) ?>" target="_blank">
            <i class="material-icons"><?= $imagem ?></i>
            <br>
            <?= $nome_arquivo ?>
        </a>
    <?php } else { ?>
        <center>
            <h1 class="text-primary">
                Nenhum anexo adicionado
            </h1>
        </center>
    <?php } ?>
</div>


<!--<i class="material-icons">picture_as_pdf</i>-->