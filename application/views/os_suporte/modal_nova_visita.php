<div id="modal-nova-visita" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4>Nova Visita Técnica</h4>
            </div>
            <div class="modal-body">
                <form id="cadastrar_agendamento" method="post" action="<?= base_url('Agendamento/cadastrar') ?>">
                    <div class="row">
                        <input type="hidden" name="id_ordem_de_servico" value="<?= $ordem_de_servico[0]->id_ordem_de_servico?>">
                        <input type="hidden" name="id_pessoa_cliente" value="<?= $ordem_de_servico[0]->cliente_id?>">
                        <div class='col-sm-6'>
                            <label>Data</label>
                            <input type="date" name="data" class="form-control" required="">
                        </div>

                        <div class='col-sm-6'>
                            <label>Horário</label>
                            <input type="time" name="horario" class="form-control" required="">
                        </div>

                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6"></div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <button class="btn btn-warning btn-block" type="reset">Cancelar</button>
                                        </div>
                                        <div class="col-md-6">
                                            <button class="btn btn-success btn-block" type="submit">Salvar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>                      
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


