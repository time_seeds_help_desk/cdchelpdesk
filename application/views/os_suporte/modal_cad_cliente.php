
<div id="cad-cliente" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="modal-title text-center" id="titulo-cad-cliente">Cadastro de Cliente Jurídico</h2>
            </div>

            <!--=================CLIENTE JURÍDICO=================-->
            <form id="cad-cliente-juridico">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12" style="color: red">
                            Campos com asterísco (*) são obrigatórios
                            <br>
                            <br>
                        </div>
                        <div class="col-md-6">
                            <label>Nome Fantasia<span> *</span></label>
                            <input autocomplete="off" type="text" class="form-control" required=""  name="nome_nm_fantasia">
                            <br>
                        </div>

                        <div class="col-md-6">
                            <label>Razão Social<span> *</span></label>
                            <input autocomplete="off" type="text" class="form-control" required=""  name="razao_social">
                            <br>
                        </div>

                        <div class="col-md-6">
                            <label>CNPJ<span> *</span></label>
                            <input autocomplete="off" type="text" class="form-control mascara-cnpj" required=""  name="cpf_cnpj">
                            <br>
                        </div>

                        <div class="col-md-6">
                            <label>Responsável<span> *</span></label>
                            <input autocomplete="off" type="text" onkeypress="mascara(this, soLetras)" class="form-control" autofocus=""  required="" name="responsavel">
                            <br>
                        </div>


                        <div class="col-md-6">
                            <label>Telefone<span> *</span></label>
                            <input autocomplete="off" type="text" class="form-control mascara-telefone" name="telefone">
                            <br>
                        </div>

                        <div class="col-md-6">
                            <label>Rua<span> *</span></label>
                            <input autocomplete="off" type="text" onkeypress="mascara(this, soLetrasNum)" class="form-control" required=""  name="rua" value="<?= set_value('rua') ?>">
                            <br>
                        </div>

                        <div class="col-md-2">
                            <label>Número<span> *</span></label>
                            <input autocomplete="off" type="text" class="form-control" required=""  name="numero">
                            <br>
                        </div>

                        <div class="col-md-5">
                            <label>Bairro<span> *</span></label>
                            <input autocomplete="off" type="text" onkeypress="mascara(this, soLetrasNum)" class="form-control" required=""  name="bairro">
                            <br>
                        </div>

                        <div class="col-md-2">
                            <label>CEP<span> *</span></label>
                            <input autocomplete="off" type="text" class="form-control mascara-cep" required=""  name="cep">
                            <br>
                        </div>

                        <div class="col-md-3">
                            <label>Complemento</label>
                            <input autocomplete="off" type="text" class="form-control" onkeypress="mascara(this, soLetrasNum)" name="complemento">
                            <br>
                        </div>

                        <div class="col-sm-6">
                            <label>Estado<span> *</span></label>
                            <select class="form-control sigla_estado" name="sigla_estado" id="cj_sigla_estado" required="" >
                                <option value="" selected="">Selecione</option>
                                <option value="1">teste</option>
                            </select>
                            <br>
                        </div>

                        <div class="col-sm-6">
                            <label>Cidade<span> *</span></label>
                            <select class="form-control id_cidade" name="id_cidade" required="" >
                                <option value="" selected="">Selecione</option>
                                <option value="1">teste</option>
                            </select>
                            <br>
                        </div>

                        <div class="col-md-12">
                            <label>E-mail<span> *</span></label>
                            <input autocomplete="off" type="email" class="form-control"  name="email" required="" >
                            <br>
                        </div>

                        <div class="col-md-3" style="display: none">
                            <label>Senha<span> *</span></label>
                            <input autocomplete="off" type="password" class="form-control" name="senha">
                            <br>
                        </div>

                        <div class="col-md-3" style="display: none">
                            <label>Confirmar Senha<span> *</span></label>
                            <input autocomplete="off" type="password" class="form-control" name="confirma_senha">
                            <br>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reser" class="btn btn-warning" data-dismiss="modal"><i class="material-icons">cancel</i> Cancelar</button>
                    <button type="submit" class="btn btn-success"><i class="material-icons">sd_storage</i> Cadastrar</button>
                </div>
            </form>

            <!--=================CLIENTE JURÍDICO==================-->
        </div>
    </div>
</div>


<div id="msm-sucesso-cad-cli" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" onclick="voltarListaOSAssis()">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/sucesso.png') ?>"></h4>
            </div>
            <div class="modal-body">

                <p class="msg-modal text-success text-center"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
            </div>
        </div>

    </div>
</div>



<div id="msg-erro-cad-cli" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/erro.png') ?>"></h4>
            </div>
            <div class="modal-body">
                <p class="msg-modal text-danger text-center">

                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
            </div>
        </div>

    </div>
</div>

<script>

    $(document).ready(function () {
        /*
         * busca de estados
         */
        $.ajax({
            url: base_url + "Estado/buscar_todos",
            method: "POST",
            data: {},
            contentType: false,
            cache: false,
            processData: false,
            dataType: "json"
        }).done(function (resposta) {
            if (resposta.status == 'sucesso') {
                console.log(resposta.estados);
                html = "<option value=\"\">Selecione</option>";

                for (i = 0; i < resposta.estados.length; i++) {
                    html += "<option value=\"" + resposta.estados[i].sigla + "\">" + resposta.estados[i].nome_estado + "</option>";
                }

                $('#cf_sigla_estado').html(html);
                $('#cj_sigla_estado').html(html);
            } else {
                $('.msg-modal').html(resposta.msg);
                $('#msg-erro').modal();
            }

        }).fail(function (jqXHR, textStatus) {
            $('.msg-modal').html("<h3>Erro 500</h3>Falha interna no servidor.<br>Não conseguimos carregar algumas informações.");
            $('#msg-erro').modal();
        });
        //fim de busca de estados


        $(".sigla_estado").change(function (event) {
            estado = $(this).val();
            $.ajax({
                method: "POST",
                url: base_url + "Cidade/buscar_por_sigla/",
                data: {sigla_estado: estado},
                dataType: "json"
            }).done(function (resposta) {
                cidades = resposta.cidades;
                html = "<option value=\"\">Selecione a Cidade</option>";


                for (var i = 0; i < cidades.length; i++) {
                    html += "<option value=\"" + cidades[i].id_cidade + "\">" + cidades[i].nome_cidade + "</option>";
                }

                $(".id_cidade").html(html);
            }).fail(function (jqXHR, textStatus) {
                $('.msg-modal').html("<h3>Erro 500</h3>Falha interna no servidor.<br>Não conseguimos carregar algumas informações.");
                $('#msg-erro').modal();
            });
            event.preventDefault();
        });

        /*
         * Cadastro de cliente Jurídico
         */
        $('#cad-cliente-juridico').submit(function (e) {
            e.preventDefault();
            $('#pelicula').show();
            $.ajax({
                url: base_url + "ClienteJuridico/cadastrar_ajax",
                method: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                dataType: "json"
            }).done(function (resposta) {
                $('#pelicula').hide();
                if (resposta.status == 'sucesso') {
                    $('.msg-modal').html(resposta.msg);
                    $('#msm-sucesso-cad-cli').modal();
                    $('#cad-cliente-juridico')[0].reset();
                    $('#cad-cliente').modal('hide');
                    $(".id_cidade").html("<option value=\"\">Selecione a Cidade</option>");
                } else {
                    $('.msg-modal').html(resposta.msg);
                    $('#msg-erro').modal();
                }

            }).fail(function (jqXHR, textStatus) {
                $('#pelicula').hide();

                $('.msg-modal').html("<h3>Erro 500</h3>Falha interna no servidor");
                $('#msg-erro').modal();
            });
        });
        //fim de cadastro de cliente Jurídico
    });

</script>
