<script>
    var idOrdemDeServico = '<?= $ordem_de_servico[0]->id_ordem_de_servico ?>';
</script>
<script src="<?= base_url('assets/js/requisicoes/os_suporte.js') ?>" type="text/javascript"></script>


<div class="body">
    <div class="row">
        <div class="col-md-2">
            <a href="<?= base_url('OSSuporte') ?>" class="btn btn-primary btn-block">
                <i class="material-icons">arrow_back</i> Voltar
            </a>
        </div>
        <div class="col-md-8">
            <ul class="abas">
                <li class="selecionada" pagina="informacoes">
                    <a>Informações da O.S.</a>
                </li>

                <li pagina="laudo">
                    <a>Laudo Técnico</a>      
                </li>

                <li pagina="anexo">
                    <a>Anexo</a>
                </li>
            </ul>
        </div>

        <div class="col-md-2">

        </div>
    </div>

    <div class="conteudo-abas">
        <div class="opcao ativo" id="informacoes">
            <!--================FORMULÁRIO ALTERAR==========================-->
            <form id="form-editar-os" method="post" action="<?= base_url('OSSuporte/editar') ?>">
                <input type="hidden" name="id_ordem_de_servico" id="id_ordem_de_servico" value="<?= $ordem_de_servico[0]->id_ordem_de_servico ?>">
                <input type="hidden" name="fl_enviar_sms" id="fl_enviar_sms" value="<?= $ordem_de_servico[0]->fl_enviar_sms != 0 ? "true" : "false" ?>">
                <input type="hidden" name="fl_enviar_email" id="fl_enviar_email" value="<?= $ordem_de_servico[0]->fl_enviar_email != 0 ? "true" : "false" ?>">
                <input type="hidden" name="id_cliente" id="id_cliente" value="<?= $ordem_de_servico[0]->id_cliente ?>">
                <input type="hidden" name="protocolo" id="protocolo" value="<?= $ordem_de_servico[0]->protocolo ?>">
                <div class="row">
                    <div class="col-md-12" style="color: red">
                        Campos com asterísco (*) são obrigatórios
                    </div>
                    <div class="col-md-4">
                        <label>Protocolo</label>
                        <input type="text" class="form-control" disabled="" style="background-color: #efefef" value="<?= $ordem_de_servico[0]->protocolo ?>">
                    </div>

                    <div class="col-md-4">
                        <label>Usuário</label>
                        <input type="text" class="form-control" disabled="" value="<?= $ordem_de_servico[0]->nome_responsavel ?>" style="background-color: #efefef">
                    </div>

                    <div class="col-md-4">
                        <label>Técnico</label>
                        <input type="text" class="form-control" disabled="" value="<?= $ordem_de_servico[0]->nome_tecnico ?>" style="background-color: #efefef">
                    </div>

                    <div class="col-md-4">
                        <label>Data de Abertura</label>
                        <input type="text" class="form-control" value="<?= formatar_data($ordem_de_servico[0]->data_abertura) ?>" disabled="" style="background-color: #efefef">
                    </div>

                    <div class="col-md-4">
                        <label>Status</label>
                        <select class="form-control" name="id_status_os" id="id_status" required="">
                            <option value="">Selecione</option>
                            <?php foreach ($status_os as $s): ?>
                                <option value="<?= $s->id_status_os ?>" <?= $ordem_de_servico[0]->id_status == $s->id_status_os ? 'selected' : '' ?>><?= $s->nome_status_os ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="col-md-4">
                        <label>Setor</label>
                        <input type="text" class="form-control" disabled="" value="Assistência Técnica" style="background-color: #efefef">
                    </div>

                    <div class="col-md-4">
                        <label class="col-12">Cliente</label>
                        <input type="text" class="form-control" disabled="" value="<?= $ordem_de_servico[0]->nome_cliente ?>" style="background-color: #efefef">
                    </div>
                </div>

                <div class="row">
                    <fieldset class="col-md-12">
                        <legend class="text-primary">Informações do Sistema</legend>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Sistema</label>
                                <input type="text" class="form-control" disabled="" value="<?= $ordem_de_servico[0]->nome_sistema ?>" style="background-color: #efefef">
                            </div>     

                            <div class="col-md-6">
                                <label>Módulo</label>
                                <input type="text" class="form-control" disabled="" value="<?= $ordem_de_servico[0]->nome_modulo_sistema ?>" style="background-color: #efefef">
                            </div> 

                            <div class="col-md-6">
                                <label>Versão</label>
                                <input type="text" class="form-control" disabled="" value="<?= $ordem_de_servico[0]->versao ?>" style="background-color: #efefef">
                            </div> 
                        </div>
                    </fieldset>
                </div>


                <div class="row">
                    <fieldset class="col-md-12">
                        <legend class="text-primary">Problema</legend>
                        <div class="row">
                            <div class="col-md-12">
                                <label >Descrição do Problema</label>
                                <textarea required="" class="form-control" rows="5" disabled="" style="background-color: #efefef"><?= $ordem_de_servico[0]->descricao_problema ?></textarea>
                            </div>     
                        </div>
                    </fieldset>
                </div>

                <div class="row">
                    <div class="col-md-6" style="text-align: right">

                    </div>

                    <div class="col-md-6" style="text-align: right">
                        <a href="<?= base_url('OSSuporte') ?>" class="btn btn-warning"> <i class="material-icons">cancel</i> Cancelar
                        </a>
                        <button type="submit" class="btn btn-primary"><i class="material-icons">sd_storage</i> Alterar
                        </button>
                    </div>
                </div>
            </form>
            <!--================FIM FORMULÁRIO ALTERAR==========================-->


        </div>

        <div class="opcao" id="laudo">
            <?php
            $this->load->view('os_suporte/abrir_laudo_tecnico', array('ordem_de_servico' => $ordem_de_servico));
            ?>
        </div>

        <div class="opcao" id="anexo"> 
             <?php
            $this->load->view('os_suporte/abrir_anexo', array('ordem_de_servico' => $ordem_de_servico));
            ?>
        </div>
    </div>
</div>

<div id="msm-sucesso" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/sucesso.png') ?>"></h4>
            </div>
            <div class="modal-body">
                <p class="msg-modal text-success text-center"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
            </div>
        </div>

    </div>
</div>



<div id="msg-erro" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/erro.png') ?>"></h4>
            </div>
            <div class="modal-body">
                <p class="msg-modal text-danger text-center">

                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
            </div>
        </div>

    </div>
</div>




