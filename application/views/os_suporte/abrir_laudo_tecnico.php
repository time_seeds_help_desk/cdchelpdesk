<div class="row">
    <div class="col-md-4">
        <strong>Protocolo:</strong> <?= $ordem_de_servico[0]->protocolo ?>
    </div>
    <div class="col-md-4">
        <strong>Sistema:</strong> <?= $ordem_de_servico[0]->nome_sistema ?>
    </div>
    <div class="col-md-4">
        <strong>Módulo:</strong> <?= $ordem_de_servico[0]->nome_modulo_sistema ?>
    </div>
</div>

<fieldset>
    <legend class="text-primary">Visita Técnica</legend>
    <div class="row">
        <div class="col-md-12">
            <button class="btn btn-primary"  data-toggle="modal" data-target="#modal-nova-visita"><i class="material-icons">add</i> Nova Vísita</button>&nbsp;&nbsp;
            <button class="btn btn-primary" id="abrir-modal-agenda"><i class="material-icons">date_range</i> Consultar Agenda</button>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 table-responsive" >
            <table class="table table-hover" id="tbl-agendamentos-os">
                <thead>
                    <tr style="background-color: #222; color: #fff">
                        <th>Data</th>
                        <th>Horário</th>
                        <th>Status</th>
                    </tr>
                </thead>

                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</fieldset>

<fieldset>
    <legend class="text-primary">Informações Técnicas</legend>
    <div class="row">
        <!--<div class="col-md-6">
            <label>Descrição do Problema</label>
            <textarea class="form-control" placeholder="Descreva o problema encontrado após sua análise"></textarea>
        </div>-->

        <form id="adicionar-laudo"  action="<?= base_url('OSSuporte/adicionar_laudo') ?>" method="post">
            <input type="hidden" name="id_ordem_de_servico" id="id_ordem_de_servico" value="<?= $ordem_de_servico[0]->id_ordem_de_servico ?>">
            <div class="col-md-6">
                <label>Laudo Técnico<span> *</span></label>
                <textarea name="laudo_tecnico" class="form-control" placeholder="Descreva o problema encontrado" rows="4" maxlength="400"><?= $ordem_de_servico[0]->laudo_tecnico ?></textarea>
            </div>

            <div class="col-md-6">
                <label>Solução<span> *</span></label>
                <textarea name="solucao_tecnica" class="form-control" placeholder="Descreva a solução encontrada para o problema" rows="4" maxlength="400"><?= $ordem_de_servico[0]->solucao_tecnica ?></textarea>
            </div>

            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6"></div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6"><button type="reset" class="btn btn-warning btn-block"><i class="material-icons">cancel</i> Cancelar</button></div>
                            <div class="col-md-6"><button class="btn btn-success btn-block"><i class="material-icons">sd_storage</i> Salvar</button></div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</fieldset>

<?php 
$this->load->view('os_suporte/modal_nova_visita');
$this->load->view('os_suporte/modal_agenda');
?>

