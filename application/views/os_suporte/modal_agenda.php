<div id="modal-agenda" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4>Sua Agenda - (Agendamentos Pendente)</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-12 table-responsive" style="height: 400px; border: 1px solid #000; padding: 0; overflow: auto">
                            <table class="table table-hover" id="tbl-agendamento-tecnico">
                                <thead>
                                    <tr style="background-color: #222; color: #fff">
                                        <th>Data</th>
                                        <th>Horário</th>
                                        <th>Cliente</th>
                                        <th>Endereço</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
