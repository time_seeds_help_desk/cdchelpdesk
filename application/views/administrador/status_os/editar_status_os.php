<form method="POST" action="<?= base_url('StatusOS/editar') ?>">
    <input name="id_status_os" type="hidden" value="<?=$status_os->id_status_os?>">
    <div class="body">
        <div class="row">
            <div class="col-md-12">
                <a href="<?= base_url('StatusOS') ?>" class="btn btn-primary" title="voltar">
                    <i class="material-icons">arrow_back</i> Voltar
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label>Nome</label>
                <input type="text" onkeypress="mascara(this, soLetras)" class="form-control" autofocus="" required="" name="nome_status_os" value="<?=$status_os->nome_status_os?>">
            </div>
        </div>    


        <div class="row">
            <div class="col-md-12" style="text-align: right">
                <a href="<?= base_url("StatusOS") ?>" class="btn btn-warning btn-lg">Cancelar</a>
                <button type="submit" class="btn btn-success btn-lg">Alterar</button>
            </div>
        </div>

    </div>
</form>







