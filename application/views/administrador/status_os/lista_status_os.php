<div class="body">
    <div class="row">
        <div class="col-lg-6">
            <a href="<?= base_url('StatusOS/pagina_cadastro') ?>" class="btn btn-primary"><i class="material-icons">add</i> Novo Status de OS</a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Status</th>
                        <th>Opções</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    $count = 1;
                    foreach ($status_os as $status):
                        ?>
                        <tr>
                            <td><?= $count ?></td>
                            <td><?= $status->nome_status_os ?></td>
                            <td>
                                <a href="<?= base_url('StatusOS/pagina_editar/' . $status->id_status_os) ?>" class="btn btn-primary">Editar</a>
                                <?php if ($status->fl_pode_excluir == true): ?>
                                    <a href="javascript:void(0)" onclick="abrirConfirmaExcluir(<?= $status->id_status_os ?>, '<?= $status->nome_status_os ?>')" class="btn btn-danger">Excluir</a>
                                <?php else: ?>
                                    <a href="javascript:void(0)" class="btn" style="cursor: not-allowed" title="Não é permitido excluir">Excluir</a>
                                <?php endif; ?>
                            </td>
                        </tr> 
                        <?php
                        $count++;
                    endforeach;
                    ?>


                </tbody>
            </table>

            <br>
            <br>

            <?php if (count($status_os) < 1): ?>
                <center>
                    <h1 style="font-size: 30px;" class="text-primary">
                        Nenhum status de OS cadastrado!
                    </h1>
                </center>
            <?php endif; ?>

        </div>
    </div>

</div>


<div id="excluir-status-os" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/atencao.png') ?>"></h4>
            </div>
            <div class="modal-body">
                <p class="msg-modal text-center"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="deletarStatusOs()">Excluir</button>
            </div>
        </div>

    </div>
</div>


<div id="msm-sucesso" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/sucesso.png') ?>"></h4>
            </div>
            <div class="modal-body">

                <p class="msg-modal text-success text-center"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="location.reload()">Ok</button>
            </div>
        </div>

    </div>
</div>


<div id="msg-erro" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/erro.png') ?>"></h4>
            </div>
            <div class="modal-body">
                <p class="msg-modal text-danger text-center">

                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="location.reload()">Ok</button>
            </div>
        </div>

    </div>
</div>
<script src="<?= base_url('assets/js/requisicoes/status_os.js') ?>" type="text/javascript"></script>




