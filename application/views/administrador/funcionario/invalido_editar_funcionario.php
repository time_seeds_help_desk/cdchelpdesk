
<form method="POST" action="<?= base_url('Funcionario/editar') ?>">
    <input type="hidden" name="id_pessoa" value="<?= isset($id_pessoa) ? $id_pessoa : "" ?>">
    <input type="hidden" name="cpf_cnpj" value="<?= isset($cpf_cnpj) ? $cpf_cnpj : "" ?>">
    <div class="body">
        <div class="row">
            <div class="col-md-12">
                <a href="<?= base_url('Funcionario') ?>" class="btn btn-primary" title="voltar">
                    <i class="material-icons">arrow_back</i> Voltar
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" style="color: red">
                Campos com asterísco (*) são obrigatórios
            </div>
            <div class="col-md-12">
                <label>Nome<span> *</span></label>
                <input type="text" class="form-control" autofocus="" required="" name="nome_nm_fantasia" value="<?= set_value('nome_nm_fantasia') ?>">
                <span class="help-block" style="color: red"><?= form_error('nome_nm_fantasia') ?></span>
            </div>

            <div class="col-md-6">
                <label>Telefone<span> *</span></label>
                <input type="text" class="form-control mascara-telefone" name="telefone" value="<?= set_value('telefone') ?>">
                <span class="help-block" style="color: red"><?= form_error('telefone') ?></span>
            </div>

            <div class="col-md-3">
                <label>E-mail<span> *</span></label>
                <input type="e-mail" class="form-control"  required="" name="email" value="<?= set_value('email') ?>">
                <span class="help-block with-errors" style="color: red">
                    <?= form_error('email') ?>
                    <?= isset($valid_email) ? $valid_email : ''; ?>
                </span>
            </div>

            <div class="col-sm-3">
                <label>Nível de Acesso<span> *</span></label>
                <select class="form-control" name="id_tipo_funcionario">
                    <?php foreach ($tipos_funcionario as $tipos): ?>
                        <option value="<?= $tipos->id_tipo_funcionario ?>"><?= $tipos->nome_tipo_funcionario ?></option>
                    <?php endforeach; ?>
                </select>
                <span class="help-block" style="color: red"><?= form_error('id_tipo_funcionario') ?></span>
            </div>

        </div>

        <div class="row">
            <div class="col-md-12" style="text-align: right">
                <a href="<?= base_url('Funcionario') ?>" class="btn btn-warning btn-lg">Cancelar</a>
                <button type="submit" class="btn btn-primary btn-lg">Alterar</button>
            </div>
        </div>

    </div>
</form>

<div id="msm-sucesso" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/sucesso.png') ?>"></h4>
            </div>
            <div class="modal-body">

                <p class="msg-modal text-success text-center"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="voltarListaFunc()">Ok</button>
            </div>
        </div>

    </div>
</div>



<div id="msg-erro" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/erro.png') ?>"></h4>
            </div>
            <div class="modal-body">
                <p class="msg-modal text-danger text-center">

                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="voltarListaFunc()">Ok</button>
            </div>
        </div>

    </div>
</div>

<script src="<?= base_url('assets/js/requisicoes/funcionario.js') ?>" type="text/javascript"></script>

<?php if (isset($msg)): ?>
    <script>
                    abrirMsmCadastro("<?= $msg ?>", "<?= $alterou == true ? "sim" : "nao" ?>");
    </script>
<?php endif; ?>

