
<form method="POST" action="<?= base_url('Funcionario/editar') ?>">
    <input type="hidden" name="id_pessoa" value="<?= $funcionario[0]->id_pessoa ?>">
    <input type="hidden" name="cpf_cnpj" value="<?= $funcionario[0]->cpf_cnpj ?>">
    <div class="body">
        <div class="row">
            <div class="col-md-12">
                <a href="<?= base_url('Funcionario') ?>" class="btn btn-primary" title="voltar">
                    <i class="material-icons">arrow_back</i> Voltar
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" style="color: red">
                Campos com asterísco (*) são obrigatórios
            </div>
            <div class="col-md-12">
                <label>Nome<span> *</span></label>
                <input type="text" class="form-control" autofocus="" required="" name="nome_nm_fantasia" value="<?= $funcionario[0]->nome_nm_fantasia ?>">
                <span class="help-block" style="color: red"><?= form_error('nome_nm_fantasia') ?></span>
            </div>


            <div class="col-md-6">
                <label>Telefone<span> *</span></label>
                <input type="text" class="form-control mascara-telefone" required="" name="telefone" value="<?= $funcionario[0]->telefone ?>">
            </div>

            <div class="col-md-3">
                <label>E-mail<span> *</span></label>
                <input type="e-mail" class="form-control"  required="" name="email" value="<?= $funcionario[0]->email ?>">
                <span class="help-block with-errors" style="color: red">
                    <?= form_error('email') ?>
                    <?= isset($valid_email) ? $valid_email : ''; ?>
                </span>
            </div>

            <div class="col-sm-3">

                <label>Nivel de Acesso<span> *</span></label>
                <select class="form-control" name="id_tipo_funcionario">
                    <?php foreach ($tipos_funcionario as $tipos): ?>

                        <option value="<?= $tipos->id_tipo_funcionario ?>" 
                        <?= ($tipos->id_tipo_funcionario == $funcionario[0]->id_tipo_funcionario) ? "selected=''" : '' ?>
                                ><?= $tipos->nome_tipo_funcionario ?></option>
                            <?php endforeach; ?>
                </select>
                <span class="help-block" style="color: red"><?= form_error('id_tipo_funcionario') ?></span>
            </div>

        </div>

        <div class="row">
            <div class="col-md-12" style="text-align: right">
                <a href="<?= base_url('Funcionario')?>" class="btn btn-warning btn-lg">Cancelar</a>
                <button type="submit" class="btn btn-primary btn-lg">Alterar</button>
            </div>
        </div>

    </div>
</form>

