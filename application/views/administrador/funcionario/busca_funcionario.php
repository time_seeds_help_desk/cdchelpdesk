<div class="body">
    <div class="row">
        <div class="col-lg-6">
            <a href="<?= base_url('Funcionario/pagina_cadastro') ?>" class="btn btn-primary"><i class="material-icons">add</i> Novo Funcionário</a>
        </div>

        <div class="col-lg-6">
            <form action="<?= base_url('Funcionario/buscar') ?>" method="post">
                <div class="col-md-9">
                    <input name="nome" onkeypress="mascara(this, soLetras)" type="text" class="form-control" placeholder="Buscar por nome" value="<?= $busca ?>">
                </div>
                <button class="btn btn-primary" style="height: 33px">BUSCAR</button>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nome</th>
                        <th>Telefone</th>
                        <th>E-mail</th>
                        <th>Função</th>
                        <th>Opçoes</th>
                    </tr>
                </thead>

                <tbody>

                    <?php
                    $count = 1;
                    foreach ($funcionarios as $funcionario):
                        ?>
                        <tr>
                            <td><?= $count ?></td>
                            <td><?= $funcionario->nome_nm_fantasia ?></td>
                            <td><?= $funcionario->telefone ?></td>
                            <td><?= $funcionario->email ?></td>
                            <td><?= $funcionario->nome_tipo_funcionario ?></td>
                            <td>
                                <a href="<?= base_url('Funcionario/pagina_editar/' . $funcionario->id_pessoa) ?>" class="btn btn-primary">Editar</a>
                                <a href="javascript:void(0)" onclick="abrirConfirmaExcluir(<?= $funcionario->id_pessoa ?>, '<?= $funcionario->nome_nm_fantasia ?>')" class="btn btn-danger">Excluir</a>
                            </td>
                        </tr> 
                        <?php
                        $count++;
                    endforeach;
                    ?>

                </tbody>
            </table>

            <br>
            <br>
            <?php if (count($funcionarios) < 1): ?>
                <center>
                    <h1 style="font-size: 30px;" class="text-primary">
                        O funcionários de nome <?= $busca ?> não foi encontrado!
                    </h1>
                </center>
            <?php endif; ?>

        </div>
    </div>

</div>


<div id="excluir-funcionario" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/atencao.png') ?>"></h4>
            </div>
            <div class="modal-body">
                <p class="msg-modal text-center"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="deletarFuncionario()">Excluir</button>
            </div>
        </div>

    </div>
</div>


<div id="msm-sucesso" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/sucesso.png') ?>"></h4>
            </div>
            <div class="modal-body">

                <p class="msg-modal text-success text-center"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="location.reload()">Ok</button>
            </div>
        </div>

    </div>
</div>


<div id="msg-erro" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/erro.png') ?>"></h4>
            </div>
            <div class="modal-body">
                <p class="msg-modal text-danger text-center">

                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="location.reload()">Ok</button>
            </div>
        </div>

    </div>
</div>
<script src="<?= base_url('assets/js/requisicoes/funcionario.js') ?>" type="text/javascript"></script>

