
<form method="POST" action="<?= base_url('Funcionario/cadastrar') ?>">
    <div class="body">
        <div class="row">
            <div class="col-md-12">
                <a href="<?= base_url('Funcionario') ?>" class="btn btn-primary" title="voltar">
                    <i class="material-icons">arrow_back</i> Voltar
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" style="color: red">
                Campos com asterísco (*) são obrigatórios
            </div>
            <div class="col-md-12">
                <label>Nome<span> *</span></label>
                <input type="text" onkeypress="mascara(this,soLetras)" class="form-control" autofocus="" required="" name="nome_nm_fantasia" value="<?= set_value('nome_nm_fantasia') ?>">
                <span class="help-block" style="color: red"><?= form_error('nome_nm_fantasia') ?></span>
            </div>

            <div class="col-md-12">
                <label>Telefone<span> *</span></label>
                <input type="text" class="form-control mascara-telefone" name="telefone" value="<?= set_value('telefone') ?>">
                <span class="help-block" style="color: red"><?= form_error('telefone') ?></span>
            </div>

            <div class="col-md-12">
                <label>E-mail<span> *</span></label>
                <input type="e-mail" class="form-control"  required="" name="email" value="<?= set_value('email') ?>">
                <span class="help-block with-errors" style="color: red">
                    <?= form_error('email') ?>
                    <?= isset($valid_email) ? $valid_email : ''; ?>
                </span>
            </div>

            <div class="col-md-12">
                <label>Senha<span> *</span></label>
                <input type="password" class="form-control"  required="" name="senha" value="<?= set_value('senha') ?>">
                <span class="help-block" style="color: red">
                    <?= form_error('senha') ?>
                </span>
            </div>

            <div class="col-md-12">
                <label>Confirmar Senha<span> *</span></label>
                <input type="password" class="form-control"  required="" name="confirma_senha" value="<?= set_value('confirma_senha') ?>">
                <span class="help-block" style="color: red"><?= form_error('confirma_senha') ?></span>
            </div>

            <div class="col-sm-12">
                <label>Nível de Acesso<span> *</span></label>
                <select class="form-control" name="id_tipo_funcionario">
                    <?php foreach ($tipos_funcionario as $tipos): ?>
                        <option value="<?= $tipos->id_tipo_funcionario ?>" 
                        <?php if (isset($tipo_func_selecionado)): ?>
                            <?= ($tipo_func_selecionado == $tipos->id_tipo_funcionario) ? "selected=''" : '' ?>
                        <?php endif; ?>
                                >
                                    <?= $tipos->nome_tipo_funcionario ?>
                        </option>
                    <?php endforeach; ?>
                </select>
                <span class="help-block" style="color: red"><?= form_error('id_tipo_funcionario') ?></span>
            </div>

        </div>

        <div class="row">
            <div class="col-md-12" style="text-align: right">
                <button type="reset" class="btn btn-warning btn-lg">Cancelar</button>
                <button type="submit" class="btn btn-success btn-lg">Cadastrar</button>
            </div>
        </div>

    </div>
</form>


<div id="msm-sucesso" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/sucesso.png') ?>"></h4>
            </div>
            <div class="modal-body">

                <p class="msg-modal text-success text-center"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="voltarListaFunc()">Ok</button>
            </div>
        </div>

    </div>
</div>



<div id="msg-erro" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/erro.png') ?>"></h4>
            </div>
            <div class="modal-body">
                <p class="msg-modal text-danger text-center">

                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="voltarListaFunc()">Ok</button>
            </div>
        </div>

    </div>
</div>


<script src="<?= base_url('assets/js/requisicoes/funcionario.js') ?>" type="text/javascript"></script>

<?php if (isset($msg)): ?>
    <script>
                    abrirMsmCadastro("<?= $msg ?>", "<?= $cadastrou == true ? "sim" : "nao" ?>");
    </script>
<?php endif; ?>
