<div class="body">
    <div class="row">
        <div class="col-lg-6">
            <a href="<?= base_url('ClienteFisico/pagina_cadastro') ?>" class="btn btn-primary"><i class="material-icons">add</i> Novo Cliente Físico</a>
        </div>
        <div class="col-lg-6">
            <form action="<?= base_url("ClienteFisico/buscar") ?>" method="post">
                <div class="col-md-9">
                    <input name="cpf_nome" type="text" class="form-control" onkeypress="mascara(this, soLetrasNum)" placeholder="Buscar por nome ou CPF">
                </div>
                <button class="btn btn-primary" style="height: 33px">BUSCAR</button>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nome</th>
                        <th>CPF</th>
                        <th>Telefone</th>
                        <th>E-mail</th>
                        <th>Opçoes</th>
                    </tr>
                </thead>

                <tbody>

                    <?php
                    $count = $pag_inicio;
                    foreach ($clientes_fisicos as $cliente):
                        ?>
                        <tr>
                            <td><?= $count ?></td>
                            <td><?= $cliente->nome_nm_fantasia ?></td>
                            <td><?= $cliente->cpf_cnpj ?></td>
                            <td><?= $cliente->telefone ?></td>
                            <td><?= $cliente->email ?></td>
                            <td>
                                <a href="<?= base_url('ClienteFisico/pagina_editar/' . $cliente->id_pessoa) ?>" class="btn btn-primary">Editar</a>
                                <a href="javascript:void(0)" onclick="abrirConfirmaExcluir(<?= $cliente->id_pessoa ?>, '<?= $cliente->nome_nm_fantasia ?>')" class="btn btn-danger">Excluir</a>
                            </td>
                        </tr> 
                        <?php
                        $count++;
                    endforeach;
                    ?>

                </tbody>
            </table>

            <br>
            <br>

            <div class="row">
                <div class="col-md-12">
                    <?= $tem_pagina == true ? $paginacao : "" ?>
                </div>
            </div>

            <?php if (count($clientes_fisicos) < 1): ?>
                <center>
                    <h1 style="font-size: 30px;" class="text-primary">
                        Nenhum Cliente Físico Cadastrado
                    </h1>
                </center>
            <?php endif; ?>

        </div>
    </div>

</div>


<div id="excluir-cliente" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/atencao.png') ?>"></h4>
            </div>
            <div class="modal-body">
                <p class="msg-modal text-center"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="deletarClienteFisico()">Excluir</button>
            </div>
        </div>

    </div>
</div>


<div id="msm-sucesso" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/sucesso.png') ?>"></h4>
            </div>
            <div class="modal-body">

                <p class="msg-modal text-success text-center"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="location.reload()">Ok</button>
            </div>
        </div>

    </div>
</div>


<div id="msg-erro" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/erro.png') ?>"></h4>
            </div>
            <div class="modal-body">
                <p class="msg-modal text-danger text-center">

                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="location.reload()">Ok</button>
            </div>
        </div>

    </div>
</div>
<script src="<?= base_url('assets/js/requisicoes/cliente_fisico.js') ?>" type="text/javascript"></script>

