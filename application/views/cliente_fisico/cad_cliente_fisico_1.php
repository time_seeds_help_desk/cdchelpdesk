<script type="text/javascript" src="<?= base_url('assets/js/requisicoes/cliente.js') ?>"></script>
<div class="body">
    <div class="row">
        <div class="col-md-12">
            <a href="<?= base_url('ClienteFisico') ?>" class="btn btn-primary" title="voltar">
                <i class="material-icons">arrow_back</i> Voltar
            </a>
        </div>
    </div>

    <!--==================CLIENTE FÍSICO==================-->
    <form id="cad-cliente-fisico" class="form-visible">
            <div class="row">
                <div class="col-md-12" style="color: red">
                    Campos com asterísco (*) são obrigatórios
                </div>
                <div class="col-md-12">
                    <label>Nome<span> *</span></label>
                    <input autocomplete="off" type="text" onkeypress="mascara(this, soLetras)" class="form-control" autofocus="" required="" name="nome_nm_fantasia">
                </div>

                <div class="col-md-3">
                    <label>CPF</label>
                    <input autocomplete="off" type="text" class="form-control mascara-cpf" name="cpf_cnpj" >
                </div>

                <div class="col-md-3">
                    <label>Telefone<span> *</span></label>
                    <input autocomplete="off" type="text" class="form-control mascara-telefone" name="telefone" required="">
                </div>



                <div class="col-md-6">
                    <label>E-mail</label>
                    <input autocomplete="off" type="email" class="form-control"  name="email" >
                </div>

                <div class="col-md-8">
                    <label>Rua<span> *</span></label>
                    <input autocomplete="off" type="text" class="form-control" onkeypress="mascara(this, soLetrasNum)" required="" name="rua">
                </div>

                <div class="col-md-4">
                    <label>Número<span> *</span></label>
                    <input autocomplete="off" required="" type="text" class="form-control"onkeypress="mascara(this, soLetrasNum)"  name="numero">
                </div>

                <div class="col-md-5">
                    <label>Bairro<span> *</span></label>
                    <input autocomplete="off" type="text" class="form-control" onkeypress="mascara(this, soLetrasNum)" required="" name="bairro" >
                </div>

                <div class="col-md-2">
                    <label>CEP<span> *</span></label>
                    <input autocomplete="off" type="text" class="form-control mascara-cep" name="cep" required="">
                </div>

                <div class="col-md-5">
                    <label>Complemento</label>
                    <input autocomplete="off" type="text" class="form-control" onkeypress="mascara(this, soLetrasNum)" name="complemento">
                </div>

                <div class="col-sm-6">
                    <label>Estado<span> *</span></label>
                    <select class="form-control sigla_estado" name="sigla_estado" id="cf_sigla_estado" required="">
                        <option value="" selected="">Selecione o Estado</option>
                    </select>
                </div>

                <div class="col-sm-6">
                    <label>Cidade<span> *</span></label>
                    <select class="form-control id_cidade" name="id_cidade" required="">

                    </select>
                </div>
                <div class="col-md-12" style="text-align: right">
                    <a href="<?= base_url("ClienteFisico") ?>" class="btn btn-warning"><i class="material-icons">cancel</i> Cancelar</a>
                    <button type="submit" class="btn btn-success"><i class="material-icons">sd_storage</i> Cadastrar</button>
                </div>
            </div>
        

    </form>
    <!--================CLIENTE FÍCICO====================-->
</div>



<div id="msm-sucesso-cad-cli" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" onclick="voltar('ClienteFisico')">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/sucesso.png') ?>"></h4>
            </div>
            <div class="modal-body">

                <p class="msg-modal text-success text-center"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="voltar('ClienteFisico')">Ok</button>
            </div>
        </div>

    </div>
</div>



<div id="msg-erro" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" >&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/erro.png') ?>"></h4>
            </div>
            <div class="modal-body">
                <p class="msg-modal text-danger text-center">

                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" >Ok</button>
            </div>
        </div>

    </div>
</div>




