<form method="POST" action="<?= base_url('ClienteFisico/cadastrar') ?>">
    <div class="body">
        <div class="row">
            <div class="col-md-12">
                <a href="<?= base_url('ClienteFisico') ?>" class="btn btn-primary" title="voltar">
                    <i class="material-icons">arrow_back</i> Voltar
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label>Nome</label>
                <input type="text" onkeypress="mascara(this,soLetras)" class="form-control" autofocus="" required="" name="nome_nm_fantasia" value="<?= set_value('nome_nm_fantasia') ?>">
                <span class="help-block" style="color: red"><?= form_error('nome_nm_fantasia') ?></span>
            </div>
            
            <div class="col-md-6">
                <label>CPF</label>
                <input type="text" class="form-control mascara-cpf" name="cpf_cnpj" value="<?= set_value('cpf_cnpj') ?>">

                <span class="help-block with-errors" style="color: red">
                    <?= form_error('cpf_cnpj') ?>
                    <?= isset($valid_cpf) ? $valid_cpf : ''; ?>
                </span>
            </div>

            <div class="col-md-6">
                <label>Telefone</label>
                <input type="text" class="form-control mascara-telefone" name="telefone" value="<?= set_value('telefone') ?>">
            </div>

            

            <div class="col-md-12">
                <label>E-mail</label>
                <input type="email" class="form-control"  name="email" value="<?= set_value('email') ?>">
                <span class="help-block with-errors" style="color: red">
                    <?= form_error('email') ?>
                    <?= isset($valid_email) ? $valid_email : ''; ?>
                </span>
            </div>

            <div class="col-md-8">
                <label>Rua</label>
                <input type="text" class="form-control" onkeypress="mascara(this,soLetrasNum)" required="" name="rua" value="<?= set_value('rua') ?>">
                <span class="help-block" style="color: red"><?= form_error('rua') ?></span>
            </div>

            <div class="col-md-4">
                <label>Número</label>
                <input type="text" class="form-control"onkeypress="mascara(this,soLetrasNum)"  name="numero" value="<?= set_value('numero') ?>">
            </div>

            <div class="col-md-12">
                <label>Bairro</label>
                <input type="text" class="form-control" onkeypress="mascara(this,soLetrasNum)" required="" name="bairro" value="<?= set_value('bairro') ?>">
                <span class="help-block" style="color: red"><?= form_error('bairro') ?></span>
            </div>

            <div class="col-md-6">
                <label>CEP</label>
                <input type="text" class="form-control mascara-cep" name="cep" value="<?= set_value('cep') ?>">
            </div>

            <div class="col-md-6">
                <label>Complemento</label>
                <input type="text" class="form-control" onkeypress="mascara(this,soLetrasNum)" name="complemento" value="<?= set_value('complemento') ?>">
            </div>

            <div class="col-sm-6">
                <label>Estado</label>
                <select class="form-control" name="sigla_estado" id="sigla_estado" required="">
                    <option value="" selected="">Selecione o Estado</option>
                    <?php
                    $opcao_selecionada = "";
                    foreach ($estados as $estado):
                        if ($estado->sigla == $estado_selecionado) {
                            $opcao_selecionada = "selected=''";
                        }
                        ?>
                        <option value="<?= $estado->sigla ?>" <?= $opcao_selecionada ?> ><?= $estado->nome_estado ?></option>
                        <?php
                        $opcao_selecionada = "";
                    endforeach;
                    ?>
                </select>
                <span class="help-block" style="color: red"><?= form_error('sigla_estado') ?></span>
            </div>

            <div class="col-sm-6">
                <label>Cidade</label>
                <select class="form-control" name="id_cidade" id="id_cidade" required="">
                    <?php
                    $opcao_selecionada = "";
                    if (isset($cidades)):
                        echo "<option value=''>Selecione a Cidade</option>";
                        foreach ($cidades as $cidade):
                            if ($cidade->id_cidade == $cidade_selecionada) {
                                $opcao_selecionada = "selected=''";
                            }
                            ?>
                            <option value="<?= $cidade->id_cidade ?>" <?= $opcao_selecionada ?>><?= $cidade->nome_cidade ?></option>
                            <?php
                            $opcao_selecionada = "";
                        endforeach;
                    endif;
                    ?>
                </select>
                <span class="help-block" style="color: red"><?= form_error('id_cidade') ?></span>
            </div>



        </div>

        <div class="row">
            <div class="col-md-12" style="text-align: right">
                <a href="<?= base_url("ClienteFisico") ?>" class="btn btn-warning btn-lg">Cancelar</a>
                <button type="submit" class="btn btn-success btn-lg">Cadastrar</button>
            </div>
        </div>

    </div>
</form>


<div id="msm-sucesso" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/sucesso.png') ?>"></h4>
            </div>
            <div class="modal-body">

                <p class="msg-modal text-success text-center"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="voltarListaCliFisico()">Ok</button>
            </div>
        </div>

    </div>
</div>



<div id="msg-erro" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/erro.png') ?>"></h4>
            </div>
            <div class="modal-body">
                <p class="msg-modal text-danger text-center">

                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="voltarListaCliFisico()">Ok</button>
            </div>
        </div>

    </div>
</div>


<script src="<?= base_url('assets/js/requisicoes/cliente_fisico.js') ?>" type="text/javascript"></script>

<?php if (isset($msg)): ?>
    <script>
                        abrirMsmCadastro("<?= $msg ?>", "<?= $cadastrou == true ? "sim" : "nao" ?>");
    </script>
<?php endif; ?>


