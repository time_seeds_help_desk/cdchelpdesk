
<form method="POST" action="<?= base_url('ClienteFisico/editar') ?>">
    <input type="hidden" name="id_pessoa" value="<?= $cliente[0]->id_pessoa ?>">
    <input type="hidden" name="cpf_cnpj" value="<?= $cliente[0]->cpf_cnpj ?>">
    <div class="body">
        <div class="row">
            <div class="col-md-12">
                <a href="<?= base_url('ClienteFisico') ?>" class="btn btn-primary" title="voltar">
                    <i class="material-icons">arrow_back</i> Voltar
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" style="color: red">
                Campos com asterísco (*) são obrigatórios
            </div>
            <div class="col-md-12">
                <label>Nome<span> *</span></label>
                <input type="text" class="form-control" onkeypress="mascara(this,soLetras)" autofocus="" required="" name="nome_nm_fantasia" value="<?= $cliente[0]->nome_nm_fantasia ?>">
                <span class="help-block" style="color: red"><?= form_error('nome_nm_fantasia') ?></span>
            </div>

            <div class="col-md-6">
                <label>CPF</label>
                <input type="text" class="form-control mascara-cpf" disabled="" name="cpf_cnpj" value="<?= $cliente[0]->cpf_cnpj ?>">
            </div>
            <div class="col-md-6">
                <label>Telefone<span> *</span></label>
                <input type="text" class="form-control mascara-telefone" name="telefone" value="<?= $cliente[0]->telefone ?>">
            </div>

            <div class="col-md-12">
                <label>E-mail</label>
                <input type="e-mail" class="form-control" name="email" value="<?= $cliente[0]->email ?>">
                <span class="help-block with-errors" style="color: red">
                    <?= form_error('email') ?>
                    <?= isset($valid_email) ? $valid_email : ''; ?>
                </span>
            </div>

            <div class="col-md-8">
                <label>Rua<span> *</span></label>
                <input type="text" class="form-control" name="rua" onkeypress="mascara(this,soLetrasNum)" value="<?= $cliente[0]->rua ?>">
            </div>

            <div class="col-md-4">
                <label>Número<span> *</span></label>
                <input type="text" onkeypress="mascara(this,soLetrasNum)" class="form-control" name="numero" value="<?= $cliente[0]->numero ?>">
            </div>

            <div class="col-md-4">
                <label>Bairro<span> *</span></label>
                <input type="text" class="form-control" onkeypress="mascara(this,soLetrasNum)" name="bairro" value="<?= $cliente[0]->bairro ?>">
            </div>

            <div class="col-md-4">
                <label>CEP<span> *</span></label>
                <input type="text" class="form-control mascara-cep" name="cep" value="<?= $cliente[0]->cep ?>">
            </div>

            <div class="col-md-4">
                <label>Complemento</label>
                <input type="text" class="form-control" onkeypress="mascara(this,soLetrasNum)" name="complemento" value="<?= $cliente[0]->complemento ?>">
            </div>

            <div class="col-sm-6">
                <label>Estado<span> *</span></label>
                <select class="form-control" name="sigla_estado" id="sigla_estado">
                    <option value="" selected="">Selecione o Estado</option>
                    <?php
                    $opcao_selecionada = "";
                    foreach ($estados as $estado):
                        if ($estado->sigla == $estado_selecionado) {
                            $opcao_selecionada = "selected=''";
                        }
                        ?>
                        <option value="<?= $estado->sigla ?>" <?= $opcao_selecionada ?> ><?= $estado->nome_estado ?></option>
                        <?php
                        $opcao_selecionada = "";
                    endforeach;
                    ?>
                </select>
            </div>

            <div class="col-sm-6">
                <label>Cidade<span> *</span></label>
                <select class="form-control" name="id_cidade" id="id_cidade">
                    <?php
                    $opcao_selecionada = "";
                    if (isset($cidades)):
                        echo "<option value=''>Selecione a Cidade</option>";
                        foreach ($cidades as $cidade):
                            if ($cidade->id_cidade == $cidade_selecionada) {
                                $opcao_selecionada = "selected=''";
                            }
                            ?>
                            <option value="<?= $cidade->id_cidade ?>" <?= $opcao_selecionada ?>><?= $cidade->nome_cidade ?></option>
                            <?php
                            $opcao_selecionada = "";
                        endforeach;
                    endif;
                    ?>
                </select>
            </div>

        </div>

        <div class="row">
            <div class="col-md-12" style="text-align: right">
                <a href="<?= base_url('ClienteFisico') ?>" class="btn btn-warning btn-lg">Cancelar</a>
                <button type="submit" class="btn btn-primary btn-lg">Alterar</button>
            </div>
        </div>

    </div>
</form>

