<div class="row">
    <div class="col-md-12">
        <div class="col-md-12 table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr style="background-color: #222; color: #fff">
                        <th>Data</th>
                        <th>Horário</th>
                        <th>Cliente</th>
                        <th>Telefone</th>
                        <th>Endereço</th>
                        <th>Opção</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($agendamentos as $a): ?>
                        <tr>
                            <td><?= formatar_data($a->data) ?></td>
                            <td><?= $a->horario ?></td>
                            <td><?= $a->nome_nm_fantasia ?></td>
                            <td><?= $a->telefone ?></td>
                            <td><?= $a->rua . ", " . $a->numero . ", " . $a->bairro . ", " . $a->nome_cidade . " - " . $a->sigla ?></td>
                            <td><a class="btn btn-primary" href="<?= base_url('OSSuporte/abrir/'.$a->id_ordem_de_servico )?>">Ver Ordem de Serviço</a></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

