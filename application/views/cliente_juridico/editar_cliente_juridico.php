<form method="POST" action="<?= base_url('ClienteJuridico/editar') ?>">

    <input type="hidden" name="id_pessoa" value="<?= $cliente[0]->id_pessoa ?>" >
    <input type="hidden" name="cpf_cnpj" value="<?= $cliente[0]->cpf_cnpj ?>" >
    <div class="body">
        <div class="row">
            <div class="col-md-12">
                <a href="<?= base_url('ClienteJuridico') ?>" class="btn btn-primary" title="voltar">
                    <i class="material-icons">arrow_back</i> Voltar
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" style="color: red">
                Campos com asterísco (*) são obrigatórios
            </div>
            
            <div class="col-md-6">
                <label>Nome Fantasia<span> *</span></label>
                <input type="text" class="form-control" required=""  name="nome_nm_fantasia" value="<?= $cliente[0]->nome_nm_fantasia ?>">
                <span class="help-block" style="color: red"><?= form_error('nome_nm_fantasia') ?></span>
            </div>

            <div class="col-md-6">
                <label>Razão Social<span> *</span></label>
                <input type="text" class="form-control" required=""  name="razao_social" value="<?= $cliente[0]->razao_social ?>">
                <span class="help-block" style="color: red"><?= form_error('razao_social') ?></span>
            </div>
            <div class="col-md-6">
                <label>CNPJ<span> *</span></label>
                <input type="text" class="form-control mascara-cnpj" required=""  name="cpf_cnpj" value="<?= $cliente[0]->cpf_cnpj ?>" disabled="">
            </div>
            
            <div class="col-md-6">
                <label>Responsável<span> *</span></label>
                <input type="text" onkeypress="mascara(this,soLetras)" class="form-control" autofocus=""  required="" name="responsavel" value="<?= $cliente[0]->responsavel ?>">
                <span class="help-block" style="color: red"><?= form_error('responsavel') ?></span>
            </div>

            <div class="col-md-6">
                <label>Telefone<span> *</span></label>
                <input type="text" class="form-control mascara-telefone" name="telefone" value="<?= $cliente[0]->telefone ?>">
            </div>


            <div class="col-md-6">
                <label>E-mail<span> *</span></label>
                <input type="email" class="form-control"  name="email" required=""  value="<?= $cliente[0]->email ?>">
            </div>

            <div class="col-md-8">
                <label>Rua<span> *</span></label>
                <input type="text" onkeypress="mascara(this, soLetrasNum)" class="form-control" required=""  name="rua" value="<?= $cliente[0]->rua ?>">
                <span class="help-block" style="color: red"><?= form_error('rua') ?></span>
            </div>

            <div class="col-md-4">
                <label>Número<span> *</span></label>
                <input type="text" onkeypress="mascara(this, soLetrasNum)" class="form-control" required=""  name="numero" value="<?= $cliente[0]->numero ?>">
            </div>

            <div class="col-md-12">
                <label>Bairro<span> *</span></label>
                <input type="text" onkeypress="mascara(this, soLetrasNum)" class="form-control" required=""  name="bairro" value="<?= $cliente[0]->bairro ?>">
                <span class="help-block" style="color: red"><?= form_error('bairro') ?></span>
            </div>

            <div class="col-md-6">
                <label>CEP<span> *</span></label>
                <input type="text" class="form-control mascara-cep" required=""  name="cep" value="<?= $cliente[0]->cep ?>">
                <span class="help-block" style="color: red"><?= form_error('cep') ?></span>
            </div>

            <div class="col-md-6">
                <label>Complemento</label>
                <input type="text" class="form-control" onkeypress="mascara(this, soLetrasNum)" name="complemento" value="<?= $cliente[0]->complemento ?>">
            </div>

            <div class="col-sm-12">
                <label>Estado<span> *</span></label>
                <select class="form-control" name="sigla_estado" id="sigla_estado">
                    <option value="" selected="">Selecione o Estado</option>
                    <?php
                    $opcao_selecionada = "";
                    foreach ($estados as $estado):
                        if ($estado->sigla == $estado_selecionado) {
                            $opcao_selecionada = "selected=''";
                        }
                        ?>
                        <option value="<?= $estado->sigla ?>" <?= $opcao_selecionada ?> ><?= $estado->nome_estado ?></option>
                        <?php
                        $opcao_selecionada = "";
                    endforeach;
                    ?>
                </select>
            </div>

            <div class="col-sm-12">
                <label>Cidade<span> *</span></label>
                <select class="form-control" name="id_cidade" id="id_cidade">
                    <?php
                    $opcao_selecionada = "";
                    if (isset($cidades)):
                        echo "<option value=''>Selecione a Cidade</option>";
                        foreach ($cidades as $cidade):
                            if ($cidade->id_cidade == $cidade_selecionada) {
                                $opcao_selecionada = "selected=''";
                            }
                            ?>
                            <option value="<?= $cidade->id_cidade ?>" <?= $opcao_selecionada ?>><?= $cidade->nome_cidade ?></option>
                            <?php
                            $opcao_selecionada = "";
                        endforeach;
                    endif;
                    ?>
                </select>
            </div>

        </div>



        <div class="row">
            <div class="col-md-12" style="text-align: right">
                <a href="<?= base_url('ClienteJuridico') ?>" class="btn btn-warning"><i class="material-icons">cancel</i> Cancelar</a>
                &nbsp;&nbsp;<button type="submit" class="btn btn-primary"><i class="material-icons">sd_storage</i> Alterar</button>
            </div>
        </div>

    </div>
</form>


<div id="msm-sucesso" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/sucesso.png') ?>"></h4>
            </div>
            <div class="modal-body">

                <p class="msg-modal text-success text-center"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="voltarListaFunc()">Ok</button>
            </div>
        </div>

    </div>
</div>



<div id="msg-erro" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/erro.png') ?>"></h4>
            </div>
            <div class="modal-body">
                <p class="msg-modal text-danger text-center">

                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="voltarListaFunc()">Ok</button>
            </div>
        </div>

    </div>
</div>


<script src="<?= base_url('assets/js/requisicoes/cliente_juridico.js') ?>" type="text/javascript"></script>

<?php if (isset($msg)): ?>
    <script>
                    abrirMsmCadastro("<?= $msg ?>", "<?= $cadastrou == true ? "sim" : "nao" ?>");
    </script>
<?php endif; ?>



