
<form method="POST" action="<?= base_url('ClienteJuridico/cadastrar') ?>">
    <div class="body">
        <div class="row">
            <div class="col-md-12">
                <a href="<?= base_url('ClienteJuridico') ?>" class="btn btn-primary" title="voltar">
                    <i class="material-icons">arrow_back</i> Voltar
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label>Responsável</label>
                <input type="text" onkeypress="mascara(this,soLetras)" class="form-control" autofocus=""  required="" name="responsavel" value="<?= set_value('responsavel') ?>">
                <span class="help-block" style="color: red"><?= form_error('responsavel') ?></span>
            </div>
            
            <div class="col-md-6">
                <label>Nome Fantasia</label>
                <input type="text" class="form-control" required=""  name="nome_nm_fantasia" value="<?= set_value('nome_nm_fantasia') ?>">
                <span class="help-block" style="color: red"><?= form_error('nome_nm_fantasia') ?></span>
            </div>
            
            <div class="col-md-6">
                <label>Razão Social</label>
                <input type="text" class="form-control" required=""  name="razao_social" value="<?= set_value('razao_social') ?>">
                <span class="help-block" style="color: red"><?= form_error('razao_social') ?></span>
            </div>
            <div class="col-md-6">
                <label>CNPJ</label>
                <input type="text" class="form-control mascara-cnpj" required=""  name="cpf_cnpj" value="<?= set_value('cpf_cnpj') ?>">
                <span class="help-block" style="color: red">
                    <?= form_error('cpf_cnpj') ?>
                    <?= isset($valid_cpf_cnpj) ? $valid_cpf_cnpj : ''; ?>
                </span>
            </div>

            <div class="col-md-6">
                <label>Telefone</label>
                <input type="text" class="form-control mascara-telefone" name="telefone" value="<?= set_value('telefone') ?>">
            </div>

            <div class="col-md-8">
                <label>Rua</label>
                <input type="text" onkeypress="mascara(this,soLetrasNum)" class="form-control" required=""  name="rua" value="<?= set_value('rua') ?>">
                <span class="help-block" style="color: red"><?= form_error('rua') ?></span>
            </div>

            <div class="col-md-4">
                <label>Número</label>
                <input type="text" class="form-control" required=""  name="numero" value="<?= set_value('numero') ?>">
            </div>

            <div class="col-md-12">
                <label>Bairro</label>
                <input type="text" onkeypress="mascara(this,soLetrasNum)" class="form-control" required=""  name="bairro" value="<?= set_value('bairro') ?>">
                <span class="help-block" style="color: red"><?= form_error('bairro') ?></span>
            </div>

            <div class="col-md-6">
                <label>CEP</label>
                <input type="text" class="form-control mascara-cep" required=""  name="cep" value="<?= set_value('cep') ?>">
                <span class="help-block" style="color: red"><?= form_error('cep') ?></span>
            </div>

            <div class="col-md-6">
                <label>Complemento</label>
                <input type="text" class="form-control" onkeypress="mascara(this,soLetrasNum)" name="complemento" value="<?= set_value('complemento') ?>">
            </div>
            
            <div class="col-sm-12">
                <label>Estado</label>
                <select class="form-control" name="sigla_estado" id="sigla_estado" required="" >
                    <option value="" selected="">Selecione o Estado</option>
                    <?php
                    $opcao_selecionada = "";
                    foreach ($estados as $estado):
                        if ($estado->sigla == $estado_selecionado) {
                            $opcao_selecionada = "selected=''";
                        }
                        ?>
                        <option value="<?= $estado->sigla ?>" <?= $opcao_selecionada ?> ><?= $estado->nome_estado ?></option>
                        <?php
                        $opcao_selecionada = "";
                    endforeach;
                    ?>
                </select>
                <span class="help-block" style="color: red"><?= form_error('sigla_estado') ?></span>
            </div>

            <div class="col-sm-6">
                <label>Cidade</label>
                <select class="form-control" name="id_cidade" id="id_cidade" required="" >
                    <?php
                    $opcao_selecionada = "";
                    if (isset($cidades)):
                        echo "<option value=''>Selecione a Cidade</option>";
                        foreach ($cidades as $cidade):
                            if ($cidade->id_cidade == $cidade_selecionada) {
                                $opcao_selecionada = "selected=''";
                            }
                            ?>
                            <option value="<?= $cidade->id_cidade ?>" <?= $opcao_selecionada ?>><?= $cidade->nome_cidade ?></option>
                            <?php
                            $opcao_selecionada = "";
                        endforeach;
                    endif;
                    ?>
                </select>
                <span class="help-block" style="color: red"><?= form_error('id_cidade') ?></span>
            </div>

            <div class="col-md-6">
                <label>E-mail</label>
                <input type="email" class="form-control"  name="email" required=""  value="<?= set_value('email') ?>">
                <span class="help-block with-errors" style="color: red">
                    <?= form_error('email') ?>
                    <?= isset($valid_email) ? $valid_email : ''; ?>
                </span>
            </div>

            <!--DEVIDO NÃO SER NECESSÁRIO O USUÁRIO FAZER ACESSO AO SISTEMA, O CAMPO SENHA FOI REMOVIDO DO FORMULÁRIO-->
            <div class="col-md-3" style="display: none">
                <label>Senha</label>
                <input type="password" class="form-control" required=""  name="senha" value="<?= set_value('senha') ?>">
                <span class="help-block" style="color: red">
                    <?= form_error('senha') ?>
                </span>
            </div>

            <div class="col-md-3" style="display: none">
                <label>Confirmar Senha</label>
                <input type="password" class="form-control" required=""   name="confirma_senha" value="<?= set_value('confirma_senha') ?>">
                <span class="help-block" style="color: red"><?= form_error('confirma_senha') ?></span>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12" style="text-align: right">
                <button type="reset" class="btn btn-warning btn-lg">Cancelar</button>
                <button type="submit" class="btn btn-success btn-lg">Cadastrar</button>
            </div>
        </div>

    </div>
</form>


<div id="msm-sucesso" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/sucesso.png') ?>"></h4>
            </div>
            <div class="modal-body">

                <p class="msg-modal text-success text-center"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="voltarListaCliJuridico()">Ok</button>
            </div>
        </div>

    </div>
</div>



<div id="msg-erro" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/erro.png') ?>"></h4>
            </div>
            <div class="modal-body">
                <p class="msg-modal text-danger text-center">

                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="voltarListaCliJuridico()">Ok</button>
            </div>
        </div>

    </div>
</div>
    
    
<script src="<?= base_url('assets/js/requisicoes/cliente_juridico.js') ?>" type="text/javascript"></script>

<?php if (isset($msg)): ?>
    <script>
                    abrirMsmCadastro("<?= $msg ?>", "<?= $cadastrou == true ? "sim" : "nao" ?>");
    </script>
<?php endif; ?>

