<script type="text/javascript" src="<?= base_url('assets/js/requisicoes/cliente.js') ?>"></script>
<div class="body">
    <div class="row">
        <div class="col-md-12">
            <a href="<?= base_url('ClienteJuridico') ?>" class="btn btn-primary" title="voltar">
                <i class="material-icons">arrow_back</i> Voltar
            </a>
        </div>
    </div>


    <!--=================CLIENTE JURÍDICO=================-->
    <form id="cad-cliente-juridico"  class="form-visible">
        <div class="row">
            <div class="col-md-12" style="color: red">
                Campos com asterísco (*) são obrigatórios
            </div>
            <div class="col-md-4">
                <label>Nome Fantasia<span> *</span></label>
                <input autocomplete="off" type="text" class="form-control" required=""  name="nome_nm_fantasia">
            </div>

            <div class="col-md-4">
                <label>Razão Social<span> *</span></label>
                <input autocomplete="off" type="text" class="form-control" required=""  name="razao_social">
            </div>

            <div class="col-md-4">
                <label>CNPJ<span> *</span></label>
                <input autocomplete="off" type="text" class="form-control mascara-cnpj" required=""  name="cpf_cnpj">
            </div>

            <div class="col-md-5">
                <label>Responsável<span> *</span></label>
                <input autocomplete="off" type="text" onkeypress="mascara(this, soLetras)" class="form-control" autofocus=""  required="" name="responsavel">
            </div>


            <div class="col-md-2">
                <label>Telefone<span> *</span></label>
                <input autocomplete="off" type="text" class="form-control mascara-telefone" name="telefone">
            </div>

            <div class="col-md-5">
                <label>Rua<span> *</span></label>
                <input autocomplete="off" type="text" onkeypress="mascara(this, soLetrasNum)" class="form-control" required=""  name="rua" value="<?= set_value('rua') ?>">
            </div>

            <div class="col-md-2">
                <label>Número<span> *</span></label>
                <input autocomplete="off" type="text" class="form-control" required=""  name="numero">
            </div>

            <div class="col-md-5">
                <label>Bairro<span> *</span></label>
                <input autocomplete="off" type="text" onkeypress="mascara(this, soLetrasNum)" class="form-control" required=""  name="bairro">
            </div>

            <div class="col-md-2">
                <label>CEP<span> *</span></label>
                <input autocomplete="off" type="text" class="form-control mascara-cep" required=""  name="cep">
            </div>

            <div class="col-md-3">
                <label>Complemento</label>
                <input autocomplete="off" type="text" class="form-control" onkeypress="mascara(this, soLetrasNum)" name="complemento">
            </div>

            <div class="col-sm-6">
                <label>Estado<span> *</span></label>
                <select class="form-control sigla_estado" name="sigla_estado" id="cj_sigla_estado" required="" >
                    <option value="" selected="">Selecione</option>
                    <option value="1">teste</option>
                </select>
            </div>

            <div class="col-sm-6">
                <label>Cidade<span> *</span></label>
                <select class="form-control id_cidade" name="id_cidade" required="" >
                    <option value="" selected="">Selecione</option>
                    <option value="1">teste</option>
                </select>
            </div>

            <div class="col-md-12">
                <label>E-mail<span> *</span></label>
                <input autocomplete="off" type="email" class="form-control"  name="email" required="" >
            </div>

            <div class="col-md-3" style="display: none">
                <label>Senha<span> *</span></label>
                <input autocomplete="off" type="password" class="form-control" name="senha">
            </div>

            <div class="col-md-3" style="display: none">
                <label>Confirmar Senha<span> *</span></label>
                <input autocomplete="off" type="password" class="form-control" name="confirma_senha">
            </div>

            <div class="col-md-12" style="text-align: right">
                <a href="<?= base_url("ClienteFisico") ?>" class="btn btn-warning"><i class="material-icons">cancel</i> Cancelar</a>
                <button type="submit" class="btn btn-success"><i class="material-icons">sd_storage</i> Cadastrar</button>
            </div>
        </div>
    </form>

    <!--=================CLIENTE JURÍDICO==================-->
</div>



<div id="msm-sucesso-cad-cli" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" onclick="voltar('ClienteJuridico')">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/sucesso.png') ?>"></h4>
            </div>
            <div class="modal-body">

                <p class="msg-modal text-success text-center"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="voltar('ClienteJuridico')">Ok</button>
            </div>
        </div>

    </div>
</div>



<div id="msg-erro" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center"><img src="<?= base_url('assets/img/erro.png') ?>"></h4>
            </div>
            <div class="modal-body">
                <p class="msg-modal text-danger text-center">

                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
            </div>
        </div>

    </div>
</div>



