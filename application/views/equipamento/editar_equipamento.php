
<form method="POST" action="<?= base_url('Equipamento/editar') ?>">
    <input type="hidden" name="id_equipamento" value="<?= $equipamento->id_equipamento ?>">
    <div class="body">
        <div class="row">
            <div class="col-md-12">
                <a href="<?= base_url('Equipamento') ?>" class="btn btn-primary" title="voltar">
                    <i class="material-icons">arrow_back</i> Voltar
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label>Nome</label>
                <input type="text" onkeypress="mascara(this, soLetrasNum)" class="form-control" autofocus="" required="" name="nome_equipamento" value="<?= $equipamento->nome_equipamento ?>">
            </div>

            <div class="col-md-12">
                <label>Valor(R$)</label>
                <input type="text" class="form-control mascara-dinheiro" name="valor_equipamento" value="<?= converte_valor_usuario($equipamento->valor_equipamento)?>" required="">
            </div>

        </div>

        <div class="row">
            <div class="col-md-12" style="text-align: right">
                <a href="<?= base_url('Equipamento') ?>" class="btn btn-warning btn-lg">Cancelar</a>
                <button type="submit" class="btn btn-success btn-lg">Alterar</button>
            </div>
        </div>

    </div>
</form>




