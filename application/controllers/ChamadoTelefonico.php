<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ChamadoTelefonico extends CI_Controller {

    public function __construct() {
        parent::__construct();
        verificar_permissao($this->session->userdata(), array(ADMINISTRADOR, SUPORTE));
        $this->load->model('Chamado_Telefonico_Model', 'm_chamado_telefonico');
    }

    public function index() {
        $this->buscar_todos();
    }

    public function buscar_todos($id_inicio = 0) {
        $dados_view = get_dados_view('', 'Chamados', 'chamados');
        $dados_view['chamados_telefonicos'] = $this->m_chamado_telefonico->buscar_todos($id_inicio);
        $this->load->library('pagination');

        $config_paginacao = config_paginacao('ChamadoTelefonico/buscar_todos/', $this->m_chamado_telefonico->contar_todos());

        $this->pagination->initialize($config_paginacao);

        $dados_view['paginacao'] = $this->pagination->create_links();
        $dados_view['tem_pagina'] = true;
        $this->carregar_pagina($dados_view, 'chamado_telefonico/lista_chamado_telefonico');
    }

    public function cadastrar() {
        $dados_view = [
            'status' => 'erro',
            'msg' => 'Preencha todos os campos obrigátorios.',
            'chamado' => $this->input->post()
        ];

        if ($this->input->post('id_pessoa_cliente') != '' && $this->input->post('descricao_problema') != "") {
            $chamado = $this->input->post();
            if ($this->input->post('fl_resolvido') == "") {
                $chamado['fl_resolvido'] = true;
            } else {
                $chamado['fl_resolvido'] = false;
            }
            $resultado = $this->m_chamado_telefonico->cadastrar($chamado);
            if ($resultado['status'] != false) {
                $dados_view = [
                    'status' => 'sucesso',
                    'msg' => 'Chamado Finalizado com sucesso.',
                    'id_chamado_telefonico' => $resultado['id_chamado_telefonico'],
                    'chamado' => $this->input->post()
                ];
            } else {
                $dados_view['msg'] = "Erro ao cadastrar chamado.";
            }
        }

        echo json_encode($dados_view);
    }

    public function buscar($id_inicio = 0, $fl_resolvido = "", $nome_nm_fantasia = "") {
        $this->load->library('pagination');

        $dados_view['status'] = "erro";
        $dados_view['smg'] = "Nenhuma Chamado encontrada com esses parâmetros";

        $nome_nm_fantasia = str_replace("%20", " ", $nome_nm_fantasia);
        //$fl_resolvido = $fl_resolvido == '-1' ? "" : $fl_resolvido;

        $dados_view['chamados_telefonicos'] = $this->m_chamado_telefonico->buscar($id_inicio, $nome_nm_fantasia, $fl_resolvido);
        $config_paginacao = config_paginacao('ChamadoTelefonico/buscar/', $this->m_chamado_telefonico->contar_para_busca($nome_nm_fantasia, $fl_resolvido));

        $this->pagination->initialize($config_paginacao);
        $dados_view['paginacao'] = $this->pagination->create_links();

        if (count($dados_view['chamados_telefonicos'] > 0)) {
            $dados_view['status'] = "sucesso";
        }
        echo json_encode($dados_view);
    }

    public function abrir($id_chamado) {
        $dados_view = get_dados_view('', 'Chamados', 'chamados');
        if (is_numeric($id_chamado)) {
            $dados_view['chamado_telefonico'] = $this->m_chamado_telefonico->buscar_por_id($id_chamado);
            $this->carregar_pagina($dados_view, 'chamado_telefonico/abrir_chamado_telefonico');
        } else {
            redirect(base_url('ChamadoTelefonico'));
        }
    }

    public function pagina_cadastro() {
        $dados_view = get_dados_view('', 'Cadastrar Chamado', 'chamados');
        $dados_view['protocolo'] = $this->gerar_protocolo();
        $this->carregar_pagina($dados_view, 'chamado_telefonico/cad_chamado_telefonico');
    }

    private function gerar_protocolo() {
        $quantidade = $this->m_chamado_telefonico->contar_todos();
        $data = date('d/m/Y');
        $data = explode('/', $data);

        return $data[2] . $data[1] . $data[0] . ($quantidade + 1);
    }

    private function carregar_pagina($dados_view, $view) {
        $this->load->view('includes/cabecalho', $dados_view);
        $this->load->view(carregar_menu($this->session->userdata()), $dados_view);
        $this->load->view($view, $dados_view);
        $this->load->view('includes/rodape');
    }

    public function teste() {
        print_r($this->m_chamado_telefonico->buscar(0, "SM Empresa"));
    }

}
