<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Autorizada extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Autorizada_Model', 'm_autorizada');
        $this->load->model('Estado_Model', 'm_estado');
    }

    public function index() {
        $this->buscar_todas();
    }

    public function pagina_cadastro() {
        $dados_view = $this->get_dados_view('', 'Cadastro de Terceirizada', 'autorizada');
        $dados_view['estados'] = $this->m_estado->buscar_todos();
        $this->carregar_pagina($dados_view, 'autorizada/cad_autorizada');
    }

    public function pagina_editar($id_autorizada) {
        $dados_view = $this->get_dados_view('', 'Editar Autorizada', 'autorizada');
        $dados_view['estados'] = $this->m_estado->buscar_todos();

        $autorizada = $this->m_autorizada->buscar_por_id($id_autorizada);

        if (count($autorizada) === 0) {
            redirect(base_url('Autorizada'));
        } else {
            $this->load->model('Cidade_Model', 'm_cidade');
            $dados_view['autorizada'] = $autorizada;
            $dados_view['estado_selecionado'] = $autorizada[0]->sigla;
            $dados_view['cidade_selecionada'] = $autorizada[0]->id_cidade;
            $dados_view['cidades'] = $this->m_cidade->buscar_por_sigla($autorizada[0]->sigla);
        }

        $this->carregar_pagina($dados_view, 'autorizada/editar_autorizada');
    }

    public function cadastrar() {
        $dados_view = array(
            'msg' => 'Preencha todos os campos obrigatórios.',
            'status' => 'erro'
        );

        $autorizada = $this->receber_dados();

        if ($autorizada['cnpj'] != "" && $this->input->post('tipo_pessoa') == 'fisica' && !validar_cpf($autorizada['cnpj'])) {
            $dados_view['msg'] = 'CPF inválido.';
        } else if ($autorizada['cnpj'] != "" && $this->input->post('tipo_pessoa') == 'juridica' && !validar_cnpj($autorizada['cnpj'])) {
            $dados_view['msg'] = 'CNPJ inválido.';
            //if (!validar_cnpj($autorizada['cnpj']) && $autorizada['cnpj'] != "") {
            //$dados_view['msg'] = 'CNPJ inválido.';
        } else if (!validar_email($autorizada['email'])) {
            $dados_view['msg'] = 'E-mail inválido.';
        } else if (count($this->m_autorizada->buscar_por_cnpj($autorizada['cnpj'])) > 0) {
            $dados_view['msg'] = 'Este CPF/CNPJ já está cadastrado no sistema.';
        } else if (count($this->m_autorizada->buscar_por_email($autorizada['email'])) > 0) {
            $dados_view['msg'] = 'Este E-mail já está cadastrado no sistema.';
        } else if (
                $autorizada['nome_autorizada'] != "" &&
                $autorizada['cnpj'] != "" &&
                $autorizada['telefone'] != "" &&
                $autorizada['ramo'] != "" &&
                $autorizada['rua'] != "" &&
                $autorizada['email'] != "" &&
                $autorizada['numero'] != "" &&
                $autorizada['cep'] != "" &&
                $autorizada['bairro'] != "" &&
                $autorizada['id_cidade'] != "" &&
                $autorizada['responsavel'] != ""
        ) {
            if ($this->m_autorizada->cadastrar($autorizada)) {
                $dados_view['msg'] = 'Terceirizada cadastrada com sucesso.';
                $dados_view['status'] = 'sucesso';
            } else {
                $dados_view['msg'] = 'Erro ao cadastrar Terceirizada.';
            }
        }

        echo json_encode($dados_view);
    }

    public function editar() {
        $dados_view = array(
            'msg' => 'Preencha todos os campos obrigatórios.',
            'status' => 'erro'
        );

        $autorizada = $this->receber_dados();
        $id_autorizada = $this->input->post('id_autorizada');

        if ($autorizada['cnpj'] != "" && $this->input->post('tipo_pessoa') == 'fisica' && !validar_cpf($autorizada['cnpj'])) {
            $dados_view['msg'] = 'CPF inválido.';
        } else if ($autorizada['cnpj'] != "" && $this->input->post('tipo_pessoa') == 'juridica' && !validar_cnpj($autorizada['cnpj'])) {
            $dados_view['msg'] = 'CNPJ inválido.';
        } else if (!validar_email($autorizada['email'])) {
            $dados_view['msg'] = 'E-mail inválido.';
        } else if (count($this->m_autorizada->buscar_por_cnpj_editar($autorizada['cnpj'], $id_autorizada)) > 0) {
            $dados_view['msg'] = 'Este CNPJ já é cadastrado no sistema.';
        } else if (count($this->m_autorizada->buscar_por_email_editar($autorizada['email'], $id_autorizada)) > 0) {
            $dados_view['msg'] = 'Este E-mail já é cadastrado no sistema.';
        } else if (
                $autorizada['nome_autorizada'] != "" &&
                $autorizada['cnpj'] != "" &&
                $autorizada['telefone'] != "" &&
                $autorizada['ramo'] != "" &&
                $autorizada['rua'] != "" &&
                $autorizada['email'] != "" &&
                $autorizada['numero'] != "" &&
                $autorizada['cep'] != "" &&
                $autorizada['bairro'] != "" &&
                $autorizada['id_cidade'] != "" &&
                $autorizada['responsavel'] != ""
        ) {
            if ($this->m_autorizada->alterar($autorizada, $id_autorizada)) {
                $dados_view['msg'] = 'Terceirizada alterada com sucesso.';
                $dados_view['status'] = 'sucesso';
            } else {
                $dados_view['msg'] = 'Erro ao alterar Terceirizada.';
            }
        }

        echo json_encode($dados_view);
    }

    public function excluir($id) {
        $dados_view['msg'] = "Terceirizada excluída com sucesso!";
        $dados_view['status'] = "sucesso";

        if ($this->verifica_permissao_exclusao($id)) {
            if (!$this->m_autorizada->excluir($id)) {
                $dados_view['msg'] = "Erro ao excluir Terceirizada!";
                $dados_view['status'] = "erro";
            }
        } else {
            $dados_view['msg'] = "Esta terceirizada está vinculada pelo menos uma Ordem de Serviço e não pode ser excluída!";
            $dados_view['status'] = "erro";
        }

        echo json_encode($dados_view);
    }

    private function verifica_permissao_exclusao($id_autorizada) {
        if (count($this->m_autorizada->buscar_autorizada_os($id_autorizada)) > 0) {
            return false;
        }

        return true;
    }

    public function buscar_todas($id_inicio = 0, $paginacao = true) {
        if ($paginacao === "false" || $paginacao === false) {
            echo json_encode(array(
                'autorizadas' => $this->m_autorizada->buscar_todas(0, false)
            ));
        } else {
            $this->load->library('pagination');
            $dados_view = get_dados_view('', 'Terceirizada Cadastradas', 'autorizada');
            $dados_view['autorizadas'] = $this->m_autorizada->buscar_todas($id_inicio);
            $dados_view['pag_inicio'] = $id_inicio + 1;

            $config_paginacao = config_paginacao('Autorizada/buscar_todos/', $this->m_autorizada->contar_todas());

            $this->pagination->initialize($config_paginacao);

            $dados_view['paginacao'] = $this->pagination->create_links();
            $dados_view['tem_pagina'] = true;

            $this->carregar_pagina($dados_view, "autorizada/lista_autorizada");
        }
    }

    public function buscar() {
        $busca = $this->input->post('nome_autorizada');

        if ($busca != '') {
            $dados_view = get_dados_view('', 'Terceirizadas Cadastradas', 'autorizada');
            $dados_view['autorizadas'] = $this->m_autorizada->buscar_por_nome($busca);
            $dados_view['busca'] = $busca;

            $this->carregar_pagina($dados_view, "autorizada/busca_autorizada");
        } else {
            redirect(base_url('Autorizada'));
        }
    }

    public function buscar_por_id($id_autorizada) {
        echo json_encode(array('autorizadas' => $this->m_autorizada->buscar_por_id($id_autorizada)));
    }

    private function carregar_pagina($dados_view, $view) {
        $this->load->view('includes/cabecalho', $dados_view);
        $this->load->view(carregar_menu($this->session->userdata()), $dados_view);
        $this->load->view($view, $dados_view);
        $this->load->view('includes/rodape');
    }

    private function receber_dados() {
        return array(
            'nome_autorizada' => $this->input->post('nome_autorizada'),
            'cnpj' => $this->input->post('cnpj'),
            'telefone' => $this->input->post('telefone'),
            'ramo' => $this->input->post('ramo'),
            'email' => $this->input->post('email'),
            'rua' => $this->input->post('rua'),
            'numero' => $this->input->post('numero'),
            'complemento' => $this->input->post('complemento'),
            'cep' => $this->input->post('cep'),
            'bairro' => $this->input->post('bairro'),
            'id_cidade' => $this->input->post('id_cidade'),
            'responsavel' => $this->input->post('responsavel')
        );
    }

    /**
     * gera um array com as informações necessárias para a view
     * @param type $titulo
     * @param type $titulo2
     * @param type $menu_selecionado
     * @return type
     */
    public function get_dados_view($titulo, $titulo2, $menu_selecionado) {
        return array(
            'titulo' => $titulo,
            'titulo2' => $titulo2,
            'menu_selecionado' => $menu_selecionado
        );
    }

}
