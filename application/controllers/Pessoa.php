<?php

defined('BASEPATH') OR exit('No direct script access allowed');

abstract class Pessoa extends CI_Controller {

    abstract function pagina_editar($id);

    abstract function pagina_cadastro();

    abstract function validar();

    /**
     * Busca no banco de dados se existe algum CPF casdastrado
     * igual ao que está sendo informado,
     * caso encontre, retorna o valor false (CPF inválido)
     * caso não encontre, retorna o valor true (CPF válido)
     * @param type $cpf_cnpj
     * @return boolean
     */
    public function verificar_cpf_cnpj($cpf_cnpj) {
        if (!empty($cpf_cnpj)) {
            $this->load->model("Funcionario_Model", 'm_funcionario');
            $result = $this->m_funcionario->buscar_por_cpf_cnpj($cpf_cnpj);

            if (count($result) > 0) {
                return FALSE;
            } else {
                return TRUE;
            }
        } else {
            return false;
        }
    }

    public function verificar_email($email, $id_pessoa = 0) {
        if ($email != "") {
            $this->load->model("Funcionario_Model", 'm_funcionario');
            $result = null;

            if ($id_pessoa != 0) {
                $result = $this->m_funcionario->buscar_por_email_id($email, $id_pessoa);
            } else {
                $result = $this->m_funcionario->buscar_por_email($email);
            }

            if (count($result) > 0) {
                return FALSE;
            } else {
                return TRUE;
            }
        } else {
            return false;
        }
    }

    public function verificar_email_editar($email, $id_pessoa) {
        $this->load->model("Funcionario_Model", 'm_funcionario');
    }

    abstract function cadastrar();

    abstract function editar();

    abstract function buscar();

    abstract function excluir($id);

    abstract function receber_dados_pessoa();

    /**
     * gera um array com as informações necessárias para a view
     * @param type $titulo
     * @param type $titulo2
     * @param type $menu_selecionado
     * @return type
     */
    public function get_dados_view($titulo, $titulo2, $menu_selecionado) {
        return array(
            'titulo' => $titulo,
            'titulo2' => $titulo2,
            'menu_selecionado' => $menu_selecionado
        );
    }

}
