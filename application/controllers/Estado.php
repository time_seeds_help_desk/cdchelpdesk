<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Estado extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Estado_Model', 'm_estado');
    }

    public function buscar_todos() {
        $dados_view['msg'] = "Erro ao buscar Estados";
        $dados_view['status'] = 'erro';
        $estados = $this->m_estado->buscar_todos();

        if (count($estados) > 0) {
            $dados_view['msg'] = "Estados encontrado";
            $dados_view['status'] = 'sucesso';
            $dados_view['estados'] = $estados;
        }
        echo json_encode($dados_view);
    }

}
