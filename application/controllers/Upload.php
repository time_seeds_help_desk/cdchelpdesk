<?php

class Upload extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
    }

    public function index() {
        $this->load->view('testeUpload', array('error' => ' '));
    }

    public function do_upload() {
        $config['upload_path'] = 'assets/uploads';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 100;
        $config['max_width'] = 1024;
        $config['max_height'] = 768;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('userfile')) {
            $error = array('error' => $this->upload->display_errors());

            $this->load->view('testeUpload', $error);
        } else {
            echo 'Upload realizado com sucesso!';            
        }
    }

}