<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Equipamento
 *
 * @author Helder dos Santos
 */
class Equipamento extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Equipamento_Model', 'm_equipamento');
    }

    public function index() {
        $this->buscar_todos();
    }

    public function buscar_todos($id_inicio = 0) {

        $this->load->library('pagination');
        $dados_view = get_dados_view('', 'Produtos Cadastrados', 'equipamentos');
        $dados_view['equipamentos'] = $this->m_equipamento->buscar_todos($id_inicio);
        $dados_view['pag_inicio'] = $id_inicio + 1;

        $config_paginacao = config_paginacao('Equipamento/buscar_todos/', $this->m_equipamento->contar_todos());

        $this->pagination->initialize($config_paginacao);

        $dados_view['paginacao'] = $this->pagination->create_links();
        $dados_view['tem_pagina'] = true;

        $this->carregar_pagina($dados_view, "equipamento/lista_equipamento");
    }

    public function pagina_cadastro() {
        $dados_view = get_dados_view('', 'Cadastro de Produto', 'equipamento');
        $this->carregar_pagina($dados_view, 'equipamento/cad_equipamento');
    }

    public function cadastrar() {
        $valido = $this->validar();
        $dados_view = get_dados_view('', 'Cadastro de Produtos', 'equipamento');

        if ($valido === true) {
            $equipamento = array(
                'nome_equipamento' => $this->input->post('nome_equipamento'),
                'valor_equipamento' => converte_valor_db($this->input->post('valor_equipamento'))
            );

            $retorno = $this->m_equipamento->cadastrar($equipamento);

            if ($retorno === true) {
                $dados_view['msg'] = "Produto cadastrado com sucesso.";
                $dados_view['cadastrou'] = true;
            } else {
                $dados_view['msg'] = "Erro ao cadastrar Produtos.";
                $dados_view['cadastrou'] = false;
            }
        }

        $this->carregar_pagina($dados_view, 'equipamento/cad_equipamento');
    }

    public function excluir($id) {
        $dados_view['msg'] = "Erro ao excluir Produto.";
        $dados_view['status'] = "erro";

        $this->load->model('OS_Assistencia_Model', 'm_os_assistencia');

        if (count($this->m_os_assistencia->buscar_por_id_equipamento($id)) == 0) {
            if ($this->m_equipamento->excluir($id)) {
                $dados_view['msg'] = "Produto excluído com sucesso!";
                $dados_view['status'] = "sucesso";
            }
        }else {
            $dados_view['msg'] = "Este Produto está vinculado a pelo menos uma Ordem de Serviço e não pode ser excluído!";
            $dados_view['status'] = "erro";
        }
            echo json_encode($dados_view);
        
    }

    public function buscar() {
        $busca = $this->input->post('nome_equipamento');

        if ($busca != '') {
            $dados_view = get_dados_view('', 'Produtos Cadastrados', 'equipamento');
            $dados_view['equipamentos'] = $this->m_equipamento->buscar_por_nome($busca);
            $dados_view['busca'] = $busca;
            $dados_view['paginacao'] = "";
            $dados_view['tem_pagina'] = false;

            $this->carregar_pagina($dados_view, "equipamento/busca_equipamento");
        } else {
            redirect(base_url('Equipamento'));
        }
    }

    public function buscar_por_id($id_equipamento = "") {
        $dados_view = array(
            "status" => "erro",
            "msg" => "Não foi possível concluir a operação"
        );

        if (is_numeric($id_equipamento) && $id_equipamento != "") {
            $dados_view['status'] = "sucesso";
            $dados_view['equipamento_os'] = $this->m_equipamento->buscar_por_id($id_equipamento);
        }

        echo json_encode($dados_view);
    }

    public function pagina_editar($id) {
        if (is_numeric($id)) {
            $equipamento = $this->m_equipamento->buscar_por_id($id);

            if ($equipamento != null) {
                $dados_view = get_dados_view('', 'Editar Produto', 'equipamento');
                $dados_view['equipamento'] = $equipamento;
                $this->carregar_pagina($dados_view, 'equipamento/editar_equipamento');
            } else {
                redirect(base_url('Equipamento'));
            }
        } else {
            redirect(base_url('Equipamento'));
        }
    }

    public function editar() {
        $valido = $this->validar();
        $dados_view = get_dados_view('', 'Editar Produto', 'equipamento');

        if ($valido === true) {
            $equipamento = array(
                'id_equipamento' => $this->input->post('id_equipamento'),
                'nome_equipamento' => $this->input->post('nome_equipamento'),
                'valor_equipamento' => converte_valor_db($this->input->post('valor_equipamento'))
            );

            $retorno = $this->m_equipamento->alterar($equipamento);

            if ($retorno === true) {
                $dados_view['msg'] = "Produto alterado com sucesso.";
                $dados_view['editou'] = true;
            } else {
                $dados_view['msg'] = "Erro ao alterar Produto.";
                $dados_view['editou'] = false;
            }
        }

        $this->carregar_pagina($dados_view, 'equipamento/invalido_editar_equipamento');
    }

    private function carregar_pagina($dados_view, $view) {
        $this->load->view('includes/cabecalho', $dados_view);
        $this->load->view(carregar_menu($this->session->userdata()), $dados_view);
        $this->load->view($view, $dados_view);
        $this->load->view('includes/rodape');
    }

    private function validar() {
        $this->form_validation->set_rules('nome_equipamento', 'equipamento', 'required');
        $this->form_validation->set_rules('valor_equipamento', 'equipamento', 'required');
        return $this->form_validation->run();
    }

}
