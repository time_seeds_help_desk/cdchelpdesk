<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'application/controllers/Pessoa.php';

/**
 * Classe rsponsável por realização de CRUD de cliente jurídico
 * 
 */
class ClienteJuridico extends Pessoa {

    private $dados_view;

    public function __construct() {
        parent::__construct();
        $this->load->model('Cliente_Juridico_Model', 'm_cliente_juridico');
        $this->load->model("Estado_Model", 'm_estado');
        $this->load->model('Cidade_Model', 'm_cidade');
    }

    public function index() {
        $this->buscar_todos();
    }

    public function buscar_todos($id_inicio = 0) {
        verificar_permissao($this->session->userdata, array(ADMINISTRADOR, SUPORTE, ASSISTENCIA));

        $this->load->library('pagination');
        $this->dados_view = $this->get_dados_view('', 'Clientes Jurídicos Cadastrados', 'clientes juridicos');
        $this->dados_view['cliente_juridico'] = $this->m_cliente_juridico->buscar_todos($id_inicio);
        $this->dados_view['pag_inicio'] = $id_inicio + 1;

        $config_paginacao = config_paginacao('ClienteJuridico/buscar_todos', $this->m_cliente_juridico->contar_todos());

        $this->pagination->initialize($config_paginacao);

        $this->dados_view['paginacao'] = $this->pagination->create_links();
        $this->dados_view['tem_pagina'] = true;

        $this->carregar_pagina($this->dados_view, "cliente_juridico/lista_cliente_juridico");
    }

    public function buscar() {
        verificar_permissao($this->session->userdata, array(ADMINISTRADOR, SUPORTE, ASSISTENCIA));

        $busca = $this->input->post('cnpj_nome');
        $result = array();


        if ($busca != '') {

            if (is_numeric($this->input->post('cpf'))) {
                $busca = formatar_cpf($busca);
                $result = $this->m_cliente_juridico->buscar_por_cpf_cnpj($busca);
            } else {
                $result = $this->m_cliente_juridico->buscar_por_nome_fantasia($busca);
            }

            $dados_view = $this->get_dados_view('', 'Clientes Jurídicos Cadastrados', 'clientes juridicos');
            $dados_view['cliente_juridico'] = $result;
            $dados_view['busca'] = $busca;

            $this->carregar_pagina($dados_view, "cliente_juridico/busca_cliente_juridico");
        } else {
            redirect(base_url('ClienteJuridico'));
        }
    }

    public function cadastrar() {
        verificar_permissao($this->session->userdata, array(ADMINISTRADOR, SUPORTE, ASSISTENCIA));
        $status_validacao = $this->validar();
        $dados_view = $this->get_dados_view('', 'Cadastro de Cliente Jurídico', 'clientes juridicos');

        if ($this->verificar_cpf_cnpj($this->input->post('cpf_cnpj')) === false) {
            $status_validacao = false;
            $dados_view['valid_cpf_cnpj'] = 'CNPJ já cadastrado';
        }

        if (!validar_cnpj($this->input->post('cpf_cnpj'))) {
            $status_validacao = false;
            $dados_view['valid_cpf_cnpj'] = 'CNPJ inválido';
        }

        if ($this->verificar_email($this->input->post('email')) === false) {
            $status_validacao = false;
            $dados_view['valid_email'] = 'E-mail já cadastrado';
        }

        if ($status_validacao) {
            $pessoa = $this->receber_dados_pessoa();

            $resposta = $this->m_cliente_juridico->cadastrar($pessoa);

            if ($resposta == true) {
                $dados_view['msg'] = "Cliente jurídico cadastrado com sucesso!";
                $dados_view['cadastrou'] = true;
            } else {
                $dados_view['msg'] = "Erro ao cadastrar Cliente Jurídico!";
                $dados_view['cadastrou'] = false;
            }
        } else {

            $this->load->model('Cidade_Model', 'm_cidade');
            $sigla = $this->input->post('sigla_estado');
            $dados_view['estado_selecionado'] = $sigla;
            $dados_view['cidade_selecionada'] = $this->input->post('id_cidade');
            $dados_view['cidades'] = $this->m_cidade->buscar_por_sigla($sigla);
            $dados_view['estados'] = $this->m_estado->buscar_todos();
        }

        $this->carregar_pagina($dados_view, 'cliente_juridico/cad_cliente_juridico');
    }

    /**
     * Cadastro de cliente juríco quando solicitado via AJAX
     */
    public function cadastrar_ajax() {
        verificar_permissao($this->session->userdata, array(ADMINISTRADOR, SUPORTE, ASSISTENCIA));
        $dados_view['msg'] = "Preencha todos os campos obrigatórios";
        $dados_view['status'] = 'erro';
        $cadastrar = true;

        if (!validar_cnpj($this->input->post('cpf_cnpj')) || $this->input->post('cpf_cnpj') == "00.000.000/0000-00") {
            $cadastrar = false;
            $dados_view['msg'] = 'CNPJ inválido';
        } else if ($this->verificar_cpf_cnpj($this->input->post('cpf_cnpj')) === false) {
            $cadastrar = false;
            $dados_view['msg'] = 'CNPJ já cadastrado';
        } else if ($this->verificar_email($this->input->post('email')) === false) {
            $cadastrar = false;
            $dados_view['msg'] = 'E-mail já cadastrado';
        } /* else if ($this->input->post('senha') != $this->input->post('confirma_senha')) {
          $dados_view['msg'] = 'Os campos Senha e Confirma Senha devem ser iguais';
          } */ else if (
                $this->input->post('responsavel') != "" &&
                $this->input->post('nome_nm_fantasia') != "" &&
                $this->input->post('razao_social') != "" &&
                $this->input->post('telefone') != "" &&
                $this->input->post('email') != "" &&
                $this->input->post('rua') != "" &&
                $this->input->post('bairro') != "" &&
                $this->input->post('sigla_estado') != "" &&
                $this->input->post('id_cidade') != "" &&
                $this->input->post('cpf_cnpj') != "" &&
                $cadastrar == true
        //$this->input->post('senha') != "" &&
        //$this->input->post('confirma_senha') != ""
        ) {
            $pessoa = $this->receber_dados_pessoa();

            $resposta = $this->m_cliente_juridico->cadastrar($pessoa);

            if ($resposta == true) {
                $dados_view['msg'] = "Cliente jurídico cadastrado com sucesso!";
                $dados_view['status'] = "sucesso";
            } else {
                $dados_view['msg'] = "Erro ao cadastrar Cliente Jurídico!";
            }
        }

        echo json_encode($dados_view);
    }

    public function editar() {
        verificar_permissao($this->session->userdata, array(ADMINISTRADOR, SUPORTE, ASSISTENCIA));
        $status_validacao = $this->validar(true);
        $dados_view = $this->get_dados_view('', 'Editar Clientes Jurídico', 'clientes juridicos');
        $dados_view['cnpj'] = $this->input->post('cpf_cnpj');
        $dados_view['id_pessoa'] = $this->input->post('id_pessoa');

        if (!$this->verificar_email($this->input->post('email'), $this->input->post('id_pessoa'))) {
            $status_validacao = false;
            $dados_view['valid_email'] = 'E-mail já cadastrado.';
        }

        if ($status_validacao) {
            $pessoa = $this->receber_dados_pessoa(true);

            $resposta = $this->m_cliente_juridico->atualizar($this->input->post('id_pessoa'), $pessoa);

            if ($resposta == true) {
                $dados_view['msg'] = "Cliente jurídico alterado com sucesso!";
                $dados_view['alterou'] = true;
            } else {
                $dados_view['msg'] = "Erro ao alterar cliente jurídico!";
                $dados_view['alterou'] = false;
            }
        } else {
            $this->load->model('Cidade_Model', 'm_cidade');
            $sigla = $this->input->post('sigla_estado');
            $dados_view['estado_selecionado'] = $sigla;
            $dados_view['estados'] = $this->m_estado->buscar_todos();
            $dados_view['cidade_selecionada'] = $this->input->post('id_cidade');
            $dados_view['cidades'] = $this->m_cidade->buscar_por_sigla($sigla);
            $dados_view['id_pessoa'] = $this->input->post('id_pessoa');
        }

        $this->carregar_pagina($dados_view, 'cliente_juridico/invalido_editar_cliente_juridico');
    }

    public function excluir($id) {
        verificar_permissao($this->session->userdata, array(ADMINISTRADOR, SUPORTE, ASSISTENCIA));
        $dados_view['msg'] = "Erro ao excluir cliente jurídico!";
        $dados_view['status'] = "erro";

        $this->load->model('OS_Assistencia_Model', 'm_os_assistencia');

        if (count($this->m_os_assistencia->buscar_por_id_cliente($id)) == 0) {
            if ($this->m_cliente_juridico->excluir($id)) {
                $dados_view['msg'] = "Cliente jurídico excluído com sucesso!";
                $dados_view['status'] = "sucesso";
            }
        } else {
            $dados_view['msg'] = "Este cliente está vinculado a pelo menos uma Ordem de Serviço e não pode ser excluído!";
            $dados_view['status'] = "erro";
        }

        echo json_encode($dados_view);
    }

    public function pagina_editar($id) {
        verificar_permissao($this->session->userdata, array(ADMINISTRADOR, SUPORTE, ASSISTENCIA));
        if ($id != null) {
            if (is_numeric($id)) {
                $cliente = $this->m_cliente_juridico->buscar_por_id($id);

                if (count($cliente) < 1) {
                    redirect('ClienteJuridico');
                }
                $dados_view = $this->get_dados_view('', 'Editar Clientes Jurídico', 'clientes juridicos');
                $dados_view['cliente'] = $cliente;
                $dados_view['estados'] = $this->m_estado->buscar_todos();
                $dados_view['estado_selecionado'] = $dados_view['cliente'][0]->sigla;
                $dados_view['cidade_selecionada'] = $dados_view['cliente'][0]->id_cidade;
                $dados_view['cidades'] = $this->m_cidade->buscar_por_sigla($dados_view['cliente'][0]->sigla);
                $this->carregar_pagina($dados_view, 'cliente_juridico/editar_cliente_juridico');
            } else {
                redirect(base_url('ClienteJuridico'));
            }
        } else {
            redirect(base_url('ClienteJuridico'));
        }
    }

    /**
     * faz a validação dos dados informados pelo usuário
     * caso seja o formulário de edição, é necessário passar por parametro 
     * o valor true
     * @param type $editar
     * @return type
     */
    public function validar($editar = false) {
        verificar_permissao($this->session->userdata, array(ADMINISTRADOR, SUPORTE, ASSISTENCIA));
        $this->form_validation->set_rules('responsavel', 'responsável', 'required|min_length[5]');
        $this->form_validation->set_rules('nome_nm_fantasia', 'nome fantasia', 'required|min_length[5]');
        $this->form_validation->set_rules('razao_social', 'razao social', 'required');
        $this->form_validation->set_rules('email', 'email', 'required|valid_email');
        $this->form_validation->set_rules('rua', 'rua', 'required');
        $this->form_validation->set_rules('bairro', 'bairro', 'required');
        $this->form_validation->set_rules('sigla_estado', 'estado', 'required');
        $this->form_validation->set_rules('id_cidade', 'cidade', 'required');

        if ($editar === false) {
            $this->form_validation->set_rules('cpf_cnpj', 'CNPJ', 'required');
            //$this->form_validation->set_rules('senha', 'senha', 'required|min_length[8]');
            //$this->form_validation->set_rules('confirma_senha', 'confirme senha', 'trim|required|matches[senha]');
        }


        return $this->form_validation->run();
    }

    public function pagina_cadastro() {
        verificar_permissao($this->session->userdata, array(ADMINISTRADOR, SUPORTE, ASSISTENCIA));
        $dados_view = $this->get_dados_view('', 'Cadastro de Cliente Jurídico', 'clientes juridicos');
        $dados_view['estados'] = $this->m_estado->buscar_todos();
        $this->carregar_pagina($dados_view, 'cliente_juridico/cad_cliente_juridico_1');
    }

    public function receber_dados_pessoa($editar = false) {
        verificar_permissao($this->session->userdata, array(ADMINISTRADOR, SUPORTE, ASSISTENCIA));
        $pessoa = array(
            'responsavel' => $this->input->post('responsavel'),
            'razao_social' => $this->input->post('razao_social'),
            'nome_nm_fantasia' => $this->input->post('nome_nm_fantasia'),
            'cpf_cnpj' => $this->input->post('cpf_cnpj'),
            'cep' => $this->input->post('cep'),
            'rg' => $this->input->post('rg'),
            'telefone' => $this->input->post('telefone'),
            'email' => $this->input->post('email'),
            'rua' => $this->input->post('rua'),
            'numero' => $this->input->post('numero'),
            'complemento' => $this->input->post('complemento'),
            'fl_funcionario' => false,
            'bairro' => $this->input->post('bairro'),
            'id_cidade' => $this->input->post('id_cidade')
        );

        if ($editar === FALSE) {
            $pessoa['senha'] = password_hash($this->input->post('senha'), PASSWORD_DEFAULT);
        }

        return $pessoa;
    }

    private function carregar_pagina($dados_view, $view) {
        $this->load->view('includes/cabecalho', $dados_view);
        $this->load->view(carregar_menu($this->session->userdata()), $dados_view);
        $this->load->view($view, $dados_view);
        $this->load->view('includes/rodape');
    }

}
