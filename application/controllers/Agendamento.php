<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Agendamento
 *
 * @author Helder dos Santos
 */
class Agendamento extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Agendamento_Model', 'm_agendamento');
    }

    public function index() {
        verificar_permissao($this->session->userdata(), array(ADMINISTRADOR, SUPORTE));
        $dados_view = get_dados_view('', 'Sua Agenda - (Agendamento Pendentes)', 'agendamento');
        $dados_view['agendamentos'] = $this->m_agendamento->buscar_todos_tecnico($this->session->userdata('id_pessoa'));
        $this->carregar_pagina($dados_view, "pagina_agenda");
    }

    public function cadastrar() {
        verificar_permissao($this->session->userdata(), array(ADMINISTRADOR, SUPORTE));
        $agendamento = $this->input->post();
        $dados_view = array(
            "status" => "erro",
            "msg" => "Erro ao cadastrar visita técnica."
        );

        if(new DateTime($agendamento['data']) >= new DateTime(date('Y-m-d'))) {
            if ($agendamento['data'] != "" && $agendamento['horario'] != "") {
                $agendamento['id_pessoa_tecnico'] = $this->session->userdata('id_pessoa');
                if ($this->m_agendamento->cadastrar($agendamento)) {
                    $dados_view = array(
                        "status" => "sucesso",
                        "msg" => "Visita técnica cadastrada com sucesso."
                    );
                }
            } else {
                $dados_view['msg'] = 'Infrome a data e o horário.';
            }
        }else{
            $dados_view['msg'] = 'A data informada é inválida.';
        }

        echo json_encode($dados_view);
    }

    public function editar($id_agendamento) {
        verificar_permissao($this->session->userdata(), array(ADMINISTRADOR, SUPORTE));
        $dado_view['status'] = 'erro';

        if ($this->m_agendamento->alterar($this->input->post(), $id_agendamento)) {
            $dado_view['status'] = 'sucesso';
        }

        echo json_encode($dado_view);
    }

    public function buscar_por_id_os($id_ordem_de_servico) {
        if ($this->session->userdata('tipo_usuario') != null) {
            $dados_view['agendamentos'] = $this->m_agendamento->buscar_por_id_os_tecnico($id_ordem_de_servico, $this->session->userdata('id_pessoa'));
            echo json_encode($dados_view);
        } else {
            redirect(base_url());
        }
    }

    public function buscar_por_tecnico() {
        if ($this->session->userdata('tipo_usuario') != null) {
            $dados_view['agendamentos'] = $this->m_agendamento->buscar_todos_tecnico($this->session->userdata('id_pessoa'));
            $dados_view['status'] = 'sucesso';
            echo json_encode($dados_view);
        } else {
            redirect(base_url());
        }
    }

    private function carregar_pagina($dados_view, $view) {
        $this->load->view('includes/cabecalho', $dados_view);
        $this->load->view(carregar_menu($this->session->userdata()), $dados_view);
        $this->load->view($view, $dados_view);
        $this->load->view('includes/rodape');
    }

}
