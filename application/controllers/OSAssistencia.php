<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class OSAssistencia extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('OS_Assistencia_Model', 'm_os_assistencia');
        $this->load->model('Status_Os_Model', 'm_status_os');
        $this->load->model('Funcionario_Model', 'm_funcionario');
        $this->load->model('Autorizada_Model', 'm_autorizada');
        verificar_permissao($this->session->userdata(), array(ASSISTENCIA, ATENDENTE, ADMINISTRADOR));
    }

    public function index() {
        $this->buscar_todos();
    }

    public function buscar_todos($id_inicio = 0) {
        $this->load->library('pagination');
        $dados_view = get_dados_view('', 'Painel de OS - Assistência', 'OS-Assistencia');
        $dados_view['os'] = $this->m_os_assistencia->buscar_todos($id_inicio);
        $dados_view['status_os'] = $this->m_status_os->buscar_todos();
        $dados_view['tecnicos'] = $this->m_funcionario->buscar_todos_assistencia();
        $dados_view['funcionarios'] = $this->m_funcionario->listar_todos();
        $config_paginacao = config_paginacao('OSAssistencia/buscar_todos/', $this->m_os_assistencia->contar_todas());

        $this->pagination->initialize($config_paginacao);

        $dados_view['paginacao'] = $this->pagination->create_links();
        $dados_view['tem_pagina'] = true;

        $this->carregar_pagina($dados_view, "lista_os_assistencia");
    }

    public function buscar($id_inicio = 0, $id_tecnico = 0, $id_usuario = 0, $id_status = 0, $protocolo = "") {
        $this->load->library('pagination');
        $config_paginacao = config_paginacao('OSAssistencia/buscar/', $this->m_os_assistencia->contar_todas_busca($id_tecnico, $id_usuario, $id_status, $protocolo));
        $this->pagination->initialize($config_paginacao);

        $dados_view['status'] = "erro";
        $dados_view['smg'] = "Nenhuma Ordem de Serviço encontrada com esses parâmetros";
        $dados_view['paginacao'] = $this->pagination->create_links();
        $dados_view['ordens_de_servico'] = $this->m_os_assistencia->buscar($id_inicio, $id_tecnico, $id_usuario, $id_status, $protocolo);

        if (count($dados_view['ordens_de_servico'] > 0)) {
            $dados_view['status'] = "sucesso";
        }
        echo json_encode($dados_view);



        /* $this->load->library('pagination');
          $dados_view = get_dados_view('', 'Pesquisar por OS - Assistência', 'OS-Assistencia');
          $dados_view['os'] = $this->m_os_assistencia->buscar_todos($id_inicio);
          $dados_view['status_os'] = $this->m_status_os->buscar_todos();
          $dados_view['tecnicos'] = $this->m_funcionario->buscar_todos_assistencia();
          $dados_view['funcionarios'] = $this->m_funcionario->listar_todos();
          $config_paginacao = config_paginacao('OSAssistencia/buscar/', $this->m_os_assistencia->contar_todas());

          $this->pagination->initialize($config_paginacao);

          $dados_view['paginacao'] = $this->pagination->create_links();
          $dados_view['tem_pagina'] = true;

          $this->carregar_pagina($dados_view, "lista_os_assistencia"); */
    }

    public function abrir($id_ordem_de_servico, $paramJson = false) {
        if (is_numeric($id_ordem_de_servico)) {
            $os = $this->m_os_assistencia->buscar_por_id($id_ordem_de_servico);

            if ($paramJson === false) {
                if (!empty($os)) {
                    $dados_view = get_dados_view('', 'OS - Assistência', 'OS-Assistencia');
                    $dados_view['ordem_de_servico'] = $os;
                    $dados_view['status_os'] = $this->m_status_os->buscar_todos();
                    $this->carregar_pagina($dados_view, "abrir_decricao_cliente");
                } else {
                    redirect('OSAssistencia');
                }
            } else {
                $dados_view['status'] = 'sucesso';
                $dados_view['ordem_de_servico'] = $os;
                echo json_encode($dados_view);
            }
        } else {
            redirect('OSAssistencia');
        }
    }

    public function buscarPorFuncionario() {
        
    }

    public function editar() {
        $os_assistencia = array('id_status' => $this->input->post('id_status_os'));
        $id_ordem_de_servico = $this->input->post('id_ordem_de_servico');
        $dados_view['msg'] = "Erro ao alterar Ordem de Serviço!";
        $dados_view['status'] = "erro";
        $pode_editar = true;

        if ($this->input->post('id_status_os') != "" && $this->input->post('id_ordem_de_servico') != "") {

            if ($this->is_concluida($id_ordem_de_servico)) {
                $dados_view['msg'] = "Após concluída ou fechada a OS não pode ser editada!";
                $pode_editar = false;
            }

            if ($pode_editar) {
                $status = $this->m_status_os->buscar_por_id($os_assistencia['id_status']);

                if ($status->id_status_os == $os_assistencia['id_status']) {
                    if ($status->fl_concluido == true) {
                        $os_assistencia['data_conclusao'] = date('Y-m-d');
                    }
                }

                if ($this->m_os_assistencia->alterar($id_ordem_de_servico, $os_assistencia)) {

                    if ($this->input->post('fl_enviar_email') === "true") {
                        $this->enviar_email($this->input->post('id_cliente'), "Ordem de Serviço - Assistência Técnica", "O status da Ordem de Serviço com o protocolo de número " . $this->input->post('protocolo') . " foi alterado para " . $this->input->post('nome_status'));
                    }

                    if ($this->input->post('fl_enviar_sms') === true) {
                        $this->enviar_sms($os_suporte['id_cliente'], 'CDC Tecnologia: Acabamos de abrir uma nova Ordem de Serviço do Suporte para você!');
                    }

                    //$this->adicionar_historico($id_ordem_de_servico, "Mudou o status da OS para " . $this->input->post('nome_status'));
                    $dados_view['msg'] = "Alteração realizada com sucesso.";
                    $dados_view['status'] = "sucesso";
                }
            }
        }

        echo json_encode($dados_view);
    }

    /**
     * Verifica se a OS foi concluída, se sim retorna TRUE se não retorna FALSE
     * @param type $id_ordem_de_servico
     * @return boolean
     */
    private function is_concluida($id_ordem_de_servico) {
        $os = $this->m_os_assistencia->buscar_por_id($id_ordem_de_servico);

        if (count($os) > 0) {
            if ($os[0]->fl_concluido == true) {
                return true;
            }
        }

        return false;
    }

    public function cadastrar() {
        $os_assistencia = $this->input->post();
        $os_assistencia['fl_suporte'] = false;
        $os_assistencia['data_abertura'] = date('Y-m-d');
        $os_assistencia['id_responsavel_abertura'] = $this->session->userdata('id_pessoa');
        $continua = false;
        $dados_view['msg'] = "Preencha todos os campos!";
        $dados_view['status'] = "erro";

        //verifica se o está sendo recebido algum arquivo
        if (!empty($_FILES['anexo']['name'])) {
            $resposta = $this->upload();
            if ($resposta === null || $resposta === true) {
                $pasta = md5($this->input->post('id_cliente'));
                $arquivo = "assets/uploads/" . $pasta . "/" . str_replace(' ', '_', $_FILES['anexo']['name']);
                $os_assistencia['anexo'] = $arquivo;
                $continua = true;
            } else {
                $dados_view['msg'] = $resposta;
            }
        } else {
            $continua = true;
        }

        if ($continua && $this->input->post('id_cliente') !== "" && $this->input->post('id_tecnico') !== "" && $this->input->post('id_status') !== "" && $this->input->post('nome_equipamento_recebido') !== "" && $this->input->post('descricao_problema') !== "") {
            $os_assistencia['fl_enviar_sms'] = $this->input->post('fl_enviar_sms') != "" ? true : false;
            $os_assistencia['fl_enviar_email'] = $this->input->post('fl_enviar_email') != "" ? true : false;
            $resultato = $this->m_os_assistencia->cadastrar($os_assistencia);
            if ($resultato['status'] === true) {
                //$this->adicionar_historico($resultato['id_ordem_de_servico'], "Abertura da Ordem de Serviço");

                if ($os_assistencia['fl_enviar_email'] === true) {
                    $this->enviar_email($this->input->post('id_cliente'), "Ordem de Serviço - Assistência Técnica", "Acabamos de abrir uma nova Ordem de Serviço da Assistência Técnica para você.<br><br><strong>Agradecemos a Preferência!</strong>");
                }

                if ($os_assistencia['fl_enviar_sms'] === true) {
                    $this->enviar_sms($os_assistencia['id_cliente'], 'CDC Tecnologia: Acabamos de abrir uma nova Ordem de Serviço da Assistência para você!');
                }

                $dados_view['msg'] = "Ordem de Serviço cadastrada com sucesso!";
                $dados_view['status'] = "sucesso";
            } else {
                $dados_view['msg'] = "Erro ao cadastrar Ordem de Serviço!";
            }
        }

        echo json_encode($dados_view);
    }

    public function pagina_cadastro() {
        $dados_view = get_dados_view('', 'Nova OS - Assistência', 'OS-Assistencia');
        $dados_view['protocolo'] = $this->gerar_protocolo();
        $dados_view['status_os'] = $this->m_status_os->buscar_todos();
        $dados_view['tecnicos'] = $this->m_funcionario->buscar_todos_assistencia();
        $this->carregar_pagina($dados_view, "cad_os_assistencia");
    }

    private function gerar_protocolo() {
        $quantidade = $this->m_os_assistencia->contar_para_protocolo();
        $data = date('d/m/Y');
        $data = explode('/', $data);

        return $data[2] . $data[1] . $data[0] . ($quantidade + 1);
    }

    private function upload() {
        if (!empty($_FILES['anexo']['name'])) {
            $folder = md5($this->input->post('id_cliente'));
            $path = "assets/uploads/" . $folder;

            if (!is_dir($path)) {
                mkdir($path, 0777, $recursive = true);
            }

            $config['upload_path'] = $path;
            $config['allowed_types'] = 'gif|jpg|png|pdf';

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('anexo')) {
                return $this->upload->display_errors();
            } else {
                return true;
            }
        } else {
            return null;
        }
    }

    public function buscar_custos($id_ordem_de_servico) {
        $this->load->model('Atividade_Model', 'm_atividade');
        $this->load->model('Equipamento_Model', 'm_equipamento');
        $dados_view = array(
            "status" => "sucesso",
            "msg" => "Funcionando busca de dados da form Custos",
            "atividades" => $this->m_atividade->buscar_todos(0, false),
            "equipamentos" => $this->m_equipamento->buscar_todos(0, false),
            "atividades_os" => $this->m_os_assistencia->buscar_atividades($id_ordem_de_servico),
            "equipamentos_os" => $this->m_os_assistencia->buscar_equipamentos($id_ordem_de_servico)
        );
        echo json_encode($dados_view);
    }

    /**
     * Adiciona uma atorizada a uma OS da Assistencia técnica
     */
    public function adicionarAutorizada() {
        $id_ordem_de_servico = $this->input->post('id_ordem_de_servico');
        $autorizada = array(
            'id_autorizada' => $this->input->post('id_autorizada'),
            'descricao_problema_autorizada' => $this->input->post('descricao_problema_autorizada')
        );

        $dados_view = array(
            "status" => "erro",
            "msg" => "Erro ao adicionar a autorizada",
            "autorizada" => $this->m_autorizada->buscar_por_id($this->input->post('id_autorizada'))
        );

        if ($this->is_concluida($id_ordem_de_servico) == false) {
            if ($this->input->post('id_autorizada') != "" && $this->input->post('descricao_problema_autorizada') != "") {
                if ($this->m_os_assistencia->alterar($id_ordem_de_servico, $autorizada)) {
                    //$this->adicionar_historico($id_ordem_de_servico, "Produto enviado para autorizada");
                    if ($this->input->post('fl_enviar_email') === "true") {
                        $this->enviar_email($this->input->post('id_cliente'), "Ordem de Serviço - Assistência Técnica", "Nosso técnico acaba de enviar seu equipamento para a autorizada.");
                    }
                    $dados_view['status'] = "sucesso";
                    $dados_view['msg'] = "Equipamento adicionado a autorizada com sucesso";
                }
            } else {
                $dados_view['status'] = "erro";
                $dados_view['msg'] = "Preencha os campos obrigatórios.";
            }
        } else {
            $dados_view['msg'] = "Após concluída ou fechada a OS não pode ser editada!";
        }

        echo json_encode($dados_view);
    }

    /**
     * Responsável cadastrar as informações de um laudo técnico de uma O da Assistência técnica
     */
    public function cadastrar_laudo() {
        //$dados_view['dados'] = $this->input->post();
        $dados_view['msg'] = "Erro ao salvar informações";
        $dados_view['status'] = "erro";
        $id_os = $this->input->post('id_os');
        if ($this->is_concluida($id_os) == false) {
            $ordem_de_servico = array(
                'laudo_tecnico' => $this->input->post('laudo_tecnico')
            );

            if ($this->input->post('fl_enviar_email') === "true") {
                $this->enviar_email($this->input->post('id_cliente'), "Ordem de Serviço - Assistência Técnica", "Nosso técnico acabou de realizar a seguinte análise técnica de seu equipamento: " . $this->input->post('laudo_tecnico'));
            }

            $dados_view['laudo'] = $ordem_de_servico;


            if (
                    $this->m_os_assistencia->alterar($id_os, $ordem_de_servico) &&
                    $this->adicionar_atividades($this->input->post('ativ_adicionar'), $id_os) &&
                    $this->adicionar_equipamentos($this->input->post('equi_adicionar'), $id_os) &&
                    $this->excluir_atividades_os($this->input->post('ativ_excluir')) &&
                    $this->excluir_equipamentos_os($this->input->post('equi_excluir'))
            ) {
                //$this->adicionar_historico($id_os, "Adicionou o laudo técnico");
                $dados_view['msg'] = "Dados salvos com sucesso";
                $dados_view['status'] = "sucesso";
            }
        } else {
            $dados_view['msg'] = "Após concluída ou fechada a OS não pode ser editada!";
        }

        echo json_encode($dados_view);
    }

    /**
     * Adiciona tividades a Ordem de serviço
     * Formato para cadastro de atividade: id-valor|id-valor|id-valor
     * 
     * @param type $atividades
     * @param type $id_ordem_de_servico
     * @return boolean
     */
    private function adicionar_atividades($atividades, $id_ordem_de_servico) {
        if ($atividades != "") {
            //os dados da atividade vem no formato id-valor|id-valor|id-vaor
            $atividades = explode('|', $atividades);
            $atividadeCad = array();

            foreach ($atividades as $a) {
                $elem = explode("-", $a);
                $atividadeCad[] = array(
                    'id_atividade' => $elem[0],
                    'valor' => $elem[1],
                    'id_ordem_de_servico' => $id_ordem_de_servico
                );
            }

            if ($this->m_os_assistencia->adicionar_atividade($atividadeCad)) {
                return true;
            }
        } else {
            return true;
        }

        return false;
    }

    /**
     * Adiciona equipamentos a uma ordem de serviço
     * Formato para cadastro de equipamentos: id-valor|id-valor|id-valor
     * 
     * @param type $equipamentos
     * @param type $id_ordem_de_servico
     * @return boolean
     */
    private function adicionar_equipamentos($equipamentos, $id_ordem_de_servico) {
        if ($equipamentos != "") {
            //os dados do equipamento vem no formato id-valor|id-valor|id-vaor
            $equipamentos = explode('|', $equipamentos);
            $equipamentosCad = array();

            foreach ($equipamentos as $a) {
                $elem = explode("-", $a);
                $equipamentosCad[] = array(
                    'id_equipamento' => $elem[0],
                    'valor' => $elem[1],
                    'id_ordem_de_servico' => $id_ordem_de_servico
                );
            }

            if ($this->m_os_assistencia->adicionar_equipamento($equipamentosCad)) {
                return true;
            }
        } else {
            return true;
        }

        return false;
    }

    /**
     * exclui atividades vinculada a uma OS
     * O formatos da String deve ser id|id|id|id
     * @param type $atividades = String
     * @return boolean
     */
    private function excluir_atividades_os($atividades) {
        if ($atividades != "") {
            $atividades = explode("|", $atividades);
            foreach ($atividades as $a) {
                if (!$this->m_os_assistencia->excluir_atividade($a)) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * exclui um equipamento vinculada a uma OS
     * O formato dos equipamentos deve ser: id|id|id|id
     * @param type $equipamentos = String
     * @return boolean
     */
    private function excluir_equipamentos_os($equipamentos) {
        if ($equipamentos != "") {
            $equipamentos = explode("|", $equipamentos);
            foreach ($equipamentos as $e) {
                if (!$this->m_os_assistencia->excluir_equipamento($e)) {
                    return false;
                }
            }
        }

        return true;
    }

    public function buscar_cliente() {
        $this->load->model('Cliente_Fisico_Model', 'm_cliente_fisico');
        $nomeCliente = $this->input->post('cliente');
        echo json_encode($this->m_cliente_fisico->buscarParaOS($nomeCliente));
    }

    private function carregar_pagina($dados_view, $view) {
        $this->load->view('includes/cabecalho', $dados_view);
        $this->load->view(carregar_menu($this->session->userdata()), $dados_view);
        $this->load->view("os_assistencia/" . $view, $dados_view);
        $this->load->view('includes/rodape');
    }

    private function adicionar_historico($id_ordem_de_servico, $descricao) {
        $this->load->model('Historico_Os_Model', 'm_historico_os');

        $historico_os = array(
            'id_ordem_de_servico' => $id_ordem_de_servico,
            'descricao' => $descricao,
            'responsavel' => $this->session->userdata('nome')
        );

        $this->m_historico_os->cadastrar($historico_os);
    }

    public function buscar_historico_os() {
        $this->load->model('Historico_Os_Model', 'm_historico_os');
        $id_os = $this->input->post('id_ordem_de_servico');

        $dados_view['historico'] = $this->m_historico_os->buscar_por_id_os($id_os);

        echo json_encode($dados_view);
    }

    public function gerar_pdf($id_ordem_de_servico) {
        if (is_numeric($id_ordem_de_servico)) {
            $os = $this->m_os_assistencia->buscar_por_id($id_ordem_de_servico);


            if (count($os) > 0) {
                $this->load->library('Pdf');
                $dados_view = array(
                    "ordem_de_servico" => $os,
                    "atividades_os" => $this->m_os_assistencia->buscar_atividades($id_ordem_de_servico),
                    "equipamentos_os" => $this->m_os_assistencia->buscar_equipamentos($id_ordem_de_servico),
                    "autorizada" => $this->m_autorizada->buscar_por_id($os[0]->id_autorizada)
                );

                $this->pdf->gerar('os_assistencia/relatorio_pdf', $dados_view);
            } else {
                redirect(base_url('Pagina_Erro'));
            }
        } else {
            redirect(base_url('Pagina_Erro'));
        }
    }

    private function enviar_email($id_cliente, $titulo, $mensagem) {
        $this->load->library("Send_Email");
        $this->load->model('Cliente_Fisico_Model', 'm_cliente_fisico');
        $cliente = $this->m_cliente_fisico->buscar_por_id($id_cliente);

        if (count($cliente) === 0) {
            $this->load->model('Cliente_Juridico_Model', 'm_cliente_juridico');
            $cliente = $this->m_cliente_juridico->buscar_por_id($id_cliente);
        }

        if (count($cliente) > 0) {
            if ($this->send_email->enviar_email("CDC Tecnologia - Ordem de Serviço", $mensagem, $cliente[0]->email, $titulo)) {
                return true;
            }
        }

        return false;
    }

    private function enviar_sms($id_cliente, $mensagem) {
        $this->load->library("Send_Sms");
        $this->load->model('Cliente_Fisico_Model', 'm_cliente_fisico');
        $cliente = $this->m_cliente_fisico->buscar_por_id($id_cliente);

        if (count($cliente) === 0) {
            $this->load->model('Cliente_Juridico_Model', 'm_cliente_juridico');
            $cliente = $this->m_cliente_juridico->buscar_por_id($id_cliente);
        }

        if (count($cliente) > 0) {
            if ($this->send_sms->enviar_sms($cliente[0]->telefone, $mensagem)) {
                return true;
            }
        }

        return false;
    }

}
