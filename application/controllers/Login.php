<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("Usuario_Model", "m_usuario");
    }

    public function index() {
        $this->load->view('login');
        if ($this->session->userdata("logado") === true) {
            redirect(base_url());
        }
    }

    public function entrar() {

        if ($this->session->userdata("logado") === true) {
            redirect(base_url());
        }

        $dados_view = array(
            'msg' => 'Usuário ou senha incorreta',
            'status' => 'erro',
            'email' => $this->input->post('email')
        );

        if ($this->input->post('codigo') == "") {
            $dados_view['msg'] = 'Informe o código de segurança';
        } else if ($this->input->post('codigo') != $this->session->userdata('cod_captcha')) {
            $dados_view['msg'] = 'Código de segurança não confere';
        } else {
            if ($this->input->post('email') == "" || $this->input->post('senha') == "") {
                $dados_view['msg'] = 'Informe usuário e senha';
            } else {
                $usuario = $this->m_usuario->buscar($this->input->post('email'));
                $dados_sessao = null;

                //verifica se encontrou o usuário
                if (count($usuario) > 0) {
                    //verifica se a senha informada está correta
                    if (password_verify($this->input->post('senha'), $usuario[0]->senha) === true) {
                        //verifica se é um funcio~ário
                        //se sim, buscas os dados do funcionário
                        //senão, busca coloca o tipo de usuário como cliente
                        if ($usuario[0]->fl_funcionario == true) {
                            $this->load->model("Funcionario_Model", 'm_funcionario');
                            $usuario = $this->m_funcionario->buscar_por_id($usuario[0]->id_pessoa);

                            $dados_sessao = array(
                                'id_pessoa' => $usuario[0]->id_pessoa,
                                "nome" => $usuario[0]->nome_nm_fantasia,
                                "email" => $usuario[0]->email,
                                "tipo_usuario" => $usuario[0]->nome_tipo_funcionario,
                                "logado" => true
                            );
                        } else {
                            $dados_sessao = array(
                                'id_pessoa' => $usuario[0]->id_pessoa,
                                "nome" => $usuario[0]->nome_nm_fantasia,
                                "email" => $usuario[0]->email,
                                "tipo_usuario" => "Cliente",
                                "logado" => true
                            );
                        }
                        $this->session->set_userdata($dados_sessao);
                        $dados_view['status'] = 'ok';
                    }
                }
            }
        }
        echo json_encode($dados_view);
    }

    /**
     * fazer logout no sistema e redirecionar para a tela inicial do sistema
     */
    public function sair() {
        $this->session->sess_destroy();
        redirect(base_url("Login"));
    }

    public function gerar_captcha() {

        $imagens = array("fundo_captcha", "fundo_captcha2", "fundo_captcha3");

        $cod = substr(md5(time()), 0, 5);
        $this->session->set_userdata(array('cod_captcha' => $cod));

        $imagemCaptcha = imagecreatefrompng(base_url("assets/img/" . $imagens[rand(0, 2)] . ".png"));

        $fonteCaptcha = imageloadfont("assets/fontes/anonymous.gdf");

        $corCaptcha = imagecolorallocate($imagemCaptcha, 255, 255, 255);

        imagestring($imagemCaptcha, $fonteCaptcha, 50, 3, $cod, $corCaptcha);

        imagepng($imagemCaptcha);

        header('Content-Disposition: Attachment;filename=image.png');
        header('Content-Type: image/png');

        imagedestroy($imagemCaptcha);
    }

    public function pagina_recuperar_senha() {
        $this->load->view('recuperar_senha');
    }

    public function pagina_alterar_senha($chave = "") {
        if ($chave != "") {
            $dados_view = array('chave' => $chave);
            $this->load->view('alterar_senha', $dados_view);
        } else {
            redirect(base_url('Login'));
        }
    }

    public function recuperar_senha() {
        $dados_view = array(
            'msg' => 'Usuário não cadastrado',
            'status' => 'erro',
            'email' => $this->input->post('email')
        );

        $usuario = $this->input->post('email');

        if ($this->input->post('codigo') == "") {
            $dados_view['msg'] = 'Informe o código de segurança';
        } else if ($this->input->post('codigo') != $this->session->userdata('cod_captcha')) {
            $dados_view['msg'] = 'Código de segurança não confere';
        } else {
            $usuario = $this->m_usuario->buscar($usuario);

            if (count($usuario) > 0) {
                $codigo = md5($this->input->post('mail') . date('Y-m-d H:i'));

                $codigo_criado = $this->m_usuario->adicionar_cod_recupera_senha(array('chave' => $codigo, 'id_pessoa' => $usuario[0]->id_pessoa));

                if ($codigo_criado) {
                    $email_enviado = $this->enviar_email($usuario, $codigo);

                    if ($email_enviado) {
                        $dados_view = array(
                            'msg' => 'Confira o e-mail que enviamos para você e altere sua senha',
                            'status' => 'sucesso'
                        );
                    } else {
                        $dados_view = array(
                            'msg' => 'Erro ao enviar e-mail. Tente novamente mais tarde.'
                        );
                    }
                } else {
                    $dados_view = array(
                        'msg' => 'Não foi possível gerar o link de recuperação de senha. Tente novamente mais tarde.'
                    );
                }
            }
        }

        echo json_encode($dados_view);
    }

    public function alterar_senha() {
        $chave = $this->input->post('chave');
        $senha = $this->input->post('senha');

        $dados_view = array(
            'msg' => 'Erro ao alterar senha',
            'status' => 'erro'
        );

        if ($this->input->post('codigo') == "") {
            $dados_view['msg'] = 'Informe o código de segurança';
        } else if ($this->input->post('codigo') != $this->session->userdata('cod_captcha')) {
            $dados_view['msg'] = 'Código de segurança não confere';
        } else {

            $recupera_senha = $this->m_usuario->buscar_chave_recuperar_senha($chave);

            if ($recupera_senha[0]->bloqueado == false) {
                if (strlen($senha) >= 8) {
                    if (count($recupera_senha) > 0) {
                        $alterado = $this->m_usuario->alterar_senha($recupera_senha[0]->id_pessoa, array('senha' => password_hash($senha, PASSWORD_DEFAULT)));
                        if ($alterado) {
                            $this->m_usuario->bloquear_recupera_senha($recupera_senha[0]->id_recupera_senha);
                            $dados_view = array(
                                'msg' => 'Senha alterada com sucesso.',
                                'status' => 'sucesso'
                            );
                        }
                    } else {
                        $dados_view = array(
                            'msg' => "Este link expirou, solicite uma nova alteração de senha a partir da janela de login."
                        );
                    }
                } else {
                    $dados_view = array(
                        'msg' => "A senha deve ter no mínimo 8 caracteres."
                    );
                }
            } else {
                $dados_view = array(
                    'msg' => "Este link expirou, solicite uma nova alteração de senha a partir da janela de login."
                );
            }
        }

        echo json_encode($dados_view);
    }

    public function enviar_email($pessoa, $chave) {
        $this->load->library("Send_Email");

        $msg = "<p style='font-family: arial; text-align:center' >Recebemos uma solicitação de alteração de senha. <br>Clique no link abaixo para realizar a alteração."
                . "<br><br><a href='" . base_url('Login/pagina_alterar_senha/' . $chave) . "' style='color:#fff; text-decoration:none; background-color:#4CAF50; padding: 10px 20px; margin-top: 10px; text-align: center; display: inline-block; font-family:arial'>ALTERAR SENHA</a> </p>";

        if (count($pessoa) > 0) {
            if ($this->send_email->enviar_email("CDC Tecnologia - Recuperação de Senha", $msg, $pessoa[0]->email, "CDC Tecnologia - Recuperação de Senha")) {
                return true;
            }
        }

        return false;
    }

}
