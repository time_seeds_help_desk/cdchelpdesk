<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'application/controllers/Pessoa.php';

class Funcionario extends Pessoa {

    public function __construct() {
        parent::__construct();
        $this->load->model("Funcionario_Model", 'm_funcionario');
        $this->load->model("Estado_Model", 'm_estado');
        $this->load->model("Tipo_Funcionario_Model", 'm_tipo_funcionario');
        $this->load->model('Cidade_Model', 'm_cidade');
    }

    public function index() {
        verificar_permissao($this->session->userdata(), array(ADMINISTRADOR));
        $this->buscar_todos();
    }

    public function buscar_todos($id_inicio = 0) {
        verificar_permissao($this->session->userdata(), array(ADMINISTRADOR));
        $dados_view = $this->get_dados_view('', 'Funcionários Cadastrados', 'funcionarios');
        $dados_view['funcionarios'] = $this->m_funcionario->buscar_todos($id_inicio);
        $dados_view['pag_inicio'] = $id_inicio + 1;

        $this->load->library('pagination');

        $config_paginacao = config_paginacao('Funcionario/buscar_todos', $this->m_funcionario->contar_todos());

        $this->pagination->initialize($config_paginacao);

        $dados_view['paginacao'] = $this->pagination->create_links();
        $dados_view['tem_pagina'] = true;

        $this->carregar_pagina($dados_view, "funcionario/lista_funcionarios");
    }

    public function pagina_cadastro() {
        verificar_permissao($this->session->userdata(), array(ADMINISTRADOR));

        $dados_view = $this->get_dados_view('', 'Cadastro de Funcionário', 'funcionarios');
        $dados_view['estados'] = $this->m_estado->buscar_todos();
        $dados_view['tipos_funcionario'] = $this->m_tipo_funcionario->buscar_todos();
        $this->carregar_pagina($dados_view, 'funcionario/cad_funcionario');
    }

    public function buscar() {
        verificar_permissao($this->session->userdata(), array(ADMINISTRADOR));

        $busca = $this->input->post('nome');
        $result = array();

        if ($busca != '') {

            $result = $this->m_funcionario->buscar_por_nome($busca);

            $dados_view = $this->get_dados_view('', 'Funcionários Cadastrados', 'funcionarios');
            $dados_view['funcionarios'] = $result;
            $dados_view['busca'] = $busca;

            $this->carregar_pagina($dados_view, "funcionario/busca_funcionario");
        } else {
            redirect(base_url('Funcionario'));
        }
    }

    public function cadastrar() {
        verificar_permissao($this->session->userdata(), array(ADMINISTRADOR));

        $status_validacao = $this->validar();
        $dados_view = $this->get_dados_view('', 'Cadastro de Funcionário', 'funcionarios');
        $dados_view['tipo_func_selecionado'] = $this->input->post("id_tipo_funcionario");

        if (!$this->verificar_email($this->input->post('email'))) {
            $status_validacao = false;
            $dados_view['valid_email'] = 'E-mail já cadastrado';
        }

        if ($status_validacao) {
            $pessoa = $this->receber_dados_pessoa();

            $resposta = $this->m_funcionario->cadastrar($pessoa);

            if ($resposta == true) {
                $dados_view['msg'] = "Funcionário cadastrado com sucesso!";
                $dados_view['cadastrou'] = true;
            } else {
                $dados_view['msg'] = "Erro ao cadastrar funcionário!";
                $dados_view['cadastrou'] = false;
            }
        } else {
            $dados_view['tipos_funcionario'] = $this->m_tipo_funcionario->buscar_todos();
        }


        $this->carregar_pagina($dados_view, 'funcionario/cad_funcionario');
    }

    /**
     * Método responsável por editar os dados de um funcionário
     */
    public function editar() {
        verificar_permissao($this->session->userdata(), array(ADMINISTRADOR));

        $status_validacao = $this->validar(true);
        $dados_view = $this->get_dados_view('', 'Cadastro de Funcionário', 'funcionarios');


        if (!$this->verificar_email($this->input->post('email'), $this->input->post('id_pessoa'))) {
            $status_validacao = false;
            $dados_view['valid_email'] = 'E-mail já cadastrado';
        }



        if ($status_validacao) {
            $pessoa = $this->receber_dados_pessoa();

            $resposta = $this->m_funcionario->atualizar($this->input->post('id_pessoa'), $pessoa);

            if ($resposta == true) {
                $dados_view['msg'] = "Funcionário alterado com sucesso!";
                $dados_view['cpf_cnpj'] = $this->input->post('cpf_cnpj');
                $dados_view['alterou'] = true;
                $this->carregar_pagina($dados_view, 'funcionario/invalido_editar_funcionario');
            } else {
                $dados_view['msg'] = "Erro ao alterar funcionário!";
                $dados_view['cpf_cnpj'] = $this->input->post('cpf_cnpj');
                $dados_view['alterou'] = false;
                $this->carregar_pagina($dados_view, 'funcionario/invalido_editar_funcionario');
            }
        } else {

            $this->load->model('Cidade_Model', 'm_cidade');
            $sigla = $this->input->post('sigla_estado');
            $dados_view['estado_selecionado'] = $sigla;
            $dados_view['cpf_cnpj'] = $this->input->post('cpf_cnpj');
            $dados_view['cidade_selecionada'] = $this->input->post('id_cidade');
            $dados_view['id_pessoa'] = $this->input->post('id_pessoa');
            $dados_view['cidades'] = $this->m_cidade->buscar_por_sigla($sigla);
            $dados_view['estados'] = $this->m_estado->buscar_todos();
            $dados_view['tipos_funcionario'] = $this->m_tipo_funcionario->buscar_todos();


            $this->carregar_pagina($dados_view, 'funcionario/invalido_editar_funcionario');
        }
    }

    /**
     * Faz a exclusão dos dados de um funcionario
     * para isso é necessário informar o id des funcionário
     * @param type $id
     */
    public function excluir($id) {
        verificar_permissao($this->session->userdata(), array(ADMINISTRADOR));

        $dados_view['msg'] = "Você não pode excluir seu próprio registro!";
        $dados_view['status'] = "erro";

        if ($this->session->userdata('id_pessoa') != $id) {
            $dados_view['msg'] = "Erro ao Excluir Funcionário!";
            $dados_view['status'] = "erro";

            $this->load->model('OS_Assistencia_Model', 'm_os_assistencia');

            if (count($this->m_os_assistencia->buscar_por_id_tecnico($id)) == 0 && count($this->m_os_assistencia->buscar_por_id_responsavel($id)) == 0 ) {
                if ($this->m_funcionario->excluir($id)) {
                    $dados_view['msg'] = "Funcionário excluído com sucesso!";
                    $dados_view['status'] = "sucesso";
                }
            } else {
                $dados_view['msg'] = "Este funcionário está vinculado a pelo menos uma Ordem de Serviço e não pode ser excluído!";
                $dados_view['status'] = "erro";
            }
        }

        echo json_encode($dados_view);
    }

    /**
     * Carrega a página editar com os dados do funcionários
     * caso tente passar um id inválodo diretos pela URL
     * o site redireciona para a lista de funcionários
     * @param type $id
     */
    public function pagina_editar($id) {
        verificar_permissao($this->session->userdata(), array(ADMINISTRADOR));

        if ($id != null) {
            if (is_numeric($id)) {
                $funcionario = $this->m_funcionario->buscar_por_id($id);

                if (count($funcionario) < 1) {
                    redirect('Funcionario');
                }
                $dados_view = $this->get_dados_view('', 'Editar de Funcionário', 'funcionarios');
                $dados_view['funcionario'] = $funcionario;
                $dados_view['tipos_funcionario'] = $this->m_tipo_funcionario->buscar_todos();
                $this->carregar_pagina($dados_view, 'funcionario/editar_funcionario');
            } else {
                redirect(base_url('Funcionario'));
            }
        } else {
            redirect(base_url('Funcionario'));
        }
    }

    /**
     * faz a validação dos dados informados pelo usuário
     * caso seja o formulário de edição, é necessário passar por parametro 
     * o valor true
     * @param type $editar
     * @return type
     */
    public function validar($editar = false) {
        verificar_permissao($this->session->userdata(), array(ADMINISTRADOR));

        $this->form_validation->set_rules('nome_nm_fantasia', 'nome', 'required|min_length[5]');
        $this->form_validation->set_rules('email', 'email', 'required|valid_email');
        $this->form_validation->set_rules('telefone', 'telefpne', 'required');
        if ($editar == true) {
            if ($this->input->post('senha') != '') {
                $this->form_validation->set_rules('senha', 'senha', 'required|min_length[8]');
                $this->form_validation->set_rules('confirma_senha', 'confirme senha', 'trim|required|matches[senha]');
            }
        } else {
            $this->form_validation->set_rules('senha', 'senha', 'required|min_length[8]');
            $this->form_validation->set_rules('confirma_senha', 'confirme senha', 'trim|required|matches[senha]');
        }

        $this->form_validation->set_rules('id_tipo_funcionario', 'Nível de Acesso', 'required');

        return $this->form_validation->run();
    }

    private function carregar_pagina($dados_view, $view) {
        $this->load->view('includes/cabecalho', $dados_view);
        $this->load->view(carregar_menu($this->session->userdata()), $dados_view);
        $this->load->view("administrador/$view", $dados_view);
        $this->load->view('includes/rodape');
    }

    public function receber_dados_pessoa() {
        verificar_permissao($this->session->userdata(), array(ADMINISTRADOR));

        $pessoa = array(
            'nome_nm_fantasia' => $this->input->post('nome_nm_fantasia'),
            'telefone' => $this->input->post('telefone'),
            'email' => $this->input->post('email'),
            'fl_funcionario' => true,
            'id_tipo_funcionario' => $this->input->post('id_tipo_funcionario'),
            'senha' => password_hash($this->input->post('senha'), PASSWORD_DEFAULT)
        );

        return $pessoa;
    }

}
