<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Sistema
 *
 * @author Helder dos Santos
 */
class Sistema extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Sistema_Model', 'm_sistema');
    }

    public function index() {
        verificar_permissao($this->session->userdata(), array(ADMINISTRADOR));
        $dados_view = get_dados_view('', 'Sistemas Cadastrados', 'sistemas');
        $dados_view['sistemas'] = $this->m_sistema->buscar_todos();
        $this->carregar_pagina($dados_view, "lista_sistemas");
    }

    public function cadastrar() {
        verificar_permissao($this->session->userdata(), array(ADMINISTRADOR));
        $sistema = $this->input->post();
        $dados_view['msg'] = "Preencha todos os campos obrigatórios.";
        $dados_view['status'] = 'erro';

        if ($sistema['nome_sistema'] != "" && $sistema['fornecedor'] != "" && $sistema['valor_mensalidade'] != "" && $sistema['versao'] != "") {
            $sistema['valor_mensalidade'] = converte_valor_db($sistema['valor_mensalidade']);
            if ($this->m_sistema->cadastrar($sistema)) {
                $dados_view['msg'] = "Sistema cadastrado com sucesso.";
                $dados_view['status'] = 'sucesso';
            } else {
                $dados_view['msg'] = "Erro ao cadastrar sistema.";
            }
        }

        echo json_encode($dados_view);
    }

    public function editar() {
        verificar_permissao($this->session->userdata(), array(ADMINISTRADOR));
        $sistema = $this->input->post();
        $dados_view['msg'] = "Preencha todos os campos obrigatórios.";
        $dados_view['status'] = 'erro';
        $id = $sistema['id_sistema'];
        unset($sistema['id_sistema']);

        if ($sistema['nome_sistema'] != "" && $sistema['fornecedor'] != "" && $sistema['valor_mensalidade'] != "" && $sistema['versao'] != "") {
            $sistema['valor_mensalidade'] = converte_valor_db($sistema['valor_mensalidade']);
            if ($this->m_sistema->alterar($sistema, $id)) {
                $dados_view['msg'] = "Sistema alterado com sucesso.";
                $dados_view['status'] = 'sucesso';
            } else {
                $dados_view['msg'] = "Erro ao alterar sistema.";
            }
        }

        echo json_encode($dados_view);
    }

    public function excluir($id) {
        verificar_permissao($this->session->userdata(), array(ADMINISTRADOR));
        $dados_view['msg'] = "Erro ao excluir Sistema";
        $dados_view['status'] = "erro";

        if ($this->m_sistema->excluir($id)) {
            $dados_view['msg'] = "Sistema excluído com sucesso!";
            $dados_view['status'] = "sucesso";
        }

        echo json_encode($dados_view);
    }

    public function pagina_cadastro() {
        verificar_permissao($this->session->userdata(), array(ADMINISTRADOR));
        $dados_view = get_dados_view('', 'Cadastro de Sistema', 'sistemas');
        $this->carregar_pagina($dados_view, "cad_sistema");
    }

    public function pagina_editar($id_sistema) {
        verificar_permissao($this->session->userdata(), array(ADMINISTRADOR));
        $dados_view = get_dados_view('', 'Editar de Sistema', 'sistemas');
        $dados_view['sistema'] = $this->m_sistema->buscar_por_id($id_sistema);
        $this->carregar_pagina($dados_view, "editar_sistema");
    }

    private function carregar_pagina($dados_view, $view) {
        $this->load->view('includes/cabecalho', $dados_view);
        $this->load->view(carregar_menu($this->session->userdata()), $dados_view);
        $this->load->view("sistema/" . $view, $dados_view);
        $this->load->view('includes/rodape');
    }

}
