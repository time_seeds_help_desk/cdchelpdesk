<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'application/controllers/Pessoa.php';

/**
 * Controlador responsável pela realização do 
 * CRUD de Clientes físicos e pelo carregamento de views
 * @author Helder dos Santos Sousa
 */
class ClienteFisico extends Pessoa {

    /**
     * faz o carregamento das models Cliente_Físico, Estado e Cidade
     * para serem utilizadas nos métodos da classe
     */
    public function __construct() {
        parent::__construct();
        $this->load->model("Cliente_Fisico_Model", 'm_cliente_fisico');
        $this->load->model("Estado_Model", 'm_estado');
        $this->load->model('Cidade_Model', 'm_cidade');
        verificar_permissao($this->session->userdata, array(ADMINISTRADOR, ATENDENTE, ASSISTENCIA));
    }

    /**
     * Carrega a página inicial de Clientes Físicos
     * onde são listados todos os clientes físicos cadastrados no
     * sistema
     */
    public function index() {
        $this->buscar_todos();
    }

    public function buscar_todos($id_inicio = 0) {
        $this->load->library('pagination');
        $dados_view = $this->get_dados_view('', 'Clientes Físicos Cadastrados', 'clientes fisicos');
        $dados_view['clientes_fisicos'] = $this->m_cliente_fisico->buscar_todos($id_inicio);
        $dados_view['pag_inicio'] = $id_inicio + 1;

        $config_paginacao = config_paginacao('ClienteFisico/buscar_todos', $this->m_cliente_fisico->contar_todos());

        $this->pagination->initialize($config_paginacao);

        $dados_view['paginacao'] = $this->pagination->create_links();
        $dados_view['tem_pagina'] = true;

        $this->carregar_pagina($dados_view, "cliente_fisico/lista_clientes_fisicos");
    }

    public function pagina_editar($id) {
        if ($id != null) {
            if (is_numeric($id)) {
                $cliente = $this->m_cliente_fisico->buscar_por_id($id);

                if (count($cliente) < 1) {
                    redirect('ClienteFisico');
                }
                $dados_view = $this->get_dados_view('', 'Editar Clientes Físicos', 'clientes fisicos');
                $dados_view['cliente'] = $cliente;
                //$dados_view['id_pessoa'] = $id;
                $dados_view['estados'] = $this->m_estado->buscar_todos();
                $dados_view['estado_selecionado'] = $dados_view['cliente'][0]->sigla;
                $dados_view['cidade_selecionada'] = $dados_view['cliente'][0]->id_cidade;
                $dados_view['cidades'] = $this->m_cidade->buscar_por_sigla($dados_view['cliente'][0]->sigla);
                $this->carregar_pagina($dados_view, 'cliente_fisico/editar_cliente_fisico');
            } else {
                redirect(base_url('ClienteFisico'));
            }
        } else {
            redirect(base_url('ClienteFisico'));
        }
    }

    public function buscar() {
        $busca = $this->input->post('cpf_nome');
        $result = array();


        if ($busca != '') {

            if (is_numeric($this->input->post('cpf'))) {
                $busca = formatar_cpf($busca);
                $result = $this->m_cliente_fisico->buscar_por_cpf_cnpj($busca);
            } else {
                $result = $this->m_cliente_fisico->buscar_por_nome($busca);
            }

            $dados_view = $this->get_dados_view('', 'Clientes Físicos Cadastrados', 'clientes fisicos');
            $dados_view['clientes_fisicos'] = $result;
            $dados_view['busca'] = $busca;

            $this->carregar_pagina($dados_view, "cliente_fisico/busca_cliente_fisico");
        } else {
            redirect(base_url('ClienteFisico'));
        }
    }

    public function cadastrar() {
        $status_validacao = $this->validar();
        $dados_view = $this->get_dados_view('', 'Cadastro de Clinte Físico', 'clientes fisicos');


        if (!$this->verificar_email($this->input->post('email')) && $this->input->post('email') != "") {
            $status_validacao = false;
            $dados_view['valid_email'] = 'E-mail já cadastrado';
        }

        if (!$this->verificar_cpf_cnpj($this->input->post('cpf_cnpj'))) {
            $status_validacao = false;
            $dados_view['valid_cpf'] = 'CPF já cadastrado';
        }

        if ($status_validacao) {
            $pessoa = $this->receber_dados_pessoa();

            $resposta = $this->m_cliente_fisico->cadastrar($pessoa);

            if ($resposta == true) {
                $dados_view['msg'] = "Cliente físico cadastrado com sucesso!";
                $dados_view['cadastrou'] = true;
            } else {
                $dados_view['msg'] = "Erro ao cadastrar cliente físico!";
                $dados_view['cadastrou'] = false;
            }
        } else {
            $this->load->model('Cidade_Model', 'm_cidade');
            $sigla = $this->input->post('sigla_estado');
            $dados_view['estado_selecionado'] = $sigla;
            $dados_view['cidade_selecionada'] = $this->input->post('id_cidade');
            $dados_view['cidades'] = $this->m_cidade->buscar_por_sigla($sigla);
            $dados_view['estados'] = $this->m_estado->buscar_todos();
        }

        $this->carregar_pagina($dados_view, 'cliente_fisico/cad_cliente_fisico');
    }

    public function cadastrar_ajax() {
        $dados_view['msg'] = "Preencha todos os campos obrigatórios";
        $dados_view['status'] = 'erro';

        if (!$this->verificar_email($this->input->post('email')) && $this->input->post('email') != "") {
            $dados_view['msg'] = 'E-mail já cadastrado';
        } else if (!$this->verificar_cpf_cnpj($this->input->post('cpf_cnpj')) && $this->input->post('cpf_cnpj') != "") {
            $dados_view['msg'] = 'CPF já cadastrado';
        } else if (!validar_cpf($this->input->post('cpf_cnpj')) && $this->input->post('cpf_cnpj') != "") {
            $dados_view['msg'] = 'CPF inválido';
        } else if (
                $this->input->post('nome_nm_fantasia') != "" &&
                $this->input->post('rua') != "" &&
                $this->input->post('telefone') != "" &&
                $this->input->post('numero') != "" &&
                $this->input->post('bairro') != "" &&
                $this->input->post('sigla_estado') != "" &&
                $this->input->post('id_cidade') != ""
        ) {
            $pessoa = $this->receber_dados_pessoa(false);

            $resposta = $this->m_cliente_fisico->cadastrar($pessoa);

            if ($resposta == true) {
                $dados_view['msg'] = "Cliente físico cadastrado com sucesso!";
                $dados_view['status'] = 'sucesso';
            } else {
                $dados_view['msg'] = "Erro ao cadastrar cliente físico!";
            }
        }

        echo json_encode($dados_view);
    }

    /**
     * executa as ações necessárias para editar um cliente físico
     */
    public function editar() {
        $status_validacao = $this->validar(true);
        $dados_view = $this->get_dados_view('', 'Editar Clientes Físicos', 'clientes fisicos');
        $dados_view['cpf_cnpj'] = $this->input->post('cpf_cnpj');

        if (!$this->verificar_email($this->input->post('email'), $this->input->post('id_pessoa')) && $this->input->post('email') != "") {
            $status_validacao = false;
            $dados_view['valid_email'] = 'E-mail já cadastrado';
        }

        if ($status_validacao) {
            $pessoa = $this->receber_dados_pessoa(true);

            $resposta = $this->m_cliente_fisico->atualizar($this->input->post('id_pessoa'), $pessoa);

            if ($resposta == true) {
                $dados_view['msg'] = "Cliente físico alterado com sucesso!";
                $dados_view['alterou'] = true;
            } else {
                $dados_view['msg'] = "Erro ao alterar cliente físico!";
                $dados_view['alterou'] = false;
            }
        } else {
            $this->load->model('Cidade_Model', 'm_cidade');
            $sigla = $this->input->post('sigla_estado');
            $dados_view['estado_selecionado'] = $sigla;
            $dados_view['cidade_selecionada'] = $this->input->post('id_cidade');
            $dados_view['id_pessoa'] = $this->input->post('id_pessoa');
            $dados_view['cidades'] = $this->m_cidade->buscar_por_sigla($sigla);
            $dados_view['estados'] = $this->m_estado->buscar_todos();
        }

        $this->carregar_pagina($dados_view, 'cliente_fisico/invalido_editar_cliente_fisico');
    }

    public function excluir($id) {
        $dados_view['msg'] = "Erro ao excluir cliente físico!";
        $dados_view['status'] = "erro";

        $this->load->model('OS_Assistencia_Model', 'm_os_assistencia');

        if (count($this->m_os_assistencia->buscar_por_id_cliente($id)) == 0) {
            if ($this->m_cliente_fisico->excluir($id)) {
                $dados_view['msg'] = "Cliente físico excluído com sucesso!";
                $dados_view['status'] = "sucesso";
            }
        } else {
            $dados_view['msg'] = "Este cliente está vinculado a pelo menos uma Ordem de Serviço e não pode ser excluído!";
            $dados_view['status'] = "erro";
        }

        echo json_encode($dados_view);
    }

    public function validar($editar = false) {
        $this->form_validation->set_rules('nome_nm_fantasia', 'nome', 'required|min_length[5]');
        $this->form_validation->set_rules('email', 'email', 'valid_email');
        $this->form_validation->set_rules('rua', 'rua', 'required');
        $this->form_validation->set_rules('bairro', 'bairro', 'required');
        $this->form_validation->set_rules('sigla_estado', 'estado', 'required');
        $this->form_validation->set_rules('id_cidade', 'cidade', 'required');

        if ($editar === false) {
            $this->form_validation->set_rules('cpf_cnpj', 'CPF', 'required|validar_cpf');
        }

        return $this->form_validation->run();
    }

    public function pagina_cadastro() {
        $dados_view = $this->get_dados_view('', 'Cadastro de Clinte Físico', 'clientes fisicos');
        $dados_view['estados'] = $this->m_estado->buscar_todos();
        $this->carregar_pagina($dados_view, 'cliente_fisico/cad_cliente_fisico_1');
    }

    private function carregar_pagina($dados_view, $view) {
        $this->load->view('includes/cabecalho', $dados_view);
        $this->load->view(carregar_menu($this->session->userdata()), $dados_view);
        $this->load->view($view, $dados_view);
        $this->load->view('includes/rodape');
    }

    public function receber_dados_pessoa($editar = false) {
        $pessoa = array(
            'nome_nm_fantasia' => $this->input->post('nome_nm_fantasia'),
            'cep' => $this->input->post('cep'),
            'telefone' => $this->input->post('telefone'),
            'email' => $this->input->post('email'),
            'rua' => $this->input->post('rua'),
            'numero' => $this->input->post('numero'),
            'complemento' => $this->input->post('complemento'),
            'fl_funcionario' => false,
            'fl_cliente_fisico' => true,
            'bairro' => $this->input->post('bairro'),
            'id_cidade' => $this->input->post('id_cidade')
        );

        if ($editar === false) {
            $pessoa['cpf_cnpj'] = $this->input->post('cpf_cnpj');
        }

        return $pessoa;
    }
}
