<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of OSSuporte
 *
 * @author Helder dos Santos
 */
class OSSuporte extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('OS_Suporte_Model', 'm_os_suporte');
        $this->load->model('Status_Os_Model', 'm_status_os');
        $this->load->model('Funcionario_Model', 'm_funcionario');
    }

    public function index() {
        verificar_permissao($this->session->userdata(), array(ADMINISTRADOR, SUPORTE));
        $this->buscar_todos();
    }

    public function cadastrar() {
        verificar_permissao($this->session->userdata(), array(ADMINISTRADOR, SUPORTE));
        $os_suporte = $this->input->post();
        $os_suporte['fl_suporte'] = true;
        $os_suporte['data_abertura'] = date('Y-m-d');
        $os_suporte['id_responsavel_abertura'] = $this->session->userdata('id_pessoa');
        $continua = false;
        $dados_view['msg'] = "Preencha todos os campos!";
        $dados_view['status'] = "erro";

        //verifica se o está sendo recebido algum arquivo
        if (!empty($_FILES['anexo']['name'])) {
            $resposta = $this->upload();
            if ($resposta === null || $resposta === true) {
                $pasta = md5($this->input->post('id_cliente'));
                $arquivo = "assets/uploads/" . $pasta . "/" . str_replace(' ', '_', $_FILES['anexo']['name']);
                $os_suporte['anexo'] = $arquivo;
                $continua = true;
            } else {
                $dados_view['msg'] = $resposta;
            }
        } else {
            $continua = true;
        }

        if ($continua && $this->input->post('id_cliente') !== "" && $this->input->post('id_tecnico') !== "" && $this->input->post('id_status') !== "" && $this->input->post('descricao_problema') !== "" && $this->input->post('id_modulo') !== "") {
            $os_suporte['fl_enviar_sms'] = $this->input->post('fl_enviar_sms') != "" ? true : false;
            $os_suporte['fl_enviar_email'] = $this->input->post('fl_enviar_email') != "" ? true : false;

            $modulo_os['modulo_sistema'] = $os_suporte['id_modulo'];

            unset($os_suporte['id_modulo']);
            $id_chamado_telefonico = "";
            /*
             * verifica se há um id de chamado, se tiver, remove dos dados da OS e adiciona em uma variável
             */
            if(isset($os_suporte['id_chamado_telefonico'])){
                $id_chamado_telefonico = $os_suporte['id_chamado_telefonico'];
                unset($os_suporte['id_chamado_telefonico']);                
            }

            $resultado = $this->m_os_suporte->cadastrar($os_suporte);
            $modulo_os['ordem_de_servico'] = $resultado['id_ordem_de_servico'];

            /*
             * caso o cadastro tenha sido através de um chamado telefônico
             * o id da OS do chamado será atualizado
             */
            if ($id_chamado_telefonico != "") {
                $this->load->model('Chamado_Telefonico_Model', 'm_chamado_telefonico');
                $chamado = array('id_ordem_de_servico' => $resultado['id_ordem_de_servico']);
                $this->m_chamado_telefonico->alterar($chamado, $id_chamado_telefonico);
            }

            if ($resultado['status'] === true) {
                //$this->adicionar_historico($resultato['id_ordem_de_servico'], "Abertura da Ordem de Serviço");
                $this->load->model('Modulo_Os_Model', 'm_modulo_os');
                $resultado = $this->m_modulo_os->cadastrar($modulo_os);

                if ($resultado === true) {
                    if ($os_suporte['fl_enviar_email'] === true) {
                        $this->enviar_email($this->input->post('id_cliente'), "Ordem de Serviço - Supote Técnico", "Acabamos de abrir uma nova Ordem de Serviço do Suporte para você.<br><br><strong>Agradecemos a Preferência!</strong>");
                    }
                    
                    if($os_suporte['fl_enviar_sms'] === true){
                        $this->enviar_sms($os_suporte['id_cliente'], 'CDC Tecnologia: Acabamos de abrir uma nova Ordem de Serviço do Suporte para você!');
                    }
                    
                    
                    $dados_view['msg'] = "Ordem de Serviço cadastrada com sucesso!";
                    $dados_view['status'] = "sucesso";
                }
            } else {
                $dados_view['msg'] = "Erro ao cadastrar Ordem de Serviço!";
            }
        }

        echo json_encode($dados_view);
    }

    public function buscar_todos($id_inicio = 0) {
        verificar_permissao($this->session->userdata(), array(ADMINISTRADOR, SUPORTE));
        $this->load->library('pagination');
        $dados_view = get_dados_view('', 'Painel de OS - Suporte', 'OS-Suporte');
        $dados_view['os'] = $this->m_os_suporte->buscar_todos($id_inicio);

        $dados_view['status_os'] = $this->m_status_os->buscar_todos();
        $dados_view['tecnicos'] = $this->m_funcionario->buscar_todos_suporte();
        $dados_view['funcionarios'] = $this->m_funcionario->listar_todos();
        $config_paginacao = config_paginacao('OSSuporte/buscar_todos/', $this->m_os_suporte->contar_todas());

        $this->pagination->initialize($config_paginacao);

        $dados_view['paginacao'] = $this->pagination->create_links();
        $dados_view['tem_pagina'] = true;

        $this->carregar_pagina($dados_view, "lista_os_suporte");
    }

    public function buscar($id_inicio = 0, $id_tecnico = 0, $id_usuario = 0, $id_status = 0, $protocolo = "") {
        verificar_permissao($this->session->userdata(), array(ADMINISTRADOR, SUPORTE));
        $this->load->library('pagination');
        $config_paginacao = config_paginacao('OSSuporte/buscar/', $this->m_os_suporte->contar_todas_busca($id_tecnico, $id_usuario, $id_status, $protocolo));
        $this->pagination->initialize($config_paginacao);

        $dados_view['status'] = "erro";
        $dados_view['smg'] = "Nenhuma Ordem de Serviço encontrada com esses parâmetros";
        $dados_view['paginacao'] = $this->pagination->create_links();
        $dados_view['ordens_de_servico'] = $this->m_os_suporte->buscar($id_inicio, $id_tecnico, $id_usuario, $id_status, $protocolo);

        if (count($dados_view['ordens_de_servico'] > 0)) {
            $dados_view['status'] = "sucesso";
        }
        echo json_encode($dados_view);
    }

    public function abrir($id_ordem_de_servico) {
        verificar_permissao($this->session->userdata(), array(ADMINISTRADOR, SUPORTE));
        if (is_numeric($id_ordem_de_servico)) {

            $os = $this->m_os_suporte->buscar_por_id($id_ordem_de_servico);
            if (!empty($os)) {
                $dados_view = get_dados_view('', 'OS - Suporte', 'OS-Suporte');
                $dados_view['ordem_de_servico'] = $os;
                $dados_view['status_os'] = $this->m_status_os->buscar_todos();
                $this->carregar_pagina($dados_view, 'abrir_descricao_cliente');
            } else {
                redirect('OSAssistencia');
            }
        } else {
            redirect('OSAssistencia');
        }
    }

    public function editar() {
        verificar_permissao($this->session->userdata(), array(ADMINISTRADOR, SUPORTE));
        $os_assistencia = array('id_status' => $this->input->post('id_status_os'));
        $id_ordem_de_servico = $this->input->post('id_ordem_de_servico');
        $dados_view['msg'] = "Erro ao alterar Ordem de Serviço!";
        $dados_view['status'] = "erro";
        $pode_editar = true;

        if ($this->input->post('id_status_os') != "" && $this->input->post('id_ordem_de_servico') != "") {

            if ($this->is_concluida($id_ordem_de_servico)) {
                $dados_view['msg'] = "Após concluída ou fechada a OS não pode ser editada!";
                $pode_editar = false;
            }

            if ($pode_editar) {
                $status = $this->m_status_os->buscar_por_id($os_assistencia['id_status']);

                if ($status->id_status_os == $os_assistencia['id_status']) {
                    if ($status->fl_concluido == true) {
                        $os_assistencia['data_conclusao'] = date('Y-m-d');
                    }
                }

                if ($this->m_os_suporte->alterar($id_ordem_de_servico, $os_assistencia)) {

                    if ($this->input->post('fl_enviar_email') === "true") {
                        $this->enviar_email($this->input->post('id_cliente'), "Ordem de Serviço - Suporte Técnico", "O status da Ordem de Serviço com o protocolo de número " . $this->input->post('protocolo') . " foi alterado para " . $this->input->post('nome_status'));
                    }
                    
                     if($this->input->post('fl_enviar_sms') === true){
                        $this->enviar_sms($this->input->post('id_cliente'), 'CDC Tecnologia: Acabamos de abrir uma nova Ordem de Serviço do Suporte para você!');
                    }

                    //$this->adicionar_historico($id_ordem_de_servico, "Mudou o status da OS para " . $this->input->post('nome_status'));
                    $dados_view['msg'] = "Alteração realizada com sucesso.";
                    $dados_view['status'] = "sucesso";
                }
            }
        }

        echo json_encode($dados_view);
    }

    /**
     * Adiciona o laudo técnico e a solução técnica
     * a Ordem de Serviço do tipo suporte tácnico
     */
    public function adicionar_laudo() {
        verificar_permissao($this->session->userdata(), array(ADMINISTRADOR, SUPORTE));
        $os_suporte = $this->input->post();
        $id_ordem_de_servico = $os_suporte['id_ordem_de_servico'];
        unset($os_suporte['id_ordem_de_servico']);
        $dados_view['msg'] = "Após concluída ou fechada a OS não pode ser editada!";
        $dados_view['status'] = "erro";

        if (!$this->is_concluida($id_ordem_de_servico)) {
            if ($os_suporte['laudo_tecnico'] != "" && $os_suporte['solucao_tecnica'] != "") {
                if ($this->m_os_suporte->alterar($id_ordem_de_servico, $os_suporte)) {
                    $dados_view['msg'] = "Informações técnicas salvas com sucesso.";
                    $dados_view['status'] = "sucesso";
                } else {
                    $dados_view['msg'] = "Erro ao salvar informações técnicas.";
                }
            } else {
                $dados_view['msg'] = "Preencha os campos obrigatórios.";
            }
        }

        echo json_encode($dados_view);
    }

    public function gerar_pdf($id_ordem_de_servico) {
        verificar_permissao($this->session->userdata(), array(ADMINISTRADOR, SUPORTE));
        if (is_numeric($id_ordem_de_servico)) {
            $os = $this->m_os_suporte->buscar_por_id($id_ordem_de_servico);


            if (count($os) > 0) {
                $this->load->library('Pdf');
                $dados_view = array("ordem_de_servico" => $os);

                $this->pdf->gerar('os_suporte/relatorio_pdf', $dados_view);
            } else {
                redirect(base_url('Pagina_Erro'));
            }
        } else {
            redirect(base_url('Pagina_Erro'));
        }
    }

    /**
     * Verifica se a OS foi concluída, se sim retorna TRUE se não retorna FALSE
     * @param type $id_ordem_de_servico
     * @return boolean
     */
    private function is_concluida($id_ordem_de_servico) {
        $os = $this->m_os_suporte->buscar_por_id($id_ordem_de_servico);

        if (count($os) > 0) {
            if ($os[0]->fl_concluido == true) {
                return true;
            }
        }

        return false;
    }

    public function buscar_cliente() {
        verificar_permissao($this->session->userdata(), array(ADMINISTRADOR, SUPORTE));
        $this->load->model('Cliente_Juridico_Model', 'm_cliente_juridico');
        $nomeCliente = $this->input->post('cliente');
        echo json_encode($this->m_cliente_juridico->buscarParaOS($nomeCliente));
    }

    public function pagina_cadastro($id_chamado = null) {
        verificar_permissao($this->session->userdata(), array(ADMINISTRADOR, SUPORTE));
        $this->load->model('Sistema_Model', 'm_sistema');
        $dados_view = get_dados_view('', 'Nova OS - Suporte Técnico', 'OS-Suporte');
        $dados_view['protocolo'] = $this->gerar_protocolo();
        $dados_view['status_os'] = $this->m_status_os->buscar_todos();
        $dados_view['tecnicos'] = $this->m_funcionario->buscar_todos_suporte();
        $dados_view['sistemas'] = $this->m_sistema->buscar_todos();
        
        if($id_chamado != null && is_numeric($id_chamado)){
            $dados_view['id_chamado_telefonico'] = $id_chamado;
        }
        
        $this->carregar_pagina($dados_view, "cad_os_suporte");
    }

    private function upload() {
        if (!empty($_FILES['anexo']['name'])) {
            $folder = md5($this->input->post('id_cliente'));
            $path = "assets/uploads/" . $folder;

            if (!is_dir($path)) {
                mkdir($path, 0777, $recursive = true);
            }

            $config['upload_path'] = $path;
            $config['allowed_types'] = 'gif|jpg|png|pdf';

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('anexo')) {
                return $this->upload->display_errors();
            } else {
                return true;
            }
        } else {
            return null;
        }
    }

    private function gerar_protocolo() {
        $this->load->model("OS_Assistencia_Model", 'm_os_assistencia');
        $quantidade = $this->m_os_assistencia->contar_para_protocolo();
        $data = date('d/m/Y');
        $data = explode('/', $data);

        return $data[2] . $data[1] . $data[0] . ($quantidade + 1);
    }

    private function carregar_pagina($dados_view, $view) {
        $this->load->view('includes/cabecalho', $dados_view);
        $this->load->view(carregar_menu($this->session->userdata()), $dados_view);
        $this->load->view("os_suporte/" . $view, $dados_view);
        $this->load->view('includes/rodape');
    }

    private function enviar_email($id_cliente, $titulo, $mensagem) {
        $this->load->library("Send_Email");
        $this->load->model('Cliente_Fisico_Model', 'm_cliente_fisico');
        $cliente = $this->m_cliente_fisico->buscar_por_id($id_cliente);

        if (count($cliente) === 0) {
            $this->load->model('Cliente_Juridico_Model', 'm_cliente_juridico');
            $cliente = $this->m_cliente_juridico->buscar_por_id($id_cliente);
        }

        if (count($cliente) > 0) {
            if ($this->send_email->enviar_email("CDC Tecnologia - Ordem de Serviço", $mensagem, $cliente[0]->email, $titulo)) {
                return true;
            }
        }

        return false;
    }
    
    private function enviar_sms($id_cliente, $mensagem){
         $this->load->library("Send_Sms");
        $this->load->model('Cliente_Fisico_Model', 'm_cliente_fisico');
        $cliente = $this->m_cliente_fisico->buscar_por_id($id_cliente);

        if (count($cliente) === 0) {
            $this->load->model('Cliente_Juridico_Model', 'm_cliente_juridico');
            $cliente = $this->m_cliente_juridico->buscar_por_id($id_cliente);
        }

        if (count($cliente) > 0) {
            if ($this->send_sms->enviar_sms($cliente[0]->telefone, $mensagem)) {
                return true;
            }
        }

        return false;
    }

}
