<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cidade extends CI_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->model('Cidade_Model', 'm_cidade');
    }
    
    public function buscar_por_sigla(){
        $sigla = $this->input->post('sigla_estado');
        $dados['cidades'] = $this->m_cidade->buscar_por_sigla($sigla);
        echo json_encode($dados);
    }
}
