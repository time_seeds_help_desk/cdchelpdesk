<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $logado = $this->session->userdata('logado') == true ? true : false;

        if ($logado === true) {
            if ($this->session->userdata('tipo_usuario') == ADMINISTRADOR) {
                $this->carregar_pagina_admin();
            } else if ($this->session->userdata('tipo_usuario') == ATENDENTE) {
                $this->carregar_pagina_atend();
            } else if ($this->session->userdata('tipo_usuario') == SUPORTE) {
                $this->carregar_pagina_suporte();
            } else if ($this->session->userdata('tipo_usuario') == ASSISTENCIA) {
                $this->carregar_pagina_assis();
            } else if ($this->session->userdata('tipo_usuario') == FINANCEIRO) {
                $this->carregar_pagina_financeiro();
            } else if ($this->session->userdata('tipo_usuario') == CLIENTE) {
                $this->carregar_pagina_cliente();
            }
        } else {
            redirect(base_url('Login'));
        }
        
    }

    private function carregar_pagina($dados_view, $view) {
        $this->load->view('includes/cabecalho', $dados_view);
        $this->load->view(carregar_menu($this->session->userdata()), $dados_view);
        $this->load->view($view, $dados_view);
        $this->load->view('includes/rodape');
    }

    private function carregar_pagina_admin() {
        $this->load->model("Cliente_Fisico_Model", 'm_cliente_fisico');
        $this->load->model("Cliente_Juridico_Model", 'm_cliente_juridico');
        $this->load->model("OS_Assistencia_Model", 'm_os_assistencia');
        $this->load->model("OS_Suporte_Model", 'm_os_suporte');
        $dados_view = $this->get_dados_view('inicio', '', 'inicio');
        $dados_view['clientes_fisicos'] = $this->m_cliente_fisico->contar_todos();
        $dados_view['clientes_juridicos'] = $this->m_cliente_juridico->contar_todos();
        $dados_view['os_assistencia'] = $this->m_os_assistencia->contar_todas();
        $dados_view['os_suporte'] = $this->m_os_suporte->contar_todas();
        $this->carregar_pagina($dados_view, "administrador/inicio");
    }

    private function carregar_pagina_assis() {
        $this->load->model("OS_Assistencia_Model", 'm_os_assistencia');
        $dados_view = $this->get_dados_view('inicio', '', 'inicio');
        $dados_view['os_assistencia'] = $this->m_os_assistencia->contar_todas();
        $this->carregar_pagina($dados_view, "inicio_assistencia");
    }

    private function carregar_pagina_atend() {
        $this->load->model("Cliente_Fisico_Model", 'm_cliente_fisico');
        $this->load->model("Cliente_Juridico_Model", 'm_cliente_juridico');
        $this->load->model("OS_Assistencia_Model", 'm_os_assistencia');
        $this->load->model("OS_Suporte_Model", 'm_os_suporte');
        $dados_view = $this->get_dados_view('inicio', '', 'inicio');
        $dados_view['clientes_fisicos'] = $this->m_cliente_fisico->contar_todos();
        $dados_view['clientes_juridicos'] = $this->m_cliente_juridico->contar_todos();
        $dados_view['os_assistencia'] = $this->m_os_assistencia->contar_todas();
        $dados_view['os_suporte'] = $this->m_os_suporte->contar_todas();
        $this->carregar_pagina($dados_view, "atendente/inicio");
    }

    private function carregar_pagina_suporte() {
        $this->load->model("OS_Suporte_Model", 'm_os_suporte');
        $dados_view = $this->get_dados_view('inicio', '', 'inicio');
        $dados_view['os_suporte'] = $this->m_os_suporte->contar_todas();
        $this->carregar_pagina($dados_view, "inicio_suporte");
    }

    private function carregar_pagina_cliente(){
        $dados_view = $this->get_dados_view('inicio', '', 'inicio');
        $this->carregar_pagina($dados_view, "inicio_cliente");
    }
    /**
     * gera um array com as informações necessárias para a view
     * @param type $titulo
     * @param type $titulo2
     * @param type $menu_selecionado
     * @return type
     */
    public function get_dados_view($titulo, $titulo2, $menu_selecionado) {
        return array(
            'titulo' => $titulo,
            'titulo2' => $titulo2,
            'menu_selecionado' => $menu_selecionado
        );
    }

}
