<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Atividade
 *
 * @author Helder dos Santos
 */
class Atividade extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Atividade_Model', 'm_atividade');
    }

    public function index() {
        $this->buscar_todos();
    }

    public function buscar_todos($id_inicio = 0) {
        $this->load->library('pagination');
        $dados_view = get_dados_view('', 'Serviços Cadastrados', 'atividade');
        $dados_view['atividades'] = $this->m_atividade->buscar_todos($id_inicio);
        $dados_view['pag_inicio'] = $id_inicio + 1;

        $config_paginacao = config_paginacao('Atividade/buscar_todos/', $this->m_atividade->contar_todos());

        $this->pagination->initialize($config_paginacao);

        $dados_view['paginacao'] = $this->pagination->create_links();
        $dados_view['tem_pagina'] = true;

        $this->carregar_pagina($dados_view, "atividades/lista_atividade");
    }

    public function cadastrar() {
        $valido = $this->validar();
        $dados_view = get_dados_view('', 'Cadastro de Serviço', 'atividade');

        if ($valido === true) {
            $atividade = array(
                'nome_atividade' => $this->input->post('nome_atividade'),
                'valor_atividade' => converte_valor_db($this->input->post('valor_atividade'))
            );

            $retorno = $this->m_atividade->cadastrar($atividade);

            if ($retorno === true) {
                $dados_view['msg'] = "Serviço cadastrado com sucesso!";
                $dados_view['cadastrou'] = true;
            } else {
                $dados_view['msg'] = "Erro ao cadastrar serviço!";
                $dados_view['cadastrou'] = false;
            }
        }

        $this->carregar_pagina($dados_view, 'atividades/cad_atividade');
    }

    public function editar() {
        $valido = $this->validar();
        $dados_view = get_dados_view('', 'Editar Atividade', 'atividade');

        if ($valido === true) {
            $atividade = array(
                'id_atividade' => $this->input->post('id_atividade'),
                'nome_atividade' => $this->input->post('nome_atividade'),
                'valor_atividade' => converte_valor_db($this->input->post('valor_atividade'))
            );

            $retorno = $this->m_atividade->alterar($atividade);

            if ($retorno === true) {
                $dados_view['msg'] = "Serviço alterado com sucesso!";
                $dados_view['editou'] = true;
            } else {
                $dados_view['msg'] = "Erro ao alterar serviço!";
                $dados_view['editou'] = false;
            }
        }

        $this->carregar_pagina($dados_view, 'atividades/invalido_editar_atividade');
    }

    public function buscar() {
        $busca = $this->input->post('nome_atividade');

        if ($busca != '') {
            $dados_view = get_dados_view('', 'Serviços Cadastrados', 'atividade');
            $dados_view['atividades'] = $this->m_atividade->buscar_por_nome($busca);
            $dados_view['busca'] = $busca;
            $dados_view['paginacao'] = "";
            $dados_view['tem_pagina'] = false;

            $this->carregar_pagina($dados_view, "atividades/busca_atividade");
        } else {
            redirect(base_url('Atividade'));
        }
    }

    public function buscar_por_id($id_atividade = "") {
        $dados_view = array(
            "status" => "erro",
            "msg" => "Não foi possível concluir a operação"
        );

        if (is_numeric($id_atividade) && $id_atividade != "") {
            $dados_view['status'] = "sucesso";
            $dados_view['atividade_os'] = $this->m_atividade->buscar_por_id($id_atividade);
        }

        echo json_encode($dados_view);
    }

    public function excluir($id) {
        $dados_view['msg'] = "Erro ao excluir serviço!";
        $dados_view['status'] = "erro";

        $this->load->model('OS_Assistencia_Model', 'm_os_assistencia');

        if (count($this->m_os_assistencia->buscar_por_id_atividade($id)) == 0) {
            if ($this->m_atividade->excluir($id)) {
                $dados_view['msg'] = "Serviço excluído com sucesso!";
                $dados_view['status'] = "sucesso";
            }
        } else {
            $dados_view['msg'] = "Este serviço está vinculado a pelo menos uma Ordem de Serviço e não pode ser excluído!";
            $dados_view['status'] = "erro";
        }

        echo json_encode($dados_view);
    }

    public function pagina_cadastro() {
        $dados_view = get_dados_view('', 'Cadastro de Serviço', 'atividade');
        $this->carregar_pagina($dados_view, 'atividades/cad_atividade');
    }

    public function pagina_editar($id) {
        if (is_numeric($id)) {
            $atividade = $this->m_atividade->buscar_por_id($id);

            if ($atividade != null) {
                $dados_view = get_dados_view('', 'Editar Serviço', 'atividade');
                $dados_view['atividade'] = $atividade;
                $this->carregar_pagina($dados_view, 'atividades/editar_atividade');
            } else {
                redirect(base_url('Atividade'));
            }
        } else {
            redirect(base_url('Atividade'));
        }
    }

    private function carregar_pagina($dados_view, $view) {

        $this->load->view('includes/cabecalho', $dados_view);
        $this->load->view(carregar_menu($this->session->userdata()), $dados_view);
        $this->load->view($view, $dados_view);
        $this->load->view('includes/rodape');
    }

    private function validar() {
        $this->form_validation->set_rules('nome_atividade', 'nome', 'required');
        $this->form_validation->set_rules('valor_atividade', 'valor', 'required');
        return $this->form_validation->run();
    }

}
