<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class StatusOS extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Status_Os_Model', 'm_status_os');
        verificar_permissao($this->session->userdata, array(ADMINISTRADOR));
    }

    public function index() {
        $dados_view = get_dados_view('', 'Lista de Status', 'status os');
        $dados_view['status_os'] = $this->m_status_os->buscar_todos();
        $this->carregar_pagina($dados_view, "lista_status_os");
    }

    public function pagina_cadastro() {
        $dados_view = get_dados_view('', 'Lista de Status', 'status os');
        $this->carregar_pagina($dados_view, "cad_status_os");
    }

    public function pagina_editar($id_status_os) {
        if (is_numeric($id_status_os)) {
            $status_os = $this->m_status_os->buscar_por_id($id_status_os);

            if ($status_os != null) {
                $dados_view = get_dados_view('', 'Editar Status', 'status os');
                $dados_view['status_os'] = $status_os;
                $this->carregar_pagina($dados_view, 'editar_status_os');
            } else {
                redirect('StatusOS');
            }
        } else {
            redirect('StatusOS');
        }
    }

    public function editar() {
        $valido = $this->validar();
        $dados_view = get_dados_view('', 'Editar Status', 'status os');

        if ($valido === true) {
            $status_os = array(
                'id_status_os' => $this->input->post('id_status_os'),
                'nome_status_os' => $this->input->post('nome_status_os')
            );

            $retorno = $this->m_status_os->alterar($status_os);

            if ($retorno === true) {
                $dados_view['msg'] = "Status O.S. alterada com sucesso!";
                $dados_view['editou'] = true;
            } else {
                $dados_view['msg'] = "Erro ao alterar Status O.S.!";
                $dados_view['editou'] = false;
            }
        }

        $this->carregar_pagina($dados_view, 'invalido_editar_status_os');
    }

    public function cadastrar() {
        $dados_view = get_dados_view('', 'Lista de Status', 'status os');
        $dados_view['msg'] = "Erro ao cadastrar Status de O.S.!";
        $dados_view['cadastrou'] = false;

        if ($this->validar()) {
            $dados = array('nome_status_os' => $this->input->post('nome_status_os'));
            $result = $this->m_status_os->cadastrar($dados);

            if ($result) {
                $dados_view['msg'] = "Status de O.S. cadastrado com sucesso!";
                $dados_view['cadastrou'] = true;
            }
        }

        $this->carregar_pagina($dados_view, "cad_status_os");
    }

    public function excluir($id) {
        $dados_view['msg'] = "Erro ao excluir Status de O.S.!";
        $dados_view['status'] = "erro";

        if ($this->m_status_os->buscar_por_id($id)->fl_pode_excluir == true) {
            $this->load->model('OS_Assistencia_Model', 'm_assistencia_model');
            if (count($this->m_assistencia_model->buscar(0, 0, 0, $id)) === 0) {
                if ($this->m_status_os->excluir($id)) {
                    $dados_view['msg'] = "Status de O.S. excluído com sucesso!";
                    $dados_view['status'] = "sucesso";
                }
            } else {
                $dados_view['msg'] = "Este Status está vinculado a pelo menos uma Ordem de Serviço e não pode ser excluído.";
            }
        } else {
            $dados_view['msg'] = "Este Status de O.S. não pode ser excluído.";
        }


        /* if (!$this->m_status_os->excluir($id)) {
          $dados_view['msg'] = "Erro ao excluir Status de O.S.!";
          $dados_view['status'] = "erro";
          } */

        echo json_encode($dados_view);
    }

    private function validar() {
        $this->form_validation->set_rules('nome_status_os', 'Status', 'required');
        return $this->form_validation->run();
    }

    private function carregar_pagina($dados_view, $view) {
        $this->load->view('includes/cabecalho', $dados_view);
        $this->load->view('menus/administrador', $dados_view);
        $this->load->view('administrador/status_os/' . $view, $dados_view);
        $this->load->view('includes/rodape');
    }

}
