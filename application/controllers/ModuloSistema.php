<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of ModuloSistema
 *
 * @author Helder dos Santos
 */
class ModuloSistema extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Modulo_Sistema_Model', 'm_modulo_sistema');
        verificar_permissao($this->session->userdata(), array(ADMINISTRADOR, SUPORTE, ATENDENTE));
    }

    public function index() {
        redirect('Sistema');
    }

    public function cadastrar() {
        verificar_permissao($this->session->userdata(), array(ADMINISTRADOR));
        $dados_view = array(
            'msg' => "Erro ao cadastrar módulo.",
            'status' => 'erro'
        );

        if ($this->input->post('id_sistema') != "" && $this->input->post('nome_modulo_sistema') != "") {
            $modulo = $this->input->post();
            if ($this->m_modulo_sistema->cadastrar($modulo)) {
                $dados_view = array(
                    'msg' => "Módulo cadastrado com sucesso.",
                    'status' => 'sucesso'
                );
            }
        } else {
            $dados_view['msg'] = "Preencha todos os campos.";
        }

        echo json_encode($dados_view);
    }

    public function editar() {
        verificar_permissao($this->session->userdata(), array(ADMINISTRADOR));
        $dados_view = array(
            'msg' => "Erro ao alterar módulo.",
            'status' => 'erro'
        );

        if ($this->input->post('id_modulo_sistema') != "" && $this->input->post('nome_modulo_sistema') != "") {
            $modulo = $this->input->post();
            $id_modulo_sistema = $modulo['id_modulo_sistema'];
            unset($modulo['id_modulo_sistema']);
            if ($this->m_modulo_sistema->alterar($modulo, $id_modulo_sistema)) {
                $dados_view = array(
                    'msg' => "Módulo alterado com sucesso.",
                    'status' => 'sucesso'
                );
            }
        } else {
            $dados_view['msg'] = "Preencha todos os campos.";
        }

        echo json_encode($dados_view);
    }
    
    public function buscar_por_id_sistema(){
        verificar_permissao($this->session->userdata(), array(ADMINISTRADOR, SUPORTE, ATENDENTE));
        $id_sistema = $this->input->post('id_sistema');
        $dados['modulos'] = $this->m_modulo_sistema->buscar_todos($id_sistema);
        echo json_encode($dados);
    } 

    public function buscar_todos($id_sistema) {
        verificar_permissao($this->session->userdata(), array(ADMINISTRADOR, SUPORTE, ATENDENTE));
        $dados_view['modulos'] = array();
        $dados_view['status'] = "erro";

        if (is_numeric($id_sistema)) {
            $dados_view['modulos'] = $this->m_modulo_sistema->buscar_todos($id_sistema);
            $dados_view['status'] = "sucesso";
        }

        echo json_encode($dados_view);
    }

    public function buscar_por_id($id_modulo) {
        verificar_permissao($this->session->userdata(), array(ADMINISTRADOR, SUPORTE, ATENDENTE));
        $dados_view['modulos'] = array();
        $dados_view['status'] = "erro";

        if (is_numeric($id_modulo)) {
            $dados_view['modulos'] = $this->m_modulo_sistema->buscar_por_id($id_modulo);
            if (count([$dados_view['modulos']]) > 0) {
                $dados_view['status'] = "sucesso";
            }
        }

        echo json_encode($dados_view);
    }

    public function excluir($id_modulo_sistema) {
        verificar_permissao($this->session->userdata(), array(ADMINISTRADOR));
        $dado_view = array(
            'msg' => "Erro ao excluir módulo.",
            'status' => 'erro'
        );

        if ($this->m_modulo_sistema->excluir($id_modulo_sistema)) {
            $dado_view = array(
                'msg' => "Módulo excluído com sucesso.",
                'status' => 'sucesso'
            );
        }

        echo json_encode($dado_view);
    }

}
