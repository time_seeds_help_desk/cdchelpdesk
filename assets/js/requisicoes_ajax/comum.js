$(document).ready(function () {

    //máscaras
    $('.mascara-telefone').mask('(00) 0 0000-0000');
    $('.mascara-cnpj').mask('00.000.000/0000-00');
    $('.mascara-cpf').mask('000.000.000-00');
    $('.mascara-dinheiro').mask("#.##0,00", {reverse: true});
    $('.mascara-cep').mask('00000-000');

    $("#sigla_estado").change(function (event) {
        estado = $("#sigla_estado").val();
        $.ajax({
            method: "POST",
            url: base_url + "cidade/buscar_por_sigla/",
            data: {sigla_estado: estado},
            dataType: "json"
        }).done(function (resposta) {
            cidades = resposta.cidades;
            html = "<option value=\"\">Selecione a Cidade</option>";


            for (var i = 0; i < cidades.length; i++) {
                html += "<option value=\"" + cidades[i].id_cidade + "\">" + cidades[i].nome_cidade + "</option>";
            }

            $("#id_cidade").html(html);
        }).fail(function (jqXHR, textStatus) {
            swal(
                    'Aconteceu algo de errado!',
                    '',
                    'error'
                    );
        });
        event.preventDefault();
    });
});


