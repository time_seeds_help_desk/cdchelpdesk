var btnOsAssAtivo = true;
var novosEquipamentoOS = [];
var novasAtividadesOS = [];
var antigosEquipamentoOS = [];
var antigasAtividadesOS = [];
var valorTotal = 0;

$(document).ready(function () {
    $("#load-cli").hide();
    addEventos();

    //envio de formulário de cadastro de OS
    $("#form-cadastro").on('submit', function (e) {
        e.preventDefault();

        continuar = false;

        if ($('#anexo').val() != "") {
            if ($('#anexo').get(0).files[0].size <= 8388608) {
                continuar = true;
            }
        } else {
            continuar = true;
        }

        if (continuar) {

            $("#pelicula").show();
            $.ajax({
                url: base_url + "OSAssistencia/cadastrar",
                method: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                dataType: "json"
            }).done(function (resposta) {
                $("#pelicula").hide();
                console.log(resposta);
                if (resposta.status == 'sucesso') {
                    $('.msg-modal').html(resposta.msg);
                    $('#msm-sucesso').modal();
                } else {
                    $('.msg-modal').html(resposta.msg);
                    $('#msg-erro').modal();
                }

            }).fail(function (jqXHR, textStatus) {
                $('#pelicula').hide();

                $('.msg-modal').html("<h3>Erro 500</h3>Falha interna no servidor");
                $('#msg-erro').modal();
            });
        } else {
            $('.msg-modal').html("Não é permitido upload de arquivo maior que 8 MB.");
            $('#msg-erro').modal();
        }
    });
    //envio de formulário de cadastro de OS

    //envio de dados do formlário de busca de Ordem de Serviço
    $('#form-os-assistencia form').submit(function (e) {
        e.preventDefault();
        $("#pelicula").show();
        id_inicio = 0;
        id_tecnico = $("#tecnico").val();
        id_usuario = $("#usuario").val();
        id_status = $("#status").val();
        protocolo = $("#protocolo").val();

        buscarOrdensDeServico(id_inicio, id_tecnico, id_usuario, id_status, protocolo);
    });
    //Fim de envio de dados do formlário de busca de Ordem de Serviço


    //menu da página abrir OS
    $('.abas li').click(function (i) {
        $(this).val();
        $('.abas li').removeClass('selecionada');
        $(this).addClass('selecionada');
        opcaoSelecionada = $(this).attr('pagina');

        $('.conteudo-abas .opcao').removeClass('ativo');
        if (opcaoSelecionada === "informacoes") {
            $("#informacoes").addClass('ativo');
        } else if (opcaoSelecionada === "laudo") {
            $("#laudo").addClass('ativo');
            buscarTodasAutorizadas();
        } else if (opcaoSelecionada === "anexo") {
            $("#anexo").addClass('ativo');
        } else if (opcaoSelecionada === "historico") {
            $("#historico").addClass('ativo');
            atualizarHistoricoOS();
        }
    });

    /*
     * verifica o click nos botões enniar para autorizada e solucionar da janela abrir_laudo_tecnico
     */
    $('button').click(function (i) {
        id = $(this).attr('id');
        if (id === 'btn-enviar-autorizada') {
            $('#btn-solucionar').fadeOut(1000);
            $('#btn-enviar-autorizada').fadeOut(1000);
            $('#btn-enviar-autorizada').hide();
            $('#btn-solucionar').hide();
            $('#form-enviar-autorizada').fadeIn(1000);
            $('#form-solucionar').fadeOut(1000);
            $('#form-enviar-autorizada').show();
            $('#form-solucionar').hide();
        } else if (id == 'btn-solucionar') {
            $('#btn-solucionar').fadeOut(1000);
            $('#btn-enviar-autorizada').fadeOut(1000);
            $('#btn-solucionar').hide();
            $('#btn-enviar-autorizada').hide();
            $('#form-solucionar').fadeIn(1000);
            $('#form-enviar-autorizada').fadeOut(1000);
            $('#form-solucionar').show();
            $('#form-enviar-autorizada').hide();
            carregarDadosFormSolucaoTec();
        } else if (
                id == 'btn-cancel-solucionar' ||
                id == 'btn-cancel-autorizada') {
            $('#btn-solucionar').fadeIn(1000);
            $('#btn-enviar-autorizada').fadeIn(1000);
            $('#btn-solucionar').show();
            $('#btn-enviar-autorizada').show();
            $('#form-solucionar').hide();
            $('#form-enviar-autorizada').hide();
        }

    });

    $(".btn-atendente").click(function (e) {
        e.preventDefault();
        id = $(this).attr('id');
        if (id == 'btn-cancel-solucionar-atendente' || id == 'btn-cancel-autorizada-atendente') {
            $('#btn-solucionar').fadeIn(1000);
            $('#btn-enviar-autorizada').fadeIn(1000);
            $('#btn-solucionar').show();
            $('#btn-enviar-autorizada').show();
            $('#form-solucionar').hide();
            $('#form-enviar-autorizada').hide();
        }

    });

    /**
     * enviar o dados do formulário para adicionar autorizada aa um esquipamento
     */
    $("#form-add-autorizada").submit(function (e) {
        e.preventDefault();
        dados = {
            id_autorizada: $("#add-id-autorizada").val(),
            descricao_problema_autorizada: $('#descricao_problema_autorizada').val(),
            id_ordem_de_servico: $("#form-add-id-os").val(),
            fl_enviar_sms: $('#fl_enviar_sms').val(),
            fl_enviar_email: $('#fl_enviar_email').val(),
            id_cliente: $('#id_cliente').val(),
            protocolo: $("#protocolo").val()
        };
        $.ajax({
            method: "POST",
            url: base_url + "OSAssistencia/adicionarAutorizada",
            data: dados,
            dataType: "json"
        }).done(function (resposta) {
            $('#pelicula').removeClass('pelicula-ativa').addClass('pelicula-inativa');
            console.log(resposta);
            if (resposta.status == 'sucesso') {
                $('.msg-modal').html(resposta.msg);
                $('#msm-sucesso').modal();
                autorizadas = resposta.autorizada;
                html = "<strong>Autorizada: </strong>";
                for (var i = 0; i < autorizadas.length; i++) {
                    html += autorizadas[i].nome_autorizada + " / Ramo: " + autorizadas[i].ramo + " / Cidade: " + autorizadas[i].nome_cidade + " / Telefone: " + autorizadas[i].telefone + " / E-mail: " + autorizadas[i].email;
                }
                $("#info-autorizada").html(html);
                $('#btn-solucionar').show();
                $('#btn-enviar-autorizada').show();
                $('#form-solucionar').hide();
                $('#form-enviar-autorizada').hide();
            } else {
                $('.msg-modal').html(resposta.msg);
                $('#msg-erro').modal();
            }

        }).fail(function (jqXHR, textStatus) {
            $('#pelicula').removeClass('pelicula-ativa').addClass('pelicula-inativa');

            $('.msg-modal').html("<h3>Erro 500</h3>Falha interna no servidor");
            $('#msg-erro').modal();
        });
    });

    /*
     * faz a busca da atividade e
     * e a adiciona na tabela de custos
     */
    $("#btn-adicionar-atividade").click(function (i) {
        i.preventDefault();


        id_atividade = $("#sol-atividade").val();

        if (id_atividade != "") {
            $("#pelicula").show();
            $.ajax({
                method: "POST",
                url: base_url + "Atividade/buscar_por_id/" + id_atividade,
                data: {},
                dataType: "json"
            }).done(function (resposta) {

                $("#pelicula").hide();
                if (resposta.status == 'sucesso') {
                    if (resposta.atividade_os != null && resposta.atividade_os != "") {
                        linha = $("<tr>");
                        vl = parseFloat(resposta.atividade_os.valor_atividade);
                        valorTotal += parseFloat(resposta.atividade_os.valor_atividade);
                        $("#rodape-tbl-custos").html("Total: " + valorTotal.toLocaleString('pt-br', {style: 'currency', currency: 'BRL'}));
                        col = "";
                        col += "<td>" + resposta.atividade_os.nome_atividade + "</td>";
                        col += "<td>" + (vl.toLocaleString('pt-br', {style: 'currency', currency: 'BRL'})) + "</td>";
                        col += "<td>";
                        col += "<a onclick=\"excluirAtividade(this)\" issave=\"false\" cod=\"" + resposta.atividade_os.id_atividade + "\" valor=\"" + resposta.atividade_os.valor_atividade + "\" class=\"text-danger\" style=\"cursor: pointer\"><i class=\"material-icons\">cancel</i></a>";
                        col += "</td>";
                        linha.append(col);
                        $("#custos-os tbody").append(linha);
                        novasAtividadesOS.push(resposta.atividade_os.id_atividade + "-" + resposta.atividade_os.valor_atividade);
                    }
                } else {
                    $('.msg-modal').html(resposta.msg);
                    $('#msg-erro').modal();
                }

            }).fail(function (jqXHR, textStatus) {
                $("#pelicula").hide();

                $('.msg-modal').html("<h3>Erro 500</h3>Falha interna no servidor");
                $('#msg-erro').modal();
            });
        } else {
            $('.msg-modal').html("Selecione uma atividade");
            $('#msg-erro').modal();
        }
    });//fechamento requisição de atividade para adicionar a tabela de custos


    /*
     * faz a busca da equipamentos e
     * e a adiciona na tabela de custos
     */
    $("#btn-adicionar-equipamento").click(function (i) {
        i.preventDefault();

        id_equipamento = $("#sol-equipamento").val();

        if (id_equipamento != "") {
            $("#pelicula").show();
            $.ajax({
                method: "POST",
                url: base_url + "Equipamento/buscar_por_id/" + id_equipamento,
                data: {},
                dataType: "json"
            }).done(function (resposta) {

                $("#pelicula").hide();

                if (resposta.status == 'sucesso') {
                    if (resposta.equipamento_os != null && resposta.equipamento_os != "") {
                        valorTotal += parseFloat(resposta.equipamento_os.valor_equipamento);
                        vl = parseFloat(resposta.equipamento_os.valor_equipamento);
                        $("#rodape-tbl-custos").html("Total: " + valorTotal.toLocaleString('pt-br', {style: 'currency', currency: 'BRL'}));
                        linha = $("<tr>");
                        col = "";
                        col += "<td>" + resposta.equipamento_os.nome_equipamento + "</td>";
                        col += "<td>" + (vl.toLocaleString('pt-br', {style: 'currency', currency: 'BRL'})) + "</td>";
                        col += "<td>";
                        col += "<a onclick=\"excluirEquipamento(this)\" issave=\"false\" cod=\"" + resposta.equipamento_os.id_equipamento + "\" valor=\"" + resposta.equipamento_os.valor_equipamento + "\" class=\"text-danger\" style=\"cursor: pointer\"><i class=\"material-icons\">cancel</i></a>";
                        col += "</td>";
                        linha.append(col);
                        $("#custos-os tbody").append(linha);
                        novosEquipamentoOS.push(resposta.equipamento_os.id_equipamento + "-" + resposta.equipamento_os.valor_equipamento);
                    }
                } else {
                    $('.msg-modal').html(resposta.msg);
                    $('#msg-erro').modal();
                }

            }).fail(function (jqXHR, textStatus) {
                $("#pelicula").hide();

                $('.msg-modal').html("<h3>Erro 500</h3>Falha interna no servidor");
                $('#msg-erro').modal();
            });
        } else {
            $('.msg-modal').html("Selecione uma atividade");
            $('#msg-erro').modal();
        }
    });//fechamento requisição de equipamentos para adicionar a tabela de custos


    /*
     * submit do formulário de editar os da página abrir_descricao_cliente
     */
    $('#form-editar-os').submit(function (e) {
        e.preventDefault();
        dados = {
            id_ordem_de_servico: $('#id_ordem_de_servico').val(),
            id_status_os: $('#id_status').val(),
            nome_status: $('#id_status').find(":selected").text(),
            fl_enviar_sms: $('#fl_enviar_sms').val(),
            fl_enviar_email: $('#fl_enviar_email').val(),
            id_cliente: $('#id_cliente').val(),
            protocolo: $("#protocolo").val()
        };



        $("#pelicula").show();
        $.ajax({
            method: "POST",
            url: base_url + "OSAssistencia/editar/",
            data: dados,
            dataType: "json"
        }).done(function (resposta) {
            $("#pelicula").hide();
            if (resposta.status === 'sucesso') {
                $("#pelicula").hide();
                $('.msg-modal').html(resposta.msg);
                $('#msm-sucesso').modal();
            } else {
                $('.msg-modal').html(resposta.msg);
                $('#msg-erro').modal();
            }

        }).fail(function (jqXHR, textStatus) {
            $("#pelicula").hide();

            $('.msg-modal').html("<h3>Erro 500</h3>Falha interna no servidor");
            $('#msg-erro').modal();
        });

    });



    /*
     * faz o cadastro do laudo técnico e das atividades e quipamentos
     * necessários para o conserto do equipamento do cliente
     */
    $("#formulario-laudo").submit(function (event) {
        event.preventDefault();
        atividadesExcluir = "";
        equipamentosExcluir = "";
        atividadesAdd = "";
        equipamentosAdd = "";
        $("#pelicula").show();

        for (i = 0; i < antigasAtividadesOS.length; i++) {
            atividadesExcluir += antigasAtividadesOS[i];
            if ((i + 1) !== antigasAtividadesOS.length) {
                atividadesExcluir += "|";
            }
        }

        for (i = 0; i < antigosEquipamentoOS.length; i++) {
            equipamentosExcluir += antigosEquipamentoOS[i];
            if ((i + 1) !== antigosEquipamentoOS.length) {
                equipamentosExcluir += "|";
            }
        }

        for (i = 0; i < novosEquipamentoOS.length; i++) {
            equipamentosAdd += novosEquipamentoOS[i];
            if ((i + 1) !== novosEquipamentoOS.length) {
                equipamentosAdd += "|";
            }
        }

        for (i = 0; i < novasAtividadesOS.length; i++) {
            atividadesAdd += novasAtividadesOS[i];
            if ((i + 1) !== novasAtividadesOS.length) {
                atividadesAdd += "|";
            }
        }

        if ($("#texto-laudo").val() != "") {
            dados = {
                id_os: idOsAssistenciaSelecionada,
                laudo_tecnico: $("#texto-laudo").val(),
                ativ_excluir: atividadesExcluir.length !== 0 ? atividadesExcluir : "",
                equi_excluir: equipamentosExcluir.length !== 0 ? equipamentosExcluir : "",
                ativ_adicionar: atividadesAdd.length !== 0 ? atividadesAdd : "",
                equi_adicionar: equipamentosAdd.length !== 0 ? equipamentosAdd : "",
                fl_enviar_sms: $('#fl_enviar_sms').val(),
                fl_enviar_email: $('#fl_enviar_email').val(),
                id_cliente: $('#id_cliente').val(),
                protocolo: $("#protocolo").val()
            };

            $.ajax({
                method: "POST",
                url: base_url + "OSAssistencia/cadastrar_laudo/",
                data: dados,
                dataType: "json"
            }).done(function (resposta) {
                $('#pelicula').removeClass('pelicula-ativa').addClass('pelicula-inativa');

                console.log(resposta);
                if (resposta.status == 'sucesso') {
                    carregarDadosFormSolucaoTec();
                    novosEquipamentoOS = [];
                    novasAtividadesOS = [];
                    antigosEquipamentoOS = [];
                    antigasAtividadesOS = [];
                    $("#pelicula").hide();
                    $('.msg-modal').html(resposta.msg);
                    $('#msm-sucesso').modal();

                } else {
                    $("#pelicula").hide();
                    carregarDadosFormSolucaoTec();
                    $('.msg-modal').html(resposta.msg);
                    $('#msg-erro').modal();
                }

            }).fail(function (jqXHR, textStatus) {
                $("#pelicula").hide();

                $('.msg-modal').html("<h3>Erro 500</h3>Falha interna no servidor");
                $('#msg-erro').modal();
            });
        } else {
            $("#pelicula").hide();
            $('.msg-modal').html("Laudo Técnico é obrigatório");
            $('#msg-erro').modal();
        }
    });
});//fechamento da função ready

//=============================REQUISIÇÕES AJAX=========================================
var idOsAssisSelecionada;

/*
 * remove equipamentos na tabela de custos da OS
 */
function excluirEquipamento(elemento) {
    if ($(elemento).attr("issave") === "true") {
        antigosEquipamentoOS.push($(elemento).attr('cod'));
    } else {
        for (i = 0; i < novosEquipamentoOS.length; i++) {
            if ($(elemento).attr('cod') + "-" + $(elemento).attr('valor') === novosEquipamentoOS[i]) {
                novosEquipamentoOS.splice(i, 1);
                break;
            }
        }
    }

    valorTotal -= parseFloat($(elemento).attr('valor'));
    $("#rodape-tbl-custos").html("Total: " + valorTotal.toLocaleString('pt-br', {style: 'currency', currency: 'BRL'}));

    var tr = $(elemento).closest('tr');
    tr.fadeOut(400, function () {
        tr.remove();
    });
}

function formatarData(dataNaoFormatada) {
    data = dataNaoFormatada;
    data = data.substring(0, 10);
    data = data.split("-");
    dataFormatada = data[2] + "/" + data[1] + "/" + data[0];
    return dataFormatada;
}

/**
 * Requição para o formulário de busca de ORdem de Servico e ao clicar no link da paginação
 * @param {type} id_inicio
 * @param {type} id_tecnico
 * @param {type} id_usuario
 * @param {type} id_status
 * @param {type} protocolo
 * @returns {undefined}
 */
function buscarOrdensDeServico(id_inicio, id_tecnico, id_usuario, id_status, protocolo) {
    if (id_tecnico != "0" || id_usuario != "0" || id_status != "0" || protocolo != "") {
        $.ajax({
            method: "POST",
            url: base_url + "OSAssistencia/buscar/" + id_inicio + "/" + id_tecnico + "/" + id_usuario + "/" + id_status + "/" + protocolo,
            data: {},
            dataType: "json"
        }).done(function (resposta) {
            $("#pelicula").hide();
            if (resposta.status == 'sucesso') {
                html = "";
                for (var i = 0; i < resposta.ordens_de_servico.length; i++) {
                    dataConclusao = resposta.ordens_de_servico[i].data_conclusao == null ? "" : formatarData(resposta.ordens_de_servico[i].data_conclusao);
                    html += "<tr>";
                    html += "<td>" + resposta.ordens_de_servico[i].protocolo + "</td>";
                    html += "<td>" + formatarData(resposta.ordens_de_servico[i].data_abertura) + "</td>";
                    html += "<td>" + dataConclusao + "</td>";
                    html += "<td>" + resposta.ordens_de_servico[i].nome_status_os + "</td>";
                    html += "<td>" + resposta.ordens_de_servico[i].nome_tecnico + "</td>";
                    html += "<td>" + resposta.ordens_de_servico[i].nome_cliente + "</td>";
                    html += "<td>" + resposta.ordens_de_servico[i].nome_equipamento_recebido + "</td>";
                    html += "<td>";
                    html += "<div class=\"row\">";
                    html += "<a target=\"_blank\" title=\"PDF\" href=\"" + base_url + "OSAssistencia/gerar_pdf/" + resposta.ordens_de_servico[i].id_ordem_de_servico + "\" class=\"btn btn-danger\">";
                    html += "<i class=\"material-icons\">picture_as_pdf</i> ";
                    html += "</a>";
                    html += "<a title=\"ABRIR\" href=\"" + base_url + "OSAssistencia/abrir/" + resposta.ordens_de_servico[i].id_ordem_de_servico + "\" class=\"btn btn-primary\">";
                    html += "<i class=\"material-icons\">folder_open</i>";
                    html += "</a>";
                    html += "</div>";
                    html += "</td>";
                    html += "</tr>";
                }
                $('#tbl-lista-os tbody').html(html);

                //html = "<div id=\"links-busca\">";
                html = resposta.paginacao;
                //html += "</div>";

                $('#paginacao div').html(html);
                adicionarEventiPaginacao();

            } else {
                $('.msg-modal').html(resposta.msg);
                $('#msg-erro').modal();
            }

        }).fail(function (jqXHR, textStatus) {
            $("#pelicula").hide();
            $('.msg-modal').html("<h3>Erro 500</h3>Falha interna no servidor");
            $('#msg-erro').modal();
        });
    } else {
        window.location.href = base_url + "OSAssistencia";
    }
}

/**
 * Adiciona os eventos nos botões de paginação
 * @returns {undefined}
 */
function adicionarEventiPaginacao() {
    $("#paginacao a").click(function (e) {
        e.preventDefault();
        if ($(this).attr("href") !== "javascript:void(0)") {
            id_inicio = $(this).attr("href").split("/");
            id_inicio = id_inicio[id_inicio.length - 1];
            if (id_inicio === "") {
                id_inicio = 0;
            }
            id_tecnico = $("#tecnico").val();
            id_usuario = $("#usuario").val();
            id_status = $("#status").val();
            protocolo = $("#protocolo").val();
            buscarOrdensDeServico(id_inicio, id_tecnico, id_usuario, id_status, protocolo);
        }
    });
}
/*
 * Remove atividades na tabela de custos da OS
 */
function excluirAtividade(elemento) {
    if ($(elemento).attr("issave") === "true") {
        antigasAtividadesOS.push($(elemento).attr('cod'));
    } else {
        for (i = 0; i < novasAtividadesOS.length; i++) {
            if ($(elemento).attr('cod') + "-" + $(elemento).attr('valor') === novasAtividadesOS[i]) {
                novasAtividadesOS.splice(i, 1);//remove o elemento do array

                break;
            }
        }
    }
    valorTotal -= parseFloat($(elemento).attr('valor'));
    $("#rodape-tbl-custos").html("Total: " + valorTotal.toLocaleString('pt-br', {style: 'currency', currency: 'BRL'}));
    var tr = $(elemento).closest('tr');
    tr.fadeOut(400, function () {
        tr.remove();
    });
}

/**
 * carrega os dados do formulário de solução técnica da assistência
 * @param {type} idOrdemDeServicoAssistencia
 * @returns {undefined}
 */
function carregarDadosFormSolucaoTec() {
    $("#pelicula").show();
    $.ajax({
        method: "POST",
        url: base_url + "OSAssistencia/buscar_custos/" + idOsAssistenciaSelecionada, //A variável idOsAssistenciaSelecionada está no arquivo abrir_descricao_cliente.php
        data: {},
        dataType: "json"
    }).done(function (resposta) {
        if (resposta.status == 'sucesso') {
            //preenche a caixa de seleção de atividades
            atividades = resposta.atividades;
            html = "<option value=\"\">Selecione o Serviço</option>";
            for (var i = 0; i < atividades.length; i++) {
                html += "<option value=\"" + atividades[i].id_atividade + "\">" + atividades[i].nome_atividade + "</option>";
            }
            $("#sol-atividade").html(html);

            //preenche a caixa de seleção de equipamentos
            equipamentos = resposta.equipamentos;
            valorTotal = 0;
            html = "<option value=\"\">Selecione o Produto</option>";
            for (var i = 0; i < equipamentos.length; i++) {
                html += "<option value=\"" + equipamentos[i].id_equipamento + "\">" + equipamentos[i].nome_equipamento + "</option>";
            }
            $("#sol-equipamento").html(html);

            html = "";
            for (var i = 0; i < resposta.equipamentos_os.length; i++) {
                valorTotal += parseFloat(resposta.equipamentos_os[i].valor);
                vl = parseFloat(resposta.equipamentos_os[i].valor);
                html += "<tr>";
                html += "<td>" + resposta.equipamentos_os[i].nome_equipamento + "</td>";
                html += "<td>" + (vl.toLocaleString('pt-br', {style: 'currency', currency: 'BRL'})) + "</td>";
                html += "<td>";
                html += "<a onclick=\"excluirEquipamento(this)\" issave=\"true\" cod=\"" + resposta.equipamentos_os[i].id_equipamento_os + "\" valor=\"" + resposta.equipamentos_os[i].valor + "\" class=\"text-danger link-equipamento\" style=\"cursor: pointer\"><i class=\"material-icons\">cancel</i></a>";
                html += "</td>";
                html += "</tr>";
            }

            for (var i = 0; i < resposta.atividades_os.length; i++) {
                valorTotal += parseFloat(resposta.atividades_os[i].valor);
                vl = parseFloat(resposta.atividades_os[i].valor);
                html += "<tr>";
                html += "<td>" + resposta.atividades_os[i].nome_atividade + "</td>";
                html += "<td>" + (vl.toLocaleString('pt-br', {style: 'currency', currency: 'BRL'})) + "</td>";
                html += "<td>";
                html += "<a onclick=\"excluirAtividade(this)\" issave=\"true\" cod=\"" + resposta.atividades_os[i].id_atividade_os + "\" valor=\"" + resposta.atividades_os[i].valor + "\" class=\"text-danger\" style=\"cursor: pointer\"><i class=\"material-icons\">cancel</i></a>";
                html += "</td>";
                html += "</tr>";
            }

            $("#custos-os tbody").html(html);
            $("#rodape-tbl-custos").html("Total: " + valorTotal.toLocaleString('pt-br', {style: 'currency', currency: 'BRL'}));

            $("#pelicula").hide();
        } else {
            $("#pelicula").hide();
            $('.msg-modal').html(resposta.msg);
            $('#msg-erro').modal();
        }

    }).fail(function (jqXHR, textStatus) {
        $("#pelicula").hide();

        $('.msg-modal').html("<h3>Erro 500</h3>Falha interna no servidor");
        $('#msg-erro').modal();
    });
}



/**
 * Busca todas as autorizadas via ajax
 * @returns {undefined}
 */
function buscarTodasAutorizadas() {
    $("#pelicula").show();
    $.ajax({
        method: "POST",
        url: base_url + "Autorizada/buscar_todas/0/false",
        data: {id_ordem_de_servico: idOsAssistenciaSelecionada},
        dataType: "json"
    }).done(function (resposta) {
        autorizadas = resposta.autorizadas;
        console.log(resposta);
        html = "<option value=\"\">Selecione a Autorizada</option>";
        for (var i = 0; i < autorizadas.length; i++) {
            if (idAutorizadaOsSelecionada == autorizadas[i].id_autorizada) {
                html += "<option value=\"" + autorizadas[i].id_autorizada + "\" selected=\"\">" + autorizadas[i].nome_autorizada + " - Ramo: " + autorizadas[i].ramo + "</option>";
            } else {
                html += "<option value=\"" + autorizadas[i].id_autorizada + "\">" + autorizadas[i].nome_autorizada + " - Ramo: " + autorizadas[i].ramo + "</option>";
            }
        }
        $("#add-id-autorizada").html(html);
        $("#pelicula").hide();
    }).fail(function (jqXHR, textStatus) {
        $("#pelicula").hide();
        $('.msg-modal').html("Por algum motivo não foi possível carregar algumas informações.<br>Por favor, tente recarregar a página.");
        $('#msg-erro').modal();
    });
}

function abrirConfirmaExcluir(idAtividade, nome) {
    idOsAssisSelecionada = idAtividade;
    $('.msg-modal').html('Excluir a atividade <b>' + nome + "</b>?");
    $("#excluir-atividade").modal();
}


function deletarAtividade() {

    $.ajax({
        method: "POST",
        url: base_url + "OSAssistencia/excluir/" + idOsAssisSelecionada,
        data: {},
        dataType: "json"
    }).done(function (resposta) {
        $('#pelicula').removeClass('pelicula-ativa').addClass('pelicula-inativa');

        if (resposta.status == 'sucesso') {
            $('.msg-modal').html(resposta.msg);
            $('#msm-sucesso').modal();
        } else {
            $('.msg-modal').html(resposta.msg);
            $('#msg-erro').modal();
        }

    }).fail(function (jqXHR, textStatus) {
        $('#pelicula').removeClass('pelicula-ativa').addClass('pelicula-inativa');

        $('.msg-modal').html("<h3>Erro 500</h3>Falha interna no servidor");
        $('#msg-erro').modal();
    });

}

function abrirMsmCadastro(msg, status) {
    $('.msg-modal').html(msg);

    if (status === "sim") {
        $("#msm-sucesso").modal();
    } else {
        $("#msg-erro").modal();
    }
}

function voltarListaOSAssis() {
    window.location.href = base_url + "OSAssistencia";
}

function atualizarHistoricoOS() {
    $("#pelicula").show();
    dados = {
        id_ordem_de_servico: idOsAssistenciaSelecionada
    };

    $.ajax({
        method: "POST",
        url: base_url + "OSAssistencia/buscar_historico_os",
        data: dados,
        dataType: "json"
    }).done(function (resposta) {
        html = "";

        if (resposta !== null) {
            for (var i = 0; i < resposta.historico.length; i++) {
                data = resposta.historico[i].data;
                data = data.substring(0, 10);
                data = data.split("-");
                html += "<tr>";
                html += "<td>" + data[2] + "/" + data[1] + "/" + data[0] + "</td>";
                html += "<td>" + resposta.historico[i].responsavel + "</td>";
                html += "<td>" + resposta.historico[i].descricao + "</td>";
                html += "</tr>";
            }
        }
        $("#tabela-historico-os tbody").html(html);
        $("#pelicula").hide();
    }).fail(function (jqXHR, textStatus) {
        $("#pelicula").hide();
    });

}

//============================FIM REQUISIÇÕES AJAX========================================



function ativaFormBuscaOS() {
    if (btnOsAssAtivo === true) {
        document.getElementById("form-os-assistencia").style.display = "block";
        document.getElementById("btn-os-assistencia").style.display = "none";
        btnOsAssAtivo = false;
    } else {
        document.getElementById("form-os-assistencia").style.display = "none";
        document.getElementById("btn-os-assistencia").style.display = "block";
        btnOsAssAtivo = true;
    }
}


//-----------------------------CÓDIGO DO AUTOCOMPLETE------------------------------------
function addEventos() {
    $('#cliente-autocomplete tbody tr').mousedown(function () {
        $('#id_cliente').val($(this).attr('idcliente'));
        $('#cliente').val($(this).attr('nomecliente'));
    });

    $('#cliente').focus(function () {
        $("#cliente-autocomplete").css('display', 'block');
    });

    $('#cliente').focusout(function () {
        $("#cliente-autocomplete").css('display', 'none');
    });


    $("#cliente").keyup(function () {
        nomeBusca = $("#cliente").val();

        if (nomeBusca.length > 3) {
            buscarPessoas(nomeBusca);
        }
    });
}

function buscarPessoas(nome) {
    $("#load-cli").show();
    $.ajax({
        method: "POST",
        url: base_url + "OSAssistencia/buscar_cliente",
        data: {cliente: $("#cliente").val()},
        dataType: "json"
    }).done(function (resposta) {
        html = "";
        if (resposta !== null) {
            for (var i in resposta) {
                html = "<tr idcliente=\"" + resposta[i].id_pessoa + "\" nomecliente=\"" + resposta[i].nome_nm_fantasia + "\"><td>" + resposta[i].nome_nm_fantasia + "</td><td>" + resposta[i].cpf_cnpj + "</td></tr>";
            }
        }
        $("#opcoes-cliente").html(html);
        addEventos();
        $("#load-cli").hide();
    }).fail(function (jqXHR, textStatus) {
        $("#load-cli").hide();
    });
}


//-----------------------------FIM DO CÓDIGO DO AUTOCOMPLETE------------------------------------