var idAutorizadaSelecionada;

$(document).ready(function () {

    $("#cpf-cnpj").keydown(function (e) {
        if (e.keyCode != 9) {
            try {
                $("#cpf-cnpj").unmask();
            } catch (e) {
            }

            var tamanho = $("#cpf-cnpj").val().length;

            if (tamanho < 11) {
                $("#cpf-cnpj").mask("999.999.999-99");
                $('#tipo_pessoa').val('fisica');
            } else {
                $("#cpf-cnpj").mask("99.999.999/9999-99");
                $('#tipo_pessoa').val('juridica');
            }

            // ajustando foco
            var elem = this;
            setTimeout(function () {
                // mudo a posição do seletor
                elem.selectionStart = elem.selectionEnd = 10000;
            }, 0);
            // reaplico o valor para mudar o foco
            var currentValue = $(this).val();
            $(this).val('');
            $(this).val(currentValue);
        }
    });



    $('#form-cad-autorizada').submit(function (e) {
        e.preventDefault();
        $('#pelicula').show();
        $.ajax({
            url: $(this).attr('action'),
            method: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            dataType: "json"
        }).done(function (resposta) {

            $('#pelicula').hide();
            if (resposta.status == 'sucesso') {
                $('.msg-modal').html(resposta.msg);
                $('#msm-sucesso').modal();
            } else {
                $('.msg-modal').html(resposta.msg);
                $('#msg-erro').modal();
            }

        }).fail(function (jqXHR, textStatus) {
            $('#pelicula').hide();

            $('.msg-modal').html("<h3>Erro 500</h3>Falha interna no servidor");
            $('#msg-erro').modal();
        });
    });

    $('#form-editar-autorizada').submit(function (e) {
        e.preventDefault();
        $('#pelicula').show();
        $.ajax({
            url: $(this).attr('action'),
            method: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            dataType: "json"
        }).done(function (resposta) {

            $('#pelicula').hide();
            if (resposta.status == 'sucesso') {
                $('.msg-modal').html(resposta.msg);
                $('#msm-sucesso').modal();
            } else {
                $('.msg-modal').html(resposta.msg);
                $('#msg-erro').modal();
            }

        }).fail(function (jqXHR, textStatus) {
            $('#pelicula').hide();

            $('.msg-modal').html("<h3>Erro 500</h3>Falha interna no servidor");
            $('#msg-erro').modal();
        });
    });
});


function abrirConfirmaExcluir(idAtividade, nome) {
    idAutorizadaSelecionada = idAtividade;
    $('.msg-modal').html('Excluir a terceirizada <b>' + nome + "</b>?");
    $("#excluir-autorizada").modal();
}


function deletarAutorizada() {
    $('#pelicula').show();
    $.ajax({
        method: "POST",
        url: base_url + "Autorizada/excluir/" + idAutorizadaSelecionada,
        data: {},
        dataType: "json"
    }).done(function (resposta) {
        $('#pelicula').hide();

        if (resposta.status == 'sucesso') {
            $('.msg-modal').html(resposta.msg);
            $('#msm-sucesso').modal();
        } else {
            $('.msg-modal').html(resposta.msg);
            $('#msg-erro').modal();
        }

    }).fail(function (jqXHR, textStatus) {
        $('#pelicula').hide();

        $('.msg-modal').html("<h3>Erro 500</h3>Falha interna no servidor");
        $('#msg-erro').modal();
    });

}

function abrirMsmCadastro(msg, status) {
    $('.msg-modal').html(msg);

    if (status === "sim") {
        $("#msm-sucesso").modal();
    } else {
        $("#msg-erro").modal();
    }
}

function voltarListaAutorizada() {
    window.location.href = base_url + "Autorizada";
}


