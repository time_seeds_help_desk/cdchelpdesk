btnOsChamado = true;

$(document).ready(function () {
    addEventos();
    moveRelogio();
    dataAtual = new Date();
    segundos = dataAtual.getSeconds().toString().length === 1 ? "0" + dataAtual.getSeconds() : dataAtual.getSeconds();
    minutos = dataAtual.getMinutes().toString().length === 1 ? "0" + dataAtual.getMinutes() : dataAtual.getMinutes();
    $('#hora_inicio').val(dataAtual.getHours() + ":" + dataAtual.getMinutes() + ":" + dataAtual.getSeconds());
    $('#input_hora_inicio').val(dataAtual.getHours() + ":" + minutos + ":" + segundos);

    $('#cad-chamado-telefonico').submit(function (e) {
        e.preventDefault();
        $("#pelicula").show();
        $.ajax({
            url: $(this).attr('action'),
            method: $(this).attr('method'),
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            dataType: "json"
        }).done(function (resposta) {
            $("#pelicula").hide();
            if (resposta.status == 'sucesso') {
                $('.msg-modal').html(resposta.msg);
                $('#msg-sucesso').modal();
            } else {
                $('.msg-modal').html(resposta.msg);
                $('#msg-erro').modal();
            }

        }).fail(function (jqXHR, textStatus) {
            $('#pelicula').hide();

            $('.msg-modal').html("<h3>Erro 500</h3>Falha interna no servidor");
            $('#msg-erro').modal();
        });
    });


    $('#btn-vincular-os').click(function (e) {
        e.preventDefault();
        $("#pelicula").show();
        chamadoTelefonico = {
            id_pessoa_cliente: $('#id_cliente').val(),
            protocolo: $('#protocolo').val(),
            data: $('#data').val(),
            hora_inicio: $('#hora_inicio').val(),
            hora_termino: $('#hora_termino').val(),
            descricao_problema: $('#descricao_problema').val(),
            fl_resolvido: 0
        };
        console.log(chamadoTelefonico);
        $.ajax({
            url: base_url + "ChamadoTelefonico/cadastrar",
            method: 'POST',
            data: chamadoTelefonico,
            dataType: "json"
        }).done(function (resposta) {
            $("#pelicula").hide();
            console.log(resposta);
            if (resposta.status == 'sucesso') {
                window.location.href = base_url + "OSSuporte/pagina_cadastro/" + resposta.id_chamado_telefonico;
            } else {
                $('.msg-modal').html(resposta.msg);
                $('#msg-erro').modal();
            }

        }).fail(function (jqXHR, textStatus) {
            $('#pelicula').hide();

            $('.msg-modal').html("<h3>Erro 500</h3>Falha interna no servidor");
            $('#msg-erro').modal();
        });
    });


    //envio de dados do formlário de busca de Chamado
    $('#form-os-chamado-buscar form').submit(function (e) {
        e.preventDefault();

        $("#pelicula").show();
        id_inicio = 0;
        nome_nm_cliente = $("#buscar_nome_nm_fantasia").val();
        fl_resolvido = $("#buscar_fl_resolvido").val();

        buscarChamado(id_inicio, nome_nm_cliente, fl_resolvido);
    });
    //Fim de envio de dados do formlário de busca de Chamado
});

/**
 * Requição para o formulário de busca de Chamado Telefônico e ao clicar no link da paginação
 * @param {type} id_inicio
 * @param {type} nome_nm_fantasia
 * @param {type} fl_resolvido
 * @returns {undefined}
 */
function buscarChamado(id_inicio, nome_nm_fantasia, fl_resolvido) {
    if (nome_nm_fantasia != "" || fl_resolvido != "") {
        url = "ChamadoTelefonico/buscar/" + id_inicio + "/" + fl_resolvido + "/" + nome_nm_fantasia;
        //alert(base_url + url);
        $.ajax({
            method: "POST",
            url: base_url + url,
            data: {},
            dataType: "json"
        }).done(function (resposta) {
            console.log(resposta);
            $("#pelicula").hide();
            if (resposta.status == 'sucesso') {
                html = "";
                for (var i = 0; i < resposta.chamados_telefonicos.length; i++) {
                    status = resposta.chamados_telefonicos[i].fl_resolvido;
                    
                    if (status == true) {
                        status = "Resolvido";
                    } else {
                        link = base_url+"OSSuporte/abrir/"+resposta.chamados_telefonicos[i].id_ordem_de_servico;
                        status = "<a class=\"btn btn-success\" href=\""+link+"\">Vinculado a O.S.</a>";
                    }
                    html += "<tr>";
                    html += "<td>" + resposta.chamados_telefonicos[i].protocolo + "</td>";
                    html += "<td>" + resposta.chamados_telefonicos[i].nome_nm_fantasia + "</td>";
                    html += "<td>" + formatarData(resposta.chamados_telefonicos[i].data) + "</td>";
                    html += "<td>" + resposta.chamados_telefonicos[i].hora_inicio + "</td>";
                    html += "<td>" + resposta.chamados_telefonicos[i].hora_termino + "</td>";
                    html += "<td>" + status + "</td>";
                    html += "<td><a class=\"btn btn-primary\" href=\""+base_url+"ChamadoTelefonico/abrir/"+resposta.chamados_telefonicos[i].id_chamado_telefonico+"\">Abrir</a></td>";
                    html += "</tr>";
                }
                $('#tbl-lista-chamados tbody').html(html);

                //html = "<div id=\"links-busca\">";
                html = resposta.paginacao;
                //html += "</div>";

                $('#paginacao div').html(html);
                adicionarEventosPaginacao();

            } else {
                $('.msg-modal').html(resposta.msg);
                $('#msg-erro').modal();
            }

        }).fail(function (jqXHR, textStatus) {
            $("#pelicula").hide();
            $('.msg-modal').html("<h3>Erro 500</h3>Falha interna no servidor");
            $('#msg-erro').modal();
        });
    } else {
        window.location.href = base_url + "ChamadoTelefonico";
    }
}

/**
 * Adiciona os eventos nos botões de paginação
 * @returns {undefined}
 */
function adicionarEventosPaginacao() {
    $("#paginacao a").click(function (e) {
        e.preventDefault();
        if ($(this).attr("href") !== "javascript:void(0)") {
            id_inicio = $(this).attr("href").split("/");
            id_inicio = id_inicio[id_inicio.length - 1];
            if (id_inicio === "") {
                id_inicio = 0;
            }
            $("#pelicula").show();
            nome_nm_cliente = $("#buscar_nome_nm_fantasia").val();
            fl_resolvido = $("#buscar_fl_resolvido").val();

            buscarChamado(id_inicio, nome_nm_cliente, fl_resolvido);
        }
    });
}


/**
 * Atualiza o tempo no campos hora termino como se fosse um relógio
 * @return {undefined}
 */
function moveRelogio() {
    momentoAtual = new Date();
    hora = momentoAtual.getHours();
    minuto = momentoAtual.getMinutes();
    segundo = momentoAtual.getSeconds();

    str_segundo = new String(segundo);
    if (str_segundo.length == 1) {
        segundo = "0" + segundo;
    }
    str_minuto = new String(minuto);
    if (str_minuto.length == 1) {
        minuto = "0" + minuto;
    }
    str_hora = new String(hora);
    if (str_hora.length == 1) {
        hora = "0" + hora;
    }
    hora = hora + ":" + minuto + ":" + segundo;

    $('#hora_termino').val(hora);
    $('#input_hora_terminio').val(hora);


    setTimeout("moveRelogio()", 1000);
}

function voltarListaChamadosTelefonicos() {
    window.location.href = base_url + "ChamadoTelefonico";
}

function ativaFormBuscaOS() {
    if (btnOsChamado === true) {
        document.getElementById("form-os-chamado-buscar").style.display = "block";
        document.getElementById("btn-os-chamado").style.display = "none";
        btnOsChamado = false;
    } else {
        document.getElementById("form-os-chamado-buscar").style.display = "none";
        document.getElementById("btn-os-chamado").style.display = "block";
        btnOsChamado = true;
    }
}



function formatarData(dataNaoFormatada) {
    data = dataNaoFormatada;
    data = data.substring(0, 10);
    data = data.split("-");
    dataFormatada = data[2] + "/" + data[1] + "/" + data[0];
    return dataFormatada;
}


//-----------------------------CÓDIGO DO AUTOCOMPLETE------------------------------------
/**
 * Cria o evento do campo de auto complete
 * @returns {undefined}
 */
function addEventos() {
    $("#load-cli").hide();
    $('#cliente-autocomplete tbody tr').mousedown(function () {
        $('#id_cliente').val($(this).attr('idcliente'));
        $('#cliente').val($(this).attr('nomecliente'));
    });
    $('#cliente').focus(function () {
        $("#cliente-autocomplete").css('display', 'block');
    });
    $('#cliente').focusout(function () {
        $("#cliente-autocomplete").css('display', 'none');
    });
    $("#cliente").keyup(function () {
        nomeBusca = $("#cliente").val();
        if (nomeBusca.length > 3) {
            buscarPessoas(nomeBusca);
        }
    });
}

function buscarPessoas(nome) {
    $("#load-cli").show();
    $.ajax({
        method: "POST",
        url: base_url + "OSSuporte/buscar_cliente",
        data: {cliente: $("#cliente").val()},
        dataType: "json"
    }).done(function (resposta) {
        html = "";
        if (resposta !== null) {
            for (var i in resposta) {
                html = "<tr idcliente=\"" + resposta[i].id_pessoa + "\" nomecliente=\"" + resposta[i].nome_nm_fantasia + "\"><td>" + resposta[i].nome_nm_fantasia + "</td><td>" + resposta[i].cpf_cnpj + "</td></tr>";
            }
        }
        $("#opcoes-cliente").html(html);
        addEventos();
        $("#load-cli").hide();
    }).fail(function (jqXHR, textStatus) {
        $("#load-cli").hide();
    });
}
//-----------------------------FIM DO CÓDIGO DO AUTOCOMPLETE------------------------------------


