var idAtivSelecionada;


function abrirConfirmaExcluir(idAtividade, nome) {
    idAtivSelecionada = idAtividade;
    $('.msg-modal').html('Excluir o serviço <b>' + nome + "</b>?");
    $("#excluir-atividade").modal();
}


function deletarAtividade() {
    $('#pelicula').show();
    $.ajax({
        method: "POST",
        url: base_url + "Atividade/excluir/" + idAtivSelecionada,
        data: {},
        dataType: "json"
    }).done(function (resposta) {
        $('#pelicula').hide();

        if (resposta.status == 'sucesso') {
            $('.msg-modal').html(resposta.msg);
            $('#msm-sucesso').modal();
        } else {
            $('.msg-modal').html(resposta.msg);
            $('#msg-erro').modal();
        }

    }).fail(function (jqXHR, textStatus) {
        $('#pelicula').hide();

        $('.msg-modal').html("<h3>Erro 500</h3>Falha interna no servidor");
        $('#msg-erro').modal();
    });

}

function abrirMsmCadastro(msg, status) {
    $('.msg-modal').html(msg);

    if (status === "sim") {
        $("#msm-sucesso").modal();
    } else {
        $("#msg-erro").modal();
    }
}

function voltarListaAtiv() {
    window.location.href = base_url + "Atividade";
}






