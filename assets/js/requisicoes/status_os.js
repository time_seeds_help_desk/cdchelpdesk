var idStatusSelecionado;

function abrirConfirmaExcluir(idStatus, nome) {
    idStatusSelecionado = idStatus;
    $('.msg-modal').html('Excluir o Status de O.S. <b>' + nome + "</b>?");
    $("#excluir-status-os").modal();
}

function deletarStatusOs(){

    $.ajax({
        method: "POST",
        url: base_url + "StatusOS/excluir/" + idStatusSelecionado,
        data: {},
        dataType: "json"
    }).done(function (resposta) {
        $('#pelicula').removeClass('pelicula-ativa').addClass('pelicula-inativa');

        if (resposta.status == 'sucesso') {
            $('.msg-modal').html(resposta.msg);
            $('#msm-sucesso').modal();
        } else {
            $('.msg-modal').html(resposta.msg);
            $('#msg-erro').modal();
        }

    }).fail(function (jqXHR, textStatus) {
        $('#pelicula').removeClass('pelicula-ativa').addClass('pelicula-inativa');

        $('.msg-modal').html("<h3>Erro 500</h3>Falha interna no servidor");
        $('#msg-erro').modal();
    });

}

function abrirMsmCadastro(msg, status) {
    $('.msg-modal').html(msg);
    
    if(status == "sim"){
        $("#msm-sucesso").modal();
    }else{
         $("#msg-erro").modal();
    }
}

function voltarListaStatus(){
    window.location.href = base_url+"StatusOS";
}







