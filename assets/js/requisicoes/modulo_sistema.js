var idModuloSelecionado;
var idSistemaSelecionado;
var flCadastro = false;

$(document).ready(function () {
    var idSistema = 0;
    $(".carregar-modulos").click(function (event) {
        event.preventDefault();
        idSistema = $(this).attr('idsistema');
        nomeSistema = $(this).attr('nomesistema');
        $('#lista-modulos-sistema').show();
        idSistemaSelecionado = idSistema;
        buscar_modulos(idSistema);

        $("#titulo-modal-modulo").html("Módulos do Sistema " + nomeSistema);
        $("#modal-modulos").modal();
        $('#input_id_sistema').val(idSistema);
        $('#form-cad-modulo').hide();
        $('#form-editar-modulo').hide();

    });

    $('.btn-opcao-modulo').click(function (e) {
        e.preventDefault();

        if ($(this).attr('pagina') === "listar-modulo") {
            $('#lista-modulos-sistema').show();
            $('#form-cad-modulo').hide();
            $('#form-editar-modulo').hide();
            buscar_modulos(idSistema);
        } else if ($(this).attr('pagina') === "cadastrar-modulo") {
            $('#lista-modulos-sistema').hide();
            $('#form-cad-modulo').show();
            $('#form-editar-modulo').hide();
        }
    });


    /*
     * envio do formulário de cadastro de Módulo
     */
    $("#form-cad-modulo form").submit(function (e) {
        e.preventDefault();
        $('#pelicula').show();

        $.ajax({
            url: $(this).attr('action'),
            method: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            dataType: "json"
        }).done(function (resposta) {
            $('#pelicula').hide();
            if (resposta.status == 'sucesso') {
                $('#pelicula').hide();
                $('.msg-modal').html(resposta.msg);
                $('.img-sucesso').show();
                $('.img-erro').hide();
                $('.img-atencao').hide();
                $('.btn-msg').show();
                $('.btn-confirma').hide();
                $('#confirma-excluir-modulo').modal();
                flCadastro = true;
            } else {
                $('#pelicula').hide();
                $('.msg-modal').html(resposta.msg);
                $('.img-sucesso').hide();
                $('.img-erro').show();
                $('.img-atencao').hide();
                $('.btn-msg').show();
                $('.btn-confirma').hide();
                $('#confirma-excluir-modulo').modal();
            }

        }).fail(function (jqXHR, textStatus) {
            $('#pelicula').hide();
            $('.msg-modal').html("<h3>Erro 500</h3>Falha interna no servidor");
            $('.img-sucesso').hide();
            $('.img-erro').show();
            $('.img-atencao').hide();
            $('.btn-msg').show();
            $('.btn-confirma').hide();
            $('#confirma-excluir-modulo').modal();
        });
    });


    /*
     * envio do formulário editar Módulo
     */
    $("#form-editar-modulo form").submit(function (e) {
        e.preventDefault();
        $('#pelicula').show();

        $.ajax({
            url: $(this).attr('action'),
            method: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            dataType: "json"
        }).done(function (resposta) {
            $('#pelicula').hide();
            if (resposta.status == 'sucesso') {
                $('#pelicula').hide();
                $('.msg-modal').html(resposta.msg);
                $('.img-sucesso').show();
                $('.img-erro').hide();
                $('.img-atencao').hide();
                $('.btn-msg').show();
                $('.btn-confirma').hide();
                $('#confirma-excluir-modulo').modal();
                flCadastro = true;
            } else {
                $('#pelicula').hide();
                $('.msg-modal').html(resposta.msg);
                $('.img-sucesso').hide();
                $('.img-erro').show();
                $('.img-atencao').hide();
                $('.btn-msg').show();
                $('.btn-confirma').hide();
                $('#confirma-excluir-modulo').modal();
            }

        }).fail(function (jqXHR, textStatus) {
            $('#pelicula').hide();
            $('.msg-modal').html("<h3>Erro 500</h3>Falha interna no servidor");
            $('.img-sucesso').hide();
            $('.img-erro').show();
            $('.img-atencao').hide();
            $('.btn-msg').show();
            $('.btn-confirma').hide();
            $('#confirma-excluir-modulo').modal();
        });
    });
});

function buscar_modulos(idSistema) {
    $('#pelicula').show();
    $.ajax({
        method: "POST",
        url: base_url + "ModuloSistema/buscar_todos/" + idSistema,
        data: {},
        dataType: "json"
    }).done(function (resposta) {
        $('#pelicula').hide();
        html = "";
        var vl = 0;
        if (resposta.status == 'sucesso') {
            for (i = 0; i < resposta.modulos.length; i++) {
                vl = parseFloat(resposta.modulos[i].valor_modulo);
                html += "<tr>";
                html += "<td>" + resposta.modulos[i].nome_modulo_sistema + "</td>";
                html += "<td>";
                html += "<button class=\"btn btn-primary\" pagina=\"editar-modulo\" onclick=\"abrirFormEditar(" + resposta.modulos[i].id_modulo_sistema + ")\">Editar</button>&nbsp;&nbsp;";
                html += "<button class=\"btn btn-danger\" onclick=\"confirmaExcluirModulo(" + resposta.modulos[i].id_modulo_sistema + ",'" + resposta.modulos[i].nome_modulo_sistema + "\')\">Excluir</button>";
                html += "</td>";
                html += "</tr>";
            }
        }

        $('#tabela-modulo-sistema tbody').html(html);

    }).fail(function (jqXHR, textStatus) {
        $('#pelicula').hide();

        $('.msg-modal').html("<h3>Erro 500</h3>Falha interna no servidor");
        $('#msg-erro').modal();
    });


}

function abrirFormEditar(idModulo) {
    $('#lista-modulos-sistema').hide();
    $('#form-cad-modulo').hide();
    $('#form-editar-modulo').show();

    $('#pelicula').show();
    $.ajax({
        method: "POST",
        url: base_url + "ModuloSistema/buscar_por_id/" + idModulo,
        data: {},
        dataType: "json"
    }).done(function (resposta) {
        $('#pelicula').hide();
        if (resposta.status == 'sucesso') {
            $('#editar_nome_modulo_sistema').val(resposta.modulos[0].nome_modulo_sistema);
            $('#editar_valor_modulo').val(resposta.modulos[0].valor_modulo);
            $('#id_modulo').val(resposta.modulos[0].id_modulo_sistema);
        } else {
            $('.msg-modal').html("Erro ao carregar dados do módulo");
            $('#msg-erro').modal();
        }

    }).fail(function (jqXHR, textStatus) {
        $('#pelicula').hide();

        $('.msg-modal').html("<h3>Erro 500</h3>Falha interna no servidor");
        $('#msg-erro').modal();
    });
}


function excluirModulo() {
    $('#pelicula').show();
    $.ajax({
        method: "POST",
        url: base_url + "ModuloSistema/excluir/" + idModuloSelecionado,
        data: {},
        dataType: "json"
    }).done(function (resposta) {
        $('#pelicula').hide();
        //$('#confirma-excluir-modulo').modal('hide');
        if (resposta.status == 'sucesso') {
            $('.msg-modal').html(resposta.msg);
            $('.img-sucesso').show();
            $('.img-erro').hide();
            $('.img-atencao').hide();
            $('.btn-msg').show();
            $('.btn-confirma').hide();
        } else {
            $('.msg-modal').html(resposta.msg);
            $('.img-sucesso').hide();
            $('.img-erro').show();
            $('.img-atencao').hide();
            $('.btn-msg').show();
            $('.btn-confirma').hide();

        }

    }).fail(function (jqXHR, textStatus) {
        $('#pelicula').hide();
        //$('#confirma-excluir-modulo').modal('hide');
        $('.msg-modal').html("<h3>Erro 500</h3>Falha interna no servidor");
        $('#msg-erro').modal();
    });
}

function confirmaExcluirModulo(idModulo, nomeSistema) {
    idModuloSelecionado = idModulo;
    $('#confirma-excluir-modulo').modal('show');
    $('.msg-modal').html("Deseja realmente excluir o módulo " + nomeSistema);
    $('.img-sucesso').hide();
    $('.img-erro').hide();
    $('.img-atencao').show();
    $('.btn-confirma').show();
    $('.img-sucesso').hide();
    $('.btn-msg').hide();
}

function atualizaListaModolos() {
    buscar_modulos(idSistemaSelecionado);
    if (flCadastro === true) {
        $('#lista-modulos-sistema').show();
        $('#form-cad-modulo').hide();
        $('#form-editar-modulo').hide();
    }
}



