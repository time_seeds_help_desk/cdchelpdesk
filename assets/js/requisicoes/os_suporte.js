var btnOsAssAtivo = true;

$(document).ready(function () {
    addEventos();
    $("#id_sistema").change(function (event) {
        sistema = $("#id_sistema").val();
        versao = $("#id_sistema :selected").attr('versao');
        $('#versao').val(versao);
        html = "<option value=\"\">Carregando...</option>";
        $("#id_modulo").html(html);
        $.ajax({
            method: "POST",
            url: base_url + "ModuloSistema/buscar_por_id_sistema/",
            data: {id_sistema: sistema},
            dataType: "json"
        }).done(function (resposta) {
            modulo = resposta.modulos;
            html = "<option value=\"\">Selecione o Módulo</option>";
            console.log(resposta);
            for (var i = 0; i < modulo.length; i++) {
                html += "<option value=\"" + modulo[i].id_modulo_sistema + "\">" + modulo[i].nome_modulo_sistema + "</option>";
            }

            $("#id_modulo").html(html);
        }).fail(function (jqXHR, textStatus) {
            swal(
                    'Aconteceu algo de errado!',
                    '',
                    'error'
                    );
        });
        event.preventDefault();
    });


    $('#abrir-modal-agenda').click(function () {
        $("#pelicula").show();
        $.ajax({
            method: "POST",
            url: base_url + "Agendamento/buscar_por_tecnico",
            data: {},
            dataType: "json"
        }).done(function (resposta) {
            $("#pelicula").hide();
            if (resposta.status == 'sucesso') {
                html = "";
                agenda = resposta.agendamentos;
                for (i = 0; i < agenda.length; i++) {
                    html += "<tr>";
                    html += "<td>" + formatarData(agenda[i].data) + "</td>";
                    html += "<td>" + agenda[i].horario + "</td>";
                    html += "<td>" + agenda[i].nome_nm_fantasia + "</td>";
                    html += "<td>" + agenda[i].rua + ", " + agenda[i].numero + ", " + agenda[i].bairro + ", " + agenda[i].nome_cidade + " - " + agenda[i].sigla + "</td>";
                    html += "</tr>";
                }
                $('#tbl-agendamento-tecnico tbody').html(html);
                $('#modal-agenda').modal();
            } else {
                $('.msg-modal').html("Erro ao buscar agendamentos.");
                $('#msg-erro').modal();
            }

        }).fail(function (jqXHR, textStatus) {
            $('#pelicula').hide();
            $('.msg-modal').html("<h3>Erro 500</h3>Falha interna no servidor");
            $('#msg-erro').modal();
        });
    });



    //menu da página abrir OS
    $('.abas li').click(function (i) {
        $(this).val();
        $('.abas li').removeClass('selecionada');
        $(this).addClass('selecionada');
        opcaoSelecionada = $(this).attr('pagina');
        $('.conteudo-abas .opcao').removeClass('ativo');
        if (opcaoSelecionada === "informacoes") {
            $("#informacoes").addClass('ativo');
        } else if (opcaoSelecionada === "laudo") {
            $("#laudo").addClass('ativo');
            atualizarListaAgendamento();
            //buscarTodasAutorizadas();
        } else if (opcaoSelecionada === "anexo") {
            $("#anexo").addClass('ativo');
        } else if (opcaoSelecionada === "historico") {
            $("#historico").addClass('ativo');
            atualizarHistoricoOS();
        }
    });
    //envio de formulário de cadastro de OS
    $("#form-cadastro").on('submit', function (e) {
        e.preventDefault();
        continuar = false;

        if ($('#anexo').val() != "") {
            if ($('#anexo').get(0).files[0].size <= 8388608) {
                continuar = true;
            }
        } else {
            continuar = true;
        }

        if (continuar) {
            $("#pelicula").show();
            $.ajax({
                url: $(this).attr('action'),
                method: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                dataType: "json"
            }).done(function (resposta) {
                $("#pelicula").hide();
                if (resposta.status == 'sucesso') {
                    $('.msg-modal').html(resposta.msg);
                    $('#msm-sucesso').modal();
                } else {
                    $('.msg-modal').html(resposta.msg);
                    $('#msg-erro').modal();
                }

            }).fail(function (jqXHR, textStatus) {
                $('#pelicula').hide();
                $('.msg-modal').html("<h3>Erro 500</h3>Falha interna no servidor");
                $('#msg-erro').modal();
            });
        } else {
            $('.msg-modal').html("Não é permitido upload de arquivo maior que 8 MB.");
            $('#msg-erro').modal();
        }
    });
    //envio de formulário de cadastro de OS

    $('#adicionar-laudo').submit(function (e) {
        e.preventDefault();
        $("#pelicula").show();
        $.ajax({
            url: $(this).attr('action'),
            method: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            dataType: "json"
        }).done(function (resposta) {
            $("#pelicula").hide();
            if (resposta.status == 'sucesso') {
                $('.msg-modal').html(resposta.msg);
                $('#msm-sucesso').modal();
            } else {
                $('.msg-modal').html(resposta.msg);
                $('#msg-erro').modal();
            }

        }).fail(function (jqXHR, textStatus) {
            $('#pelicula').hide();
            $('.msg-modal').html("<h3>Erro 500</h3>Falha interna no servidor");
            $('#msg-erro').modal();
        });
    });


    $('#cadastrar_agendamento').submit(function (e) {
        e.preventDefault();
        $("#pelicula").show();
        $.ajax({
            url: $(this).attr('action'),
            method: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            dataType: "json"
        }).done(function (resposta) {
            $("#pelicula").hide();
            if (resposta.status == 'sucesso') {
                $('.msg-modal').html(resposta.msg);
                $('#msm-sucesso').modal();
                $('#modal-nova-visita').modal('hide');
                atualizarListaAgendamento();
            } else {
                $('.msg-modal').html(resposta.msg);
                $('#msg-erro').modal();
            }

        }).fail(function (jqXHR, textStatus) {
            $('#pelicula').hide();
            $('.msg-modal').html("<h3>Erro 500</h3>Falha interna no servidor");
            $('#msg-erro').modal();
        });
    });


    //envio de dados do formlário de busca de Ordem de Serviço
    $('#form-os-suporte-buscar form').submit(function (e) {
        e.preventDefault();

        $("#pelicula").show();
        id_inicio = 0;
        id_tecnico = $("#tecnico").val();
        id_usuario = $("#usuario").val();
        id_status = $("#status").val();
        protocolo = $("#protocolo").val();

        buscarOrdensDeServico(id_inicio, id_tecnico, id_usuario, id_status, protocolo);
    });
    //Fim de envio de dados do formlário de busca de Ordem de Serviço


    $('#form-editar-os').submit(function (e) {
        e.preventDefault();
        $("#pelicula").show();
        $.ajax({
            url: $(this).attr('action'),
            method: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            dataType: "json"
        }).done(function (resposta) {
            $("#pelicula").hide();
            if (resposta.status == 'sucesso') {
                $('.msg-modal').html(resposta.msg);
                $('#msm-sucesso').modal();
                $('#modal-nova-visita').modal('hide');
                atualizarListaAgendamento();
            } else {
                $('.msg-modal').html(resposta.msg);
                $('#msg-erro').modal();
            }

        }).fail(function (jqXHR, textStatus) {
            $('#pelicula').hide();
            $('.msg-modal').html("<h3>Erro 500</h3>Falha interna no servidor");
            $('#msg-erro').modal();
        });
    });



});
//-----------------------------CÓDIGO DO AUTOCOMPLETE------------------------------------
/**
 * Cria o evento do campo de auto complete
 * @returns {undefined}
 */
function addEventos() {
    $("#load-cli").hide();
    $('#cliente-autocomplete tbody tr').mousedown(function () {
        $('#id_cliente').val($(this).attr('idcliente'));
        $('#cliente').val($(this).attr('nomecliente'));
    });
    $('#cliente').focus(function () {
        $("#cliente-autocomplete").css('display', 'block');
    });
    $('#cliente').focusout(function () {
        $("#cliente-autocomplete").css('display', 'none');
    });
    $("#cliente").keyup(function () {
        nomeBusca = $("#cliente").val();
        if (nomeBusca.length > 3) {
            buscarPessoas(nomeBusca);
        }
    });
}

function atualizarListaAgendamento() {
    $.ajax({
        method: "POST",
        url: base_url + "Agendamento/buscar_por_id_os/" + idOrdemDeServico,
        data: {},
        dataType: "json"
    }).done(function (resposta) {
        //if (resposta.status == 'sucesso') {
        html = "";
        pend = '';
        reali = '';
        console.log(resposta);
        for (i = 0; i < resposta.agendamentos.length; i++) {
            pend = resposta.agendamentos[i].fl_concluida == "0" ? " checked=\"\" " : "";
            reali = resposta.agendamentos[i].fl_concluida == "1" ? " checked=\"\" " : "";
            html += "<tr>";
            html += "<td>" + formatarData(resposta.agendamentos[i].data) + "</td>";
            html += "<td>" + resposta.agendamentos[i].horario + "</td>";
            html += "<td>";
            html += "<form>";
            html += "<input type=\"radio\" value=\"0\"" + pend + " name=\"campo-radio1\" id=\"agp" + i + "\" onclick=\"mudarStatusAgendamento(" + resposta.agendamentos[i].id_agendamento + ",0)\"/>";
            html += "<label for=\"agp" + i + "\">Pendente</label>&nbsp;&nbsp";
            html += "<input type=\"radio\" value=\"1\"" + reali + " name=\"campo-radio1\" id=\"agr" + i + "\" onclick=\"mudarStatusAgendamento(" + resposta.agendamentos[i].id_agendamento + ",1)\"/>";
            html += "<label for=\"agr" + i + "\">Realizada</label>";
            html += "</form>";
            html += "</td>";
            html += "</tr>";

        }
        console.log(html);
        $('#tbl-agendamentos-os tbody').html(html);
        //} 
    }).fail(function (jqXHR, textStatus) {
        $('.msg-modal').html("<h3>Erro 500</h3>Falha interna no servidor");
        $('#msg-erro').modal();
    });
}

function buscarPessoas(nome) {
    $("#load-cli").show();
    $.ajax({
        method: "POST",
        url: base_url + "OSSuporte/buscar_cliente",
        data: {cliente: $("#cliente").val()},
        dataType: "json"
    }).done(function (resposta) {
        html = "";
        if (resposta !== null) {
            for (var i in resposta) {
                html = "<tr idcliente=\"" + resposta[i].id_pessoa + "\" nomecliente=\"" + resposta[i].nome_nm_fantasia + "\"><td>" + resposta[i].nome_nm_fantasia + "</td><td>" + resposta[i].cpf_cnpj + "</td></tr>";
            }
        }
        $("#opcoes-cliente").html(html);
        addEventos();
        $("#load-cli").hide();
    }).fail(function (jqXHR, textStatus) {
        $("#load-cli").hide();
    });
}
//-----------------------------FIM DO CÓDIGO DO AUTOCOMPLETE------------------------------------


function voltarListaOSSuporte() {
    window.location.href = base_url + "OSSuporte";
}

function mudarStatusAgendamento(id_agendamento, fl_concluida) {
    $("#pelicula").show();
    agendamento = {
        fl_concluida: fl_concluida
    };
    $.ajax({
        method: "POST",
        url: base_url + "Agendamento/editar/" + id_agendamento,
        data: agendamento,
        dataType: "json"
    }).done(function (resposta) {
        $("#pelicula").hide();
        if (resposta.status == "erro") {
            $('.msg-modal').html("Erro ao alterar status do agendamento");
            $('#msg-erro').modal();
        } else {
            $('.msg-modal').html("Alteração de status do agendamento realizada com sucesso.");
            $('#msm-sucesso').modal();
        }
        atualizarListaAgendamento();
    }).fail(function (jqXHR, textStatus) {
        $("#pelicula").hide();
    });
}


/**
 * Requição para o formulário de busca de ORdem de Servico e ao clicar no link da paginação
 * @param {type} id_inicio
 * @param {type} id_tecnico
 * @param {type} id_usuario
 * @param {type} id_status
 * @param {type} protocolo
 * @returns {undefined}
 */
function buscarOrdensDeServico(id_inicio, id_tecnico, id_usuario, id_status, protocolo) {
    if (id_tecnico != "0" || id_usuario != "0" || id_status != "0" || protocolo != "") {
        $.ajax({
            method: "POST",
            url: base_url + "OSSuporte/buscar/" + id_inicio + "/" + id_tecnico + "/" + id_usuario + "/" + id_status + "/" + protocolo,
            data: {},
            dataType: "json"
        }).done(function (resposta) {
            console.log(resposta);
            $("#pelicula").hide();
            if (resposta.status == 'sucesso') {
                html = "";
                for (var i = 0; i < resposta.ordens_de_servico.length; i++) {
                    dataConclusao = resposta.ordens_de_servico[i].data_conclusao == null ? "" : formatarData(resposta.ordens_de_servico[i].data_conclusao);
                    html += "<tr>";
                    html += "<td>" + resposta.ordens_de_servico[i].protocolo + "</td>";
                    html += "<td>" + formatarData(resposta.ordens_de_servico[i].data_abertura) + "</td>";
                    html += "<td>" + dataConclusao + "</td>";
                    html += "<td>" + resposta.ordens_de_servico[i].nome_status_os + "</td>";
                    html += "<td>" + resposta.ordens_de_servico[i].nome_tecnico + "</td>";
                    html += "<td>" + resposta.ordens_de_servico[i].nome_cliente + "</td>";
                    html += "<td>" + resposta.ordens_de_servico[i].nome_sistema + "</td>";
                    html += "<td style=\"width:120px\">";
                    html += "<a target=\"_blank\" title=\"PDF\" href=\"" + base_url + "OSSuporte/gerar_pdf/" + resposta.ordens_de_servico[i].id_ordem_de_servico + "\" class=\"btn btn-danger\">";
                    html += "<i class=\"material-icons\">picture_as_pdf</i> ";
                    html += "</a>&nbsp;";
                    html += "<a title=\"ABRIR\" href=\"" + base_url + "OSSuporte/abrir/" + resposta.ordens_de_servico[i].id_ordem_de_servico + "\" class=\"btn btn-primary\">";
                    html += "<i class=\"material-icons\">folder_open</i>";
                    html += "</a>";
                    html += "</td>";
                    html += "</tr>";
                }
                $('#tbl-lista-os tbody').html(html);

                //html = "<div id=\"links-busca\">";
                html = resposta.paginacao;
                //html += "</div>";

                $('#paginacao div').html(html);
                adicionarEventiPaginacao();

            } else {
                $('.msg-modal').html(resposta.msg);
                $('#msg-erro').modal();
            }

        }).fail(function (jqXHR, textStatus) {
            $("#pelicula").hide();
            $('.msg-modal').html("<h3>Erro 500</h3>Falha interna no servidor");
            $('#msg-erro').modal();
        });
    } else {
        window.location.href = base_url + "OSSuporte";
    }
}


/**
 * Adiciona os eventos nos botões de paginação
 * @returns {undefined}
 */
function adicionarEventiPaginacao() {
    $("#paginacao a").click(function (e) {
        e.preventDefault();
        if ($(this).attr("href") !== "javascript:void(0)") {
            id_inicio = $(this).attr("href").split("/");
            id_inicio = id_inicio[id_inicio.length - 1];
            if (id_inicio === "") {
                id_inicio = 0;
            }
            id_tecnico = $("#tecnico").val();
            id_usuario = $("#usuario").val();
            id_status = $("#status").val();
            protocolo = $("#protocolo").val();
            buscarOrdensDeServico(id_inicio, id_tecnico, id_usuario, id_status, protocolo);
        }
    });
}

function formatarData(dataNaoFormatada) {
    data = dataNaoFormatada;
    data = data.substring(0, 10);
    data = data.split("-");
    dataFormatada = data[2] + "/" + data[1] + "/" + data[0];
    return dataFormatada;
}

function ativaFormBuscaOS() {
    if (btnOsAssAtivo === true) {
        document.getElementById("form-os-suporte-buscar").style.display = "block";
        document.getElementById("btn-os-suporte").style.display = "none";
        btnOsAssAtivo = false;
    } else {
        document.getElementById("form-os-suporte-buscar").style.display = "none";
        document.getElementById("btn-os-suporte").style.display = "block";
        btnOsAssAtivo = true;
    }
}