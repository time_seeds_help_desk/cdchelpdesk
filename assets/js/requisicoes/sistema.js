$(document).ready(function () {
    $("#form-cad-sistema").submit(function (e) {
        e.preventDefault();
        $('#pelicula').show();
        $.ajax({
            url: $(this).attr('action'),
            method: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            dataType: "json"
        }).done(function (resposta) {
            $('#pelicula').hide();
            if (resposta.status == 'sucesso') {
                $('.msg-modal').html(resposta.msg);
                $('#msm-sucesso').modal();
            } else {
                $('.msg-modal').html(resposta.msg);
                $('#msg-erro').modal();
            }

        }).fail(function (jqXHR, textStatus) {
            $('#pelicula').hide();
            $('.msg-modal').html("<h3>Erro 500</h3>Falha interna no servidor");
            $('#msg-erro').modal();
        });
    });
});

var idSistemaSelecionado;

function abrirConfirmaExcluir(idSistema, nome) {
    idSistemaSelecionado = idSistema;
    $('.msg-modal').html('Excluir o sistema <b>' + nome + "</b>?");
    $("#excluir-sistema").modal();
}


function deletarSistema() {
    $('#pelicula').show();
    $.ajax({
        method: "POST",
        url: base_url + "Sistema/excluir/" + idSistemaSelecionado,
        data: {},
        dataType: "json"
    }).done(function (resposta) {
        $('#pelicula').hide();

        if (resposta.status == 'sucesso') {
            $('.msg-modal').html(resposta.msg);
            $('#msm-sucesso').modal();
        } else {
            $('.msg-modal').html(resposta.msg);
            $('#msg-erro').modal();
        }

    }).fail(function (jqXHR, textStatus) {
        $('#pelicula').hide();

        $('.msg-modal').html("<h3>Erro 500</h3>Falha interna no servidor");
        $('#msg-erro').modal();
    });

}


function voltar(tipoCliente) {
    window.location.href = base_url + tipoCliente;
}

