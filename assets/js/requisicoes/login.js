$(document).ready(function () {
    $("#form-login").submit(function (event) {
        dados = {
            email: $("#email").val(),
            senha: $("#senha").val(),
            codigo: $("#codigo").val()
        };

        $.ajax({
            method: "POST",
            url: base_url + "Login/entrar",
            data: dados,
            dataType: "json"
        }).done(function (resposta) {

            console.log(resposta);
            if (resposta.status === "ok") {
                window.location.href = base_url;
            } else {
                $('#texto-erro').removeClass('no-block').addClass('block');
                $('#texto-erro').html(resposta.msg);
            }

        }).fail(function (jqXHR, textStatus) {
            $('#texto-erro').removeClass('no-block').addClass('block');

            $('#texto-erro').html("Algo deu errado.<br>Entre em contato com o suporte!");
        });
        event.preventDefault();
    });


    $("#form-recupera-senha").submit(function (event) {
        $('#btn-confirmar-email-recuperacao').attr("disabled", true);

        dados = {
            email: $("#email").val(),
            codigo: $("#codigo").val()
        };


        $.ajax({
            method: "POST",
            url: base_url + "Login/recuperar_senha",
            data: dados,
            dataType: "json"
        }).done(function (resposta) {

            console.log(resposta);
            $('#texto-erro').removeClass('bg-red');
            $('#texto-erro').removeClass('bg-green');
            if (resposta.status === "sucesso") {
                $('#texto-erro').removeClass('no-block').addClass('block');
                $('#texto-erro').addClass('bg-green');
                $('#texto-erro').html(resposta.msg);
                setTimeout(function () {
                    window.location.href = base_url+"Login/pagina_recuperar_senha";
                }, 2000);
            } else {
                $('#btn-confirmar-email-recuperacao').attr("disabled", false);
                $('#texto-erro').addClass('bg-red');
                $('#texto-erro').removeClass('no-block').addClass('block');
                $('#texto-erro').html(resposta.msg);
            }

        }).fail(function (jqXHR, textStatus) {
            $('#btn-confirmar-email-recuperacao').attr("disabled", false);
            $('#texto-erro').removeClass('no-block').addClass('block');

            $('#texto-erro').html("Algo deu errado.<br>Entre em contato com o suporte!");
        });
        event.preventDefault();
    });

    $("#form-altera-senha").submit(function (event) {
        if ($("#senha").val() === $("#confirma-senha").val()) {
            dados = {
                senha: $("#senha").val(),
                codigo: $("#codigo").val(),
                chave: $("#chave").val()
            };

            $.ajax({
                method: "POST",
                url: base_url + "Login/alterar_senha",
                data: dados,
                dataType: "json"
            }).done(function (resposta) {

                console.log(resposta);
                $('#texto-erro').removeClass('bg-red');
                $('#texto-erro').removeClass('bg-green');
                if (resposta.status === "sucesso") {
                    $('#texto-erro').removeClass('no-block').addClass('block');
                    $('#texto-erro').addClass('bg-green');
                    $('#texto-erro').html(resposta.msg);
                } else {
                    $('#texto-erro').addClass('bg-red');
                    $('#texto-erro').removeClass('no-block').addClass('block');
                    $('#texto-erro').html(resposta.msg);
                }

            }).fail(function (jqXHR, textStatus) {
                $('#texto-erro').removeClass('no-block').addClass('block');

                $('#texto-erro').html("Algo deu errado.<br>Entre em contato com o suporte!");
            });
        } else {
            $('#texto-erro').removeClass('bg-red');
            $('#texto-erro').removeClass('bg-green');
            $('#texto-erro').addClass('bg-red');
            $('#texto-erro').removeClass('no-block').addClass('block');
            $('#texto-erro').html("As senhas informadas devem ser identicas.");
        }
        event.preventDefault();
    });
});




