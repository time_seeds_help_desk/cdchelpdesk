
$(document).ready(function () {
    $('#cad-cliente-fisico').hide();
    $('#cad-cliente-juridico').hide();
    $('.form-visible').show();

    /*
     * busca de estados
     */
    $.ajax({
        url: base_url + "Estado/buscar_todos",
        method: "POST",
        data: {},
        contentType: false,
        cache: false,
        processData: false,
        dataType: "json"
    }).done(function (resposta) {
        if (resposta.status == 'sucesso') {
            console.log(resposta.estados);
            html = "<option value=\"\">Selecione</option>";

            for (i = 0; i < resposta.estados.length; i++) {
                html += "<option value=\"" + resposta.estados[i].sigla + "\">" + resposta.estados[i].nome_estado + "</option>";
            }

            $('#cf_sigla_estado').html(html);
            $('#cj_sigla_estado').html(html);
        } else {
            $('.msg-modal').html(resposta.msg);
            $('#msg-erro').modal();
        }

    }).fail(function (jqXHR, textStatus) {
        $('.msg-modal').html("<h3>Erro 500</h3>Falha interna no servidor.<br>Não conseguimos carregar algumas informações.");
        $('#msg-erro').modal();
    });
    //fim de busca de estados


    $(".sigla_estado").change(function (event) {
        estado = $(this).val();
        $.ajax({
            method: "POST",
            url: base_url + "Cidade/buscar_por_sigla/",
            data: {sigla_estado: estado},
            dataType: "json"
        }).done(function (resposta) {
            cidades = resposta.cidades;
            html = "<option value=\"\">Selecione a Cidade</option>";


            for (var i = 0; i < cidades.length; i++) {
                html += "<option value=\"" + cidades[i].id_cidade + "\">" + cidades[i].nome_cidade + "</option>";
            }

            $(".id_cidade").html(html);
        }).fail(function (jqXHR, textStatus) {
            $('.msg-modal').html("<h3>Erro 500</h3>Falha interna no servidor.<br>Não conseguimos carregar algumas informações.");
            $('#msg-erro').modal();
        });
        event.preventDefault();
    });


    $('.btn-escolhido-cad-cli').click(function () {
        formularioAbrir = $(this).attr('formopen');

        $('#botoes-escolha-form').hide();
        $(".id_cidade").html("<option value=\"\">Selecione a Cidade</option>");
        if (formularioAbrir === "cliente-fisico") {
            $('#cad-cliente-fisico').show();
            $('#titulo-cad-cliente').html('Cadastro de Cliente Físico');
        } else {
            $('#cad-cliente-juridico').show();
            $('#titulo-cad-cliente').html('Cadastro de Cliente Jurídico');
        }
    });

    $("#abrir-modal-cad-cliente").click(function () {
        $('#botoes-escolha-form').show();
        $('#cad-cliente-fisico').hide();
        $('#cad-cliente-juridico').hide();
        $('#titulo-cad-cliente').html('Cadastro de Cliente');
        $('#cad-cliente-juridico')[0].reset();
        $('#cad-cliente-fisico')[0].reset();
    });

    /*
     * Cadastro de cliente físico
     */
    $('#cad-cliente-fisico').submit(function (e) {
        e.preventDefault();
        $('#pelicula').show();
        $.ajax({
            url: base_url + "ClienteFisico/cadastrar_ajax",
            method: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            dataType: "json"
        }).done(function (resposta) {
            $('#pelicula').hide();
            console.log(resposta);
            if (resposta.status == 'sucesso') {
                $('.msg-modal').html(resposta.msg);
                $('#msm-sucesso-cad-cli').modal();
                $('#cad-cliente-juridico')[0].reset();
                $('#cad-cliente-fisico')[0].reset();
                $(".id_cidade").html("<option value=\"\">Selecione a Cidade</option>");
            } else {
                $('.msg-modal').html(resposta.msg);
                $('#msg-erro').modal();
            }

        }).fail(function (jqXHR, textStatus) {
            $('#pelicula').hide();

            $('.msg-modal').html("<h3>Erro 500</h3>Falha interna no servidor");
            $('#msg-erro').modal();
        });
    });
    //fim de cadastro de cliente físico

    /*
     * Cadastro de cliente Jurídico
     */
    $('#cad-cliente-juridico').submit(function (e) {
        e.preventDefault();
        $('#pelicula').show();
        $.ajax({
            url: base_url + "ClienteJuridico/cadastrar_ajax",
            method: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            dataType: "json"
        }).done(function (resposta) {
            $('#pelicula').hide();
            console.log(resposta);
            if (resposta.status == 'sucesso') {
                $('.msg-modal').html(resposta.msg);
                $('#msm-sucesso-cad-cli').modal();
                $('#cad-cliente-juridico')[0].reset();
                $('#cad-cliente-fisico')[0].reset();
                $(".id_cidade").html("<option value=\"\">Selecione a Cidade</option>");
            } else {
                $('.msg-modal').html(resposta.msg);
                $('#msg-erro').modal();
            }

        }).fail(function (jqXHR, textStatus) {
            $('#pelicula').hide();

            $('.msg-modal').html("<h3>Erro 500</h3>Falha interna no servidor");
            $('#msg-erro').modal();
        });
    });
    //fim de cadastro de cliente Jurídico
});


function voltar(tipoCliente){
    window.location.href = base_url+tipoCliente;
}

