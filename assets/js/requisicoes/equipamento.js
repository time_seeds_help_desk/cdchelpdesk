var idEquipSelecionada;


function abrirConfirmaExcluir(idEquipamento, nome) {
    idEquipSelecionada = idEquipamento;
    $('.msg-modal').html('Excluir o produto <b>' + nome + "</b>?");
    $("#excluir-equipamento").modal();
}


function deletarEquipamento() {

    $.ajax({
        method: "POST",
        url: base_url + "Equipamento/excluir/" + idEquipSelecionada,
        data: {},
        dataType: "json"
    }).done(function (resposta) {
        $('#pelicula').removeClass('pelicula-ativa').addClass('pelicula-inativa');

        if (resposta.status == 'sucesso') {
            $('.msg-modal').html(resposta.msg);
            $('#msm-sucesso').modal();
        } else {
            $('.msg-modal').html(resposta.msg);
            $('#msg-erro').modal();
        }

    }).fail(function (jqXHR, textStatus) {
        $('#pelicula').removeClass('pelicula-ativa').addClass('pelicula-inativa');

        $('.msg-modal').html("<h3>Erro 500</h3>Falha interna no servidor");
        $('#msg-erro').modal();
    });

}

function abrirMsmCadastro(msg, status) {
    $('.msg-modal').html(msg);
    
    if(status === "sim"){
        $("#msm-sucesso").modal();
    }else{
         $("#msg-erro").modal();
    }
}

function voltarListaEquip(){
    window.location.href = base_url+"Equipamento";
}






